package com.sdmmllc.epicfeed;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.sdmmllc.epicfeed.utils.EpicFeedConsts;

public class CacheIntentReceiver extends BroadcastReceiver {

	String TAG = "CacheIntentReceiver";
	
	@Override
	public void onReceive(Context context, Intent intent) {
    	if (EpicFeedConsts.debugLevel > 7) 
			Log.i(TAG, "clearing cache...");
		if (intent.getAction().equals(Intent.ACTION_DEVICE_STORAGE_LOW)) {
	    	if (EpicFeedConsts.debugLevel > 7) 
    			Log.i(TAG, "clearing cache...");
			CacheManager.trimCache(context);
		}
	}
}
