package com.sdmmllc.epicfeed;

import java.lang.ref.WeakReference;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.sdmm.epicfeed.R;
import com.sdmmllc.epicfeed.data.EpicDB;
import com.sdmmllc.epicfeed.data.EpicDBConn;
import com.sdmmllc.epicfeed.ui.SpaTextAboutUs;
import com.sdmmllc.epicfeed.ui.SpaTextSettings;
import com.sdmmllc.epicfeed.ui.SpaTextShortcut;
import com.sdmmllc.epicfeed.utils.EpicFeedConsts;


public class SpaTextConfig extends Activity {
    /** Called when the activity is first created. */
	String TAG = "SpaTextConfig", PHONEID = "PhoneId";
	Button viewMsgs, setupMsgs, secretSetup;
	LinearLayout viewUpgrade, aboutUs, viewLog, shortcutInfo, passwordSet, wipeSet;
	String phoneID = "";
	EpicDB spaDB;
	Bundle savedState;
	Context spaContext;
    private SpaUserAccount mSpaUserAccount;
    private SpaUserAccountListener accountListener;
    
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.configuration);
        savedState = savedInstanceState;
        spaContext = this;
        mSpaUserAccount = EpicFeedController.getSpaUserAccount(this);
        accountListener = new SpaUserAccountAdapter(new WeakReference<Activity>(this)) {
        	
    		@Override
    		public void timeout() {
    			finish();
    		}
        	
        };
        aboutUs = (LinearLayout)findViewById(R.id.configAboutUsInfo);
        aboutUs.setBackgroundDrawable(
        		(new ListView(this)).getSelector());
        viewLog = (LinearLayout)findViewById(R.id.configViewLogInfo);
        viewLog.setBackgroundDrawable(
        		(new ListView(this)).getSelector());
        shortcutInfo = (LinearLayout)findViewById(R.id.configIconsInfo);
        shortcutInfo.setBackgroundDrawable(
        		(new ListView(this)).getSelector());
        passwordSet = (LinearLayout)findViewById(R.id.configPasswordInfo);
        passwordSet.setBackgroundDrawable(
        		(new ListView(this)).getSelector());
        wipeSet = (LinearLayout)findViewById(R.id.configWipeInfo);
        wipeSet.setBackgroundDrawable(
        		(new ListView(this)).getSelector());
        
        //allow access accordingly
        if (spaDB == null) spaDB = EpicFeedController.getEpicDB(this.getApplicationContext());
        if (EpicFeedConsts.debugLevel > 3) Log.i(TAG, "onCreate: opening and closing database");
        EpicDBConn tmpConn = spaDB.open(false);
		String[] ids = spaDB.allSpaID();
        spaDB.close(tmpConn);
        SharedPreferences settings = getSharedPreferences(EpicFeedConsts.PREFS_NAME, 0);
        cancelNotification(this);
        viewUpgrade = (LinearLayout)findViewById(R.id.configUpgradeInfo);
        viewUpgrade.setOnClickListener(new OnClickListener () {
			@Override
			public void onClick(View v) {
		        mSpaUserAccount.updateAuthActivity();
			}
        });
        if (getResources().getBoolean(R.bool.freeVersion)) {
        	viewUpgrade.setVisibility(View.VISIBLE);
        	viewUpgrade.setEnabled(true);
        } else {
        	viewUpgrade.setEnabled(false);
        	viewUpgrade.setVisibility(View.GONE);
        }
        setResult(Activity.RESULT_OK);
    }
	
	@Override
	public void onResume() {
		super.onResume();
        if (!mSpaUserAccount.authenticated(new WeakReference<Activity>(this))) finish();
        mSpaUserAccount.setSpaUserAccountListener(accountListener);
	}
	
    @Override
    public boolean onTouchEvent(MotionEvent e) {
    	//Log.i(TAG, "Touch event outside activity, do nothing...");
        mSpaUserAccount.updateAuthActivity();
    	return super.onTouchEvent(e);
    }
    
    @Override
    public void onUserInteraction() {
    	//Log.i(TAG, "Touch event...");
        mSpaUserAccount.updateAuthActivity();
    }
    
    public void spaSecretMsgSetup(View V) {
        mSpaUserAccount.updateAuthActivity();
    }

	public void smsOptions (View v) {
        mSpaUserAccount.updateAuthActivity();
	   	Intent settingsIntent = new Intent(this, SpaTextOptions.class);
		startActivityForResult(settingsIntent, EpicFeedConsts.OPTIONS);
	}
	
	public void smsSettings (View v) {
        mSpaUserAccount.updateAuthActivity();
	   	Intent settingsIntent = new Intent(this, SpaTextSettings.class);
		startActivityForResult(settingsIntent, EpicFeedConsts.SETTINGS);
	}
	
	public void wipeSet (View v) {
        mSpaUserAccount.updateAuthActivity();
	}
	
	public void spaShortcutSet (View v) {
        mSpaUserAccount.updateAuthActivity();
	   	Intent passwordIntent = new Intent(this, SpaTextShortcut.class);
		startActivityForResult(passwordIntent, EpicFeedConsts.SHORTCUT);
	}
	
	public void spaAboutUs (View v) {
        mSpaUserAccount.updateAuthActivity();
	   	Intent aboutUsIntent = new Intent(this, SpaTextAboutUs.class);
		startActivityForResult(aboutUsIntent, EpicFeedConsts.ABOUTUS);
	}
	
	public void spaSMSSetup (View v) {
        mSpaUserAccount.updateAuthActivity();
	   	Intent spaSMSSetupIntent = new Intent(this, SpaTextSMSSetup.class);
		startActivityForResult(spaSMSSetupIntent, EpicFeedConsts.ABOUTUS);
	}
	
 	public void cancelNotification(Context context) {
 		String ns = Context.NOTIFICATION_SERVICE;
 		NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(ns);
 		mNotificationManager.cancel(R.string.smsNotificationID);
 	}
 	
 	@Override
 	public void onPause() {
        mSpaUserAccount.removeSpaUserAccountListener(accountListener);
        super.onPause();
 	}
	@Override
    public void onConfigurationChanged(Configuration newConfig) {
    	onPause();
		onStop();
    	onCreate(this.savedState);
    	onStart();
    	onResume();
    }
}