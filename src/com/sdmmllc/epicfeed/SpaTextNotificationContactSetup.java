package com.sdmmllc.epicfeed;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TableLayout;
import android.widget.TextView;

import com.sdmm.epicfeed.R;
import com.sdmmllc.epicfeed.data.EpicContact;
import com.sdmmllc.epicfeed.data.EpicDB;
import com.sdmmllc.epicfeed.data.EpicDBConn;
import com.sdmmllc.epicfeed.utils.EpicFeedConsts;


public class SpaTextNotificationContactSetup extends Activity {
	private EditText tickerText, titleText, msgText;
	private CheckBox defaultIconChk, customMsgChk;
	private TableLayout icon1Border, icon2Border, icon3Border, icon4Border, icon5Border;
	private Button testNotification;
	private ImageButton icon1Btn, icon2Btn, icon3Btn, icon4Btn, icon5Btn;
	private Context spaContext;
	private Bundle savedState;
	private EpicContact tmpContact, backContact;
	private String TAG = "SpaTextNotificationContactSetup";
	private EpicDB spaDB;
    private SpaUserAccount mSpaUserAccount;
    private LayoutInflater mInflater;
    boolean saveChanges = true;
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
        mSpaUserAccount.updateAuthActivity();
		MenuInflater menuInfl = getMenuInflater();
		menuInfl.inflate(R.menu.smsnotification_menu, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
        mSpaUserAccount.updateAuthActivity();
		int itemId = item.getItemId();
		if (itemId == R.id.setupNotificationDefault) {
			defaultNotification();
			return true;
		} else if (itemId == R.id.setupNotificationHelp) {
			helpDialog();
			return true;
		} else if (itemId == R.id.setupNotificationBack) {
			saveChanges = false;
			changeSelectedIcon(backContact.notifyOptions.statusbarIcon, true);
			cancelUpdateNotificationData();
			Intent notifySettings = new Intent();
			notifySettings.putExtra(SpaTextNotification.notificationUpdated, false);
			setResult(RESULT_CANCELED, notifySettings);
			finish();
			return true;
		}
		return false;
	}

	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contactnotification);
        savedState = savedInstanceState;
        spaContext = this;
        mSpaUserAccount = EpicFeedController.getSpaUserAccount(this);
        mInflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        String pID = getIntent().getStringExtra("pID");
        if (EpicFeedConsts.debugLevel > 3) 
        	Log.i(TAG, "onCreate: pID from Intent:" + pID);
        spaDB = EpicFeedController.getEpicDB(this.getApplicationContext());
    	EpicDBConn tmpConn = spaDB.open(false);
		if (spaDB.containsPhoneID(pID)) tmpContact = spaDB.getContact(pID);
		else {
			pID = SpaTextNotification.tmpId;
			tmpContact = new EpicContact(pID);
		}
		if (spaDB.containsNotification(pID))tmpContact.notifyOptions = spaDB.getNotification(pID);
		else tmpContact.notifyOptions = new SpaTextNotification(pID);
		spaDB.close(tmpConn);
		setupCancelData();
		if (EpicFeedConsts.debugLevel > 7) Log.i(TAG, "onCreate: tmpContact.notifyOptions.statusbarIcon: " 
				+ tmpContact.notifyOptions.statusbarIcon);
        tickerText = (EditText)findViewById(R.id.contactStatusBarNotificationSetupCustomTickerEdit);
        titleText = (EditText)findViewById(R.id.contactStatusBarNotificationSetupCustomTitleEdit);
        msgText = (EditText)findViewById(R.id.contactStatusBarNotificationSetupCustomMsgEdit);
        defaultIconChk = (CheckBox)findViewById(R.id.contactStatusBarNotificationSetupDefaultIconChk);
        customMsgChk = (CheckBox)findViewById(R.id.contactStatusBarNotificationSetupCustomMsgChk);
        icon1Border = (TableLayout)findViewById(R.id.icon1Border);
        icon2Border = (TableLayout)findViewById(R.id.icon2Border);
        icon3Border = (TableLayout)findViewById(R.id.icon3Border);
        icon4Border = (TableLayout)findViewById(R.id.icon4Border);
        icon5Border = (TableLayout)findViewById(R.id.icon5Border);
        icon1Btn = (ImageButton)findViewById(R.id.iconBtn1);
        icon2Btn = (ImageButton)findViewById(R.id.iconBtn2);
        icon3Btn = (ImageButton)findViewById(R.id.iconBtn3);
        icon4Btn = (ImageButton)findViewById(R.id.iconBtn4);
        icon5Btn = (ImageButton)findViewById(R.id.iconBtn5);
        // if default icon is used, then disable icons
        defaultIconChk.setChecked(tmpContact.notifyOptions.statusbarDefaultIcon);
        if (EpicFeedConsts.debugLevel > 7) Log.i(TAG, "onCreate: tmpContact.notifyOptions.statusbarIcon:" + 
				tmpContact.notifyOptions.statusbarIcon);
        changeSelectedIcon(tmpContact.notifyOptions.statusbarIcon, false);
        if (tmpContact.notifyOptions.statusbarDefaultNotification) {
        	enableNotificationText(false);
        	customMsgChk.setChecked(false);
        } else {
        	customMsgChk.setChecked(true);
        	enableNotificationText(true);
	        if (tmpContact.notifyOptions.statusbarDefaultTicker) 
	        	tickerText.setText(getString(R.string.statusNotificationTicker));
	        else tickerText.setText(tmpContact.notifyOptions.statusbarTicker);
	        if (tmpContact.notifyOptions.statusbarDefaultTitle)
	        	titleText.setText(getString(R.string.statusNotificationTitle));
	        else titleText.setText(tmpContact.notifyOptions.statusbarTitle);
	        if (tmpContact.notifyOptions.statusbarDefaultText)
	        	msgText.setText(getString(R.string.statusNotificationText));
	        else msgText.setText(tmpContact.notifyOptions.statusbarText);
        }
	}
	
	public void helpDialog () {
        mSpaUserAccount.updateAuthActivity();
		closeOptionsMenu();
		final View helpTextView = mInflater.inflate(R.layout.helpview, null);
		final TextView helpText = (TextView)helpTextView.findViewById(R.id.helptext);
		helpText.setText(Html.fromHtml(
				getText(R.string.contactNotificationPageHelp).toString()));
		AlertDialog ad = new AlertDialog.Builder(spaContext)
        .setTitle(R.string.contactNotificationPageHelpHeader)
        .setView(helpTextView)
        .setNeutralButton(getString(R.string.sysDone), new DialogInterface.OnClickListener() {
            @Override
			public void onClick(DialogInterface dialog, int which) {
                mSpaUserAccount.updateAuthActivity();
            	dialog.cancel();
            }
        })
        .create();
		ad.show();
	}
	
	public void setupCancelData() {
		backContact = new EpicContact(tmpContact.phoneID);
		backContact.notifyOptions.statusbarDefaultIcon = tmpContact.notifyOptions.statusbarDefaultIcon;
		backContact.notifyOptions.statusbarIcon = tmpContact.notifyOptions.statusbarIcon;
		backContact.notifyOptions.statusbarDefaultNotification = tmpContact.notifyOptions.statusbarDefaultNotification;
		backContact.notifyOptions.statusbarDefaultTicker = tmpContact.notifyOptions.statusbarDefaultTicker;
		backContact.notifyOptions.statusbarTicker = tmpContact.notifyOptions.statusbarTicker;
		backContact.notifyOptions.statusbarDefaultTitle = tmpContact.notifyOptions.statusbarDefaultTitle;
		backContact.notifyOptions.statusbarTitle = tmpContact.notifyOptions.statusbarTitle;
		backContact.notifyOptions.statusbarDefaultText = tmpContact.notifyOptions.statusbarDefaultText;
		backContact.notifyOptions.statusbarText = tmpContact.notifyOptions.statusbarText;
	}
	
	public void defaultNotification() {
    	Intent defaultStatusbarNotificationIntent = new Intent(spaContext, SpaTextNotificationDefaultSetup.class);
    	startActivityForResult(defaultStatusbarNotificationIntent, EpicFeedConsts.DEFAULT_NOTIFICATION);
	}

	public void enableNotificationText(boolean enable) {
		customMsgChk.setChecked(enable);
		tickerText.setEnabled(enable);
		titleText.setEnabled(enable);
		msgText.setEnabled(enable);
	}
	
	public void useDefaultIcon(View v) {
		if (defaultIconChk.isChecked()) changeSelectedIcon(0, false);
		else changeSelectedIcon(tmpContact.notifyOptions.statusbarIcon, false);
	}
	
	public void changeSelectedIcon(int icon_selected, boolean saveChange) {
        mSpaUserAccount.updateAuthActivity();
		if (EpicFeedConsts.debugLevel > 7) Log.i(TAG, "changeSelectedIcon");
		if (icon_selected > 100) icon_selected = 0;
		if (saveChange) tmpContact.notifyOptions.statusbarIcon = icon_selected;
		icon1Border.setPadding(0, 0, 0, 0);
		icon2Border.setPadding(0, 0, 0, 0);
		icon3Border.setPadding(0, 0, 0, 0);
		icon4Border.setPadding(0, 0, 0, 0);
		icon5Border.setPadding(0, 0, 0, 0);
		switch (icon_selected) {
		case 0:
			icon1Border.setPadding(2, 2, 2, 2);
			break;
		case 1:
			if (EpicFeedConsts.debugLevel > 7) Log.i(TAG, "changeSelectedIcon: icon 2 selected");
			icon2Border.setPadding(2, 2, 2, 2);
			break;
		case 2:
			icon3Border.setPadding(2, 2, 2, 2);
			break;
		case 3:
			icon4Border.setPadding(2, 2, 2, 2);
			break;
		case 4:
			icon5Border.setPadding(2, 2, 2, 2);
			break;
		default:
			icon1Border.setPadding(2, 2, 2, 2);
			if (saveChange) tmpContact.notifyOptions.statusbarIcon = 0;
		}
	}
	
	public void icon1Select(View v) {
		defaultIconChk.setChecked(true);
		changeSelectedIcon(0, true);
	}

	public void icon2Select(View v) {
		if (EpicFeedConsts.debugLevel > 7) Log.i(TAG, "icon2Select: selected");
		defaultIconChk.setChecked(false);
		changeSelectedIcon(1, true);
	}

	public void icon3Select(View v) {
		defaultIconChk.setChecked(false);
		changeSelectedIcon(2, true);
	}

	public void icon4Select(View v) {
		defaultIconChk.setChecked(false);
		changeSelectedIcon(3, true);
	}

	public void icon5Select(View v) {
		defaultIconChk.setChecked(false);
		changeSelectedIcon(4, true);
	}
	
	public void setCustomMsg(View v) {
        mSpaUserAccount.updateAuthActivity();
        if (customMsgChk.isChecked()) {
        	tmpContact.notifyOptions.statusbarDefaultNotification = false;
        	enableNotificationText(true);
	        if (tmpContact.notifyOptions.statusbarDefaultTicker) 
	        	tickerText.setText(getString(R.string.statusNotificationTicker));
	        else tickerText.setText(tmpContact.notifyOptions.statusbarTicker);
	        if (tmpContact.notifyOptions.statusbarDefaultTitle)
	        	titleText.setText(getString(R.string.statusNotificationTitle));
	        else titleText.setText(tmpContact.notifyOptions.statusbarTitle);
	        if (tmpContact.notifyOptions.statusbarDefaultText)
	        	msgText.setText(getString(R.string.statusNotificationText));
	        else msgText.setText(tmpContact.notifyOptions.statusbarText);
        } else {
        	enableNotificationText(false);
        	tmpContact.notifyOptions.statusbarDefaultNotification = true;
        	tickerText.setText(getString(R.string.statusNotificationTicker));
        	titleText.setText(getString(R.string.statusNotificationTitle));
        	msgText.setText(getString(R.string.statusNotificationText));
        }
	}
	
	public void testNotification(View v) {
        mSpaUserAccount.updateAuthActivity();
		// clear existing notifications.
		((NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE))
			.cancel(R.drawable.notification_icon);
		((NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE))
			.cancel(R.drawable.notification_icon2);
		((NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE))
			.cancel(R.drawable.notification_icon3);
		((NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE))
			.cancel(R.drawable.notification_icon4);
		((NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE))
			.cancel(R.drawable.notification_icon5);
		updateNotificationData();
		EpicFeedController.smsHandler.sendNotification(tmpContact);
	}
	
	public void updateNotificationData() {
        mSpaUserAccount.updateAuthActivity();
        if (customMsgChk.isChecked()) {
        	tmpContact.notifyOptions.statusbarDefaultNotification = false;
        	if (tickerText.getText().equals(getString(R.string.statusNotificationTicker))) {
        		tmpContact.notifyOptions.statusbarDefaultTicker = true;
        	} else {
        		tmpContact.notifyOptions.statusbarDefaultTicker = false;
        		tmpContact.notifyOptions.statusbarTicker = tickerText.getText();
        	}
        	if (msgText.getText().equals(getString(R.string.statusNotificationText))) {
        		tmpContact.notifyOptions.statusbarDefaultText = true;
        	} else {
        		tmpContact.notifyOptions.statusbarDefaultText = false;
        		tmpContact.notifyOptions.statusbarText = msgText.getText();
        	}
        	if (titleText.getText().equals(getString(R.string.statusNotificationTitle))) {
        		tmpContact.notifyOptions.statusbarDefaultTitle = true;
        	} else {
        		tmpContact.notifyOptions.statusbarDefaultTitle = false;
        		tmpContact.notifyOptions.statusbarTitle = titleText.getText();
        	}
        } else {
        	tmpContact.notifyOptions.statusbarDefaultNotification = true;
        }
        tmpContact.notifyOptions.statusbarDefaultIcon = defaultIconChk.isChecked();
    	if (EpicFeedConsts.debugLevel > 7) 
			Log.i(TAG, "updateNotificationData: saving tmpContact.notifyOptions.statusbarIcon: " 
			+ tmpContact.notifyOptions.statusbarIcon);
    	if (EpicFeedConsts.debugLevel > 7) 
			Log.i(TAG, "updateNotificationData: saving tmpContact.notifyOptions.statusbarTicker: " 
			+ tmpContact.notifyOptions.statusbarTicker);
    	EpicDBConn tmpConn = spaDB.open(true);
 		if (spaDB.containsNotification(tmpContact.phoneID)) spaDB.updateNotificationEntry(tmpContact.notifyOptions);
 		else spaDB.insertNotificationEntry(tmpContact.notifyOptions);
    	spaDB.close(tmpConn);
    	if (EpicFeedConsts.debugLevel > 7) 
    		Log.i(TAG, "updateNotificationData: data saved");
    	if (EpicFeedConsts.debugLevel > 7) 
    		Log.i(TAG, "updateNotificationData: tmpContact.notifyOptions.statusbarIcon: " 
				+ tmpContact.notifyOptions.statusbarIcon);
	}

	public void cancelUpdateNotificationData() {
    	EpicDBConn tmpConn = spaDB.open(true);
 		if (spaDB.containsNotification(backContact.phoneID)) spaDB.updateNotificationEntry(backContact.notifyOptions);
    	spaDB.close(tmpConn);
    	if (EpicFeedConsts.debugLevel > 7) Log.i(TAG, "cancelUpdateNotificationData: data saved");
    	if (EpicFeedConsts.debugLevel > 7) Log.i(TAG, "cancelUpdateNotificationData: tmpContact.notifyOptions.statusbarIcon: " 
				+ backContact.notifyOptions.statusbarIcon);
	}

	@Override
	public void onPause() {
		if (saveChanges) {
			updateNotificationData();
		}
		super.onPause();
	}
	
    @Override
    public boolean onTouchEvent(MotionEvent e) {
    	//Log.i(TAG, "Touch event outside activity, do nothing...");
        mSpaUserAccount.updateAuthActivity();
    	return super.onTouchEvent(e);
    }
    
    @Override
    public void onUserInteraction() {
    	//Log.i(TAG, "Touch event...");
        mSpaUserAccount.updateAuthActivity();
    }
    
	@Override
	public void onBackPressed() {
        mSpaUserAccount.updateAuthActivity();
		updateNotificationData();
		Intent notifySettings = new Intent();
		notifySettings.putExtra("pID", tmpContact.phoneID);
		notifySettings.putExtra(SpaTextNotification.notificationUpdated, true);
		setResult(RESULT_CANCELED, notifySettings);
    	if (EpicFeedConsts.debugLevel > 7) 
			Log.i(TAG, "onBackPressed: sent result of tmpContact.phoneID:" + tmpContact.phoneID);
		saveChanges = false;
		finish();
	}
}