package com.sdmmllc.epicfeed;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.Contacts;
import android.provider.Contacts.People;
import android.util.Log;

import com.sdmmllc.epicfeed.data.EpicContact;
import com.sdmmllc.epicfeed.utils.EpicFeedConsts;

@SuppressWarnings("deprecation")
public class SpaContactOldAPI {
	String TAG = "SpaContactOldAPI";
	ContentResolver cr;
	Context contactCtx;
	
	String rowsToReturn = People.Phones.NUMBER + " NOT NULL";
	String rowOrder = People.NAME + " ASC";
	
	// Form an array specifying which columns to return. 
	String[] projectionPhones = new String[] {
			Contacts.Phones._ID,
			Contacts.Phones.NAME,
			Contacts.Phones.NUMBER,
			Contacts.Phones.NUMBER_KEY,
			Contacts.Phones.TYPE,
			Contacts.Phones.LABEL
		};
		
	// Get the base URI for the People table in the Contacts content provider.
	Uri contacts =  Contacts.Phones.CONTENT_URI;
	
	public SpaContactOldAPI (Context ctx) {
		contactCtx = ctx;
		cr = ctx.getContentResolver();
	}


	public InputStream openPhoto(long contactId) {
		Uri contactUri = ContentUris.withAppendedId(Contacts.CONTENT_URI, contactId);
		return People.openContactPhotoInputStream(cr, contactUri);
	}

   	
   	public InputStream openPhotoOld(long contactId) {
    	Uri contactUri = ContentUris.withAppendedId(Contacts.CONTENT_URI, contactId);
    	Uri photoUri = Uri.withAppendedPath(contactUri, Contacts.Photos.CONTENT_DIRECTORY);
    	Cursor cursor = cr.query(photoUri,
    			new String[] {Contacts.Photos.DATA}, null, null, null);
    	if (cursor == null) {
    		return null;
    	}
    	
    	try {
    		if (cursor.moveToFirst()) {
    			byte[] data = cursor.getBlob(0);
    			if (data != null) {
    				return new ByteArrayInputStream(data);
    			}
    		}
    	} finally {
    		cursor.close();
    	}
    	return null;
    }

	public EpicContact[] getContactData(Cursor cur){
    	EpicContact[] retContacts = new EpicContact[0];
    	List<EpicContact> tmpContacts = new ArrayList<EpicContact>();
    	if (EpicFeedConsts.debugLevel > 5) Log.i(TAG, "getContactData: number of contacts: " + cur.getCount());
		int idColumn = cur.getColumnIndex(People.Phones._ID);
		int nameColumn = cur.getColumnIndex(People.Phones.NAME);
		int phoneColumn = cur.getColumnIndex(People.Phones.NUMBER);
		int typeColumn = cur.getColumnIndex(People.Phones.TYPE);
		int typeLblColumn = cur.getColumnIndex(People.Phones.LABEL);
		int count = 0;
		
    	if (cur.moveToFirst()) {
        	if (EpicFeedConsts.debugLevel > 8) Log.i(TAG, "getContactData: getting contacts");
    		
    		for (int i = 0; i < cur.getCount(); i++) {
    			EpicContact tmpContact = new EpicContact(cur.getString(phoneColumn).trim());
    			// Get the field values
    			tmpContact.name = cur.getString(nameColumn).trim();
    			tmpContact.nameId = cur.getString(idColumn).trim();
    			tmpContact.exists = false;
    			switch (cur.getInt(typeColumn)) {
	    			case Contacts.Phones.TYPE_FAX_HOME: 
	    				tmpContact.type = "Home Fax";
	    				break;
	    			case Contacts.Phones.TYPE_FAX_WORK: 
	    				tmpContact.type = "Work Fax";
	    				break;
	    			case Contacts.Phones.TYPE_HOME: 
	    				tmpContact.type = "Home";
	    				break;
	    			case Contacts.Phones.TYPE_WORK: 
	    				tmpContact.type = "Work";
	    				break;
	    			case Contacts.Phones.TYPE_MOBILE: 
	    				tmpContact.type = "Mobile";
	    				break;
	    			case Contacts.Phones.TYPE_PAGER: 
	    				tmpContact.type = "Pager";
	    				break;
	    			case Contacts.Phones.TYPE_OTHER: 
	    				tmpContact.type = cur.getString(typeLblColumn);
	    				break;
	    			default: tmpContact.type = "";
    			}
    			if (tmpContact.type == null) tmpContact.type = "";
    			
    			//tmpContact.photo = Drawable.createFromStream(openPhoto(cur.getInt(idColumn)), "photo");
    			tmpContacts.add(tmpContact);
    			if (tmpContact.phoneID != null & tmpContact.phoneID.length() > 0) count++;
    			cur.moveToNext();
    		}
    	}
		cur.deactivate();
		cur.close();

		retContacts = new EpicContact[count];
    	count = 0;
		for (int i = 0; i < tmpContacts.size(); i++) {
			if (EpicFeedConsts.debugLevel > 8) Log.i(TAG, "getContactData: adding contact, phone ID null is: " + (tmpContacts.get(i).phoneID != null));
    		if (tmpContacts.get(i).phoneID != null & tmpContacts.get(i).phoneID.length() > 0) {
    			retContacts[count] = tmpContacts.get(i);
    			count++;
    		}
    	}
    	
    	return retContacts;
    }
}
