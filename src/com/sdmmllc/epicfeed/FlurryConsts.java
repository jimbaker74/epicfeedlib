package com.sdmmllc.epicfeed;

import java.util.HashMap;
import java.util.Map;

public class FlurryConsts {

    // flurry events
    public static final String PAGE_VIEW_EVENT = "page";
    public static final String WEB_VIEW_EVENT = "web_view";
    public static final String BTN_CLICK_EVENT = "btn_click";
    
    // flurry maps
    public static Map<String, String> getMap(String param, String value) {
    	Map<String, String> params = new HashMap<String, String>();
        params.put(param, value);
        return params;
    }
    
    public static Map<String, String> addMapValue(Map<String, String> map, String param, String value) {
    	if (map == null) {
    		map = new HashMap<String, String>();
    	}
    	map.put(param, value);
    	return map;
    }
    
    // for flurry ads
    public static boolean FLURRY_AD_REPORTING_ON = true;
    public static final String AD_CLICKED = "AD_CLICKED";
    public static final String AD_CLOSED = "AD_CLOSED";
    public static final String AD_COMPLETED = "AD_COMPLETED";
    public static final String AD_FAILED = "AD_FAILED";
    public static final String AD_LOADED = "AD_LOADED";
    
}
