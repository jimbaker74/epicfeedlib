package com.sdmmllc.epicfeed;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.widget.ImageView;

import com.sdmm.epicfeed.R;
import com.sdmmllc.epicfeed.utils.EpicFeedConsts;

 
public class SpaTextSplash extends Activity {
        
	private static final int STOPSPLASH = 0;
	//time in milliseconds
	private static long SPLASHTIME = EpicFeedConsts.SPLASHTIME;

	private ImageView splash;

	//handler for splash screen
	private Handler splashHandler = new Handler() {
		/* (non-Javadoc)
		 * @see android.os.Handler#handleMessage(android.os.Message)
		 */
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case STOPSPLASH:
				//remove SplashScreen from view
				//splash.setVisibility(View.GONE);
				break;
			}
			super.handleMessage(msg);
			finish();
		}
	};

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.splashscreen);
        if (EpicFeedConsts.debugLevel > 0) SPLASHTIME = EpicFeedConsts.DEBUG_SPLASHTIME;
        //splash = (ImageView) findViewById(R.id.splashscreen);
        Message msg = new Message();
        msg.what = STOPSPLASH;
        splashHandler.sendMessageDelayed(msg, SPLASHTIME);
    }
}
