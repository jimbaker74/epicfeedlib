package com.sdmmllc.epicfeed.model;

import java.util.HashMap;
import java.util.StringTokenizer;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ImageSpan;
import android.util.Log;

import com.sdmm.epicfeed.R;
import com.sdmmllc.epicfeed.utils.EpicFeedConsts;

public class FacebookParser {

	public static String TAG = "FacebookParser";
	private String mText;
	private Context ctx;
    private final HashMap<String, Integer> mFacebookToRes;
	
    static class FacebookIcons {
        private static final int[] rIconIds = {
            R.drawable.rn_emo_default,
            R.drawable.rn_emo_love,
            R.drawable.rn_emo_happy,
            R.drawable.rn_emo_sad,
            R.drawable.rn_emo_lucky,
            R.drawable.rn_emo_party,
            R.drawable.rn_emo_top_10,
            R.drawable.rn_emo_crusin,
            R.drawable.rn_emo_money,
            R.drawable.rn_emo_new,
            R.drawable.rn_emo_country,
            R.drawable.rn_emo_lol,
            R.drawable.rn_emo_weekend,
            R.drawable.rn_emo_default,
            R.drawable.rn_emo_default,
        };

        public static int PHOTO_LINK = 0;

        public static int getFacebookResource(int which) {
            return rIconIds[which];
        }
    }

    // NOTE: if you change anything about this array, you must make the corresponding change
    // to the string arrays: default_rednotes and default_rednote_names in res/values/rednote_strings.xml
    public static final int[] DEFAULT_FB_RES_IDS = {
        FacebookIcons.getFacebookResource(FacebookIcons.PHOTO_LINK),           //  0
    };

    String[] facebookIconArray;
    
    public FacebookParser(Context context) {
		ctx = context;
		facebookIconArray = ctx.getResources().getStringArray(R.array.default_rednotes);
		mFacebookToRes = buildFacebookToRes();
	}
	
	public void setText(String text) {
		mText = text;
	}
	
	public boolean hasRednote(String text) {
	    StringTokenizer st = new StringTokenizer(text);
	    int len = st.countTokens();
	    for (int i = 0; i < len; i++) {
	        String token = st.nextToken();
	        if (EpicFeedConsts.DEBUG_SMILEY_PARSER) Log.i(TAG, ".hasFacebookIcons: checking token: " + token);
	        if (token.startsWith(ctx.getString(R.string.rednoteShareHTTP))||
	        		(token.contains(EpicFeedConsts.BITLY_URL)&&token.contains(EpicFeedConsts.RDNT_STR))) {
	        	return true;
	        }
	    }
	    return false;
	}
	
    /**
     * Builds the hashtable we use for mapping the string version
     * of a smiley (e.g. ":-)") to a resource ID for the icon version.
     */
    private HashMap<String, Integer> buildFacebookToRes() {
        if (DEFAULT_FB_RES_IDS.length != facebookIconArray.length) {
            // Throw an exception if someone updated DEFAULT_RN_RES_IDS
            // and failed to update arrays.xml
            throw new IllegalStateException("Smiley resource ID/text mismatch");
        }

        HashMap<String, Integer> smileyToRes =
                            new HashMap<String, Integer>(facebookIconArray.length);
        for (int i = 0; i < facebookIconArray.length; i++) {
            smileyToRes.put(facebookIconArray[i], DEFAULT_FB_RES_IDS[i]);
        }

        return smileyToRes;
    }

	/**
	 * Retrieves the parsed text as a spannable string object.
	 * @param context the context for fetching Rednote resources.
	 * @return the spannable string as CharSequence.
	 */
	public CharSequence getSpannableString() {
		return getSpannableString(mText);
	}
	
	public CharSequence getSpannableString(String text) {
	    SpannableStringBuilder builder = new SpannableStringBuilder();
	    if (EpicFeedConsts.DEBUG_SMILEY_PARSER) Log.i(TAG, "checking string: " + text);
	    StringTokenizer st = new StringTokenizer(text);
	    int len = st.countTokens();
	    for (int i = 0; i < len; i++) {
	        String token = st.nextToken();
	        if (EpicFeedConsts.DEBUG_SMILEY_PARSER) Log.i(TAG, "checking token: " + token);
	        int start = builder.length();
	        builder.append(token);
	        if (token.startsWith(ctx.getString(R.string.rednoteShareHTTP))||
	        		(token.contains(EpicFeedConsts.BITLY_URL)&&token.contains(EpicFeedConsts.RDNT_STR))) {
	        	if (EpicFeedConsts.DEBUG_SMILEY_PARSER) Log.i(TAG, "found Rednote!");
	        	String moodCd; 
	        	if ((token.split("\\?") != null)&&(token.split("\\?").length > 1)
	        			&&(token.split("\\?")[1] != null)
	        			&&(token.split("\\?")[1].split("=") != null)&&(token.split("\\?")[1].split("=").length > 1)
	        			&&(token.split("\\?")[1].split("=")[1] != null)) {
	        		moodCd = token.split("\\?")[1].split("=")[1];
		        	if (moodCd.length() < 2) moodCd = "rn";
	        	} else {
	        		moodCd = "rn";
	        	}
	            int resid = mFacebookToRes.get(moodCd);
	            if (resid != -1) {
	                builder.setSpan(new ImageSpan(ctx, resid),
	                		start,
	                		builder.length(),
	                		Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
	            }
	        }
		    builder.append(" ");
	    }
	    return builder;
	}
	
	public ImageSpan getSpannable(String text) {
	    StringTokenizer st = new StringTokenizer(text);
	    int len = st.countTokens();
	    for (int i = 0; i < len; i++) {
	        String token = st.nextToken();
	        if (EpicFeedConsts.DEBUG_SMILEY_PARSER) Log.i(TAG, "checking token: " + token);
	        if (token.startsWith(ctx.getString(R.string.rednoteShareHTTP))||
	        		(token.contains(EpicFeedConsts.BITLY_URL)&&token.contains(EpicFeedConsts.RDNT_STR))) {
	        	if (EpicFeedConsts.DEBUG_SMILEY_PARSER) Log.i(TAG, "found Rednote!");
	        	String moodCd; 
	        	if ((token.split("\\?") != null)&&(token.split("\\?").length > 1)
	        			&&(token.split("\\?")[1] != null)
	        			&&(token.split("\\?")[1].split("=") != null)&&(token.split("\\?")[1].split("=").length > 1)
	        			&&(token.split("\\?")[1].split("=")[1] != null)) {
	        		moodCd = token.split("\\?")[1].split("=")[1];
		        	if (moodCd.length() < 2) moodCd = "rn";
	        	} else {
	        		moodCd = "rn";
	        	}
	            int resid = mFacebookToRes.get(moodCd);
	            if (resid != -1) {
	                return new ImageSpan(ctx, resid);
	            }
	        }
	    }
	    return null;
	}
	
	private class ImageGetter implements Html.ImageGetter {
	 
		public Drawable getDrawable(String source) {
			int id;
			if (source.equals("rn_btn_happy.jpg")) {
				id = R.drawable.rn_btn_happy;
			} else {
				return null;
			}
			 
			Drawable d = ctx.getResources().getDrawable(id);
			d.setBounds(0,0,d.getIntrinsicWidth(),d.getIntrinsicHeight());
			return d;
		}
	};
}
