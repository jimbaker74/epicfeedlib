package com.sdmmllc.epicfeed.model;

import java.lang.ref.WeakReference;
import java.util.Arrays;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookOperationCanceledException;
import com.facebook.FacebookRequestError;
import com.facebook.LoggingBehavior;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionDefaultAudience;
import com.facebook.SessionState;
import com.facebook.Settings;
import com.facebook.model.GraphUser;
import com.facebook.widget.FacebookDialog;
import com.sdmm.epicfeed.R;
import com.sdmmllc.epicfeed.EpicFeedController;
import com.sdmmllc.epicfeed.utils.EpicFeedConsts;

public class FacebookConnectionModel {

	public static final String TAG = "FacebookConnectionModel";
    private PendingAction pendingAction = PendingAction.NONE;
    private static final int REAUTH_ACTIVITY_CODE = 100;
    private static final String PERMISSION = "publish_actions";
    private static final Uri M_FACEBOOK_URL = Uri.parse("http://m.facebook.com");
    private Context mContext;
    private WeakReference<Activity> mActivity;
    private FacebookConnectionListener mListener;
    private Session session;
    private static final List<String> PERMISSIONS = Arrays.asList(
    		"email", "basic_info", "read_mailbox", "read_stream",
    		"user_about_me", "friends_about_me",
    		"user_events", "friends_events",
    		"user_likes", "friends_likes",
    		"user_location", "friends_location",
    		"user_photos", "friends_photos",
    		"user_questions", "friends_questions",
    		"user_status", "friends_status",
    		"user_subscriptions", "friends_subscriptions",
    		"user_videos", "friends_videos",
    		"user_website", "friends_website",
    		"user_online_presence", "friends_online_presence"
    		);
    
    private static final List<String> PUBLISH_PERMISSIONS = Arrays.asList(
    		"email", "basic_info", "read_mailbox", "read_stream",
    		"user_about_me", "friends_about_me",
    		"user_events", "friends_events",
    		"user_likes", "friends_likes",
    		"user_location", "friends_location",
    		"user_photos", "friends_photos",
    		"user_questions", "friends_questions",
    		"user_status", "friends_status",
    		"user_subscriptions", "friends_subscriptions",
    		"user_videos", "friends_videos",
    		"user_website", "friends_website",
    		"user_online_presence", "friends_online_presence",
    		"publish_actions", "publish_stream"
    		);
    
    public FacebookConnectionModel (WeakReference<Activity> activity) {
    	mActivity = activity;
    	mContext = activity.get().getApplicationContext();
    }
    
    public Session getSession() {
		return session;
	}

	public void setSession(Session session) {
		this.session = session;
	}

	public static Intent getFbIntent(String uri) {
		Intent fbIntent;
		try {
	        EpicFeedController.getContext().getPackageManager().getPackageInfo("com.facebook.katana", 0);
	        if (uri != null && uri.length() > 0) {
		        fbIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("facebook://" + uri));
	        } else {
		        fbIntent = new Intent("android.intent.category.LAUNCHER");
		        fbIntent.setClassName("com.facebook.katana", "com.facebook.katana.LoginActivity");
	        }
	    } catch (Exception e) {
	        fbIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/"));
	    }
		fbIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		return fbIntent;
	}

    private enum PendingAction {
        NONE,
        POST_PHOTO,
        POST_STATUS_UPDATE
    }

    public Session setupFB(Bundle savedInstanceState) {
        Settings.addLoggingBehavior(LoggingBehavior.INCLUDE_ACCESS_TOKENS);

        session = Session.getActiveSession();
        if (session == null) {
            if (savedInstanceState != null) {
                session = Session.restoreSession(mContext, null, callback, savedInstanceState);
            }
            if (session == null) {
                session = new Session(mContext);
            }
            Session.setActiveSession(session);
            if (session.getState().equals(SessionState.CREATED_TOKEN_LOADED)) {
                session.openForRead(new Session.OpenRequest(mActivity.get()).setCallback(callback));
            }
        }
        return session;
    }
    
    public void setFacebookConnectionListener(FacebookConnectionListener listener) {
    	mListener = listener;
    }
    
    public FacebookConnectionListener setFacebookConnectionListener() {
    	return mListener;
    }
    
    //private Session.StatusCallback statusCallback = new SessionStatusCallback();

    public Session.StatusCallback getStatusCallback() {
    	return callback;
    }

    public void setStatusCallback(Session.StatusCallback newCallback) {
    	callback = newCallback;
    }
    
    private Session.StatusCallback callback = new Session.StatusCallback() {
        @Override
        public void call(Session session, SessionState state, Exception exception) {
            onSessionStateChange(session, state, exception);
        }
    };

    public FacebookDialog.Callback getDialogCallback() {
    	return dialogCallback;
    }
    
    private FacebookDialog.Callback dialogCallback = new FacebookDialog.Callback() {
        @Override
        public void onError(FacebookDialog.PendingCall pendingCall, Exception error, Bundle data) {
            Log.d("HelloFacebook", String.format("Error: %s", error.toString()));
        }

        @Override
        public void onComplete(FacebookDialog.PendingCall pendingCall, Bundle data) {
            Log.d("HelloFacebook", "Success!");
        }
    };

    private void onSessionStateChange(Session session, SessionState state, Exception exception) {
        if (pendingAction != PendingAction.NONE &&
                (exception instanceof FacebookOperationCanceledException ||
                exception instanceof FacebookAuthorizationException)) {
                new AlertDialog.Builder(mContext)
                    .setTitle("Cancelled")
                    .setMessage("Facebook permission not granted. Please login to Facebook")
                    .setPositiveButton(R.string.ok, null)
                    .show();
            pendingAction = PendingAction.NONE;
        } else if (state == SessionState.OPENED_TOKEN_UPDATED) {
            handlePendingAction();
        }
        if (mListener != null) mListener.onStateChanged();
    }

    @SuppressWarnings("incomplete-switch")
    private void handlePendingAction() {
        PendingAction previouslyPendingAction = pendingAction;
        // These actions may re-set pendingAction if they are still pending, but we assume they
        // will succeed.
        pendingAction = PendingAction.NONE;

        switch (previouslyPendingAction) {
            case POST_PHOTO:
                //postPhoto();
                break;
            case POST_STATUS_UPDATE:
                //postStatusUpdate();
                break;
        }
    }
    
    public void onClickFbLogin(Session.StatusCallback statusCallback) {
        Session session = Session.getActiveSession();
        if (!session.isOpened() && !session.isClosed()) {
            session.openForRead(new Session. OpenRequest(mActivity.get()).setCallback(statusCallback));
        } else {
            Session.openActiveSession(mActivity.get(), true, statusCallback, PERMISSIONS);
        }
    }

    public void onClickFbLogout() {
        Session session = Session.getActiveSession();
        if (!session.isClosed()) {
            session.closeAndClearTokenInformation();
        }
    }

    private class SessionStatusCallback implements Session.StatusCallback {
        @Override
        public void call(Session session, SessionState state, Exception exception) {
        	if (EpicFeedConsts.DEBUG_FB) Log.i(TAG, "Session.StatusCallback.call() method executing");
            if (session != null && state.equals(SessionState.OPENED)) makeMeRequest(session);
        }
    }
	
    public void makeMeRequest(final Session session) {
    	if (EpicFeedConsts.DEBUG_FB) Log.i(TAG, "makeMeRequest started");
        Request request = Request.newMeRequest(session, new Request.GraphUserCallback() {
            @Override
            public void onCompleted(GraphUser user, Response response) {
                if (session == Session.getActiveSession()) {
                    if (user != null && EpicFeedController.getGraphUser() == null) {
                    	EpicFeedController.setGraphUser(user);
                    	callback.call(session, session.getState(), null);
                    }
                }
                if (response.getError() != null) {
                    handleError(response.getError());
                }
            }
        });
        request.executeAsync();
    }

    private void handleError(FacebookRequestError error) {
        DialogInterface.OnClickListener listener = null;
        String dialogBody = null;

        if (error == null) {
            dialogBody = mContext.getString(R.string.error_dialog_default_text);
        } else {
            switch (error.getCategory()) {
                case AUTHENTICATION_RETRY:
                    // tell the user what happened by getting the message id, and
                    // retry the operation later
                    String userAction = (error.shouldNotifyUser()) ? "" :
                            mContext.getString(error.getUserActionMessageId());
                    dialogBody = mContext.getString(R.string.error_authentication_retry, userAction);
                    listener = new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Intent intent = new Intent(Intent.ACTION_VIEW, M_FACEBOOK_URL);
                            mContext.startActivity(intent);
                        }
                    };
                    break;

                case AUTHENTICATION_REOPEN_SESSION:
                    // close the session and reopen it.
                    dialogBody = mContext.getString(R.string.error_authentication_reopen);
                    listener = new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Session session = Session.getActiveSession();
                            if (session != null && !session.isClosed()) {
                                session.closeAndClearTokenInformation();
                            }
                        }
                    };
                    break;

                case PERMISSION:
                    // request the publish permission
                    dialogBody = mContext.getString(R.string.error_permission);
                    listener = new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            //pendingAnnounce = true;
                            requestPublishPermissions(Session.getActiveSession());
                        }
                    };
                    break;

                case SERVER:
                case THROTTLING:
                    // this is usually temporary, don't clear the fields, and
                    // ask the user to try again
                    dialogBody = mContext.getString(R.string.error_server);
                    break;

                case BAD_REQUEST:
                    // this is likely a coding error, ask the user to file a bug
                    dialogBody = mContext.getString(R.string.error_bad_request, error.getErrorMessage());
                    break;

                case OTHER:
                case CLIENT:
                default:
                    // an unknown issue occurred, this could be a code error, or
                    // a server side issue, log the issue, and either ask the
                    // user to retry, or file a bug
                    dialogBody = mContext.getString(R.string.error_unknown, error.getErrorMessage());
                    break;
            }
        }

        new AlertDialog.Builder(mContext)
                .setPositiveButton(R.string.error_dialog_button_text, listener)
                .setTitle(R.string.error_dialog_title)
                .setMessage(dialogBody)
                .show();
    }

    private void requestPublishPermissions(Session session) {
        if (session != null) {
            Session.NewPermissionsRequest newPermissionsRequest = new Session.NewPermissionsRequest(mActivity.get(), PERMISSION)
                    // demonstrate how to set an audience for the publish permissions,
                    // if none are set, this defaults to FRIENDS
                    .setDefaultAudience(SessionDefaultAudience.FRIENDS)
                    .setRequestCode(REAUTH_ACTIVITY_CODE);
            session.requestNewPublishPermissions(newPermissionsRequest);
        }
    }


}
