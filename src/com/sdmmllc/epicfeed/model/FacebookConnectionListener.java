package com.sdmmllc.epicfeed.model;

public interface FacebookConnectionListener {
	public void onConnect();
	public void onDisconnect();
	public void onStateChanged();
}
