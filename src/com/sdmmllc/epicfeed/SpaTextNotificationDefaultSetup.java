package com.sdmmllc.epicfeed;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TableLayout;
import android.widget.TextView;

import com.sdmm.epicfeed.R;
import com.sdmmllc.epicfeed.data.EpicContact;
import com.sdmmllc.epicfeed.data.EpicDB;
import com.sdmmllc.epicfeed.utils.EpicFeedConsts;


public class SpaTextNotificationDefaultSetup extends Activity {
	private EditText tickerText, titleText, msgText;
	private CheckBox defaultIconChk, customMsgChk;
	private TableLayout icon1Border, icon2Border, icon3Border, icon4Border, icon5Border;
	private Button testNotification;
	private ImageButton icon1Btn, icon2Btn, icon3Btn, icon4Btn, icon5Btn;
	private Context spaContext;
	private Bundle savedState;
	private String TAG = "SpaTextNotificationDefaultSetup";
	private EpicDB spaDB;
	private SharedPreferences settings;
	private EpicContact backContact = new EpicContact("");
    private SpaUserAccount mSpaUserAccount;
	private LayoutInflater mInflater;
	private boolean saveChanges = true;
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
        mSpaUserAccount.updateAuthActivity();
		MenuInflater menuInfl = getMenuInflater();
		menuInfl.inflate(R.menu.smsdefaultnotification_menu, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
        mSpaUserAccount.updateAuthActivity();
		int itemId = item.getItemId();
		if (itemId == R.id.defaultNotificationHelp) {
			helpDialog();
			return true;
		} else if (itemId == R.id.defaultNotificationBack) {
			saveChanges = false;
			cancelUpdateNotificationData();
			setResult(RESULT_CANCELED);
			finish();
			return true;
		}
		return false;
	}

	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.defaultnotification);
        spaContext = this;
        mSpaUserAccount = EpicFeedController.getSpaUserAccount(this);
        settings = spaContext.getSharedPreferences(EpicFeedConsts.PREFS_NAME, 0);
        savedState = savedInstanceState;
        mInflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        tickerText = (EditText)findViewById(R.id.defaultStatusBarNotificationSetupCustomTickerEdit);
        titleText = (EditText)findViewById(R.id.defaultStatusBarNotificationSetupCustomTitleEdit);
        msgText = (EditText)findViewById(R.id.defaultStatusBarNotificationSetupCustomMsgEdit);
        defaultIconChk = (CheckBox)findViewById(R.id.defaultStatusBarNotificationSetupDefaultIconChk);
        customMsgChk = (CheckBox)findViewById(R.id.defaultStatusBarNotificationSetupCustomMsgChk);
        icon1Border = (TableLayout)findViewById(R.id.defaultIcon1Border);
        icon2Border = (TableLayout)findViewById(R.id.defaultIcon2Border);
        icon3Border = (TableLayout)findViewById(R.id.defaultIcon3Border);
        icon4Border = (TableLayout)findViewById(R.id.defaultIcon4Border);
        icon5Border = (TableLayout)findViewById(R.id.defaultIcon5Border);
        icon1Btn = (ImageButton)findViewById(R.id.defaultIconBtn1);
        icon2Btn = (ImageButton)findViewById(R.id.defaultIconBtn2);
        icon3Btn = (ImageButton)findViewById(R.id.defaultIconBtn3);
        icon4Btn = (ImageButton)findViewById(R.id.defaultIconBtn4);
        icon5Btn = (ImageButton)findViewById(R.id.defaultIconBtn5);
        // if default icon is used, then disable icons
        if (settings.getBoolean(EpicFeedConsts.NOTIFY_USE_DEF_ICON, true)) {
	        defaultIconChk.setChecked(true);
	        changeSelectedIcon(0, false);
	        backContact.notifyOptions.statusbarDefaultIcon = true;
        } else {
	        backContact.notifyOptions.statusbarDefaultIcon = false;
	        backContact.notifyOptions.statusbarIcon = settings.getInt(EpicFeedConsts.NOTIFY_DEF_ICON, 0);
	        defaultIconChk.setChecked(false);
	        changeSelectedIcon(backContact.notifyOptions.statusbarIcon, false);
        }
        if (settings.getBoolean(EpicFeedConsts.NOTIFY_USE_DEF_TEXT, true)) {
        	enableNotificationText(false);
        	customMsgChk.setChecked(false);
        	backContact.notifyOptions.statusbarDefaultNotification = true;
        } else {
        	customMsgChk.setChecked(true);
        	enableNotificationText(true);
        	backContact.notifyOptions.statusbarDefaultNotification = false;
	        if (settings.getBoolean(EpicFeedConsts.NOTIFY_USE_DEF_TICKER, true)) {
	        	tickerText.setText(getString(R.string.statusNotificationTicker));
	        	backContact.notifyOptions.statusbarDefaultTicker = true;
	        } else {
	        	tickerText.setText(settings.getString(EpicFeedConsts.NOTIFY_DEF_TICKER, ""));
	        	backContact.notifyOptions.statusbarDefaultTicker = false;
	        	backContact.notifyOptions.statusbarTicker = tickerText.getText().toString();
	        }
	        if (settings.getBoolean(EpicFeedConsts.NOTIFY_USE_DEF_TITLE, true)) {
	        	titleText.setText(getString(R.string.statusNotificationTitle));
	        	backContact.notifyOptions.statusbarDefaultTitle = true;
	        } else {
	        	titleText.setText(settings.getString(EpicFeedConsts.NOTIFY_DEF_TITLE, ""));
	        	backContact.notifyOptions.statusbarDefaultTitle = false;
	        	backContact.notifyOptions.statusbarTitle = titleText.getText().toString();
	        }
	        if (settings.getBoolean(EpicFeedConsts.NOTIFY_USE_DEF_MSG, true)) {
	        	msgText.setText(getString(R.string.statusNotificationText));
	        	backContact.notifyOptions.statusbarDefaultText = true;
	        } else {
	        	msgText.setText(settings.getString(EpicFeedConsts.NOTIFY_DEF_MSG, ""));
	        	backContact.notifyOptions.statusbarDefaultText = false;
	        	backContact.notifyOptions.statusbarText = msgText.getText().toString();
	        }
        }
	}
	
	public void helpDialog () {
        mSpaUserAccount.updateAuthActivity();
		closeOptionsMenu();
		final View helpTextView = mInflater.inflate(R.layout.helpview, null);
		final TextView helpText = (TextView)helpTextView.findViewById(R.id.helptext);
		helpText.setText(Html.fromHtml(
				getText(R.string.defaultNotificationPageHelp).toString()));
		AlertDialog ad = new AlertDialog.Builder(spaContext)
        .setTitle(R.string.defaultNotificationPageHelpHeader)
        .setView(helpTextView)
        .setNeutralButton(getString(R.string.sysDone), new DialogInterface.OnClickListener() {
            @Override
			public void onClick(DialogInterface dialog, int which) {
            	dialog.cancel();
            }
        })
        .create();
		ad.show();
	}
	
	public void enableNotificationText(boolean enable) {
		customMsgChk.setChecked(enable);
		tickerText.setEnabled(enable);
		titleText.setEnabled(enable);
		msgText.setEnabled(enable);
	}
	
	public void useDefaultIcon(View v) {
		if (defaultIconChk.isChecked()) 
			changeSelectedIcon(SpaTextNotification.getIconId(R.drawable.notification_icon), false);
		else changeSelectedIcon(
				settings.getInt(EpicFeedConsts.NOTIFY_DEF_ICON, 
						SpaTextNotification.getIconId(R.drawable.notification_icon)), false);
	}
	
	public void changeSelectedIcon(int icon_selected, boolean saveChange) {
        mSpaUserAccount.updateAuthActivity();
		if (EpicFeedConsts.debugLevel > 7) Log.i(TAG, "changeSelectedIcon");
		SharedPreferences.Editor editor = settings.edit();
		if (icon_selected > 100) icon_selected = 0;
		icon1Border.setPadding(0, 0, 0, 0);
		icon2Border.setPadding(0, 0, 0, 0);
		icon3Border.setPadding(0, 0, 0, 0);
		icon4Border.setPadding(0, 0, 0, 0);
		icon5Border.setPadding(0, 0, 0, 0);
		switch (icon_selected) {
		case 0:
			icon1Border.setPadding(2, 2, 2, 2);
			if (saveChange) editor.putInt(EpicFeedConsts.NOTIFY_DEF_ICON, 0);
			break;
		case 1:
			if (EpicFeedConsts.debugLevel > 7) Log.i(TAG, "changeSelectedIcon: icon 2 selected");
			icon2Border.setPadding(2, 2, 2, 2);
			if (saveChange) editor.putInt(EpicFeedConsts.NOTIFY_DEF_ICON, 1);
			break;
		case 2:
			icon3Border.setPadding(2, 2, 2, 2);
			if (saveChange) editor.putInt(EpicFeedConsts.NOTIFY_DEF_ICON, 2);
			break;
		case 3:
			icon4Border.setPadding(2, 2, 2, 2);
			if (saveChange) editor.putInt(EpicFeedConsts.NOTIFY_DEF_ICON, 3);
			break;
		case 4:
			icon5Border.setPadding(2, 2, 2, 2);
			if (saveChange) editor.putInt(EpicFeedConsts.NOTIFY_DEF_ICON, 4);
			break;
		default:
			icon1Border.setPadding(2, 2, 2, 2);
			if (saveChange) editor.putInt(EpicFeedConsts.NOTIFY_DEF_ICON, 0);
		}
		if (saveChange) editor.commit();
	}
	
	public void icon1Select(View v) {
        defaultIconChk.setChecked(true);
		changeSelectedIcon(0, true);
	}

	public void icon2Select(View v) {
		if (EpicFeedConsts.debugLevel > 7) Log.i(TAG, "icon2Select: selected");
        defaultIconChk.setChecked(false);
		changeSelectedIcon(1, true);
	}

	public void icon3Select(View v) {
        defaultIconChk.setChecked(false);
		changeSelectedIcon(2, true);
	}

	public void icon4Select(View v) {
        defaultIconChk.setChecked(false);
		changeSelectedIcon(3, true);
	}

	public void icon5Select(View v) {
        defaultIconChk.setChecked(false);
		changeSelectedIcon(4, true);
	}
	
	public void setCustomMsg(View v) {
        mSpaUserAccount.updateAuthActivity();
		SharedPreferences.Editor editor = settings.edit();
        if (customMsgChk.isChecked()) {
        	editor.putBoolean(EpicFeedConsts.NOTIFY_USE_DEF_TEXT, false);
        	editor.commit();
        	enableNotificationText(true);
	        if (settings.getBoolean(EpicFeedConsts.NOTIFY_USE_DEF_TICKER, true)) 
	        	tickerText.setText(getString(R.string.statusNotificationTicker));
	        else tickerText.setText(settings.getString(EpicFeedConsts.NOTIFY_DEF_TICKER, ""));
	        if (settings.getBoolean(EpicFeedConsts.NOTIFY_USE_DEF_TITLE, true))
	        	titleText.setText(getString(R.string.statusNotificationTitle));
	        else titleText.setText(settings.getString(EpicFeedConsts.NOTIFY_DEF_TITLE, ""));
	        if (settings.getBoolean(EpicFeedConsts.NOTIFY_USE_DEF_MSG, true))
	        	msgText.setText(getString(R.string.statusNotificationText));
	        else msgText.setText(settings.getString(EpicFeedConsts.NOTIFY_DEF_MSG, ""));
        } else {
        	editor.putBoolean(EpicFeedConsts.NOTIFY_USE_DEF_TEXT, true);
        	editor.commit();
        	enableNotificationText(false);
        	tickerText.setText(getString(R.string.statusNotificationTicker));
        	titleText.setText(getString(R.string.statusNotificationTitle));
        	msgText.setText(getString(R.string.statusNotificationText));
        }
	}
	
	public void testNotification(View v) {
        mSpaUserAccount.updateAuthActivity();
		// clear existing notifications
		((NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE))
			.cancel(R.drawable.notification_icon);
		((NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE))
			.cancel(R.drawable.notification_icon2);
		((NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE))
			.cancel(R.drawable.notification_icon3);
		((NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE))
			.cancel(R.drawable.notification_icon4);
		((NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE))
			.cancel(R.drawable.notification_icon5);
		updateNotificationData();
		EpicContact tmpContact = new EpicContact("");
		tmpContact.type = EpicFeedConsts.TEST_CONTACT;
        if (customMsgChk.isChecked()) {
        	tmpContact.notifyOptions.statusbarDefaultNotification = false;
        	if (tickerText.getText().equals(getString(R.string.statusNotificationTicker))) {
        		tmpContact.notifyOptions.statusbarDefaultTicker = true;
        	} else {
        		tmpContact.notifyOptions.statusbarDefaultTicker = false;
        		tmpContact.notifyOptions.statusbarTicker = tickerText.getText();
        	}
        	if (msgText.getText().equals(getString(R.string.statusNotificationText))) {
        		tmpContact.notifyOptions.statusbarDefaultText = true;
        	} else {
        		tmpContact.notifyOptions.statusbarDefaultText = false;
        		tmpContact.notifyOptions.statusbarText = msgText.getText();
        	}
        	if (titleText.getText().equals(getString(R.string.statusNotificationTitle))) {
        		tmpContact.notifyOptions.statusbarDefaultTitle = true;
        	} else {
        		tmpContact.notifyOptions.statusbarDefaultTitle = false;
        		tmpContact.notifyOptions.statusbarTitle = titleText.getText();
        	}
        } else {
        	tmpContact.notifyOptions.statusbarDefaultNotification = true;
        }
        tmpContact.notifyOptions.statusbarDefaultIcon = defaultIconChk.isChecked();
        tmpContact.notifyOptions.statusbarIcon = 
        	settings.getInt(EpicFeedConsts.NOTIFY_DEF_ICON, 0);
		EpicFeedController.smsHandler.sendNotification(tmpContact);
	}
	
	public void updateNotificationData() {
        mSpaUserAccount.updateAuthActivity();
		SharedPreferences.Editor editor = settings.edit();
        if (customMsgChk.isChecked()) {
        	editor.putBoolean(EpicFeedConsts.NOTIFY_USE_DEF_TEXT, false);
        	if (tickerText.getText().equals(getString(R.string.statusNotificationTicker))) {
            	editor.putBoolean(EpicFeedConsts.NOTIFY_USE_DEF_TICKER, true);
        	} else {
            	editor.putBoolean(EpicFeedConsts.NOTIFY_USE_DEF_TICKER, false);
            	editor.putString(EpicFeedConsts.NOTIFY_DEF_TICKER, tickerText.getText().toString());
        	}
        	if (msgText.getText().equals(getString(R.string.statusNotificationText))) {
            	editor.putBoolean(EpicFeedConsts.NOTIFY_USE_DEF_MSG, true);
        	} else {
            	editor.putBoolean(EpicFeedConsts.NOTIFY_USE_DEF_MSG, false);
            	editor.putString(EpicFeedConsts.NOTIFY_DEF_MSG, msgText.getText().toString());
        	}
        	if (titleText.getText().equals(getString(R.string.statusNotificationTitle))) {
            	editor.putBoolean(EpicFeedConsts.NOTIFY_USE_DEF_TITLE, true);
        	} else {
            	editor.putBoolean(EpicFeedConsts.NOTIFY_USE_DEF_TITLE, false);
            	editor.putString(EpicFeedConsts.NOTIFY_DEF_TITLE, titleText.getText().toString());
        	}
        } else {
        	editor.putBoolean(EpicFeedConsts.NOTIFY_USE_DEF_TEXT, true);
        }
    	editor.putBoolean(EpicFeedConsts.NOTIFY_USE_DEF_ICON, defaultIconChk.isChecked());
    	editor.commit();
	}

	public void cancelUpdateNotificationData() {
		SharedPreferences.Editor editor = settings.edit();
        if (backContact.notifyOptions.statusbarDefaultNotification) {
           	editor.putBoolean(EpicFeedConsts.NOTIFY_USE_DEF_TEXT, true);
        } else {
        	editor.putBoolean(EpicFeedConsts.NOTIFY_USE_DEF_TEXT, false);
        	if (backContact.notifyOptions.statusbarDefaultTicker) {
            	editor.putBoolean(EpicFeedConsts.NOTIFY_USE_DEF_TICKER, true);
        	} else {
            	editor.putBoolean(EpicFeedConsts.NOTIFY_USE_DEF_TICKER, false);
            	editor.putString(EpicFeedConsts.NOTIFY_DEF_TICKER, 
            			backContact.notifyOptions.statusbarTicker.toString());
        	}
        	if (backContact.notifyOptions.statusbarDefaultText) {
            	editor.putBoolean(EpicFeedConsts.NOTIFY_USE_DEF_MSG, true);
        	} else {
            	editor.putBoolean(EpicFeedConsts.NOTIFY_USE_DEF_MSG, false);
            	editor.putString(EpicFeedConsts.NOTIFY_DEF_MSG, 
            			backContact.notifyOptions.statusbarText.toString());
        	}
        	if (backContact.notifyOptions.statusbarDefaultTitle) {
            	editor.putBoolean(EpicFeedConsts.NOTIFY_USE_DEF_TITLE, true);
        	} else {
            	editor.putBoolean(EpicFeedConsts.NOTIFY_USE_DEF_TITLE, false);
            	editor.putString(EpicFeedConsts.NOTIFY_DEF_TITLE, 
            			backContact.notifyOptions.statusbarTitle.toString());
        	}
        } 
    	editor.putBoolean(EpicFeedConsts.NOTIFY_USE_DEF_ICON, 
    			backContact.notifyOptions.statusbarDefaultIcon);
    	if (!backContact.notifyOptions.statusbarDefaultIcon)
        	editor.putInt(EpicFeedConsts.NOTIFY_DEF_ICON, 
        		backContact.notifyOptions.statusbarIcon);
    	editor.commit();
	}

    @Override
    public boolean onTouchEvent(MotionEvent e) {
    	//Log.i(TAG, "Touch event outside activity, do nothing...");
        mSpaUserAccount.updateAuthActivity();
    	return super.onTouchEvent(e);
    }
    
    @Override
    public void onUserInteraction() {
    	//Log.i(TAG, "Touch event...");
        mSpaUserAccount.updateAuthActivity();
    }
    
	@Override
	public void onStop() {
		if (saveChanges) {
			updateNotificationData();
			setResult(RESULT_OK);
		}
		super.onStop();
	}
}