package com.sdmmllc.epicfeed.utils;

public interface EpicSwipeListener {
	public void swipeRight();
	public void swipeLeft();
}
