package com.sdmmllc.epicfeed.utils;

import android.graphics.Bitmap;

public interface EpicImageListener {
	public void onLoaded(Bitmap b, String imgUrl);
}
