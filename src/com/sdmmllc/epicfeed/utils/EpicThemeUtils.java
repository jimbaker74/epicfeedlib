package com.sdmmllc.epicfeed.utils;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.util.Log;
import android.widget.ImageButton;
import android.widget.TextView;

import com.sdmm.epicfeed.R;
import com.sdmmllc.epicfeed.EpicFeedController;
import com.sdmmllc.superdupersmsmanager.sdk.SDSmsConsts;

public class EpicThemeUtils {

	public static final String TAG = "ThemeUtils";
	private static EpicThemeUtils mInstance = null;
	private Context mContext;
	private Resources defRes;
	private ThemeResources themeRes, defaultRes;
	private List<ThemeListener> listeners;
	private boolean useDefault = true;
	private StateListDrawable sendBtnStateList = null;
	private Drawable dividerDrawable;
	
	private EpicThemeUtils(Context context, String packageName) {
		mContext = context;
		defRes = context.getResources();
		themeRes = new ThemeResources(packageName);
		defaultRes = new ThemeResources(EpicFeedController.getContext().getPackageName());
		listeners = new ArrayList<ThemeListener>();
	}
	
	public static EpicThemeUtils getInstance() {
		if (mInstance == null) {
			mInstance = new EpicThemeUtils(EpicFeedController.getContext(), EpicFeedController.getContext().getPackageName());
	    	if (SDSmsConsts.DEBUG_INSTALL_RECEIVER) 
	    		Log.i("ThemeUtils", "mInstance SpaTextApp.getContext().getPackageName():" + EpicFeedController.getContext().getPackageName());
		}
		return mInstance;
	}
	
	public boolean showAds() {
		return mContext.getResources().getBoolean(R.bool.showAds);
	}
	
	public boolean hideActionsOnActionBar() {
		return mContext.getResources().getBoolean(R.bool.hideActionsOnActionBar);
	}
	
	public void addListener(ThemeListener listener) {
		listeners.add(listener);
	}
	
	public void removeListener(ThemeListener listener) {
		listeners.remove(listener);
	}
	
	public boolean switchThemes(String packageName) {
		themeRes = new ThemeResources(packageName);
		if (themeRes.isReady()) {
			if (packageName.equals(EpicFeedController.getContext().getPackageName()) ||
					packageName.equals(EpicFeedConsts.STANDARD_THEME)) useDefault = true;
			else useDefault = false;
			for (ThemeListener listener : listeners) {
				listener.updateTheme();
			}
		}
		return themeRes.isReady();
	}
	
	public boolean useDefaultTheme(boolean useDef) {
		if (useDefault ^ useDef) {
			if (!useDef && themeRes != null) useDefault = useDef;
			else if (useDef) useDefault = useDef; 
			else return useDefault;
			if (themeRes.isReady()) for (ThemeListener listener : listeners) {
				listener.updateTheme();
			}
		}
		return useDefault;
	}
	
	public ThemeResources getTheme() {
		if (useDefault) return defaultRes;
		return themeRes;
	}
	
	public class ThemeResources {
		private Resources res;
		private String mPackageName;
		private boolean found = false;
		
		public ThemeResources(String packageName) {
			try {
				mPackageName = packageName;
				res = mContext.getPackageManager().getResourcesForApplication(packageName);
				found = true;
			} catch (NameNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				found = false;
			}
		}
		
		public void updateSendButton(ImageButton sendBtn) {
			if (useDefault) {
				sendBtn.setImageDrawable(defRes.getDrawable(R.drawable.sent_blk));
				return;
			}
			StateListDrawable states = new StateListDrawable();
			states.addState(new int[] {-android.R.attr.state_enabled},
			    res.getDrawable(res.getIdentifier("sent_blk", "drawable", mPackageName)));
			states.addState(new int[] { },
			    res.getDrawable(res.getIdentifier("sent_blk", "drawable", mPackageName)));
			sendBtn.setImageDrawable(states);
		}
		
		public void updateSendMmsButton(TextView sendBtn) {
			if (useDefault) {
				for(Drawable myOldDrawable : sendBtn.getCompoundDrawables()) {
					if (!(myOldDrawable == null)) myOldDrawable.setCallback(null);
				}

				//use null where you don't want a drawable
				sendBtn.setCompoundDrawablesWithIntrinsicBounds(null, null, null, 
						defRes.getDrawable(R.drawable.sent_blk));
				return;
			}
			StateListDrawable states = new StateListDrawable();
			states.addState(new int[] {-android.R.attr.state_enabled},
			    res.getDrawable(res.getIdentifier("sent_blk", "drawable", mPackageName)));
			states.addState(new int[] { },
			    res.getDrawable(res.getIdentifier("sent_blk", "drawable", mPackageName)));
			for(Drawable myOldDrawable : sendBtn.getCompoundDrawables()) {
			   if (!(myOldDrawable == null)) myOldDrawable.setCallback(null);
			}
			
			//use null where you don't want a drawable
			sendBtn.setCompoundDrawablesWithIntrinsicBounds(null, null, null, states);
		}
		
		public void updateSendDateTxt(TextView dateText) {
			if (useDefault) {
				dateText.setTextColor(defRes.getColor(R.color.instBlack));
				dateText.setBackground(defRes.getDrawable(R.drawable.sent_blk));
			} else {
				try {
					dateText.setTextColor(res.getColor(res.getIdentifier("text_hairline", "color", mPackageName)));
					dateText.setBackground(res.getDrawable(res.getIdentifier("listitem_send_background", "drawable", mPackageName)));
				} catch (Exception e) {
					dateText.setTextColor(defRes.getColor(R.color.instBlack));
					dateText.setBackground(defRes.getDrawable(R.drawable.sent_blk));
				}
			}
		}
		
		public boolean isReady() {
			return found;
		}
		
		public String getPackageName() {
			return mPackageName;
		}
	}
	
	public interface ThemeListener {
		public void updateTheme();
	}
}
