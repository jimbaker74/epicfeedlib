package com.sdmmllc.epicfeed.utils;

import java.io.FileDescriptor;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.ParcelFileDescriptor;
import android.util.Log;

import com.sdmmllc.epicfeed.EpicFeedController;

@SuppressLint("UseSparseArrays")
public class EpicImageUtils {
	
	public static String TAG = "EpicImageUtils";
	
	public static abstract class WeakReferenceStorage<Key, Value> {
		private static HashMap<String, WeakReference<Bitmap>> objectsHash = new HashMap<String, WeakReference<Bitmap>>();
		private static HashMap<String, String> objectsQueue = new HashMap<String, String>();
		private static HashMap<String, EpicImageAdapter> listeners = new HashMap<String, EpicImageAdapter>();
		private int imageCount = 0;
		
		public synchronized boolean hasUrl(String url) {
			return (objectsHash.containsKey(url) && (objectsHash.get(url).get() != null));
		}

		public synchronized Bitmap get(String url) {
		    return objectsHash.get(url).get();
		}
		
		public synchronized Bitmap getMmsImage(String _id, int REQUIRED_SIZE) {
		    if (objectsHash.containsKey(_id)) {
		        WeakReference<Bitmap> ref = objectsHash.get(_id);
		        if (ref.get() == null) {
	            	return getMmsImageFromId(_id, REQUIRED_SIZE).get();
		        } else {
		            return ref.get();
		        }
		    } else {
            	return getMmsImageFromId(_id, REQUIRED_SIZE).get();
		    }
		}
		
		public synchronized Bitmap get(String url, String key, EpicImageAdapter listener) {
		    if (objectsHash.containsKey(url)) {
		        WeakReference<Bitmap> ref = objectsHash.get(url);
		        if (ref.get() == null) {
		            if (!objectsQueue.containsValue(url)) {
		            	getImageFromUrl(url, key);
		            }
	            	objectsQueue.put(key, url);
	            	listeners.put(key, listener);
		            return null;
		        } else {
		        	listener.onLoaded(ref.get(), url);
		            return ref.get();
		        }
		    } else {
		    	if (!objectsQueue.containsValue(url)) {
	            	getImageFromUrl(url, key);
		    	}
	    		objectsQueue.put(key, url);
            	listeners.put(key, listener);
		        return null;
		    }
		}
		
		public void addListener(String key, EpicImageAdapter listener) {
			if (listener != null) listeners.put(key, listener);
		}
		
		public void removeListener(String key) {
			if (key != null) listeners.remove(key);
		}
		
		protected synchronized WeakReference<Bitmap> getMmsImageFromId(String _id, int REQUIRED_SIZE) {
		    Uri partURI = Uri.parse("content://mms/part/" + _id);
		    ParcelFileDescriptor parcelFD = null;
	        Bitmap bitmap = null;
		    try {
		        parcelFD = EpicFeedController.getContext().getContentResolver().openFileDescriptor(partURI, "r");
		        FileDescriptor imageSource = parcelFD.getFileDescriptor();
		        BitmapFactory.Options o = new BitmapFactory.Options();
		        o.inJustDecodeBounds = true;
                o.inPreferredConfig = Bitmap.Config.ARGB_8888;
		        BitmapFactory.decodeFileDescriptor(imageSource, null, o);

		        int width_tmp = o.outWidth, height_tmp = o.outHeight;
		        int scale=1;
		        while(o.outHeight/scale/2>=REQUIRED_SIZE/2)
		            scale*=2;		        

		        Log.i(TAG,  "partId: " + _id + " scale:" + scale + " REQUIRED_SIZE: " + REQUIRED_SIZE + 
		        		" width_tmp:" + width_tmp + " height_tmp:" + height_tmp);

		        BitmapFactory.Options o2 = new BitmapFactory.Options();
		        o2.inSampleSize = scale;
                o2.inPreferredConfig = Bitmap.Config.ARGB_8888;
		        bitmap = BitmapFactory.decodeFileDescriptor(imageSource, null, o2);
		    } catch (IOException e) {
		    	
		    }
		    finally {
		        if (parcelFD != null) {
		            try {
		            	parcelFD.close();
		            } catch (IOException e) {}
		        }
		    }
		    WeakReference<Bitmap> b = new WeakReference<Bitmap>(bitmap);
		    Log.i(TAG, "bitmap " + bitmap.getWidth() + ":" + bitmap.getHeight());
		    objectsHash.put(_id, b);
		    return b;
		}
		
		protected synchronized void getImageFromUrl(final String imageURL, final String key) {
	        AsyncTask<Void, Void, Bitmap> task = new AsyncTask<Void, Void, Bitmap>() {

	            @Override
	            public Bitmap doInBackground(Void... params) {
	                URL fbAvatarUrl = null;
	                Bitmap fbAvatarBitmap = null;
	                try {
	                    fbAvatarUrl = new URL(imageURL);
	                    fbAvatarBitmap = BitmapFactory.decodeStream(fbAvatarUrl.openConnection().getInputStream());
	                } catch (MalformedURLException e) {
	                    e.printStackTrace();
	                } catch (IOException e) {
	                    e.printStackTrace();
	                }
	                return fbAvatarBitmap;
	            }

	            @Override
	            protected void onPostExecute(Bitmap result) {
	            	if (result == null) return;
	            	WeakReference<Bitmap> b = new WeakReference<Bitmap>(result); 
			        objectsHash.put(imageURL, b);
			        //listeners.get(key).onLoaded(b.get(), imageURL);
			        //listeners.remove(key);
			        Iterator<Entry<String, EpicImageAdapter>> it = listeners.entrySet().iterator();
			        while (it.hasNext()) {
			            Map.Entry<String, EpicImageAdapter> pairs = (Map.Entry<String, EpicImageAdapter>)it.next();
			            EpicImageAdapter adapter = (EpicImageAdapter)pairs.getValue();
			            adapter.onLoaded(b.get(), imageURL);
			            if (adapter.done)
			            	it.remove(); // avoids a ConcurrentModificationException
			        }
			        objectsQueue.remove(imageURL);
			        imageCount++;
	            }

	        };
	        task.execute();

		}
		
		protected synchronized void checkRecycled(String imgUrl) {
			if (objectsHash.containsKey(imgUrl) && objectsHash.get(imgUrl).get() != null 
					&& ((android.graphics.Bitmap)(objectsHash.get(imgUrl).get())).isRecycled())
			{
				objectsHash.remove(imgUrl);
		        objectsQueue.remove(imgUrl);
		        imageCount--;
			}
		}

		protected synchronized void remove(Bitmap sd) {
			WeakReference<Bitmap> ref;
			for(Iterator<WeakReference<Bitmap>> iter = objectsHash.values().iterator(); iter.hasNext();) {
				ref = iter.next();
				if (ref.get() != null && ref.get().equals(sd)) {
					iter.remove();
					imageCount--;
				}
			}
		}

		public synchronized void destroy() {
	        imageCount = 0;
			objectsHash = new HashMap<String, WeakReference<Bitmap>>();
			objectsQueue = new HashMap<String, String>();
		}

		public synchronized void recycle() {
	        imageCount = 0;
			objectsHash = new HashMap<String, WeakReference<Bitmap>>();
			objectsQueue = new HashMap<String, String>();
		}
		
	}
	
	public static class EpicBitmaps extends WeakReferenceStorage<String, Bitmap>{
		private static EpicBitmaps instance = null;

		public EpicBitmaps() {
		    super();
		}

		public synchronized static EpicBitmaps getInstance() {
		    if (instance == null) {
		        instance = new EpicBitmaps();
		    }
		    return instance;
		}
		
		public synchronized Bitmap get(String url, String key, EpicImageAdapter listener) {
			checkRecycled(url);
			return super.get(url, key, listener);
		}

	}
}
