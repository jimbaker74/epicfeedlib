package com.sdmmllc.epicfeed.utils;

import com.facebook.model.GraphObject;
import com.facebook.model.GraphObjectList;
import com.facebook.model.GraphUser;

public interface EpicFeedFBUser extends GraphUser {

	GraphObjectList<GraphUserStatus> getStatusList();

    public interface GraphUserStatus extends GraphObject {
        // Getter for the ID field
        String getId();
        long getTime();
        String getStatusId();
        String getSource();
        String getPlaceId();
        String getMessage();
        
        GraphUserStatusComment getComments();
        GraphUserStatusLikeInfo getLikeInfo();
    }
    
    public interface GraphUserStatusComment extends GraphObject {
    	boolean canComment();
    	int comentCount();
    }
    
    public interface GraphUserStatusLikeInfo extends GraphObject {
    	boolean canLike();
    	int likeCount();
    	boolean userLikes();
    }
    
}
