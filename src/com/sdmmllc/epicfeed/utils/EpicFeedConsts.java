package com.sdmmllc.epicfeed.utils;

import android.content.Intent;

public class EpicFeedConsts {

	public static int debugLevel = 0;
    public static boolean 
    	DEBUG 					= false,
    	DEBUG_AD_LISTENER 		= false,
   		DEBUG_AD_HANDLER 		= false,
   		DEBUG_AD_MANAGER 		= false,
   		DEBUG_ADS 				= false,
   		DEBUG_LOGGING_ENABLED 	= false,
   		DEBUG_DISPLAY_ACTIVITY_ON_START = false,
   		DEBUG_SECURITY_QUESTION = false,
   		DEBUG_INSTALL_RECEIVER 	= false,
   		DEBUG_TEXT_MSG_LIST 	= false,
   		DEBUG_INMOBI 			= false,
   		DEBUG_SMILEY_PARSER 	= false,
   		DEBUG_REDNOTE_DEMO 		= false,
		DEBUG_REDNOTE 			= false,
		DEBUG_HANGOUTS 			= false,
		DEBUG_OOME 				= false,
		DEBUG_LICENSE 			= false,
		DEBUG_KITKAT 			= false,
		DEBUG_FB 				= true,
		DEBUG_DB_HELPER 		= false,
    	DEBUG_DB 				= false,
    	DEBUG_MSG_LIST_ACT 		= false,
    	DEBUG_TWT               = true;
    
    public static boolean NEW_REDNOTE = false;
    
    public static String SOCIAL_NETWORK = "social_type";
    public static int 
        NO_DATA = 0,
    	FACEBOOK_DATA = 1,
    	TWITTER_DATA = 2,
    	ALL_SOCIAL_NETWORKS = 3;

	public static int PROMOINST = 15;
	public static int HOMEBTN_PRESSED = 100;
	public static long SPLASHTIME = 1100;
	public static long DEBUG_SPLASHTIME = 200;
	public static long AUTH_TIME = 1000*60*5;
	
	public static int OLD_MAX_AD_HEIGHT = 60;
	public static int MAX_TALL_AD_HEIGHT = 90;

	public static String homeCat = Intent.CATEGORY_HOME;
	
	public static String
		DISCONNECT_TWITTER = "disconnect_twitter",
		DISCONNECT_FACEBOOK = "disconnect_facebook",
		FACEBOOK_INDEX = "facebook_index",
		TWITTER_INDEX = "twitter_index";

	public static String 
		AUTH_STATUS = "AuthStatus",
		SCAN_TYPE = "ScanType",
		THEME_SELECT = "Theme", 
		MSG_ORDER = "MsgOrder", 
		TXT_SIZE = "TextSize", 
		NOTIFY_SCREEN = "NotifyScreen";
	public static String 
		NOTIFY_DEF_ICON = "DefNotifyIcon", 
		NOTIFY_DEF_TICKER = "DefTicker",
		NOTIFY_DEF_TITLE = "DefTitle", 
		NOTIFY_DEF_MSG = "DefMsg",
		NOTIFY_USE_DEF_ICON = "UseDefNotifyIcon", 
		NOTIFY_USE_DEF_TEXT = "UseDefText",
		NOTIFY_USE_DEF_TICKER = "UseDefTicker",
		NOTIFY_USE_DEF_TITLE = "UseDefTitle", 
		NOTIFY_USE_DEF_MSG = "UseDefMsg",
		TEST_CONTACT = "TestContact";
	
	public static String 
		PINK_THEME = "Pink", 
		STANDARD_THEME = "Standard", 
		BLUE_THEME = "Blue",
		GREEN_THEME = "Green", 
		DARK_THEME = "Dark", 
		LIGHT_THEME = "White",
		CLASSIC_PINK_THEME = "ClassicPink", 
		CLASSIC_STANDARD_THEME = "ClassicStandard", 
		CLASSIC_BLUE_THEME = "ClassicBlue",
		CLASSIC_GREEN_THEME = "ClassicGreen", 
		CLASSIC_DARK_THEME = "ClassicDark", 
		CLASSIC_LIGHT_THEME = "ClassicWhite";
	
	public static String DEFAULT_THEME = STANDARD_THEME;
	public static String MSG_DESC_ORDER = "DESC", MSG_ASC_ORDER = "ASC";
	public static String TEXT_SM = "TxtSml", TEXT_MED = "TxtMed", TEXT_LRG = "TxtLrg";
	public static int txt_sml = 12, txt_med = 14, txt_lrg = 16;
	public static String 
		NOTIFY_LOGIN_SCREEN = "LoginScreen", 
		NOTIFY_HOME_SCREEN = "HomeScreen",
		NOTIFY_DO_NOTHING = "DoNothing",
		NOTIFY_CONTACTS_SCREEN = "ContactsScreen";

	public static boolean hideMessages = false;
	public static int freeVersionContacts = 15, licensedVerContacts = 500;
	public static int MAX_PASSWORD_ATTEMPTS = 2;
	
	public static String 
		BITLY_URL = "http://bit.ly/",
		RDNT_STR = "rdnt",
//		REDNOTE_URL = "//spc.rednote.com/share/magnetic/",
//		REDNOTE_HTTP = "http://spc.rednote.com/share/magnetic/",
		REDNOTE_VIEW_DEMO = "RednoteDemo";
    
	public static int 
		AUTHENTICATE = 0, 
		CONFIG = 1, 
		PASSWORD = 2, 
    	LOGENTRY = 3, 
    	SPLASHSCREEN = 4, 
    	INITPWD = 5, 
    	LOGLIST = 6, 
    	ABOUTUS = 7, 
    	TEXTTHREAD = 10, 
    	SECRETMSGSETUP = 11, 
    	CONTACTSETUP = 12,
    	CONTACTSELECT = 13, 
    	UPGRADE = 14, 
    	SHORTCUT = 15, 
    	CONTACTLIST = 16,
    	DEF_TEXTMSGS = 17, 
    	FAKE_MESSAGES = 18, 
    	CONTACTCALL = 19,
    	CONTACTLOGSELECT = 20, 
    	SCAN = 21, 
    	WIPE = 22, 
    	SETTINGS = 23, 
    	CUSTOM_NOTIFICATION = 24,
    	DEFAULT_NOTIFICATION = 25, 
    	OPTIONS = 26, 
    	INSTRUCTIONS = 27, 
    	REDNOTE = 28,
    	HOMESCREEN = 29, 
    	MAINSCREEN = 30, 
    	LOGINSCREEN = 31,
    	SECURITY_QUESTION = 32,
    	PASSWORD_RESET = 33,
    	INIT_INSTRUCTIONS = 34,
    	DECOY_LOGIN_DEMO = 35,
    	PASSWORD_RECOVERY_DEMO = 36,
    	REDNOTE_DEMO = 37,
    	REDNOTE_TRYIT = 38,
    	REDNOTE_SELECT = 39,
    	REDNOTE_MESSAGE = 40,
    	COMPOSE = 41,
    	ABOUT_PRIVACY = 42,
    	SDMM_INFO = 43,
    	SOCIAL_CONNECT = 44;
    
    public static String 
    	homeLogDescr = "Home Page Access", 
    	authLogDescr = "Login Attempt",
		homeButtonLogDescr = "Play/About/Settings Button Pressed",
		decoyLoginButtonDefaultTxt = "Login",
		realLoginButtonDefaultTxt = "Login",
		realLoginButtonDefaultDecoyTxt = "Update\nSubscription",
		realLoginInvisibleDemo = "DemoInvisible";
    
    public static final 
    	String PREFS_NAME = "AuthPrefFile", 
    	SCAN_FILE = "ScanFile",
		FLAGS_FILE = "FlagsFile", 
		DB_FILE ="DBFile", 
		INSTALL_FILE ="InstallFile",
		privacyNoticeStatus = "privacyNoticeStatus",
		scanDone = "need_scan", 
		warnInstall = "warn_install", 
		warnPackage = "warn_package",
		warnHangouts = "warn_hangouts",
		warnKitKat = "warn_kitkat",
		warnHangoutsInstalled = "warn_hangouts_installed",
		logEntry = "log_entry", 
		passwordKey = "passwordTxt", 
		wipeKey = "wipeTxt",
		logKey = "logEntry", 
		licenseChecks = "num_license_chks", 
		securityQuestionEnabled = "securityQuestionEnable",
		securityAnswerKey = "securityAnswer",
		securityQuestionKey = "securityQuestion",
		securityQuestionCaseKey ="securityCase",
		securityQuestionInvisibleKey ="securityInvisible",
		passwordRecoveryEnabled = "passwordRecoveryEnabled",
		passwordRecoveryEmailEnabled = "passwordRecoveryEmailEnabled",
		passwordRecoveryEmail = "passwordRecoveryEmail",
		passwordRecoveryEmailConfirm = "passwordRecoveryEmailConfirm",
		passwordRecoveryResetEnabled = "passwordRecoveryResetEnabled",
		passwordRecoveryResetQuestion = "passwordRecoveryResetQuestion",
		passwordRecoveryResetAnswer = "passwordRecoveryResetAnswer",
		decoyLoginButtonEnabled = "decoyLoginButtonEnabled",
		decoyLoginButtonCustomTxt = "decoyLoginButtonTxt",
		realLoginButtonInvisible = "realLoginButtonInvisible",
		realLoginButtonCustomTxt = "realLoginButtonTxt",
		decoyLoginWebsiteLocation = "decoyLoginWebLoc",
		decoyLoginGotoTextMessages = "decoyLoginGotoTextMessages",
		decoyLoginGotoWebsite = "decoyLoginGotoWebsite",
		
		contactInstFlg = "dispContactInst", 
		confirmSendKey = "confirmSend", 
		confirmDeliveryKey = "confirmDelivery",
		confirmSendYes = "confirmSendYes", 
		confirmSendNo = "confirmSendNo",
		confirmDeliveryYes = "confirmDeliveryYes", 
		confirmDeliveryNo = "confirmDeliveryNo",
		deviceID = "deviceId", 
		serialID = "serialId", 
		uuid = "UUID", 
		installTS = "InstallTS", 
		accountTS = "AccountTS", 
		newInstall = "NewInstall",
		timeoutEnable = "timeout_enable", 
		timeoutDuration = "timeout_duration",
		timeoutScreen = "timeout_screen",
		receivedMsgCnt = "receivedMsgCnt",
		blockedMsgCnt = "blockedMsgCnt",
		
		rnStartDemo = "StartDemo",
		rnExitDemo = "ExitDemo",
		rnPhoneNumber = "PhoneNumber",
		rnContactName = "ContactName",
		rnURL = "rnURL",
		rnReceiveMsgCnt = "rnReceiveMsgCnt",
		rnMessage = "RNMessage",
		rnSent = "MessageSent";
    
	public static String blockUnknownCalls = "blockUnknown", blockUnknownCallsNoVM = "blockUnknownNoVM",
		blockPrivateCalls = "blockPrivate", blockPrivateCallsNoVM = "blockPrivateNoVM";
	
	public static String unknownNumber = "-1", privateNumber = "-2";

	public static String 
	//GAT categories
	gat_Button = "Button", gat_Error = "Error", gat_MenuItem = "Menu", gat_Message = "Message",
	gat_Info = "Info", gat_Options = "Options", gat_Rednote = "Rednote", gat_SDMM = "SDMM",
	// GAT Actions
	gat_Click = "Click", gat_License = "License", gat_Warning = "Warning", gat_Update = "Update",
	gat_Ads = "Ads", gat_PageInfo = "PageInfo", gat_MsgInfo = "MsgInfo", 
	gat_RednoteMenu = "rnMenu", gat_RednoteSelect = "rnSelect", gat_RednoteScreen = "rnScreen", gat_RednoteMsg = "rnMessage",
	gat_SDMMIntro = "SDMMInfo",
	// GAT Labels
	
	// Epic Feed
	gatUnlinkNetwork = "unlinkSocialNetwork",
	
	// Rednote Labels
	gatRnClip = "ClipID", gatRnMood = "MoodCd", 
	gatScreenStartDemo = "Screen01_StartDemo", gatScreenTryIt = "Screen02_TryIt", gatScreenSelect = "Screen03_Select",
	gatScreenMessage = "Screen04_Message", gatSendRednoteMessage = "rnSentMsgScreen", gatSendRednote = "rnSent", 
	gatReceiveRednote = "rnReceive", gatReceivedRednoteMsgCnt = "ReceiveRednoteMsg", gatHangouts = "Hangouts", 
	gatHangoutsInstall = "HangoutsInstall", gatKitKatNotice = "KitKatNotify", gatViewSdmm = "ViewSDMM",
	gatSetSdmmDefault = "SetSdmmAsDefault",
	// Info labels
	gatAdAveRequestTime = "AdAveRequestTime", gatAdRequests = "AdRequests", gatAdImpressions = "AdImpressions", gatAdClick = "AdClick",
	gatPageTime = "PageTime", gatPageView = "PageView", gatPageAveTime = "AvePageTime", gatLocationFound = "LocData",
	gatInterstitialAdDisplayed = "InterstitialDisplayed", gatHangoutsWarning = "HangoutsWarning",
	// Contact List
	gatContactCallBlocked = "CallBlocked", gatContactTextBlocked = "TextBlocked",
	gatContactHideLogs = "HideCallLogs", gatContactForbidden = "Forbidden", gatContactSPA = "SPAChecked",
	gatContactGingerbread = "GingerbreadCallBlockMsg",
	gatModeStealth = "Mode-Stealth", gatModeQuiet = "Mode-Quiet", gatModeNormal = "Mode-Normal",
	gatContactListWarn = "SMS-Warn", gatContactListLongHold = "ContactList-LongHold",
	gatContactListHelp = "ContactList-Help", 
	gatSDMMImg1 = "SDMM_Img1", gatSDMMImg2 = "SDMM_Img2", gatSDMMImg3 = "SDMM_Img3", gatSDMMImg4 = "SDMM_Img4",
	gatSDMMImg5 = "SDMM_Img5", 
	// Text Thread
	gatThemePink = "Theme-Pink", getStandardTheme = "Theme-Standard", gatThemeBlue = "Theme-Blue",
	gatThemeGreen = "Theme-Green", gatThemeDark = "Theme-Dark", gatThemeLight = "Theme-White",
	gatClassicThemePink = "ClassicTheme-Pink", getClassicThemeStandard = "ClassicTheme-Standard", gatClassicThemeBlue = "ClassicTheme-Blue",
	gatClassicThemeGreen = "ClassicTheme-Green", gatClassicThemeDark = "ClassicTheme-Dark", gatClassicThemeLight = "ClassicTheme-White",
	gatTextListHelp = "TextList-Help", gatTextListCall = "TextList-Call",
	gatMsgOrderDesc = "MsgDesc", gatMsgOrderAsc = "MsgAsc",
	gatTxtSizeSml = "TextSmall", gatTxtSizeMed = "TextMed", gatTxtSizeLrg = "TextLrg",
	gatTextListLongHold = "TextList-LongHold", gatTxtExport = "TextThread-Export",
	gatSendMsg ="SendMsg", gatReceiveMsg = "ReceiveMsg", gatBlockedMsg = "BlockedMsg",
	gatComposeCall = "ComposeScreenCall", gatComposeMsg = "ComposeScreenMsg",
	// Initial Password (new installs) and password features
	gatInitPassword = "Password-NewInstall", gatWipeSetup = "WipeSetup", gatSetPassword = "SetPassword",
	gatResetPasswordEnabled = "ResetPasswordEnabled", gatRecoverPasswordEmailEnabled = "RecoverPasswordEmailEnabled", 
	getViewRecovery = "RecoveryOptionsView",
	gatDecoyLoginSetup = "DecoyLoginSetup", gatPasswordResetUsed = "PasswordResetUsed",
	gatPasswordRecoverUsed = "PasswordRecoverUsed", gatPasswordRecoveryViewed = "PasswordRecoveryViewed";

}

