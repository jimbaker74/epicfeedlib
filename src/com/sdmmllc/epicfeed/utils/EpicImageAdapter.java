package com.sdmmllc.epicfeed.utils;

import android.graphics.Bitmap;

public abstract class EpicImageAdapter implements EpicImageListener {
	public boolean done = false; 
	public abstract void onLoaded(Bitmap b, String imgUrl);
}
