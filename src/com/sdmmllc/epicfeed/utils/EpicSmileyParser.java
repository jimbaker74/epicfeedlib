package com.sdmmllc.epicfeed.utils;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.content.Context;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ImageSpan;
import android.util.Log;

import com.sdmm.epicfeed.R;
import com.sdmmllc.epicfeed.rednote.RednoteParser;


/**
 * A class for annotating a CharSequence with spans to convert textual emoticons
 * to graphical ones.
 */
public class EpicSmileyParser {
	
	public static String TAG = "SmileyParser";
	
    // Singleton stuff
    private static EpicSmileyParser sInstance;
    private static RednoteParser mRednoteParser;
    public static EpicSmileyParser getInstance() { return sInstance; }
    public static void init(Context context) {
        sInstance = new EpicSmileyParser(context);
    }

    private final Context mContext;
    private final String[] mSmileyTexts;
    private final String[] mAltSmileyTexts;
    private final Pattern mPattern;
    private final Pattern mAltPattern;
    private final Pattern mURLPattern;
    private final HashMap<String, Integer> mSmileyToRes;
    private final HashMap<String, Integer> mAltSmileyToRes;

    private EpicSmileyParser(Context context) {
        mContext = context;
        mSmileyTexts = mContext.getResources().getStringArray(DEFAULT_SMILEY_TEXTS);
        mAltSmileyTexts = mContext.getResources().getStringArray(ALT_SMILEY_TEXTS);
        mSmileyToRes = buildSmileyToRes();
        mAltSmileyToRes = buildAltSmileyToRes();
        mPattern = buildPattern();
        mAltPattern = buildAltPattern();
        mURLPattern = buildURLPattern();
        mRednoteParser = new RednoteParser(context);
    }

    static class Smileys {
        private static final int[] sIconIds = {
            R.drawable.epic_emo_im_happy,
            R.drawable.epic_emo_im_sad,
            R.drawable.epic_emo_im_winking,
            R.drawable.epic_emo_im_tongue_sticking_out,
            R.drawable.epic_emo_im_surprised,
            R.drawable.epic_emo_im_kissing,
            R.drawable.epic_emo_im_yelling,
            R.drawable.epic_emo_im_cool,
            R.drawable.epic_emo_im_money_mouth,
            R.drawable.epic_emo_im_foot_in_mouth,
            R.drawable.epic_emo_im_embarrassed,
            R.drawable.epic_emo_im_angel,
            R.drawable.epic_emo_im_undecided,
            R.drawable.epic_emo_im_crying,
            R.drawable.epic_emo_im_lips_are_sealed,
            R.drawable.epic_emo_im_laughing,
            R.drawable.epic_emo_im_undecided,
            //R.drawable.emo_im_wtf
        };

        public static int HAPPY = 0;
        public static int SAD = 1;
        public static int WINKING = 2;
        public static int TONGUE_STICKING_OUT = 3;
        public static int SURPRISED = 4;
        public static int KISSING = 5;
        public static int YELLING = 6;
        public static int COOL = 7;
        public static int MONEY_MOUTH = 8;
        public static int FOOT_IN_MOUTH = 9;
        public static int EMBARRASSED = 10;
        public static int ANGEL = 11;
        public static int UNDECIDED = 12;
        public static int CRYING = 13;
        public static int LIPS_ARE_SEALED = 14;
        public static int LAUGHING = 15;
        public static int UNDECIDED2 = 16;
        //public static int WTF = 16;

        public static int getSmileyResource(int which) {
            return sIconIds[which];
        }
    }

    // NOTE: if you change anything about this array, you must make the corresponding change
    // to the string arrays: default_smiley_texts and default_smiley_names in res/values/arrays.xml
    public static final int[] DEFAULT_SMILEY_RES_IDS = {
        Smileys.getSmileyResource(Smileys.HAPPY),                //  0
        Smileys.getSmileyResource(Smileys.SAD),                  //  1
        Smileys.getSmileyResource(Smileys.WINKING),              //  2
        Smileys.getSmileyResource(Smileys.TONGUE_STICKING_OUT),  //  3
        Smileys.getSmileyResource(Smileys.SURPRISED),            //  4
        Smileys.getSmileyResource(Smileys.KISSING),              //  5
        Smileys.getSmileyResource(Smileys.YELLING),              //  6
        Smileys.getSmileyResource(Smileys.COOL),                 //  7
        Smileys.getSmileyResource(Smileys.MONEY_MOUTH),          //  8
        Smileys.getSmileyResource(Smileys.FOOT_IN_MOUTH),        //  9
        Smileys.getSmileyResource(Smileys.EMBARRASSED),          //  10
        Smileys.getSmileyResource(Smileys.ANGEL),                //  11
        Smileys.getSmileyResource(Smileys.UNDECIDED),            //  12
        Smileys.getSmileyResource(Smileys.CRYING),               //  13
        Smileys.getSmileyResource(Smileys.LIPS_ARE_SEALED),      //  14
        Smileys.getSmileyResource(Smileys.LAUGHING),             //  15
        Smileys.getSmileyResource(Smileys.UNDECIDED2),           //  16
        //Smileys.getSmileyResource(Smileys.WTF),                   //  16
    };

    public static final int DEFAULT_SMILEY_TEXTS = R.array.epic_default_smiley_texts;
    public static final int ALT_SMILEY_TEXTS = R.array.epic_alt_smiley_texts;
    public static final int DEFAULT_SMILEY_NAMES = R.array.epic_default_smiley_names;

    /**
     * Builds the hashtable we use for mapping the string version
     * of a smiley (e.g. ":-)") to a resource ID for the icon version.
     */
    private HashMap<String, Integer> buildSmileyToRes() {
        if (DEFAULT_SMILEY_RES_IDS.length != mSmileyTexts.length) {
            // Throw an exception if someone updated DEFAULT_SMILEY_RES_IDS
            // and failed to update arrays.xml
            throw new IllegalStateException("Smiley resource ID/text mismatch");
        }

        HashMap<String, Integer> smileyToRes =
                            new HashMap<String, Integer>(mSmileyTexts.length);
        for (int i = 0; i < mSmileyTexts.length; i++) {
            smileyToRes.put(mSmileyTexts[i], DEFAULT_SMILEY_RES_IDS[i]);
        }

        return smileyToRes;
    }

    /**
     * Builds the hashtable we use for mapping the string version
     * of a smiley (e.g. ":-)") to a resource ID for the icon version.
     */
    private HashMap<String, Integer> buildAltSmileyToRes() {
        if (DEFAULT_SMILEY_RES_IDS.length != mAltSmileyTexts.length) {
            // Throw an exception if someone updated ALT_SMILEY_RES_IDS
            // and failed to update arrays.xml
            throw new IllegalStateException("Smiley resource ID/text mismatch");
        }

        HashMap<String, Integer> smileyToRes =
                            new HashMap<String, Integer>(mAltSmileyTexts.length);
        for (int i = 0; i < mAltSmileyTexts.length; i++) {
            smileyToRes.put(mAltSmileyTexts[i], DEFAULT_SMILEY_RES_IDS[i]);
        }

        return smileyToRes;
    }

    /**
     * Builds the regular expression we use to find Rednote URLs in {@link #addRednoteSpans}.
     */
    private Pattern buildURLPattern() {
        String regex = "((https?):((//)|(\\\\))+[\\w\\d:#@%/;$()~_?\\+-=\\\\\\.&]*)";
        Pattern p = Pattern.compile(regex, Pattern.MULTILINE|Pattern.CASE_INSENSITIVE);
        return p;
    }

    /**
     * Builds the regular expression we use to find smileys in {@link #addSmileySpans}.
     */
    private Pattern buildPattern() {
        // Set the StringBuilder capacity with the assumption that the average
        // smiley is 3 characters long.
        StringBuilder patternString = new StringBuilder(mSmileyTexts.length * 3);

        // Build a regex that looks like (:-)|:-(|...), but escaping the smilies
        // properly so they will be interpreted literally by the regex matcher.
        patternString.append('(');
        for (String s : mSmileyTexts) {
            patternString.append(Pattern.quote(s));
            patternString.append('|');
        }
        // Replace the extra '|' with a ')'
        patternString.replace(patternString.length() - 1, patternString.length(), ")");

        return Pattern.compile(patternString.toString());
    }

    /**
     * Builds the regular expression we use to find alt smileys in {@link #addAltSmileySpans}.
     */
    private Pattern buildAltPattern() {
        // Set the StringBuilder capacity with the assumption that the average
        // smiley is 3 characters long.
        StringBuilder patternString = new StringBuilder(mAltSmileyTexts.length * 3);

        // Build a regex that looks like (:-)|:-(|...), but escaping the smilies
        // properly so they will be interpreted literally by the regex matcher.
        patternString.append('(');
        for (String s : mAltSmileyTexts) {
            patternString.append(Pattern.quote(s));
            patternString.append('|');
        }
        // Replace the extra '|' with a ')'
        patternString.replace(patternString.length() - 1, patternString.length(), ")");

        return Pattern.compile(patternString.toString());
    }

    public Matcher smileyMatcher(CharSequence text) {
        return mPattern.matcher(text);
    }

    public Matcher smileyAltMatcher(CharSequence text) {
        return mAltPattern.matcher(text);
    }

    public Matcher urlMatcher(CharSequence text) {
        return mURLPattern.matcher(text);
    }

    /**
     * Adds ImageSpans to a CharSequence that replace textual emoticons such
     * as :-) with a graphical version.
     *
     * @param text A CharSequence possibly containing emoticons
     * @return A CharSequence annotated with ImageSpans covering any
     *         recognized emoticons.
     */
    public CharSequence addDefaultSmileySpans(CharSequence text) {
        SpannableStringBuilder builder = new SpannableStringBuilder(text);

        Matcher matcher = mPattern.matcher(text);
        while (matcher.find()) {
            int resId = mSmileyToRes.get(matcher.group());
            builder.setSpan(new ImageSpan(mContext, resId),
                matcher.start(), matcher.end(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        }

        return builder;
    }
    
    public CharSequence addAltSmileySpans(CharSequence text) {
        SpannableStringBuilder builder = new SpannableStringBuilder(text);

        int lastEnd = 0;
        Matcher urlMatcher = mURLPattern.matcher(text);
        while (urlMatcher.find()) {
        	// check URL for Rednote
        	builder.setSpan(mRednoteParser.getSpannable(urlMatcher.group()), urlMatcher.start(), urlMatcher.end(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        	if (EpicFeedConsts.DEBUG_SMILEY_PARSER) Log.i(TAG, "URL Span start: " + urlMatcher.start() + " - end: " + urlMatcher.end());
	        Matcher matcher = mAltPattern.matcher(text.subSequence(lastEnd, urlMatcher.start()));
	        while (matcher.find()) {
	            int resId = mAltSmileyToRes.get(matcher.group());
	            builder.setSpan(new ImageSpan(mContext, resId),
                    matcher.start(), matcher.end(),
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
	        }
	        lastEnd = urlMatcher.end();
	        if (EpicFeedConsts.DEBUG_SMILEY_PARSER) Log.i(TAG, "Checking URL for Rednote:" + urlMatcher.group());
        }
        if (lastEnd < text.length() - 1) {
	        Matcher matcher = mAltPattern.matcher(text.subSequence(lastEnd, text.length()-1));
	        while (matcher.find()) {
	            int resId = mAltSmileyToRes.get(matcher.group());
	            builder.setSpan(new ImageSpan(mContext, resId),
                    matcher.start(), matcher.end(),
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
	        }
        }

        return builder;
    }
    
    public CharSequence addSmileySpans(CharSequence text) {
    	//return (addAltSmileySpans(addDefaultSmileySpans(TextHelper.getClickableRednoteSpan(mContext, text.toString()))));
    	return (addAltSmileySpans(addDefaultSmileySpans(text)));
    }
}
