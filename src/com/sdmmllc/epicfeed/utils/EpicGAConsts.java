package com.sdmmllc.epicfeed.utils;

public class EpicGAConsts {

	// message
	public static String 
		DISPLAY_MESSAGE = "auto_display_msg",
		USER_ACTION_CATEGORY = "user_actions";
	
	public static String 
		DISPLAY_WARNING_MSG = "auto_display_warning_msg",
		USER_MESSAGING = "user_messaging";
	
	public static String 
		DISPLAY_DEFAULT_SMS_BANNER = "auto_display_default_sms_warn",
		USER_VIEW_AUTH_SCREEN = "act_view_authenticate",
		USER_VIEW_CONTACT_LIST = "act_view_contact_list",
		USER_VIEW_MESSAGE_LIST = "act_view_message_list",
		USER_VIEW_SOCIAL_LIST = "act_view_social_list",
		USER_SEND_MSG = "send_message",
		USER_RECV_MSG = "recv_message";

}
