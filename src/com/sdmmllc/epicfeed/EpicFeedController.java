package com.sdmmllc.epicfeed;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.telephony.TelephonyManager;

import com.facebook.model.GraphUser;
import com.google.analytics.tracking.android.GAServiceManager;
import com.google.analytics.tracking.android.GoogleAnalytics;
import com.google.analytics.tracking.android.Logger.LogLevel;
import com.google.analytics.tracking.android.Tracker;
import com.sdmm.epicfeed.R;
import com.sdmmllc.epicfeed.comm.CommManager;
import com.sdmmllc.epicfeed.data.Contact;
import com.sdmmllc.epicfeed.data.EpicDB;
import com.sdmmllc.epicfeed.rednote.RednoteParser;
import com.sdmmllc.epicfeed.utils.EpicSmileyParser;
import com.sdmmllc.superdupersmsmanager.sdk.SDSmsManager;

public class EpicFeedController {

	public static final String TAG = "Application";
	
	ScanPkgList currentScan;
	private static EpicDB appDB;
	private static Context mContext;
	public static SpaTextSMSHandler smsHandler;
	private static SpaUserAccount sSpaUserAccount;
	private static EpicSmileyParser sSmileyParser;
	private static RednoteParser mRednoteParser;
	private static CommManager mCommManager;
	private static EpicFeedController mInstance;
	private static boolean licenseVerified;
    private static SDSmsManager mSDSmsManager;
    private static GraphUser graphUser;
    private static List<GraphUser> tags;
    private static String twitterUserName,
    					  twitterUserScreenName;
    private String mCountryIso;
    private TelephonyManager mTelephonyManager;
    //private Twitter twitter;

	private EpicFeedController(Context context) {
		init(context);
		mInstance = this;
	}

	public static EpicFeedController getInstance(Context context) {
		if (mInstance == null) mInstance = new EpicFeedController(context);
		return mInstance;
	}
	
	public void init(Context context) {
		mContext = context;
		String token="voqrgs78wCgkp41yPZQ7";
		smsHandler = new SpaTextSMSHandler(mContext);
		sSpaUserAccount = new SpaUserAccount(mContext);
		appDB = new EpicDB(mContext);
		EpicSmileyParser.init(mContext);
		sSmileyParser = EpicSmileyParser.getInstance();
		mRednoteParser = new RednoteParser(mContext);
		mCommManager = new CommManager(mContext);
		licenseVerified = mContext.getResources().getBoolean(R.bool.licenseVerified);
		//Rednote.init(this, getString(R.string.rednoteAPI));
        mCountryIso = getTelephonyManager().getSimCountryIso();
        Contact.init(mContext);
        initializeGa(mContext.getApplicationContext());
	}
	
	/*
	 * Google Analytics
	 */
	private static GoogleAnalytics mGa;
	private static Tracker mTracker;
	// Dispatch period in seconds.
	private static final int GA_DISPATCH_PERIOD = 30;
	// Prevent hits from being sent to reports, i.e. during testing.
	private static final boolean GA_IS_DRY_RUN = false;
	// GA Logger verbosity.
	private static final LogLevel GA_LOG_VERBOSITY = LogLevel.INFO;
	// Key used to store a user's tracking preferences in SharedPreferences.
	private static final String TRACKING_PREF_KEY = "trackingPreference";
	private static void initializeGa(Context context) {
		mGa = GoogleAnalytics.getInstance(context);
		mTracker = mGa.getTracker(context.getString(R.string.ga_trackingId));
		// Set dispatch period.
		GAServiceManager.getInstance().setLocalDispatchPeriod(GA_DISPATCH_PERIOD);
		// Set dryRun flag.
		mGa.setDryRun(GA_IS_DRY_RUN);
		// Set Logger verbosity.
		mGa.getLogger().setLogLevel(GA_LOG_VERBOSITY);
	}

	public static Tracker getGaTracker(Context context) {
		if (mTracker == null) initializeGa(context);
		return mTracker;
	}

	public static GoogleAnalytics getGaInstance(Context context) {
		if (mGa == null) initializeGa(context);
		return mGa;
	}
	
	//private void initTwitter() {
	//	ConfigurationBuilder cb = new ConfigurationBuilder();
	//	cb.setDebugEnabled(true)
	//	  .setOAuthConsumerKey("mQnPxKZndwEZiLptuUH2EBEHC")
	//	  .setOAuthConsumerSecret("GaoViTudusswwRKUq5gplKrPUl9239bkduccbpKZMsYTmpoqwq")
	//	  .setOAuthAccessToken("2451785412-UqKAAOYz8HpdfgdabnKxRf4wsRA91d4aBemSHB6")
	//	  .setOAuthAccessTokenSecret("KaQVdMe78cChuEFGdssqfHupdkZq0yv09whsrZBQ9ETlU");
	//	TwitterFactory tf = new TwitterFactory(cb.build());
	//	twitter = tf.getInstance();
	//}
	
	public static Context getContext() {
		return mContext;
	}
	
	public static boolean getBoolean(int bool) {
		return mContext.getApplicationContext().getResources().getBoolean(bool);
	}
	
	public static SDSmsManager getSDSmsManager() {
		return mSDSmsManager;
	}
	
	public static void setSDSmsManager(SDSmsManager smsManager) {
		mSDSmsManager = smsManager;
	}
	
	public CommManager getCommManager() {
		return mCommManager;
	}
	
	public static boolean isLicenseVerified() {
		return licenseVerified;
	}
	
	public static void setLicenseVerified(boolean verified) {
		licenseVerified = verified;
	}
	
	public static GraphUser getGraphUser() {
		return graphUser;
	}

	public static void setGraphUser(GraphUser user) {
		graphUser = user;
	}
	
	public static String getTwitterUserName() {
		return twitterUserName;
	}
	
	public static void setTwitterUserName(String userName) {
		twitterUserName = userName;
	}
	
	public static String getTwitterUserScreenName() {
		return twitterUserScreenName;
	}
	
	public static void setTwitterUserScreenName(String userScreenName) {
		twitterUserScreenName = userScreenName;
	}

	public ScanPkgList getCurrentScan() {
		if (currentScan == null) currentScan = new ScanPkgList(new ArrayList<ScanPkgItem>());
		return currentScan;
	}
	
	public void setCurrentScan(ScanPkgList list) {
		currentScan = list;
	}
	
	public static EpicSmileyParser getSmileyParser() {
		return sSmileyParser;
	}
	
	public static RednoteParser getRednoteParser() {
		return mRednoteParser;
	}
	
	public static EpicDB getEpicDB(Context context) {
		if (appDB == null) appDB = new EpicDB(context);
		return appDB;
	}

	public static SpaUserAccount getSpaUserAccount(Context context) {
		if (sSpaUserAccount == null) sSpaUserAccount = new SpaUserAccount(context);
		return sSpaUserAccount;
	}
	
    // This function CAN return null.
    public String getCurrentCountryIso() {
        if (mCountryIso == null) {
        	mCountryIso = getTelephonyManager().getSimCountryIso();
        }
        return mCountryIso;
    }

    /**
     * @return Returns the TelephonyManager.
     */
    public TelephonyManager getTelephonyManager() {
        if (mTelephonyManager == null) {
            mTelephonyManager = (TelephonyManager)mContext
                    .getSystemService(Context.TELEPHONY_SERVICE);
        }
        return mTelephonyManager;
    }
    
    synchronized public static EpicFeedController getApplication() {
        return mInstance;
    }



/**	public static void memoryInfo(Context ctx, String TAG) {
		Log.i(TAG, "SpaTextApp: memoryInFo");
        ActivityManager aManager = (ActivityManager)ctx.getSystemService(ACTIVITY_SERVICE);
        MemoryInfo memInfo = new ActivityManager.MemoryInfo();
        aManager.getMemoryInfo(memInfo);

        List<RunningAppProcessInfo> runningProcs = aManager.getRunningAppProcesses();
        Map<Integer, String> pidMap = new TreeMap<Integer, String>();
        for (RunningAppProcessInfo runningAppProcInfo : runningProcs) {
        	pidMap.put(runningAppProcInfo.pid, runningAppProcInfo.processName);
        }
        
        Collection<Integer> keys = pidMap.keySet();
        
        for (int key : keys) {
        	if (pidMap.get(key).equals(ctx.getPackageName())) {
	        	int pids[] = {key};
	        	android.os.Debug.MemoryInfo[] memInfoArray = aManager.getProcessMemoryInfo(pids);
	        	for (android.os.Debug.MemoryInfo pidMemInfo : memInfoArray) {
	        		Log.i(TAG, String.format("** MEMINFO in pid %d [%s] **\n", pids[0], pidMap.get(pids[0])));
	        		Log.i(TAG, " pidMemInfo.getTotalPrivateDirty(): " + pidMemInfo.getTotalPrivateDirty());
	        		Log.i(TAG, " pidMemInfo.getTotalPss(): " + pidMemInfo.getTotalPss());
	        		Log.i(TAG, " pidpidMemInfo.getTotalSharedDirty(): " + pidMemInfo.getTotalSharedDirty());
	        	}
        	}
        }
	} **/
}
