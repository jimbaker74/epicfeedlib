package com.sdmmllc.epicfeed.rednote;

public class RednoteEmo {

	public static String 
		rootNode = "Response", 
		moodNode = "Mood", 
		songIdNode = "SongId", 
		moodIdNode = "MoodId", 
		moodListIdNode = "ListMoodId", 
		moodTxtNode = "MoodText";

	public String songId;

	public String moodId;

	public String moodTxt;

	public String moodCd;

	public String // use default Rednote emoticon if nothing else
	moodListId = "rn";
	
	public boolean loaded = false;
	
}
