package com.sdmmllc.epicfeed.rednote;

import java.util.HashMap;
import java.util.StringTokenizer;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ImageSpan;
import android.util.Log;

import com.sdmm.epicfeed.R;
import com.sdmmllc.epicfeed.utils.EpicFeedConsts;

public class RednoteParser {

	public static String TAG = "RednoteParser";
	private String mText;
	private Context ctx;
    private final HashMap<String, Integer> mRednoteToRes;
	
    static class Rednotes {
        private static final int[] rIconIds = {
            R.drawable.rn_emo_default,
            R.drawable.rn_emo_love,
            R.drawable.rn_emo_happy,
            R.drawable.rn_emo_sad,
            R.drawable.rn_emo_lucky,
            R.drawable.rn_emo_party,
            R.drawable.rn_emo_top_10,
            R.drawable.rn_emo_crusin,
            R.drawable.rn_emo_money,
            R.drawable.rn_emo_new,
            R.drawable.rn_emo_country,
            R.drawable.rn_emo_lol,
            R.drawable.rn_emo_weekend,
            R.drawable.rn_emo_default,
            R.drawable.rn_emo_default,
        };

        public static int DEFAULT = 0;
        public static int ROMANTIC = 1;
        public static int HAPPY = 2;
        public static int BLUE = 3;
        public static int LUCKY = 4;
        public static int PARTY = 5;
        public static int TOP_TEN = 6;
        public static int CRUISIN = 7;
        public static int MONEY = 8;
        public static int NEW_ARTIST = 9;
        public static int COUNTRY_MUSIC = 10;
        public static int LOL = 11;
        public static int WEEKEND = 12;
        public static int HALLOWEEN = 13;
        public static int CHRISTMAS = 14;

        public static int getRednoteResource(int which) {
            return rIconIds[which];
        }
    }

    // NOTE: if you change anything about this array, you must make the corresponding change
    // to the string arrays: default_rednotes and default_rednote_names in res/values/rednote_strings.xml
    public static final int[] DEFAULT_RN_RES_IDS = {
        Rednotes.getRednoteResource(Rednotes.DEFAULT),           //  0
        Rednotes.getRednoteResource(Rednotes.ROMANTIC),          //  1
        Rednotes.getRednoteResource(Rednotes.HAPPY),             //  2
        Rednotes.getRednoteResource(Rednotes.BLUE),              //  3
        Rednotes.getRednoteResource(Rednotes.LUCKY),             //  4
        Rednotes.getRednoteResource(Rednotes.PARTY),             //  5
        Rednotes.getRednoteResource(Rednotes.TOP_TEN),           //  6
        Rednotes.getRednoteResource(Rednotes.CRUISIN),           //  7
        Rednotes.getRednoteResource(Rednotes.MONEY),             //  8
        Rednotes.getRednoteResource(Rednotes.NEW_ARTIST),        //  9
        Rednotes.getRednoteResource(Rednotes.COUNTRY_MUSIC),     //  10
        Rednotes.getRednoteResource(Rednotes.LOL),               //  11
        Rednotes.getRednoteResource(Rednotes.WEEKEND),           //  12
        Rednotes.getRednoteResource(Rednotes.HALLOWEEN),         //  13
        Rednotes.getRednoteResource(Rednotes.CHRISTMAS),         //  14
    };

    String[] rednoteEmoArray;
    
    public RednoteParser(Context context) {
		ctx = context;
		rednoteEmoArray = ctx.getResources().getStringArray(R.array.default_rednotes);
		mRednoteToRes = buildRednoteToRes();
	}
	
	public void setText(String text) {
		mText = text;
	}
	
	public boolean hasRednote(String text) {
	    StringTokenizer st = new StringTokenizer(text);
	    int len = st.countTokens();
	    for (int i = 0; i < len; i++) {
	        String token = st.nextToken();
	        if (EpicFeedConsts.DEBUG_SMILEY_PARSER) Log.i(TAG, ".hasRednote: checking token: " + token);
	        if (token.startsWith(ctx.getString(R.string.rednoteShareHTTP))||
	        		(token.contains(EpicFeedConsts.BITLY_URL)&&token.contains(EpicFeedConsts.RDNT_STR))) {
	        	return true;
	        }
	    }
	    return false;
	}
	
    /**
     * Builds the hashtable we use for mapping the string version
     * of a smiley (e.g. ":-)") to a resource ID for the icon version.
     */
    private HashMap<String, Integer> buildRednoteToRes() {
        if (DEFAULT_RN_RES_IDS.length != rednoteEmoArray.length) {
            // Throw an exception if someone updated DEFAULT_RN_RES_IDS
            // and failed to update arrays.xml
            throw new IllegalStateException("Smiley resource ID/text mismatch");
        }

        HashMap<String, Integer> smileyToRes =
                            new HashMap<String, Integer>(rednoteEmoArray.length);
        for (int i = 0; i < rednoteEmoArray.length; i++) {
            smileyToRes.put(rednoteEmoArray[i], DEFAULT_RN_RES_IDS[i]);
        }

        return smileyToRes;
    }

	/**
	 * Retrieves the parsed text as a spannable string object.
	 * @param context the context for fetching Rednote resources.
	 * @return the spannable string as CharSequence.
	 */
	public CharSequence getSpannableString() {
		return getSpannableString(mText);
	}
	
	public CharSequence getSpannableString(String text) {
	    SpannableStringBuilder builder = new SpannableStringBuilder();
	    if (EpicFeedConsts.DEBUG_SMILEY_PARSER) Log.i(TAG, "checking string: " + text);
	    StringTokenizer st = new StringTokenizer(text);
	    int len = st.countTokens();
	    for (int i = 0; i < len; i++) {
	        String token = st.nextToken();
	        if (EpicFeedConsts.DEBUG_SMILEY_PARSER) Log.i(TAG, "checking token: " + token);
	        int start = builder.length();
	        builder.append(token);
	        if (token.startsWith(ctx.getString(R.string.rednoteShareHTTP))||
	        		(token.contains(EpicFeedConsts.BITLY_URL)&&token.contains(EpicFeedConsts.RDNT_STR))) {
	        	if (EpicFeedConsts.DEBUG_SMILEY_PARSER) Log.i(TAG, "found Rednote!");
	        	String moodCd; 
	        	if ((token.split("\\?") != null)&&(token.split("\\?").length > 1)
	        			&&(token.split("\\?")[1] != null)
	        			&&(token.split("\\?")[1].split("=") != null)&&(token.split("\\?")[1].split("=").length > 1)
	        			&&(token.split("\\?")[1].split("=")[1] != null)) {
	        		moodCd = token.split("\\?")[1].split("=")[1];
		        	if (moodCd.length() < 2) moodCd = "rn";
	        	} else {
	        		moodCd = "rn";
	        	}
	            int resid = mRednoteToRes.get(moodCd);
	            if (resid != -1) {
	                builder.setSpan(new ImageSpan(ctx, resid),
	                		start,
	                		builder.length(),
	                		Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
	            }
	        }
		    builder.append(" ");
	    }
	    return builder;
	}
	
	public ImageSpan getSpannable(String text) {
	    StringTokenizer st = new StringTokenizer(text);
	    int len = st.countTokens();
	    for (int i = 0; i < len; i++) {
	        String token = st.nextToken();
	        if (EpicFeedConsts.DEBUG_SMILEY_PARSER) Log.i(TAG, "checking token: " + token);
	        if (token.startsWith(ctx.getString(R.string.rednoteShareHTTP))||
	        		(token.contains(EpicFeedConsts.BITLY_URL)&&token.contains(EpicFeedConsts.RDNT_STR))) {
	        	if (EpicFeedConsts.DEBUG_SMILEY_PARSER) Log.i(TAG, "found Rednote!");
	        	String moodCd; 
	        	if ((token.split("\\?") != null)&&(token.split("\\?").length > 1)
	        			&&(token.split("\\?")[1] != null)
	        			&&(token.split("\\?")[1].split("=") != null)&&(token.split("\\?")[1].split("=").length > 1)
	        			&&(token.split("\\?")[1].split("=")[1] != null)) {
	        		moodCd = token.split("\\?")[1].split("=")[1];
		        	if (moodCd.length() < 2) moodCd = "rn";
	        	} else {
	        		moodCd = "rn";
	        	}
	            int resid = mRednoteToRes.get(moodCd);
	            if (resid != -1) {
	                return new ImageSpan(ctx, resid);
	            }
	        }
	    }
	    return null;
	}
	
	private class ImageGetter implements Html.ImageGetter {
	 
		public Drawable getDrawable(String source) {
			int id;
			if (source.equals("rn_btn_happy.jpg")) {
				id = R.drawable.rn_btn_happy;
			} else {
				return null;
			}
			 
			Drawable d = ctx.getResources().getDrawable(id);
			d.setBounds(0,0,d.getIntrinsicWidth(),d.getIntrinsicHeight());
			return d;
		}
	};
}
