package com.sdmmllc.epicfeed.rednote;

import android.text.Spannable;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.method.MovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class RednoteClickSpan extends ClickableSpan {

    private OnClickListener mListener;

    public RednoteClickSpan(OnClickListener listener) {
        mListener = listener;
    }

    @Override
    public void onClick(View widget) {
       if (mListener != null) mListener.onClick();
    }

    public interface OnClickListener {
        void onClick();
    }

    public static void clickify(TextView view, final String clickableText, 
	    final RednoteClickSpan.OnClickListener listener) {

	    CharSequence text = view.getText();
	    String string = text.toString();
	    RednoteClickSpan span = new RednoteClickSpan(listener);

	    int start = string.indexOf(clickableText);
	    int end = start + clickableText.length();
	    if (start == -1) return;

	    if (text instanceof Spannable) {
	        ((Spannable)text).setSpan(span, start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
	    } else {
	        SpannableString s = SpannableString.valueOf(text);
	        s.setSpan(span, start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
	        view.setText(s);
	    }

	    MovementMethod m = view.getMovementMethod();
	    if ((m == null) || !(m instanceof LinkMovementMethod)) {
	        view.setMovementMethod(LinkMovementMethod.getInstance());
	    }
	}

    public static void clickify(EditText view, final String clickableText, 
	    final RednoteClickSpan.OnClickListener listener) {

	    CharSequence text = view.getText();
	    String string = text.toString();
	    RednoteClickSpan span = new RednoteClickSpan(listener);

	    int start = string.indexOf(clickableText);
	    int end = start + clickableText.length();
	    if (start == -1) return;

	    if (text instanceof Spannable) {
	        ((Spannable)text).setSpan(span, start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
	    } else {
	        SpannableString s = SpannableString.valueOf(text);
	        s.setSpan(span, start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
	        view.setText(s);
	    }

	    MovementMethod m = view.getMovementMethod();
	    if ((m == null) || !(m instanceof LinkMovementMethod)) {
	        view.setMovementMethod(LinkMovementMethod.getInstance());
	    }
	}
}


