package com.sdmmllc.epicfeed.data;

public interface TwitterNameListener {

	public void onComplete();
	public void onError();
}
