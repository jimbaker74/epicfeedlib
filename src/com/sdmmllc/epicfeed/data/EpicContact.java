package com.sdmmllc.epicfeed.data;

import java.util.ArrayList;
import java.util.List;

import android.graphics.drawable.Drawable;
import android.telephony.PhoneNumberUtils;
import android.util.Log;

import com.sdmmllc.epicfeed.EpicFeedController;
import com.sdmmllc.epicfeed.SpaTextNotification;
import com.sdmmllc.epicfeed.utils.EpicFeedConsts;

public class EpicContact {
	
	String TAG = "SpaTextContact";
	public String phoneID;
	public String name = "", firstName = "", lastName = "";
	public String nameId = "", fbID = "", email = "";
	public String type = "";
	public int secCd = 0;
	public boolean spaId = false;
	public boolean exists = true;
	public int newMsgCount = 0;
	public int sendErrCount = 0;
	public int deliveryErrCount = 0;
	public int missedCallsCount = 0;
	public int msgCount = 0;
	public Long threadDt = 0L;
	public SpaTextNotification notifyOptions;
	public List<ContactFeed> contactFeeds = new ArrayList<ContactFeed>();
	public Drawable mAvatar = null;
	
	public static final int 
		SPA_ID_FLG 					= 1 << 0, 
		BLOCK_MSGS_FLG 				= 1 << 1, 
		SAVE_BLOCKED_MSGS_FLG 		= 1 << 2, 
		FORBIDDEN_FLG		 		= 1 << 3, 
		FAKE_FLG 					= 1 << 4,
		BLOCK_CALLS 				= 1 << 5, 
		KEEP_BLOCKED_CALL_LOGS 		= 1 << 6, 
		HIDE_CALLS 					= 1 << 7,
		NO_STD_CALL_LOGS 			= 1 << 8, 
		KEEP_HIDDEN_CALL_LOGS 		= 1 << 9, 
		CALL_BLOCK_NO_VOICEMAIL 	= 1 << 10;
	
	public EpicContact(String id) {
		phoneID = id;
		notifyOptions = new SpaTextNotification(id);
	}

	public EpicContact(String id, SpaTextNotification tmpNotify) {
		phoneID = id;
		notifyOptions = tmpNotify;
	}
	
	public void setContactFeeds(List<ContactFeed> newFeeds) {
		contactFeeds = newFeeds;
	}
	
	public ContactFeed getFeed(String feed_type) {
		for (ContactFeed feed : contactFeeds) {
			if (feed.getFeedType().equals(feed_type)) {
				Log.i(TAG, "+++++ IN IF block for getFeed() method EpicContact class");
				return feed;
			}
			
		}
		return null;
	}

	public void addFeed(ContactFeed newFeed) {
		if (newFeed == null) return;
		for (ContactFeed feed : contactFeeds) {
			if (feed.getFeedType().equals(newFeed.getFeedType())) {
				contactFeeds.set(contactFeeds.indexOf(feed), newFeed);
				return;
			}
		}
		contactFeeds.add(newFeed);
		
		//EpicDB db = EpicFeedController.getEpicDB(EpicFeedController.getContext());
		//EpicDBConn conn = db.open(true);
		//db.insertNewContactFeed(newFeed);
		//db.close(conn);
	}
	
	public void removeFeed(ContactFeed oldFeed) {
		if (oldFeed == null) return;
		contactFeeds.remove(oldFeed);
		
		EpicDB db = EpicFeedController.getEpicDB(EpicFeedController.getContext());
		EpicDBConn conn = db.open(true);
		db.deleteContactFeed(oldFeed);
		db.close(conn);
	}
	
	public List<ContactFeed> getFeeds() {
		return contactFeeds;
	}
	
	public boolean newMsgs() {
		return (newMsgCount > 0|
		missedCallsCount > 0|
		sendErrCount > 0|
		deliveryErrCount > 0);
	}

	public void setSpaId(boolean isSpaId) {
		spaId = isSpaId;
		if (isSpaId) secCd |= SPA_ID_FLG;
		else secCd &= ~SPA_ID_FLG;
	}
	
	public boolean isSpaId() {
		return spaId;
	}
	
	public void setBlocked(boolean block) {
		if (block) secCd |= BLOCK_MSGS_FLG;
		else {
			// if block is false, turn off save blocked messages
			secCd &= ~BLOCK_MSGS_FLG;
			secCd &= ~SAVE_BLOCKED_MSGS_FLG;
		}
	}
	
	public boolean isBlocked() {
		return ((secCd & BLOCK_MSGS_FLG) == BLOCK_MSGS_FLG);
	}
	
	public void setCallBlock(boolean call_block) {
		if (call_block) secCd |= BLOCK_CALLS;
		else secCd &= ~BLOCK_CALLS;
	}
	
	public boolean isCallBlocked() {
		return ((secCd & BLOCK_CALLS) == BLOCK_CALLS);
	}
	
	public void setKeepBlockedCallLogs(boolean keep_logs) {
		if (keep_logs) secCd |= KEEP_BLOCKED_CALL_LOGS;
		else secCd &= ~KEEP_BLOCKED_CALL_LOGS;
	}
	
	public boolean isKeepBlockedCallLogs() {
		return ((secCd & KEEP_BLOCKED_CALL_LOGS) == KEEP_BLOCKED_CALL_LOGS);
	}
	
	public void setCallBlockedNoVoicemail(boolean call_block_no_vm) {
		if (call_block_no_vm) secCd |= (CALL_BLOCK_NO_VOICEMAIL|BLOCK_CALLS);
		else secCd &= ~CALL_BLOCK_NO_VOICEMAIL;
	}
	
	public boolean isCallBlockedNoVoicemail() {
		return ((secCd & CALL_BLOCK_NO_VOICEMAIL) == CALL_BLOCK_NO_VOICEMAIL);
	}

	public void setKeepCallLogs(boolean keep_logs) {
		if (keep_logs) secCd |= KEEP_HIDDEN_CALL_LOGS;
		else secCd &= ~KEEP_HIDDEN_CALL_LOGS;
	}
	
	public boolean isKeepCallLogs() {
		return ((secCd & KEEP_HIDDEN_CALL_LOGS) == KEEP_HIDDEN_CALL_LOGS);
	}
	
	public void setCallHide(boolean call_hide) {
		if (call_hide) secCd |= HIDE_CALLS;
		else secCd &= ~HIDE_CALLS;
	}
	
	public boolean isCallHidden() {
		return ((secCd & HIDE_CALLS) == HIDE_CALLS);
	}
	
	public void setCallLogDelete(boolean call_log_delete) {
		if (call_log_delete) secCd |= NO_STD_CALL_LOGS;
		else secCd &= ~NO_STD_CALL_LOGS;
	}
	
	public boolean isCallLogDelete() {
		return ((secCd & NO_STD_CALL_LOGS) == NO_STD_CALL_LOGS);
	}
	
	public void setForbidden(boolean forbidden) {
		if (forbidden) secCd |= FORBIDDEN_FLG;
		else secCd &= ~FORBIDDEN_FLG;
	}
	
	public boolean isForbidden() {
		return ((secCd & FORBIDDEN_FLG) == FORBIDDEN_FLG);
	}
	
	public void setFake(boolean fake) {
		if (fake) secCd |= FAKE_FLG;
		else secCd &= ~FAKE_FLG;
	}
	
	public boolean isFake() {
		return ((secCd & FAKE_FLG) == FAKE_FLG);
	}
	
	public void setSaveBlockedMsgs(boolean saveMsgs) {
		if (saveMsgs) secCd |= SAVE_BLOCKED_MSGS_FLG;
		else secCd &= ~SAVE_BLOCKED_MSGS_FLG;
		if (EpicFeedConsts.debugLevel > 7) Log.i(TAG, "setSaveBlockedMsgs: " + secCd);
	}
	
	public boolean saveBlockedMsgs() {
		return ((secCd & SAVE_BLOCKED_MSGS_FLG) == SAVE_BLOCKED_MSGS_FLG);
	}
	
	public String getNameOrPhoneId() {
		if (name.length() < 1) return PhoneNumberUtils.formatNumber(phoneID);
		else return name;
		
	}
	public String makeBold(String str) {
		return "<b>" + str + "</b>";
	}
	
	public String makeItalic(String str) {
		return "<i>" + str + "</i>"; 
	}
	
	public String makeSmall(String str) {
		return "<small>" + str + "</small>";
	}

	public String makeBig(String str) {
		return "<big>" + str + "</big>";
	}
	
	@Override
	public String toString() {
		return EpicContact.class.getName() + " phoneID: " + this.phoneID +
				" firstName: " + this.firstName +
				" lastName: " + this.lastName +
				" displayName: " + this.getNameOrPhoneId() +
				" email: " + this.email;
				
	}
	
	public EpicContact clone() {
		EpicContact retval = new EpicContact("");
		
		retval.phoneID = this.phoneID;
		retval.nameId = this.nameId;
		retval.firstName = this.firstName;
		retval.lastName = this.lastName;
		
		if (retval.nameId.equals("")) { retval.name = retval.firstName + " " + retval.lastName;
		} else { retval.name = this.name; }
		
		retval.fbID = this.fbID;
		retval.email = this.email;
		retval.type = this.type;
		
		for (ContactFeed newFeed : this.contactFeeds) {
			retval.addFeed(newFeed.clone());
		}
		
		return retval;
	}
}
