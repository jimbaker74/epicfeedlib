package com.sdmmllc.epicfeed.data;


import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.sdmm.epicfeed.R;
import com.sdmmllc.epicfeed.utils.EpicFeedConsts;


public class EpicDBHelper extends SQLiteOpenHelper{
 
	public static final String TAG = "EpicDBHelper";

	public static String TAG2 = "";

	private static String DB_PATH = "";
    private static String DB_NAME = "EpicDB.db";
	private final Context spaContext;
    private SharedPreferences settings;
    private SharedPreferences.Editor editor;
    public static String TABLES_FOUND = "TabFound", COLUMNS_FOUND = "ColFound",
    	DB_CREATED = "DBCreated", DB_VERSION = "dbversion", DB_DATA_VER = "DataVersion";
    
	public static final int DATABASE_VERSION = 0, 
		DATA_VERSION1 = 0;
    int isFoundCount = 0;
	
	public EpicDBHelper(Context context) {
    	super(context, DB_NAME, null, 1);
        DB_PATH = context.getResources().getString(R.string.dbPathName);
        this.spaContext = context;
        settings = spaContext.getSharedPreferences(EpicFeedConsts.DB_FILE, 0);
        editor = settings.edit();
        if (EpicFeedConsts.DEBUG_DB_HELPER) Log.i(TAG, TAG2 + "constructor complete");
    }	
 
	public EpicDBHelper(Context context, boolean updateCheck) {
    	super(context, DB_NAME, null, 1);
        DB_PATH = context.getResources().getString(R.string.dbPathName);
        this.spaContext = context;
        settings = spaContext.getSharedPreferences(EpicFeedConsts.DB_FILE, 0);
        editor = settings.edit();
        if (EpicFeedConsts.DEBUG_DB_HELPER) Log.i(TAG, TAG2 + "constructor complete");
    }	
 
	@Override
	public void onCreate(SQLiteDatabase db) {
		createDB(db);
		//this.openDataBase();
	}
	
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

	}
	
    public void createDB(SQLiteDatabase appDB) throws SQLException{
    	boolean dbExist = (settings.contains(DB_CREATED))&&isFound();
        if (EpicFeedConsts.DEBUG_DB_HELPER) Log.i(TAG, TAG2 + "createDB: Checking database for existence...");
    	if(!dbExist){
            if (EpicFeedConsts.DEBUG_DB_HELPER) Log.i(TAG, TAG2 + "createDB: Database does not exist. Attempting creation...");
        	createDBTables(appDB);
    	}
    	// check db version 
		int dbVersion = settings.getInt(DB_VERSION, 0);
		if (EpicFeedConsts.DEBUG_DB_HELPER) Log.i(TAG, TAG2 + "openDataBase: Database opened - checking for log table.");
        if (!(dbVersion == DATABASE_VERSION)) {
        	//onUpgrade(appDB, settings.getInt(DB_VERSION, 0), DATABASE_VERSION);
            editor.putInt(DB_VERSION, DATABASE_VERSION);
            editor.commit();
        }
		//do nothing - database already exist
        if (EpicFeedConsts.DEBUG_DB_HELPER) Log.i(TAG, TAG2 + "createDB: Database exists.");
		if (appDB == null) appDB = SQLiteDatabase.openDatabase(DB_PATH+DB_NAME, null, SQLiteDatabase.OPEN_READONLY);
    }
    
    private void dbVersion4(SQLiteDatabase appDB) {
    	// do nothing yet
    }
    
	public String strNotNull(String str) {
		if (str == null) return "";
		else return str;
	}

	public boolean strToBoolean(String str) {
		return str.equals("Y");
	}
	
	public String booleanToStr(boolean val) {
		if (val) {
			return "Y";
		} else {
			return "N";
		}
	}
	
	public void setTAG2(String tag) {
		TAG2 = "(" + tag + ")";
	}
 
    public void alterPhoneTable(SQLiteDatabase appDB) throws SQLException {
    	// nothing yet
    }
    
    public void alterTxtMsgTable(SQLiteDatabase appDB) {
    	// do nothing yet
    }
    
    public void createDBTables(SQLiteDatabase appDB) throws SQLException{
 
        String DATABASE_CREATE_METADATA =
            "CREATE TABLE IF NOT EXISTS android_metadata (locale TEXT DEFAULT 'en_US'); COMMIT;";
        String INSERT_METADATA_DATA = 
        	"INSERT INTO android_metadata (locale) VALUES ('en_US'); COMMIT;";
        String DATABASE_CREATE_ERRORLOG =
        	"CREATE TABLE IF NOT EXISTS " + spaContext.getString(R.string.dbLogTableName) + " ("
        	+ spaContext.getString(R.string.dbLogIDCol) + " integer primary key autoincrement, "
            + spaContext.getString(R.string.dbLogTimestampCol) + " DATE NOT NULL, " 
            + spaContext.getString(R.string.dbLogDescrCol) + " TEXT NOT NULL); COMMIT;";
        String DATABASE_CREATE_TEXTMSGLOG =
        	"CREATE TABLE IF NOT EXISTS " + spaContext.getString(R.string.dbTxtTableName) + " ("
        	+ spaContext.getString(R.string.dbTxtIDCol) + " integer primary key autoincrement, "
            + spaContext.getString(R.string.dbTxtDraftFlgCol) + " TEXT NOT NULL, " 
            + spaContext.getString(R.string.dbTxtSenderFlgCol) + " TEXT NOT NULL, " 
            + spaContext.getString(R.string.dbTxtPhoneIDCol) + " TEXT NOT NULL, " 
            + spaContext.getString(R.string.dbTxtTimestampCol) + " LONG NOT NULL, " 
            + spaContext.getString(R.string.dbTxtFlgsCol) + " INT NOT NULL DEFAULT 0, " 
            + spaContext.getString(R.string.dbTxtMsgCol) + " TEXT NOT NULL); COMMIT;";
        String DATABASE_CREATE_PHONEID =
            	"CREATE TABLE IF NOT EXISTS " + spaContext.getString(R.string.dbPhoneIDTableName) + " ("
            	+ spaContext.getString(R.string.dbRowIDCol) + " integer primary key autoincrement, "
                + spaContext.getString(R.string.dbPhoneIDCol) + " TEXT NOT NULL, "
                + spaContext.getString(R.string.dbPhoneNameCol) + " TEXT, "
                + spaContext.getString(R.string.dbPhoneFirstNameCol) + " TEXT, "
                + spaContext.getString(R.string.dbPhoneLastNameCol) + " TEXT, "
                + spaContext.getString(R.string.dbPhoneNameIDCol) + " TEXT, "
                + spaContext.getString(R.string.dbPhoneTypeCDCol) + " TEXT, " 
                + spaContext.getString(R.string.dbPhoneSecFlgCol) + " TEXT NOT NULL DEFAULT \"0\" " 
                + "); COMMIT;";
        String DATABASE_CREATE_FEEDCONTACT =
            	"CREATE TABLE IF NOT EXISTS " + ContactFeed.DB_TABLE_NAME + " (" +
    			ContactFeed.DB_TABLE_COLUMNS[ContactFeed._ID] + " integer primary key autoincrement, " +
    			ContactFeed.DB_TABLE_COLUMNS[ContactFeed.PHONE_ID] + " TEXT NOT NULL, " +
    			ContactFeed.DB_TABLE_COLUMNS[ContactFeed.FEED_TYPE] + " TEXT, " +
    			ContactFeed.DB_TABLE_COLUMNS[ContactFeed.USER_ID] + " TEXT, " +
    			ContactFeed.DB_TABLE_COLUMNS[ContactFeed.PASSWORD_TXT] + " TEXT, " + 
    			ContactFeed.DB_TABLE_COLUMNS[ContactFeed.FEED_ID] + " TEXT NOT NULL, " + 
    			ContactFeed.DB_TABLE_COLUMNS[ContactFeed.FEED_UPDATE_COUNT] + " INTEGER, " +
    			ContactFeed.DB_TABLE_COLUMNS[ContactFeed.FEED_FLGS] + " INTEGER NOT NULL DEFAULT \"0\", " +
    			ContactFeed.DB_TABLE_COLUMNS[ContactFeed.FEED_CHECKED_TS] + " LONG NOT NULL DEFAULT \"0\", " + 
    			ContactFeed.DB_TABLE_COLUMNS[ContactFeed.FEED_ACTIVITY_TS] + " LONG NOT NULL DEFAULT \"0\" " 
                + "); COMMIT;";
        String DATABASE_CREATE_NOTIFY =
        	"CREATE TABLE IF NOT EXISTS " + spaContext.getString(R.string.dbNotifyTableName) + " ("
        	+ spaContext.getString(R.string.dbNotifyIDCol) + " integer primary key autoincrement, "
        	+ spaContext.getString(R.string.dbNotifyPhoneIDCol) + " TEXT NOT NULL, "
        	+ spaContext.getString(R.string.dbNotifyBarFlgCol) + " TEXT NOT NULL, "
        	+ spaContext.getString(R.string.dbNotifyBarDefIconFlgCol) + " TEXT, "
        	+ spaContext.getString(R.string.dbNotifyBarIconIDCol) + " TEXT, "
        	+ spaContext.getString(R.string.dbNotifyBarDefNotifyFlgCol) + " TEXT, "
        	+ spaContext.getString(R.string.dbNotifyBarTickerMsgFlgCol) + " TEXT, "
        	+ spaContext.getString(R.string.dbNotifyBarDefTickerFlgCol) + " TEXT, "
        	+ spaContext.getString(R.string.dbNotifyBarDefTitleFlgCol) + " TEXT, "
        	+ spaContext.getString(R.string.dbNotifyBarDefTextFlgCol) + " TEXT, "
        	+ spaContext.getString(R.string.dbNotifyBarTickerCol) + " TEXT, "
        	+ spaContext.getString(R.string.dbNotifyBarTitleCol) + " TEXT, "
        	+ spaContext.getString(R.string.dbNotifyBarTextCol) + " TEXT, "
        	+ spaContext.getString(R.string.dbNotifyLightFlgCol) + " TEXT NOT NULL, "
        	+ spaContext.getString(R.string.dbNotifyLightColorCol) + " TEXT, "
        	+ spaContext.getString(R.string.dbNotifyLightOnCol) + " TEXT, "
        	+ spaContext.getString(R.string.dbNotifyLightOffCol) + " TEXT, "
        	+ spaContext.getString(R.string.dbNotifyVibrateFlgCol) + " TEXT NOT NULL, "
        	+ spaContext.getString(R.string.dbNotifyVibrateDefFlgCol) + " TEXT, "
        	+ spaContext.getString(R.string.dbNotifyVibratePatternCol) + " TEXT, "
        	+ spaContext.getString(R.string.dbNotifySoundFlgCol) + " TEXT NOT NULL, "
        	+ spaContext.getString(R.string.dbNotifySoundDefFlgCol) + " TEXT, "
            + spaContext.getString(R.string.dbNotifySoundFileCol) + " TEXT); COMMIT;";
        
        if (EpicFeedConsts.DEBUG_DB_HELPER) Log.i(TAG, TAG2 + "createDBTables:" + DATABASE_CREATE_METADATA);
        if (EpicFeedConsts.DEBUG_DB_HELPER) Log.i(TAG, TAG2 + "createDBTables:" + INSERT_METADATA_DATA);
        if (EpicFeedConsts.DEBUG_DB_HELPER) Log.i(TAG, TAG2 + "createDBTables:" + DATABASE_CREATE_ERRORLOG);
        if (EpicFeedConsts.DEBUG_DB_HELPER) Log.i(TAG, TAG2 + "createDBTables:" + DATABASE_CREATE_TEXTMSGLOG);
        if (EpicFeedConsts.DEBUG_DB_HELPER) Log.i(TAG, TAG2 + "createDBTables:" + DATABASE_CREATE_PHONEID);
        if (EpicFeedConsts.DEBUG_DB_HELPER) Log.i(TAG, TAG2 + "createDBTables:" + DATABASE_CREATE_FEEDCONTACT);
        if (EpicFeedConsts.DEBUG_DB_HELPER) Log.i(TAG, TAG2 + "createDBTables:" + DATABASE_CREATE_NOTIFY);
        if (EpicFeedConsts.DEBUG_DB_HELPER) Log.i(TAG, TAG2 + "createDBTables:" + "Creating Metadata table.");
    	if (!isFoundTable(appDB, "android_metadata")) {
	    	try {
	    		appDB.execSQL(DATABASE_CREATE_METADATA);
	    	} catch (SQLException se) {
	    		throw new Error("Error creating METADATAA table SQL");
	    	}
	    	try { 
	    		appDB.execSQL(INSERT_METADATA_DATA);
	    	} catch (SQLException se) {
	    		throw new Error("Error inserting METADATA data SQL");
	    	}
    	}
    	
        if (EpicFeedConsts.DEBUG_DB_HELPER) Log.i(TAG, TAG2 + "createDBTables: Creating ErrorLog table.");
		if (!isFoundTable(appDB, spaContext.getString(R.string.dbLogTableName)))
	    	try {
	    		appDB.execSQL(DATABASE_CREATE_ERRORLOG);
	    	} catch (SQLException se) {
	    		throw new Error("Error creating ErrorLog table SQL");
	    	}

        if (EpicFeedConsts.DEBUG_DB_HELPER) Log.i(TAG, TAG2 + "createDBTables: Creating TextMsg table.");
		if (!isFoundTable(appDB, spaContext.getString(R.string.dbTxtTableName)))
	    	try {
	    		appDB.execSQL(DATABASE_CREATE_TEXTMSGLOG);
	    	} catch (SQLException se) {
	    		throw new Error("Error creating TextMsg table SQL");
	    	}

		if (EpicFeedConsts.DEBUG_DB_HELPER) Log.i(TAG, TAG2 + "createDBTables: Creating PhoneID table.");
		if (!isFoundTable(appDB, spaContext.getString(R.string.dbPhoneIDTableName)))
	    	try {
	    		appDB.execSQL(DATABASE_CREATE_PHONEID);
	    	} catch (SQLException se) {
	    		throw new Error("Error creating PhoneID table SQL");
	    	}

		if (EpicFeedConsts.DEBUG_DB_HELPER) Log.i(TAG, TAG2 + "createDBTables: Creating FeedContact table.");
		if (!isFoundTable(appDB, ContactFeed.DB_TABLE_NAME))
	    	try {
	    		appDB.execSQL(DATABASE_CREATE_FEEDCONTACT);
	    	} catch (SQLException se) {
	    		throw new Error("Error creating FEEDCONTACT table SQL");
	    	}

		if (EpicFeedConsts.DEBUG_DB_HELPER) Log.i(TAG, TAG2 + "createDBTables: Creating Notify table.");
		if (!isFoundTable(appDB, spaContext.getString(R.string.dbNotifyTableName)))
	    	try {
	    		appDB.execSQL(DATABASE_CREATE_NOTIFY);
	    	} catch (SQLException se) {
	    		throw new Error("Error creating Notify table SQL");
	    	}
    }
    
    public boolean execSQLString(SQLiteDatabase appDB, String sqlStr) throws SQLException {
    	try {
    		appDB.execSQL(sqlStr);
		} catch (SQLException se) {
    		throw new Error("Error executing: " + se + " / \nSQL: " + sqlStr);
    	}
		return true;
    }

 
    /**
     * Check if the database already exist to avoid re-copying the file each time you open the application.
     * @return true if it exists, false if it doesn't
     */
    public boolean isFound() {
        if (EpicFeedConsts.DEBUG_DB_HELPER) {
        	isFoundCount++;
        	Log.i(TAG, TAG2 + "isFound: Called " + isFoundCount + " times.");
        }
        if (EpicFeedConsts.DEBUG_DB_HELPER) Log.i(TAG, TAG2 + "isFound: Checking DB - checkDataBase");
    	SQLiteDatabase checkDB = null;
    	try{
            if (EpicFeedConsts.DEBUG_DB_HELPER) Log.i(TAG, TAG2 + "isFound: Checking DB - attempting openDatabase");
    		//String myPath = DB_PATH + DB_NAME;
    		checkDB = SQLiteDatabase.openDatabase(DB_PATH+DB_NAME, null, SQLiteDatabase.OPEN_READONLY);
            if (EpicFeedConsts.DEBUG_DB_HELPER) Log.i(TAG, TAG2 + "isFound: Checking DB - openDatabase done");
    	}catch(SQLiteException e){
    		//database does't exist yet.
    		if (EpicFeedConsts.DEBUG_DB_HELPER) Log.i(TAG, TAG2 + "isFound: Database does not exist.");
    	}
    	if(checkDB != null){
    		checkDB.close();
    	}
    	return checkDB != null ? true : false;
    }
 
	public boolean isFoundTable(SQLiteDatabase appDB, String str) {
		if (appDB == null) return false;
        if (EpicFeedConsts.DEBUG_DB_HELPER) 
        	Log.i(TAG, TAG2 + "isFoundTable: Checking Table - " + str); 
		Cursor mCursor = appDB.rawQuery("SELECT name FROM sqlite_master WHERE type='table' AND name='"
				+ str + "'", null);
		if (mCursor == null) return false;
		int countTable = mCursor.getCount();
		mCursor.moveToFirst();
		mCursor.close();
        if (EpicFeedConsts.DEBUG_DB_HELPER) 
        	Log.i(TAG, TAG2 + "isFoundTable: Found Table: " + (countTable > 0)); 
		return (countTable > 0);    
	}
 
	public boolean isFoundColumn(SQLiteDatabase appDB, String tbl, String col) {
		if (appDB == null) return false;
		int idx = -1;
		Cursor mCursor = appDB.rawQuery("SELECT sql FROM sqlite_master WHERE type='table' AND name='"
				+ tbl + "'", null);
		if (mCursor.getCount() > 0) {
			mCursor.moveToFirst();
			String sql = mCursor.getString(0);
	        if (EpicFeedConsts.DEBUG_DB_HELPER) Log.i(TAG, TAG2 + "isFoundColumn: Table SQL - " + sql); 
			idx = sql.indexOf(col);
		}
		mCursor.close();
        if (EpicFeedConsts.debugLevel > 6) Log.i(TAG, TAG2 + "isFoundColumn: Table - " + tbl 
        		+ " : Column - " + col + " index is " + idx + " and found: " + !(idx < 0));
		return !(idx < 0);    
	}
}