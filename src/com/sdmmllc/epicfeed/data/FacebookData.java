package com.sdmmllc.epicfeed.data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.util.Log;

import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.model.GraphObject;
import com.sdmmllc.epicfeed.data.FeedContactList.FeedContactListComparator;
import com.sdmmllc.epicfeed.utils.EpicFeedConsts;
import com.sdmmllc.epicfeed.view.FeedMessageListAdapter;
import com.sdmmllc.epicfeed.view.FeedTextMsg;

public class FacebookData {
	
	public static final String TAG = "FacebookData";

    private String FRIENDS_POSTS = "select title, owner from link where owner in " +
    		"(select uid2 from friend where uid1 = me() limit 100)";
    private String FRIEND_POSTS = "select title, owner from link where owner = ";
    private String MY_POSTS_GRAPH = "/me/posts";
    private String MY_POSTS_NODE = "fields";
    private String MY_POSTS_QUERY = "id,caption,created_time,description,link,message,object_id,shares";
    private String FRIEND_ALT_POSTS_GRAPH = "/posts";
    private String FRIEND_ALT_POSTS_NODE = "fields";
    private String FRIEND_ALT_POSTS_QUERY = "id,caption,created_time,description,link,message,object_id,shares";
    private String MY_THREADS = "SELECT thread_id, updated_time, folder_id, message_count, originator, unread, unseen " 
    		+ "FROM thread WHERE folder_id = ";
    private String MY_INBOX = "SELECT folder_id, total_count, unread_count, viewer_id " 
    		+ "FROM mailbox_folder WHERE viewer_id = me()";

    private String MY_DATA = "SELECT uid, first_name, last_name, email, pic_small FROM user WHERE uid = me() ";
    private String ONE_FRIEND_DATA = "SELECT uid, first_name, last_name, email, pic_small FROM user WHERE uid = ";
    private String ALL_FRIEND_DATA = "SELECT uid, first_name, last_name, email, pic_small FROM user WHERE uid IN " +
    		"(select uid2 from friend where uid1 = me())";
    private String MY_POSTS = "SELECT uid, time, status_id, source, place_id, message, like_info, comment_info " 
    		+ "FROM status WHERE uid = me() LIMIT 20";
    private String FRIEND_ALT_POSTS = "SELECT uid, time, status_id, source, place_id, message, like_info, comment_info " 
    		+ "FROM status WHERE uid = ";
    private String MY_PHOTOS = "SELECT owner, created, src_small, place_id, caption, like_info, comment_info, target_id, target_type " 
    		+ "FROM photo WHERE owner = me() LIMIT 20";
    private String FRIEND_ALT_PHOTOS = "SELECT owner, created, src_small, place_id, caption, like_info, comment_info, target_id, target_type " 
    		+ "FROM photo WHERE owner = ";
    private String MESSAGES = "SELECT author_id, created_time, message_id, body, viewer_id " 
    		+ "FROM message WHERE thread_id = ";
    private String FB_INBOX = "SELECT thread_id, author_id, created_time FROM message WHERE thread_id IN " +
    		"(SELECT thread_id FROM thread WHERE folder_id = 0 OR folder_id = 1) AND author_id = ";
    private String FB_INBOX_SUFFIX = " ORDER BY created_time DESC ";
 
	private EpicContact mContact;
    private JSONArray userFbStatus, userFbPhotos, userFbMessages, userFBInboxArray, userFBFriendThreadArray;
    private String userFBInboxId, userFBFriendThreadId;
    private JSONArray friendFbStatus, friendFbPhotos, friendFbMessages;
    private Boolean fbDataReady = false;
    private List<FeedTextMsg> txtMsgs = Collections.synchronizedList(new ArrayList<FeedTextMsg>()); 
    public JSONArray userFbFriends;
    public FeedContactList userFbContacts;
    public EpicContact me = new EpicContact("me");
    
    public FacebookData (Session session, EpicContact contact) {
    	mContact = contact;
    	mSession = session;
    }
    
    private FacebookDataListener mListener;
    
    public FacebookDataListener getFacebookDataListener() {
    	return mListener;
    }
    
    public void setFacebookDataListener(FacebookDataListener listener) {
    	mListener = listener;
    }
    
    public Boolean isFbDataReady() {
    	fbDataReady = !getMyDataRunning && 
    			!getFriendsDataRunning &&
    			!getFacebookFriendsRunning &&
    			!getOneFriendDataRunning && 
    			!getFacebookDataRunning && 
    			!getFacebookMsgRunning &&
    			!getFacebookMyPhotosRunning &&
    			!getFacebookFriendsPhotosRunning &&
    			!getFacebookMeDataRunning;
    	if (EpicFeedConsts.DEBUG_FB) Log.i(TAG, "isFbDataReady: " + fbDataReady +
    			" getMyDataRunning: " + getMyDataRunning + 
    			" getFriendsDataRunning: " + getFriendsDataRunning +
    			" getFacebookFriendsRunning: " + getFacebookFriendsRunning + 
    			" getOneFriendDataRunning: " + getOneFriendDataRunning +
    			" getFacebookDataRunning: " + getFacebookDataRunning +
    			" getFacebookMsgRunning: " + getFacebookMsgRunning +
    			" getFacebookMyPhotosRunning: " + getFacebookMyPhotosRunning +
    			" getFacebookFriendsPhotosRunning: " + getFacebookFriendsPhotosRunning +
    			" getFacebookMeDataRunning: " + getFacebookMeDataRunning);
    	return fbDataReady;
    }
    
    private Session mSession;

    public Session getSession() {
		return mSession;
	}

	public void setSession(Session session) {
		this.mSession = session;
	}

    public List<FeedTextMsg> getFbMsgs() {
    	return txtMsgs;
	}
    
    public void clearFbMsgs() {
    	txtMsgs.clear();
	}
    
    private void buildUserThreads(JSONArray json) {
    	userFBFriendThreadArray = json;
		if (userFBFriendThreadArray != null && userFBFriendThreadArray.length() > 0) {
			try {
				JSONObject thread = userFBFriendThreadArray.getJSONObject(0);
				userFBFriendThreadId = thread.getString("thread_id");
				if (EpicFeedConsts.DEBUG_FB) Log.d(TAG, "found " + userFBFriendThreadArray.length() + " facebook thread");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	  	// FB messages
        for( int ii = 0; ii < 3; ii++) {
        	// LIMIT 0,30 then looping to get LIMIT 30,60.......will get you as many messages as possible
        	String fqlQuery = MESSAGES + "'" 
            		+ userFBFriendThreadId + "' ORDER BY created_time DESC LIMIT " + String.valueOf(25 * ii) + 
            		"," + String.valueOf(25 + 25 * ii) + " ";
    	  	if (EpicFeedConsts.DEBUG_FB) Log.i(TAG, "messages fqlQuery: " + fqlQuery);
    	  	Bundle params = new Bundle();
            params.putString("q", fqlQuery);
            Request request = new Request(mSession,
    			  "/fql",                         
    			  params,                         
    			  HttpMethod.GET,                 
    			  new Request.Callback(){         
    			  		public void onCompleted(Response response) {
    			  			if (EpicFeedConsts.DEBUG_FB) Log.i(TAG, "messages response: " + response.toString());
    			  			GraphObject graphObject = response.getGraphObject();
                            if (graphObject != null) {
                                if (graphObject.getProperty("data") != null) {
                                	String arry = graphObject.getProperty("data").toString();
                                    try {
    									JSONArray jsonNArray1 = new JSONArray(arry);
    	                            	buildFriendMessagesInfoDisplay(jsonNArray1);
    								} catch (JSONException e) {
    									// TODO Auto-generated catch block
    									e.printStackTrace();
    									getFacebookMsgRunning = false;
    								}
                                } else getFacebookMsgRunning = false;
                            } else getFacebookMsgRunning = false;
                            if (EpicFeedConsts.DEBUG_FB) Log.i(TAG, "Result: " + response.toString());
    			  		}                  
    			  	}); 
    	  	Request.executeBatchAsync(request); 
        }
    }
    
    private void buildUserStatusInfoDisplay(JSONArray json) {
	    userFbStatus = json;
		if (userFbStatus != null && userFbStatus.length() > 0) {
			try {
				List<FeedTextMsg> fbStatuses = new ArrayList<FeedTextMsg>();
				for (int i = 0; i < userFbStatus.length(); i++) {
					JSONObject status = userFbStatus.getJSONObject(i);
					FeedTextMsg msg = new FeedTextMsg();
					msg.msgTimestamp = status.getLong("time") * 1000;
					msg.msgDate = new Date(msg.msgTimestamp);
					msg.msgID = i+"";
					msg.setMsgType(FeedMessageListAdapter.SENT_FB_POST);
					msg.setMsgTypeFlg(FeedTextMsg.TYPE_FB_STATUS);
					msg.msgTxt = status.getString("message");
					msg.msgLikes = status.getJSONObject("like_info").getInt("like_count");
					msg.msgComments = status.getJSONObject("comment_info").getInt("comment_count");
					msg.feedMsgID = status.getString("status_id");
					msg.setSentFlg(true);
					msg.setSentMsg(true);
					fbStatuses.add(msg);
				}
				txtMsgs.addAll(fbStatuses);
				if (mListener != null) mListener.onUpdate();
				if (EpicFeedConsts.DEBUG_FB) Log.d(TAG, "found " + txtMsgs.size() + " facebook status posts");
			} catch (JSONException e) {
				e.printStackTrace();
			} finally {
				getFacebookMeDataRunning = false;
			}
		}
    }
    
    private void buildFriendStatusInfoDisplay(JSONArray json) {
	    friendFbStatus = json;
		if (friendFbStatus != null && friendFbStatus.length() > 0) {
			try {
				List<FeedTextMsg> fbStatuses = new ArrayList<FeedTextMsg>();
				for (int i = 0; i < friendFbStatus.length(); i++) {
					JSONObject status = friendFbStatus.getJSONObject(i);
					FeedTextMsg msg = new FeedTextMsg();
					msg.msgTimestamp = status.getLong("time") * 1000;
					msg.msgDate = new Date(msg.msgTimestamp);
					msg.msgID = i+"";
					msg.setMsgType(FeedMessageListAdapter.RECEIVED_FB_POST);
					msg.setMsgTypeFlg(FeedTextMsg.TYPE_FB_STATUS);
					msg.msgTxt = status.getString("message");
					msg.msgLikes = status.getJSONObject("like_info").getInt("like_count");
					msg.msgComments = status.getJSONObject("comment_info").getInt("comment_count");
					msg.feedMsgID = status.getString("status_id");
					msg.setReceivedMsg(true);
					fbStatuses.add(msg);
				}
				txtMsgs.addAll(fbStatuses);
				if (EpicFeedConsts.DEBUG_FB) Log.d(TAG, "found " + fbStatuses.size() + " facebook friend status posts");
			} catch (JSONException e) {
				e.printStackTrace();
			} finally {
				getFacebookDataRunning = false;
			}
			if (mListener != null) mListener.onUpdate();
		}
    }
    
    private void buildUserPhotoInfoDisplay(JSONArray json) {
		userFbPhotos = json;
		if (userFbPhotos != null && userFbPhotos.length() > 0) {
			try {
				List<FeedTextMsg> fbPhotos = new ArrayList<FeedTextMsg>();
				for (int i = 0; i < userFbPhotos.length(); i++) {
					JSONObject photo = userFbPhotos.getJSONObject(i);
					FeedTextMsg msg = new FeedTextMsg();
					msg.msgTimestamp = photo.getLong("created") * 1000;
					msg.msgDate = new Date(msg.msgTimestamp);
					msg.msgID = i+"";
					msg.msgPicURL = photo.getString("src_small"); 
					msg.setMsgType(FeedMessageListAdapter.SENT_FB_POST);
					msg.setMsgTypeFlg(FeedTextMsg.TYPE_FB_PHOTO);
					msg.msgTxt = photo.getString("caption");
					msg.msgLikes = photo.getJSONObject("like_info").getInt("like_count");
					msg.msgComments = photo.getJSONObject("comment_info").getInt("comment_count");
					msg.setTargetMsgID((photo.getString("target_id")+"").trim());
					msg.setSentFlg(true);
					msg.setSentMsg(true);
					fbPhotos.add(msg);
				}
				
				txtMsgs.addAll(fbPhotos);
				if (EpicFeedConsts.DEBUG_FB) Log.d(TAG, "found " + fbPhotos.size() + " facebook user photo posts");
			} catch (JSONException e) {
				e.printStackTrace();
			} finally {
				getFacebookMyPhotosRunning = false;
			}
			if (mListener != null) mListener.onUpdate();
		}
    }
    
    private void buildFriendPhotoInfoDisplay(JSONArray json) {
		friendFbPhotos = json;
		if (friendFbPhotos != null && friendFbPhotos.length() > 0) {
			try {
				List<FeedTextMsg> fbPhotos = new ArrayList<FeedTextMsg>();
				for (int i = 0; i < friendFbPhotos.length(); i++) {
					JSONObject photo = friendFbPhotos.getJSONObject(i);
					FeedTextMsg msg = new FeedTextMsg();
					msg.msgTimestamp = photo.getLong("created") * 1000;
					msg.msgDate = new Date(msg.msgTimestamp);
					msg.msgID = i+"";
					msg.msgPicURL = photo.getString("src_small"); 
					msg.setMsgType(FeedMessageListAdapter.RECEIVED_FB_POST);
					msg.setMsgTypeFlg(FeedTextMsg.TYPE_FB_PHOTO);
					msg.msgTxt = photo.getString("caption");
					msg.msgLikes = photo.getJSONObject("like_info").getInt("like_count");
					msg.msgComments = photo.getJSONObject("comment_info").getInt("comment_count");
					msg.setTargetMsgID((photo.getString("target_id")+"").trim());
					msg.setReceivedMsg(true);
					fbPhotos.add(msg);
				}
				
				txtMsgs.addAll(fbPhotos);
				if (EpicFeedConsts.DEBUG_FB) Log.d(TAG, "found " + fbPhotos.size() + " facebook friend photo posts");
			} catch (JSONException e) {
				e.printStackTrace();
			} finally {
				getFacebookFriendsPhotosRunning = false;
			}
			if (mListener != null) mListener.onUpdate();
		}
    }
    
    private void buildFriendMessagesInfoDisplay(JSONArray json) {
		friendFbMessages = json;
		if (friendFbMessages != null && friendFbMessages.length() > 0) {
			try {
				List<FeedTextMsg> fbFriendMsgs = new ArrayList<FeedTextMsg>();
				for (int i = 0; i < friendFbMessages.length(); i++) {
					JSONObject fbMsg = friendFbMessages.getJSONObject(i);
					FeedTextMsg msg = new FeedTextMsg();
					msg.msgTimestamp = fbMsg.getLong("created_time") * 1000;
					msg.msgDate = new Date(msg.msgTimestamp);
					msg.msgID = i+"";
					msg.setMsgTypeFlg(FeedTextMsg.TYPE_FB_MSG);
					msg.msgTxt = fbMsg.getString("body");
					if (fbMsg.getString("author_id").equals(mContact.getFeed(ContactFeed.FEED_TYPE_FACEBOOK).getFeedSystemId())) {
						msg.setReceivedMsg(true);
						msg.setMsgType(FeedMessageListAdapter.RECEIVED_FB_MSG);
					} else {
						msg.setSentFlg(true);
						msg.setSentMsg(true);
						msg.setMsgType(FeedMessageListAdapter.SENT_FB_MSG);
					}
					fbFriendMsgs.add(msg);
				}
				
				txtMsgs.addAll(fbFriendMsgs);
				if (EpicFeedConsts.DEBUG_FB) Log.d(TAG, "found " + fbFriendMsgs.size() + " facebook status posts");
			} catch (JSONException e) {
				e.printStackTrace();
			} finally {
				getFacebookMsgRunning = false;
			}
			if (mListener != null) mListener.onUpdate();
		}
    }
    
    private void buildUserInfoDisplay(JSONArray json, FacebookDataListener listener) {
    	try {
    		
    	    userFbFriends = json;
    	    if (EpicFeedConsts.DEBUG_FB) Log.d(TAG, "buildUserInfoDisplay - userFbFriends: " + userFbFriends.toString());
    	    userFbContacts = new FeedContactList();
    	    
    	    JSONObject jsonFriend = null;	
        	for (int i = 0; i < userFbFriends.length(); i++) {
        		try {
        	    	jsonFriend = userFbFriends.getJSONObject(i);
                	EpicContact contact = new EpicContact(jsonFriend.getString("uid"));
        	        contact.firstName = jsonFriend.getString("first_name");
        	        contact.lastName = jsonFriend.getString("last_name");
        	        contact.name = contact.name = contact.firstName + " " + contact.lastName;
        	        contact.email = jsonFriend.getString("email");
        	        ContactFeed feed = new ContactFeed(jsonFriend.getString("uid"));
        	        feed.setFeedSystemId(jsonFriend.getString("uid")); 
        	        feed.setFeedType(ContactFeed.FEED_TYPE_FACEBOOK);
        	        feed.profileURL = jsonFriend.getString("pic_small");
        	        contact.addFeed(feed);
        	        userFbContacts.add(contact);
        	        if (EpicFeedConsts.DEBUG_FB) Log.w(TAG, "added contact: +++++++ " + contact.name + " ++++++++++");
            	} catch (Throwable t) {
            		t.printStackTrace();
            	    Log.e(TAG, "JSON: \"" + jsonFriend + "\"");
            	} finally {
                    Collections.sort(userFbContacts, FeedContactListComparator.getComparator(FeedContactListComparator.DISPLAY_NAME));
                    // Collections.reverse(phoneFbContacts);
            	}
    	    }
    	    
    	    
    	} catch (Throwable t) {
    		t.printStackTrace();
    	    Log.e(TAG, "JSON: \"" + json + "\"");
    	} finally {
    		getFacebookFriendsRunning = false;
    	}
    	if (listener != null) {
    		listener.onUpdate();
    		if (isFbDataReady()) listener.onComplete();
    	}
    }
   
    private void buildMyInfoDisplay(JSONArray json, FacebookDataListener listener) {
    	try {
    		
    	    JSONArray myData = json;
    	    if (EpicFeedConsts.DEBUG_FB) Log.d(TAG, "buildMyInfoDisplay - myData: " + myData.toString());
    	    
    	    JSONObject jsonFriend = null;	
        	if (myData != null && myData.length() > 0) {
        		try {
        	    	jsonFriend = myData.getJSONObject(0);
        	        me.firstName = jsonFriend.getString("first_name");
        	        me.lastName = jsonFriend.getString("last_name");
        	        me.name = me.firstName + " " + me.lastName;
        	        me.email = jsonFriend.getString("email");
        	        ContactFeed feed = new ContactFeed(jsonFriend.getString("uid"));
        	        feed.setFeedSystemId(jsonFriend.getString("uid"));
        	        feed.setFeedType(ContactFeed.FEED_TYPE_FACEBOOK);
        	        feed.profileURL = jsonFriend.getString("pic_small");
        	        Log.i(TAG, "+++++++++++++++++++++++++++++ profile URL >> " + feed.profileURL);
        	        me.addFeed(feed);
        	        if (EpicFeedConsts.DEBUG_FB) Log.w(TAG, "got infor for me: +++++++ " + me.name + " ++++++++++");
            	} catch (Throwable t) {
            		t.printStackTrace();
            	    Log.e(TAG, "JSON: \"" + jsonFriend + "\"");
            	} 
    	    }
    	    
    	    
    	} catch (Throwable t) {
    		t.printStackTrace();
    	    Log.e(TAG, "JSON: \"" + json + "\"");
    	} finally {
    		getMyDataRunning = false;
    	}
    	if (listener != null) {
    		listener.onUpdate();
    		if (isFbDataReady()) listener.onComplete();
    	}
    }
   
    private void buildOneFriendInfoDisplay(JSONArray json, FacebookDataListener listener, EpicContact friend) {
    	try {
    		
    	    JSONArray myData = json;
    	    if (EpicFeedConsts.DEBUG_FB) Log.d(TAG, "buildOneFriendInfoDisplay - myData: " + myData.toString());
    	    
    	    JSONObject jsonFriend = null;	
        	if (myData != null && myData.length() > 0) {
        		try {
        	    	jsonFriend = myData.getJSONObject(0);
        	        friend.firstName = jsonFriend.getString("first_name");
        	        friend.lastName = jsonFriend.getString("last_name");
        	        friend.name = friend.name = friend.firstName + " " + friend.lastName;
        	        friend.email = jsonFriend.getString("email");
        	        ContactFeed feed = new ContactFeed(jsonFriend.getString("uid"));
        	        feed.setFeedSystemId(jsonFriend.getString("uid"));
        	        feed.setFeedType(ContactFeed.FEED_TYPE_FACEBOOK);
        	        feed.profileURL = jsonFriend.getString("pic_small");
        	        friend.addFeed(feed);
        	        if (EpicFeedConsts.DEBUG_FB) Log.w(TAG, "got info for contact: +++++++ " + friend.name + " ++++++++++");
            	} catch (Throwable t) {
            		t.printStackTrace();
            	    Log.e(TAG, "JSON: \"" + jsonFriend + "\"");
            	} 
    	    }
    	    
    	    
    	} catch (Throwable t) {
    		t.printStackTrace();
    	    Log.e(TAG, "JSON: \"" + json + "\"");
    	} finally {
    		getOneFriendDataRunning = false;
    	}
    	if (listener != null) {
    		listener.onUpdate();
    		if (isFbDataReady()) listener.onComplete();
    	}
    }
   
    private Boolean getFriendsDataRunning = false;
    
    public void getFriendsData(final FacebookDataListener listener) {
    	getFriendsDataRunning = true;
    	if (EpicFeedConsts.DEBUG_FB) Log.w(TAG, "+++++++ getFriendsData() ++++++++++");
        String fqlQuery = ALL_FRIEND_DATA;
        Bundle params = new Bundle();
        params.putString("q", fqlQuery);
        mSession = Session.getActiveSession();
        Request request = new Request(mSession,
			  "/fql",                         
			  params,                         
			  HttpMethod.GET,                 
			  new Request.Callback(){         
			  		public void onCompleted(Response response) {
			  			GraphObject graphObject = response.getGraphObject();
                        if (graphObject != null) {
                            if (graphObject.getProperty("data") != null) {
                            	String arry = graphObject.getProperty("data").toString();
        			  			Log.i(TAG, "arry: " + arry);
        			  	    	if (EpicFeedConsts.DEBUG_FB) Log.w(TAG, "+++++++ getFriendsData() ++++++++++");
                                try {
									JSONArray jsonNArray1 = new JSONArray(arry);
									buildUserInfoDisplay(jsonNArray1, listener);
	        			  			if (EpicFeedConsts.DEBUG_FB && jsonNArray1.length() > 1) {
	        			  				Log.i(TAG, "jsonNArray1.getJSONObject(1).toString(): " + 
	        			  			    jsonNArray1.getJSONObject(1).toString());
	        			  			}
								} catch (JSONException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
									getFriendsDataRunning = false;
								}
                            } else getFriendsDataRunning = false;
                        } else getFriendsDataRunning = false;
			  			Log.i(TAG, "Result: " + response.toString());
			  		}                  
			  	}); 
	  	Request.executeBatchAsync(request);      
    }
    
    
    private Boolean getMyDataRunning = false;
    
    public void getMyData(final FacebookDataListener listener) {
    	if (EpicFeedConsts.DEBUG_FB) Log.w(TAG, "+++++++ getMyData() ++++++++++");
    	getMyDataRunning = true;
        String fqlQuery = MY_DATA;
        Bundle params = new Bundle();
        params.putString("q", fqlQuery);
        mSession = Session.getActiveSession();
        Request request = new Request(mSession,
			  "/fql",                         
			  params,                         
			  HttpMethod.GET,                 
			  new Request.Callback(){         
			  		public void onCompleted(Response response) {
			  			GraphObject graphObject = response.getGraphObject();
                        if (graphObject != null) {
                            if (graphObject.getProperty("data") != null) {
                            	String arry = graphObject.getProperty("data").toString();
        			  			Log.i(TAG, "arry: " + arry);
        			  	    	if (EpicFeedConsts.DEBUG_FB) Log.w(TAG, "+++++++ getMyData() ++++++++++");
                                try {
									JSONArray jsonNArray1 = new JSONArray(arry);
									buildMyInfoDisplay(jsonNArray1, listener);
	        			  			if (EpicFeedConsts.DEBUG_FB && jsonNArray1.length() > 1) Log.i(TAG, "jsonNArray1.getJSONObject(1).toString(): " + jsonNArray1.getJSONObject(1).toString());
								} catch (JSONException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
									getMyDataRunning = false;
								}
                            } else getMyDataRunning = false;
                        } else getMyDataRunning = false;
			  			Log.i(TAG, "Result: " + response.toString());
			  		}                  
			  	}); 
	  	Request.executeBatchAsync(request);      
    }
    
    private Boolean getOneFriendDataRunning = false;

    public void getOneFriendData(final FacebookDataListener listener, final EpicContact friend) {
    	getOneFriendDataRunning = true;
    	if (EpicFeedConsts.DEBUG_FB) Log.w(TAG, "+++++++ getOneFriendData() ++++++++++");
        String fqlQuery = ONE_FRIEND_DATA + "'" + friend.getFeed(ContactFeed.FEED_TYPE_FACEBOOK).getFeedSystemId() +
        		"'";
        Bundle params = new Bundle();
        params.putString("q", fqlQuery);
        mSession = Session.getActiveSession();
        Request request = new Request(mSession,
			  "/fql",                         
			  params,                         
			  HttpMethod.GET,                 
			  new Request.Callback(){         
			  		public void onCompleted(Response response) {
			  			GraphObject graphObject = response.getGraphObject();
                        if (graphObject != null) {
                            if (graphObject.getProperty("data") != null) {
                            	String arry = graphObject.getProperty("data").toString();
        			  			Log.i(TAG, "arry: " + arry);
        			  	    	if (EpicFeedConsts.DEBUG_FB) Log.w(TAG, "+++++++ getOneFriendData() ++++++++++");
                                try {
									JSONArray jsonNArray1 = new JSONArray(arry);
									buildOneFriendInfoDisplay(jsonNArray1, listener, friend);
	        			  			if (EpicFeedConsts.DEBUG_FB && jsonNArray1.length() > 1) Log.i(TAG, "jsonNArray1.getJSONObject(1).toString(): " + jsonNArray1.getJSONObject(1).toString());
								} catch (JSONException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
									getOneFriendDataRunning = false;
								}
                            } else getOneFriendDataRunning = false;
                        } else getOneFriendDataRunning = false;
			  			Log.i(TAG, "Result: " + response.toString());
			  		}                  
			  	}); 
	  	Request.executeBatchAsync(request);      
    }

    private void buildFriendInfoDisplay(JSONArray json, Contact.UpdateListener listener) {
		userFbFriends = json;
	    if (EpicFeedConsts.DEBUG_FB) Log.d(TAG, "buildFriendInfoDisplay - userFbFriends: " + userFbFriends.toString());
	    getFriendsDataRunning = false;
	    
	    if (mListener != null) {
	    	if (isFbDataReady()) mListener.onComplete();
	    }
    	FeedContactList.getContactsList(listener);    
    }
   
    private Boolean getFacebookFriendsRunning = false;
    
    public void getFacebookFriends(final Contact.UpdateListener listener) {
    	if (mSession == null) {
    		Log.w(TAG, "Facebook session is NULL - cannot get Facebook data");
    		return;
    	}
    	getFacebookFriendsRunning = true;
    	
    	if (EpicFeedConsts.DEBUG_FB) Log.w(TAG, "+++++++ getFacebookFriends() ++++++++++");
        String fqlQuery = ALL_FRIEND_DATA;
        Bundle params = new Bundle();
        params.putString("q", fqlQuery);
        mSession = Session.getActiveSession();
        Request request = new Request(mSession,
			  "/fql",                         
			  params,                         
			  HttpMethod.GET,                 
			  new Request.Callback(){         
			  		public void onCompleted(Response response) {
			  			GraphObject graphObject = response.getGraphObject();
                        if (graphObject != null) {
                            if (graphObject.getProperty("data") != null) {
                            	String arry = graphObject.getProperty("data").toString();
        			  			Log.i(TAG, "arry: " + arry);
        			  	    	if (EpicFeedConsts.DEBUG_FB) Log.w(TAG, "+++++++ getFacebookFriends() ++++++++++");
                                try {
									JSONArray jsonNArray1 = new JSONArray(arry);
									buildFriendInfoDisplay(jsonNArray1, listener);
	        			  			if (EpicFeedConsts.DEBUG_FB && jsonNArray1.length() > 1) Log.i(TAG, "jsonNArray1.getJSONObject(1).toString(): " + jsonNArray1.getJSONObject(1).toString());
								} catch (JSONException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
									getFacebookFriendsRunning = false;
								}
                            } else getFacebookFriendsRunning = false;
                        } else getFacebookFriendsRunning = false;
			  			Log.i(TAG, "Result: " + response.toString());
			  		}                  
			  	}); 
	  	Request.executeBatchAsync(request);      

    }
    
    private Boolean getFacebookDataRunning = false;
    private Boolean getFacebookMeDataRunning = false;
    private Boolean getFacebookMsgRunning = false;
    private Boolean getFacebookMyPhotosRunning = false;
    private Boolean getFacebookFriendsPhotosRunning = false;
    
    public void getFacebookData() {
    	if (mSession == null) {
    		Log.w(TAG, "Facebook session is NULL - cannot get Facebook data");
    		return;
    	}
    	getFacebookMeDataRunning = true;
        String fqlQuery = "";
        
        fqlQuery = MY_POSTS;
        Bundle params = new Bundle();
        params.putString("q", fqlQuery);
        Request request = new Request(mSession,
			  "/fql",                         
			  params,                         
        //Bundle params = new Bundle();
        //params.putString(MY_POSTS_NODE, MY_POSTS_QUERY);
        //Request request = new Request(mSession,
		//	  MY_POSTS_GRAPH,                         
		//	  params,                         
			  HttpMethod.GET,                 
			  new Request.Callback(){         
			  		public void onCompleted(Response response) {
			  			if (EpicFeedConsts.DEBUG_FB) Log.i(TAG, "my status response: " + response.toString());
			  			GraphObject graphObject = response.getGraphObject();
                        if (graphObject != null) {
                            if (graphObject.getProperty("data") != null) {
                            	String arry = graphObject.getProperty("data").toString();
                                try {
									JSONArray jsonNArray1 = new JSONArray(arry);
	                            	buildUserStatusInfoDisplay(jsonNArray1);
								} catch (JSONException e) {
									e.printStackTrace();
									getFacebookMeDataRunning = false;
								}
                            } else getFacebookMeDataRunning = false;
                        } else getFacebookMeDataRunning = false;
                        if (EpicFeedConsts.DEBUG_FB) Log.i(TAG, "Result: " + response.toString());
			  		}                  
			  	}); 
	  	Request.executeBatchAsync(request);   
			if (EpicFeedConsts.DEBUG_FB) Log.i(TAG, "*  *  *  *  * getting friend posts *  *  *  *  *");
	  	
        if (!"me".equals(mContact.getFeed(ContactFeed.FEED_TYPE_FACEBOOK))) { 
        	getFacebookDataRunning = true;
	        fqlQuery = FRIEND_ALT_POSTS + "'" 
	        		+ mContact
	        		.getFeed(
	        				ContactFeed.FEED_TYPE_FACEBOOK)
	        				.getFeedSystemId() + "' LIMIT 50 ";
	        params = new Bundle();
	        params.putString("q", fqlQuery);
	        request = new Request(mSession,
				  "/fql",                         
				  params,                         
	        //params = new Bundle();
	        //params.putString(FRIEND_ALT_POSTS_NODE, FRIEND_ALT_POSTS_QUERY);
	        //request = new Request(mSession,
			//	  "/" + mContact.getFeed(ContactFeed.FEED_TYPE_FACEBOOK).getFeedSystemId() + FRIEND_ALT_POSTS,                         
			//	  params,                         
				  HttpMethod.GET,                 
				  new Request.Callback(){         
				  		public void onCompleted(Response response) {
				  			if (EpicFeedConsts.DEBUG_FB) 
				  				Log.i(TAG, "*  *  *  *  * friend status response: " + response.toString()); // null
				  			GraphObject graphObject = response.getGraphObject();
				  			Log.i(TAG, "*  *  *  *  * graphObject >> " + graphObject.toString());
	                        if (graphObject != null) {
	                            if (graphObject.getProperty("data") != null) {
	                            	String arry = graphObject.getProperty("data").toString();
	                                try {
										JSONArray jsonNArray1 = new JSONArray(arry);
										buildFriendStatusInfoDisplay(jsonNArray1);
									} catch (JSONException e) {
										e.printStackTrace();
										getFacebookDataRunning = false;
									}
	                            } else getFacebookDataRunning = false;
	                        } else getFacebookDataRunning = false;
				  		}                  
				  	}); 
		  	Request.executeBatchAsync(request);   
			if (EpicFeedConsts.DEBUG_FB) Log.i(TAG, "*  *  *  *  * sent request for friend posts *  *  *  *  *");
        }
	  	
        getFacebookMyPhotosRunning = true;
	  	// get FB photos
	  	fqlQuery = MY_PHOTOS;
	  	if (EpicFeedConsts.DEBUG_FB) Log.i(TAG, "status fqlQuery: " + fqlQuery);
        params = new Bundle();
        params.putString("q", fqlQuery);
        request = new Request(mSession,
			  "/fql",                         
			  params,                         
			  HttpMethod.GET,                 
			  new Request.Callback(){         
			  		public void onCompleted(Response response) {
			  			if (EpicFeedConsts.DEBUG_FB) Log.i(TAG, "my photos response: " + response.toString());
			  			GraphObject graphObject = response.getGraphObject();
                        if (graphObject != null) {
                            if (graphObject.getProperty("data") != null) {
                            	String arry = graphObject.getProperty("data").toString();
                                try {
									JSONArray jsonNArray1 = new JSONArray(arry);
	                            	buildUserPhotoInfoDisplay(jsonNArray1);
								} catch (JSONException e) {
									e.printStackTrace();
									getFacebookMyPhotosRunning = false;
								}
                            } else getFacebookMyPhotosRunning = false;
                        } else getFacebookMyPhotosRunning = false;
			  		}                  
			  	}); 
	  	Request.executeBatchAsync(request);     
	  	
	  	// get FB photos
	  	if (!"me".equals(mContact.getFeed(ContactFeed.FEED_TYPE_FACEBOOK))) {
	  		getFacebookFriendsPhotosRunning = true;
	  		fqlQuery = FRIEND_ALT_PHOTOS + "'" 
	        		+ mContact.getFeed(ContactFeed.FEED_TYPE_FACEBOOK).getFeedSystemId() + "' LIMIT 20 ";
		  	if (EpicFeedConsts.DEBUG_FB) Log.i(TAG, "status fqlQuery: " + fqlQuery);
	        params = new Bundle();
	        params.putString("q", fqlQuery);
	        request = new Request(mSession,
				  "/fql",                         
				  params,                         
				  HttpMethod.GET,                 
				  new Request.Callback(){         
				  		public void onCompleted(Response response) {
				  			if (EpicFeedConsts.DEBUG_FB) Log.i(TAG, "friend photos response: " + response.toString());
				  			GraphObject graphObject = response.getGraphObject();
	                        if (graphObject != null) {
	                            if (graphObject.getProperty("data") != null) {
	                            	String arry = graphObject.getProperty("data").toString();
	                                try {
										JSONArray jsonNArray1 = new JSONArray(arry);
		                            	buildFriendPhotoInfoDisplay(jsonNArray1);
									} catch (JSONException e) {
										e.printStackTrace();
										getFacebookFriendsPhotosRunning = false;
									}
	                            } else getFacebookFriendsPhotosRunning = false;
	                        } else getFacebookFriendsPhotosRunning = false;
				  		}                  
				  	}); 
		  	Request.executeBatchAsync(request);
	  	}
	  	
	  	// FB messages
	  	getFacebookMsgRunning = true;
	  	fqlQuery = FB_INBOX + mContact.getFeed(ContactFeed.FEED_TYPE_FACEBOOK).getFeedSystemId() +  FB_INBOX_SUFFIX ;
	  	if (EpicFeedConsts.DEBUG_FB) Log.i(TAG, "inbox folder fqlQuery: " + fqlQuery);
        params = new Bundle();
        params.putString("q", fqlQuery);
        request = new Request(mSession,
			  "/fql",                         
			  params,                         
			  HttpMethod.GET,                 
			  new Request.Callback(){         
			  		public void onCompleted(Response response) {
			  			if (EpicFeedConsts.DEBUG_FB) Log.i(TAG, "inbox response: " + response.toString());
			  			GraphObject graphObject = response.getGraphObject();
                        if (graphObject != null) {
                            if (graphObject.getProperty("data") != null) {
                            	String arry = graphObject.getProperty("data").toString();
                            	if (EpicFeedConsts.DEBUG_FB) Log.i(TAG, "inbox arry: " + arry);
                                try {
									JSONArray jsonNArray1 = new JSONArray(arry);
									buildUserThreads(jsonNArray1);
								} catch (JSONException e) {
									e.printStackTrace();
									getFacebookMsgRunning = false;
								}
                            } else getFacebookMsgRunning = false;
                        } else getFacebookMsgRunning = false;
                        if (EpicFeedConsts.DEBUG_FB) Log.i(TAG, "inbox Result: " + response.toString());
			  		}                  
			  	}); 
	  	Request.executeBatchAsync(request);      
    }

}
