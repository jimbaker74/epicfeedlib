package com.sdmmllc.epicfeed.data;

public class EpicDBConn {
	
	private Integer connId;
	
	public EpicDBConn(int setId) {
		connId = setId;
	}

	public int getConnId() {
		return connId;
	}
}
