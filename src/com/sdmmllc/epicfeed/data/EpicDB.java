package com.sdmmllc.epicfeed.data;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.telephony.PhoneNumberUtils;
import android.util.Log;

import com.sdmm.epicfeed.R;
import com.sdmmllc.epicfeed.EpicFeedController;
import com.sdmmllc.epicfeed.SpaTextNotification;
import com.sdmmllc.epicfeed.SpaUserAccount;
import com.sdmmllc.epicfeed.utils.EpicFeedConsts;
import com.sdmmllc.epicfeed.view.FeedTextMsg;

public class EpicDB {
 
	public static String TAG = "EpicDB", TAG2 = "";

	private SQLiteDatabase appDB; 
    private EpicDBHelper dbHelper;
	private Context mContext;
    private SharedPreferences settings;
    private SharedPreferences.Editor editor;
    private CheckMessageListener mCheckMessageListener;
    private ContactListListener mContactListListener;
    private SpaUserAccount mSpaUserAccount;
    private List<EpicDBConn> conns;
    private Integer connPool = 0;
	
	public String strNotNull(String str) {
		if (str == null) return "";
		else return str;
	}

	public boolean strToBoolean(String str) {
		return str.equals("Y");
	}
	
	public String booleanToStr(boolean val) {
		if (val) {
			return "Y";
		} else {
			return "N";
		}
	}
	
	public EpicDB(Context context) {
		init(context);
    }	
 
	public EpicDB(Context context, boolean updateCheck) {
		init(context);
		if (updateCheck) { 
	        settings = mContext.getSharedPreferences(EpicFeedConsts.DB_FILE, 0);
	        // fix - "DB_FILE" was put in as key...
			if (settings.contains(EpicFeedConsts.DB_FILE)) {
		        editor = settings.edit();
		        editor.putInt(EpicDBHelper.DB_VERSION, settings.getInt(EpicFeedConsts.DB_FILE,
		        		0));
				editor.remove(EpicFeedConsts.DB_FILE);
				editor.commit();
			}
			// check for proper DB version
	        if (!settings.contains(EpicDBHelper.DB_VERSION)|
	        		(settings.getInt(EpicDBHelper.DB_VERSION, 0) != EpicDBHelper.DATABASE_VERSION)) {
		        editor = settings.edit();
	            SharedPreferences settings2 = mContext.getSharedPreferences(EpicFeedConsts.PREFS_NAME, 0);
	            if (!settings2.contains(EpicDBHelper.DB_VERSION)|
	            		(settings2.getInt(EpicDBHelper.DB_VERSION, 0) != EpicDBHelper.DATABASE_VERSION)|
	            		(settings.getInt(EpicDBHelper.DB_VERSION, 0) != EpicDBHelper.DATABASE_VERSION)){
					appDB = dbHelper.getWritableDatabase();
	            	dbHelper.createDB(appDB);
	            	appDB.close();
	            }
		        editor.putInt(EpicDBHelper.DB_VERSION, EpicDBHelper.DATABASE_VERSION);
		        editor.commit();
	        }
	        updateData();
	        editor = null;
	        settings = null;
		}
    }
	
	private void init(Context context) {
		mContext = context;
		dbHelper = new EpicDBHelper(context);
		appDB = dbHelper.getReadableDatabase();
		conns = new ArrayList<EpicDBConn>();
        mSpaUserAccount = EpicFeedController.getSpaUserAccount(context);
	}
	
	public void updateData() {
		if (settings.getInt(EpicDBHelper.DB_DATA_VER, 0) != EpicDBHelper.DATA_VERSION1) {
	        editor = settings.edit();
			dataUpdateVersion1();
			editor.putInt(EpicDBHelper.DB_DATA_VER, EpicDBHelper.DATA_VERSION1);
			editor.commit();
		}
	}
	
	public void dataUpdateVersion1() {
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG , "dataUpdateVersion1: Updating sent messages");
        String UPDATE_STR = 
        	"UPDATE " 
        	+ mContext.getString(R.string.dbTxtTableName) 
        	+ " SET " + mContext.getString(R.string.dbTxtFlgsCol) 
        	+ " = " + mContext.getString(R.string.dbTxtFlgsCol) + " | "
        	+ FeedTextMsg.SENT_FLG
        	+ " WHERE "
        	+ mContext.getString(R.string.dbTxtFlgsCol) + " & "
        	+ FeedTextMsg.SENDER_FLG
        	+ "; COMMIT;";
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG , "dataUpdateVersion1: update sent msgs");
        if (EpicFeedConsts.DEBUG_DB) 
        	Log.i(TAG , "dataUpdateVersion1: SQL : " + UPDATE_STR);
    	try {
        	EpicDBConn mConn = open(true);
    		execSQLString(UPDATE_STR);
        	close(mConn);
    	} catch (Exception e) {
    		if (EpicFeedConsts.DEBUG_DB) 
    			Log.i(TAG , "dataUpdateVersion1: Error executing SQL");
    		if (EpicFeedConsts.DEBUG_DB) 
    			e.printStackTrace();
    	}
		if (EpicFeedConsts.DEBUG_DB) 
    		Log.i(TAG , "dataUpdateVersion1: SQL Done");
	}
	
	public EpicDBConn open(boolean readWrite) {
		synchronized (conns) {
			if (appDB == null) {
				if (!readWrite) appDB = dbHelper.getReadableDatabase();
				else appDB = dbHelper.getWritableDatabase();
			} else {
				if (appDB.isReadOnly()&readWrite) appDB = dbHelper.getWritableDatabase();
				if (!appDB.isReadOnly()&!readWrite) appDB = dbHelper.getReadableDatabase();
				if (!appDB.isOpen()) {
					if (!readWrite) appDB = dbHelper.getReadableDatabase();
					else appDB = dbHelper.getWritableDatabase();
				}
			}
			return getConn();
		}
	}
	
	public void close(EpicDBConn conn) {
		synchronized(conns) {
			if (closeConn(conn)) appDB.close();
		}
	}
 
	public void beginTransaction() {
		appDB.beginTransaction();
	}
 
	public void endTransaction() {
		appDB.endTransaction();
	}
	
	public void setTAG2(String tag) {
		TAG2 = "(" + tag + ")";
	}
 
    public EpicDBConn getConn() {
		synchronized (connPool) {
    		connPool++;
		}
		EpicDBConn newConn = new EpicDBConn(connPool);
    	synchronized (conns) {
	    	conns.add(newConn);
    	}
    	return newConn;
    }
    
    public boolean closeConn(EpicDBConn conn) {
    	synchronized (conns) {
	    	conns.remove(conn);
    	}
    	return (conns.size() < 1);
    }
    
    public boolean execSQLString(String sqlStr) throws SQLException {
    	try {
    		appDB.execSQL(sqlStr);
		} catch (SQLException se) {
    		throw new Error("Error executing: " + se + " / \nSQL: " + sqlStr);
    	}
		return true;
    }
 
	public boolean execSQLStringSilent(String sqlStr) throws SQLException {
		try {
			appDB.execSQL(sqlStr);
		} catch (SQLException se) {
			Log.w(TAG, "Error executing: " + se + " / \nSQL: " + sqlStr);
		}
		return true;
	}

    public long insertLogEntry(String descr, Date timestamp) {
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "insertLogEntry: Inserting error log entry...");
        String INSERT_STR = 
        	"INSERT INTO " + mContext.getString(R.string.dbLogTableName) + " ("
        	//+ spaContext.getString(R.string.dbErrorIDCol) + ", "
            + mContext.getString(R.string.dbLogTimestampCol) + ", " 
            + mContext.getString(R.string.dbLogDescrCol)
        	+ ") VALUES ('" 
        	+ timestamp.toString() + "', '" 
        	+ descr + "'); COMMIT;";
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "insertLogEntry: Log entry: Descr:" + descr + " Timestamp: " + timestamp.toString());
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "insertLogEntry: SQL : " + INSERT_STR);
    	boolean inserted = false;
    	try {
    		inserted = execSQLString(INSERT_STR);
    	} catch (Exception e) {
    		if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "insertLogEntry: Error executing Log Entry SQL");
    	}
    	long itemID = 0;
    	if (inserted) {
    		Cursor mCursor = appDB.rawQuery(
    			"SELECT " + mContext.getString(R.string.dbLogIDCol) 
    			+ " FROM " + mContext.getString(R.string.dbLogTableName) 
    			+ " ORDER BY _id desc limit 1", null);
    			if (mCursor.getCount() == 0)
    				return 0;
    			else
    				mCursor.moveToFirst();
    		itemID = mCursor.getLong(0);
    		mCursor.close();
    	} 
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "insertLogEntry: New SQL Cursor - insertLogEntry");
    	return itemID;
    }
    
    public long insertNotificationEntry(SpaTextNotification notifyData) {
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "insertNotificationEntry: Inserting notification entry...");
        ContentValues cv = new ContentValues();
        cv.put(mContext.getString(R.string.dbNotifyPhoneIDCol), notifyData.phoneID);
        cv.put(mContext.getString(R.string.dbNotifyBarFlgCol), booleanToStr(notifyData.statusbar));
        cv.put(mContext.getString(R.string.dbNotifyBarDefIconFlgCol), booleanToStr(notifyData.statusbarDefaultIcon));
        cv.put(mContext.getString(R.string.dbNotifyBarIconIDCol), notifyData.statusbarIcon);
        cv.put(mContext.getString(R.string.dbNotifyBarDefNotifyFlgCol), booleanToStr(notifyData.statusbarDefaultNotification));
        cv.put(mContext.getString(R.string.dbNotifyBarTickerMsgFlgCol), booleanToStr(notifyData.statusbarTickerUseTextMsg));
        cv.put(mContext.getString(R.string.dbNotifyBarDefTickerFlgCol), booleanToStr(notifyData.statusbarDefaultTicker));
        cv.put(mContext.getString(R.string.dbNotifyBarDefTitleFlgCol), booleanToStr(notifyData.statusbarDefaultTitle));
        cv.put(mContext.getString(R.string.dbNotifyBarDefTextFlgCol), booleanToStr(notifyData.statusbarDefaultText));
        cv.put(mContext.getString(R.string.dbNotifyBarTickerCol), strNotNull(notifyData.statusbarTicker.toString()));
        cv.put(mContext.getString(R.string.dbNotifyBarTitleCol), strNotNull(notifyData.statusbarTitle.toString()));
        cv.put(mContext.getString(R.string.dbNotifyBarTextCol), strNotNull(notifyData.statusbarText.toString()));
        cv.put(mContext.getString(R.string.dbNotifyLightFlgCol), booleanToStr(notifyData.light));
        cv.put(mContext.getString(R.string.dbNotifyLightColorCol), notifyData.lightColor);
        cv.put(mContext.getString(R.string.dbNotifyLightOnCol), notifyData.lightOn);
        cv.put(mContext.getString(R.string.dbNotifyLightOffCol), notifyData.lightOff);
        cv.put(mContext.getString(R.string.dbNotifyVibrateFlgCol), booleanToStr(notifyData.vibrate));
        cv.put(mContext.getString(R.string.dbNotifyVibrateDefFlgCol), booleanToStr(notifyData.vibrateDefault));
        //cv.put(spaContext.getString(R.string.dbNotifyVibratePatternCol), notifyData.vibratePattern);
        cv.put(mContext.getString(R.string.dbNotifySoundDefFlgCol), booleanToStr(notifyData.soundDefault));
        cv.put(mContext.getString(R.string.dbNotifySoundFlgCol), booleanToStr(notifyData.sound));
        cv.put(mContext.getString(R.string.dbNotifySoundFileCol), notifyData.soundFile.toString());

        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "insertNotificationEntry: Notification data for: PhoneID:" 
        		+ notifyData.phoneID);
    	long msgID = 0;
    	try {
    		msgID = appDB.insert(mContext.getString(R.string.dbNotifyTableName), 
    				mContext.getString(R.string.dbNotifyIDCol),
    				cv
    				);
    	} catch (Exception e) {
    		if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "insertNotificationEntry: Error executing Text Entry SQL");
    		if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "Error e:" + e.toString());
    		return -1;
    	}
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "insertNotificationEntry: Row created - insertNotificationEntry");
    	return msgID;
    }
    
    public int insertTxtEntry(FeedTextMsg msgData) {
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "insertTxtEntry: Inserting text entry...");
        ContentValues cv = new ContentValues();
        cv.put(mContext.getString(R.string.dbTxtMsgCol), msgData.msgTxt);
        cv.put(mContext.getString(R.string.dbTxtPhoneIDCol), msgData.msgPhoneID);
        cv.put(mContext.getString(R.string.dbTxtSenderFlgCol), msgData.getSenderFlg());
        cv.put(mContext.getString(R.string.dbTxtDraftFlgCol), msgData.getDraftFlg());
        cv.put(mContext.getString(R.string.dbTxtFlgsCol), msgData.msgFlgs);
        cv.put(mContext.getString(R.string.dbTxtTimestampCol), msgData.msgDate.getTime());
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "insertTxtEntry: Text entry: Msg:" 
        		+ msgData.msgTxt + " PID: " + msgData.msgPhoneID);
		if (EpicFeedConsts.DEBUG_DB) {
			java.text.DateFormat df = java.text.DateFormat.getDateTimeInstance(java.text.DateFormat.SHORT, java.text.DateFormat.SHORT);
			Log.i(TAG, TAG2 + "insertTxtEntry: MSG Date:" + df.format(msgData.msgTimestamp));
		}
    	int msgID = 0;
    	try {
    		msgID = (int) appDB.insert(mContext.getString(R.string.dbTxtTableName), 
    				mContext.getString(R.string.dbTxtIDCol),
    				cv
    				);
    	} catch (Exception e) {
    		if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "insertTxtEntry: Error executing Text Entry SQL");
    		if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "insertTxtEntry: Error e:" + e.toString());
    		return -1;
    	}
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "insertTxtEntry: Cursor created - insertTxtEntry");
		if (EpicFeedConsts.DEBUG_TEXT_MSG_LIST) {
			Log.i(TAG, "insertTxtEntry: mSpaUserAccount is null:" + (mSpaUserAccount == null));
			Log.i(TAG, "insertTxtEntry: mSpaUserAccount.timeout():" + mSpaUserAccount.timeout());
			Log.i(TAG, "insertTxtEntry: msgData.isNewMsg():" + msgData.isNewMsg());
			Log.i(TAG, "insertTxtEntry: mCheckMessageListener is null:" + (mCheckMessageListener == null));
		}
        if (mCheckMessageListener != null&&!mSpaUserAccount.timeout()&&msgData.isNewMsg()) {
        	mCheckMessageListener.onNewMsg();
    		if (EpicFeedConsts.DEBUG_TEXT_MSG_LIST)
    			Log.i(TAG, "insertTxtEntry: getCheckMessageListener.onNewMsg called");
        }
        if (mContactListListener != null&&!mSpaUserAccount.timeout()&&msgData.isNewMsg()) {
        	mContactListListener.onNewMsgs();
    		if (EpicFeedConsts.DEBUG_TEXT_MSG_LIST)
    			Log.i(TAG, "insertTxtEntry: mContactListListener.onNewMsgs");
        }
        return msgID;
    }
    
    public long insertNewPhoneIDEntry(String pID) {
    	if (pID == null | pID.trim().length() < 1) return -1;
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "insertNewPhoneIDEntry: Inserting phone ID...");
        ContentValues cv = new ContentValues();
        cv.put(mContext.getString(R.string.dbPhoneIDCol), pID);
        cv.put(mContext.getString(R.string.dbPhoneTypeCDCol), "");
        cv.put(mContext.getString(R.string.dbPhoneSecFlgCol), "N");
        cv.put(mContext.getString(R.string.dbPhoneNameCol), "");
        cv.put(mContext.getString(R.string.dbPhoneNameIDCol), "");
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "insertPhoneIDEntry: Phone ID: " 
        		+ pID);
    	long contactID = 0;
    	try {
    		contactID = appDB.insert(mContext.getString(R.string.dbPhoneIDTableName), 
    				mContext.getString(R.string.dbRowIDCol),
    				cv
    				);
    	} catch (Exception e) {
    		if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "insertPhoneIDEntry: Error executing PhoneID Entry SQL");
    	}
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "insertPhoneIDEntry: Phone ID inserted.");
    	return contactID;
    }
    
    public long insertNewContactFeed(ContactFeed feed) {
    	if (feed.getPhoneId() == null | feed.getPhoneId().trim().length() < 1) return -1;
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "insertNewContactFeed: Inserting Feed ID...");
        ContentValues cv = new ContentValues();
        cv.put(ContactFeed.DB_TABLE_COLUMNS[ContactFeed.PHONE_ID], feed.getPhoneId());
        cv.put(ContactFeed.DB_TABLE_COLUMNS[ContactFeed.USER_ID], feed.getFeedLoginId());
        cv.put(ContactFeed.DB_TABLE_COLUMNS[ContactFeed.FEED_TYPE], feed.getFeedType());
        cv.put(ContactFeed.DB_TABLE_COLUMNS[ContactFeed.PASSWORD_TXT], feed.getPasswordTxt());
        cv.put(ContactFeed.DB_TABLE_COLUMNS[ContactFeed.FEED_ID], feed.getFeedSystemId());
        cv.put(ContactFeed.DB_TABLE_COLUMNS[ContactFeed.FEED_FLGS], feed.getFeedFlgs());
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "insertNewContactFeed: Phone ID: " 
        		+ feed.getPhoneId());
    	long feedId = 0;
    	try {
    		feedId = appDB.insert(ContactFeed.DB_TABLE_NAME, 
    				ContactFeed.DB_TABLE_COLUMNS[ContactFeed._ID],
    				cv
    				);
    	} catch (Exception e) {
    		if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "insertNewContactFeed: Error executing Feed ID Entry SQL");
    	}
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "insertNewContactFeed: Feed ID inserted.");
        feed.setFeedId(feedId);
    	return feedId;
    }
    
    public boolean deleteContactFeed(ContactFeed feed) {
    	if (feed == null) return false;
        if (EpicFeedConsts.DEBUG_FB) Log.i(TAG, TAG2 + "deleteContactFeed: Deleting feed entry...");
        String DELETE_STR = 
        	"DELETE FROM " 
        	+ ContactFeed.DB_TABLE_NAME 
        	+ " WHERE "
        	+ ContactFeed.DB_TABLE_COLUMNS[ContactFeed._ID] + " = "
        	+ feed.getFeedId() + "; COMMIT;";
        if (EpicFeedConsts.DEBUG_FB) Log.i(TAG, TAG2 + "deleteContactFeed: Feed entry deleting: ID:" + 
        		feed.getFeedId());
        if (EpicFeedConsts.DEBUG_FB) Log.i(TAG, TAG2 + "deleteContactFeed: SQL : " + DELETE_STR);
    	boolean deleted = false;
    	try {
    		deleted = execSQLString(DELETE_STR);
    	} catch (Exception e) {
    		if (EpicFeedConsts.DEBUG_FB) Log.i(TAG, TAG2 + "deleteContactFeed: Error executing Log Entry SQL");
    	}
    	return deleted;
    }
    
    public long insertContact(EpicContact contact) {
    	return insertPhoneIDEntry(contact);
    }
    
    public long insertPhoneIDEntry(EpicContact contact) {
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "insertPhoneIDEntry: Inserting phone ID...");
        if (contact.phoneID == null | contact.phoneID.trim().length() < 1) return -1;
        ContentValues cv = new ContentValues();
        cv.put(mContext.getString(R.string.dbPhoneIDCol), contact.phoneID);
        cv.put(mContext.getString(R.string.dbPhoneTypeCDCol), contact.type);
        cv.put(mContext.getString(R.string.dbPhoneSecFlgCol), contact.secCd);
        cv.put(mContext.getString(R.string.dbPhoneNameCol), contact.name);
        cv.put(mContext.getString(R.string.dbPhoneFirstNameCol), contact.firstName);
        cv.put(mContext.getString(R.string.dbPhoneLastNameCol), contact.lastName);
        cv.put(mContext.getString(R.string.dbPhoneNameIDCol), contact.nameId);
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "insertPhoneIDEntry: Contact Phone ID: " 
        		+ contact.phoneID);
    	long contactID = 0;
    	try {
    		contactID = appDB.insert(mContext.getString(R.string.dbPhoneIDTableName), 
    				mContext.getString(R.string.dbRowIDCol),
    				cv
    				);
    	} catch (Exception e) {
    		if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "insertPhoneIDEntry: Error executing PhoneID Entry SQL");
    		if (EpicFeedConsts.DEBUG_DB) e.printStackTrace();
    	}
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "insertPhoneIDEntry: Phone ID inserted.");
    	return contactID;
    }
    
    public boolean deleteLogEntry(String logID) {
    	if (logID == null) logID = "0";
    	if (logID.length() < 1) logID = "0";
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "deleteLogEntry: Deleting log entry...");
        String DELETE_STR = 
        	"DELETE FROM " 
        	+ mContext.getString(R.string.dbLogTableName) 
        	+ " WHERE "
        	+ mContext.getString(R.string.dbLogIDCol) + " = "
        	+ logID + "; COMMIT;";
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "deleteLogEntry: Log entry deleting: ID:" + logID);
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "deleteLogEntry: SQL : " + DELETE_STR);
    	boolean deleted = false;
    	try {
    		deleted = execSQLString(DELETE_STR);
    	} catch (Exception e) {
    		if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "deleteLogEntry: Error executing Log Entry SQL");
    	}
    	return deleted;
    }
    
    public boolean deleteTxtEntry(String txtID) {
    	if (txtID == null) return false;
    	if (txtID.length() < 1) return false;
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "deleteTxtEntry: Deleting text msg entry...");
        String DELETE_STR = 
        	"DELETE FROM " 
        	+ mContext.getString(R.string.dbTxtTableName) 
        	+ " WHERE "
        	+ mContext.getString(R.string.dbTxtIDCol) + " = " + txtID + "; COMMIT;";
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "deleteTxtEntry: Txt entry deleting: ID:" + txtID);
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "deleteTxtEntry: SQL : " + DELETE_STR);
    	boolean deleted = false;
    	try {
    		deleted = execSQLString(DELETE_STR);
    	} catch (Exception e) {
    		if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "deleteTxtEntry: Error executing Text Delete SQL");
    	}
    	return deleted;
    }
    
    public boolean deleteAllContactLogs(String pID) {
    	if (pID == null) return false;
    	if (pID.length() < 1) return false;
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "deleteTxtThread: Deleting text msg entry...");
        String DELETE_STR = 
        	"DELETE FROM " 
        	+ mContext.getString(R.string.dbTxtTableName) 
        	+ " WHERE "
        	+ mContext.getString(R.string.dbTxtPhoneIDCol) + " = '" + pID + "'";
        DELETE_STR = DELETE_STR + "; COMMIT;";
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "deleteTxtThread: Txt entry deleting: PhoneID:" + pID);
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "deleteTxtThread: SQL : " + DELETE_STR);
    	boolean deleted = false;
    	try {
    		deleted = execSQLString(DELETE_STR);
    	} catch (Exception e) {
    		if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "deleteTxtThread: Error executing Text Thread Delete SQL");
    	}
    	return deleted;
    }
    
    public boolean deleteTxtThread(String pID, boolean includeLockedChecked) {
    	if (pID == null) return false;
    	if (pID.length() < 1) return false;
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "deleteTxtThread: Deleting text msg entry...");
        String DELETE_STR = 
        	"DELETE FROM " 
        	+ mContext.getString(R.string.dbTxtTableName) 
        	+ " WHERE "
        	+ mContext.getString(R.string.dbTxtPhoneIDCol) + " = '" + pID + "'";
        // add this for call log logic...
	    	@SuppressWarnings("unused")
			String tmpAddCallLogs = " AND ("
	    	+ mContext.getString(R.string.dbTxtFlgsCol) + " & "
	    	+ FeedTextMsg.CALL_LOG + ") != " + FeedTextMsg.CALL_LOG;
        if (!includeLockedChecked) DELETE_STR = DELETE_STR
        	+ " AND ("
        	+ mContext.getString(R.string.dbTxtFlgsCol) + " & "
        	+ FeedTextMsg.LOCK_MSG_FLG + ") != " + FeedTextMsg.LOCK_MSG_FLG;
        DELETE_STR = DELETE_STR + "; COMMIT;";
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "deleteTxtThread: Txt entry deleting: PhoneID:" + pID);
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "deleteTxtThread: SQL : " + DELETE_STR);
    	boolean deleted = false;
    	try {
    		deleted = execSQLString(DELETE_STR);
    	} catch (Exception e) {
    		if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "deleteTxtThread: Error executing Text Thread Delete SQL");
    	}
    	return deleted;
    }
    
    public boolean deleteCallLogs(String pID, boolean includeLockedChecked) {
    	if (pID == null) return false;
    	if (pID.length() < 1) return false;
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "deleteTxtThread: Deleting text msg entry...");
        String DELETE_STR = 
        	"DELETE FROM " 
        	+ mContext.getString(R.string.dbTxtTableName) 
        	+ " WHERE "
        	+ mContext.getString(R.string.dbTxtPhoneIDCol) + " = '" + pID + "'"
	    	+ " AND ("
	    	+ mContext.getString(R.string.dbTxtFlgsCol) + " & "
	    	+ FeedTextMsg.CALL_LOG + ") == " + FeedTextMsg.CALL_LOG;
        DELETE_STR = DELETE_STR + "; COMMIT;";
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "deleteTxtThread: Txt entry deleting: PhoneID:" + pID);
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "deleteTxtThread: SQL : " + DELETE_STR);
    	boolean deleted = false;
    	try {
    		deleted = execSQLString(DELETE_STR);
    	} catch (Exception e) {
    		if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "deleteTxtThread: Error executing Text Thread Delete SQL");
    	}
    	return deleted;
    }
    
    public boolean clearDraftMsgs(String pID) {
    	if (pID == null) return false;
    	if (pID.length() < 1) return false;
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "clearDraftMsgs: Deleting text msg entry...");
        String DELETE_STR = 
        	"DELETE FROM " 
        	+ mContext.getString(R.string.dbTxtTableName) 
        	+ " WHERE "
        	+ mContext.getString(R.string.dbTxtPhoneIDCol) + " = '" + pID + "'" 
        	+ " AND "
        	+ mContext.getString(R.string.dbTxtFlgsCol) + " & "
        	+ FeedTextMsg.DRAFT_MSG_FLG
        	+ "; COMMIT;";
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "clearDraftMsgs: Txt entry deleting: PhoneID:" + pID);
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "clearDraftMsgs: SQL : " + DELETE_STR);
    	boolean deleted = false;
    	try {
    		deleted = execSQLString(DELETE_STR);
    	} catch (Exception e) {
    		if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "clearDraftMsgs: Error executing Text Thread Delete SQL");
    	}
    	return deleted;
    }
    
    public boolean clearNewMsgs(String pID) {
    	if (pID == null) return false;
    	if (pID.length() < 1) return false;
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG , "clearNewMsgs: Clearing new messages");
        String UPDATE_STR = 
        	"UPDATE " 
        	+ mContext.getString(R.string.dbTxtTableName) 
        	+ " SET " + mContext.getString(R.string.dbTxtFlgsCol) 
        	+ " = " + mContext.getString(R.string.dbTxtFlgsCol) + " & "
        	+ ~FeedTextMsg.NEW_MSG_FLG
        	+ " WHERE "
        	+ mContext.getString(R.string.dbTxtPhoneIDCol) + " = '"+ pID + "'" 
        	+ " AND "
        	+ mContext.getString(R.string.dbTxtFlgsCol) + " & "
        	+ FeedTextMsg.NEW_MSG_FLG
        	+ "; COMMIT;";
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG , "clearNewMsgs: Clearing new msgs: PhoneID:" + pID);
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG , "clearNewMsgs: SQL : " + UPDATE_STR);
    	boolean updated = false;
    	try {
    		updated = execSQLString(UPDATE_STR);
    	} catch (Exception e) {
    		if (EpicFeedConsts.DEBUG_DB) Log.i(TAG , "clearNewMsgs: Error executing New Msg clear SQL");
    		if (EpicFeedConsts.DEBUG_DB) e.printStackTrace();
    	}
    	return updated;
    }

    public boolean unblockMsgs(String pID) {
    	if (pID == null) return false;
    	if (pID.length() < 1) return false;
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG , "unblockMsgs: Clearing new messages");
        String UPDATE_STR = 
        	"UPDATE " 
        	+ mContext.getString(R.string.dbTxtTableName) 
        	+ " SET " + mContext.getString(R.string.dbTxtFlgsCol) 
        	+ " = " + mContext.getString(R.string.dbTxtFlgsCol) + " & "
        	+ ~FeedTextMsg.BLOCKED_MSG_FLG + " & " + FeedTextMsg.NEW_MSG_FLG
        	+ " WHERE "
        	+ mContext.getString(R.string.dbTxtPhoneIDCol) + " = '"+ pID + "'" 
        	+ " AND "
        	+ mContext.getString(R.string.dbTxtFlgsCol) + " & "
        	+ FeedTextMsg.BLOCKED_MSG_FLG
        	+ "; COMMIT;";
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG , "unblockMsgs: Clearing new msgs: PhoneID:" + pID);
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG , "unblockMsgs: SQL : " + UPDATE_STR);
    	boolean updated = false;
    	try {
    		updated = execSQLString(UPDATE_STR);
    	} catch (Exception e) {
    		if (EpicFeedConsts.DEBUG_DB) Log.i(TAG , "clearNewMsgs: Error executing New Msg clear SQL");
    		if (EpicFeedConsts.DEBUG_DB) e.printStackTrace();
    	}
    	return updated;
    }

    public boolean setSendStatus(String msgID, int sendFlg) {
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG , "setSendStatus: settings flags: " + sendFlg);
        String UPDATE_STR = 
        	"UPDATE " 
        	+ mContext.getString(R.string.dbTxtTableName) 
        	+ " SET " + mContext.getString(R.string.dbTxtFlgsCol) 
        	+ " = " + mContext.getString(R.string.dbTxtFlgsCol) + " | "
        	+ sendFlg
        	+ " WHERE "
        	+ mContext.getString(R.string.dbTxtIDCol) + " = '"+ msgID + "'" 
        	+ "; COMMIT;";
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG , "setSendStatus: Clearing new msgs: msgID:" + msgID);
        if (EpicFeedConsts.DEBUG_DB) 
        	Log.i(TAG , "setSendStatus: SQL : " + UPDATE_STR);
    	boolean updated = false;
    	try {
    		updated = execSQLString(UPDATE_STR);
    	} catch (Exception e) {
    		if (EpicFeedConsts.DEBUG_DB) Log.i(TAG , "setSendStatus: Error executing New Msg clear SQL");
    		if (EpicFeedConsts.DEBUG_DB) e.printStackTrace();
    	}
   		if (EpicFeedConsts.DEBUG_DB)
    		Log.i(TAG, "setSendStatus: updated: " + updated + " - checking for listener");
    	return updated;
    }
    
    public void notifySentStatus() {
        if (EpicFeedConsts.DEBUG_TEXT_MSG_LIST) Log.i(TAG, TAG2 + "notifySentStatus: calling listener onSent and onNewMsg");
        if (mCheckMessageListener != null) {
        	mCheckMessageListener.onSentMsg();
    		if (EpicFeedConsts.DEBUG_TEXT_MSG_LIST)
    			Log.i(TAG, "setSendStatus: mCheckMessageListener.onSentMsg");
        }
        if (mContactListListener != null) {
        	mContactListListener.onNewMsgs();
    		if (EpicFeedConsts.DEBUG_TEXT_MSG_LIST)
    			Log.i(TAG, "setSendStatus: mContactListListener.onNewMsgs");
        }
    }

    public boolean setDeliveryStatus(String msgID, int deliveryFlg) {
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG , "setSendError: Clearing new messages");
        String UPDATE_STR = 
        	"UPDATE " 
        	+ mContext.getString(R.string.dbTxtTableName) 
        	+ " SET " + mContext.getString(R.string.dbTxtFlgsCol) 
        	+ " = " + mContext.getString(R.string.dbTxtFlgsCol) + " | "
        	+ deliveryFlg
        	+ " WHERE "
        	+ mContext.getString(R.string.dbTxtIDCol) + " = '"+ msgID + "'" 
        	+ "; COMMIT;";
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG , "setSendError: Clearing new msgs: msgID:" + msgID);
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG , "setSendError: SQL : " + UPDATE_STR);
    	boolean updated = false;
    	try {
    		updated = execSQLString(UPDATE_STR);
    	} catch (Exception e) {
    		if (EpicFeedConsts.DEBUG_DB) Log.i(TAG , "setSendError: Error executing New Msg clear SQL");
    		if (EpicFeedConsts.DEBUG_DB) e.printStackTrace();
    	}
    	return updated;
    }
    
    public void notifyDeliveryStatus() {
        if (EpicFeedConsts.DEBUG_TEXT_MSG_LIST) Log.i(TAG, TAG2 + "notifyDeliveryStatus: calling listener onDelivered and onNewMsg");
        if (mCheckMessageListener != null) 
        	mCheckMessageListener.onDeliveredMsg();
        if (mContactListListener != null) 
        	mContactListListener.onNewMsgs();
    }

    public long updateNotificationEntry(SpaTextNotification notifyData) {
        if (EpicFeedConsts.DEBUG_DB) 
        	Log.i(TAG, TAG2 + "updateNotificationEntry: Updating notification entry...");
        ContentValues cv = new ContentValues();
        cv.put(mContext.getString(R.string.dbNotifyBarFlgCol), booleanToStr(notifyData.statusbar));
        cv.put(mContext.getString(R.string.dbNotifyBarDefIconFlgCol), booleanToStr(notifyData.statusbarDefaultIcon));
        cv.put(mContext.getString(R.string.dbNotifyBarIconIDCol), notifyData.statusbarIcon);
        cv.put(mContext.getString(R.string.dbNotifyBarDefNotifyFlgCol), booleanToStr(notifyData.statusbarDefaultNotification));
        cv.put(mContext.getString(R.string.dbNotifyBarTickerMsgFlgCol), booleanToStr(notifyData.statusbarTickerUseTextMsg));
        cv.put(mContext.getString(R.string.dbNotifyBarDefTickerFlgCol), booleanToStr(notifyData.statusbarDefaultTicker));
        cv.put(mContext.getString(R.string.dbNotifyBarDefTitleFlgCol), booleanToStr(notifyData.statusbarDefaultTitle));
        cv.put(mContext.getString(R.string.dbNotifyBarDefTextFlgCol), booleanToStr(notifyData.statusbarDefaultText));
    	cv.put(mContext.getString(R.string.dbNotifyBarTickerCol), strNotNull(notifyData.statusbarTicker.toString()));
    	cv.put(mContext.getString(R.string.dbNotifyBarTitleCol), strNotNull(notifyData.statusbarTitle.toString()));
    	cv.put(mContext.getString(R.string.dbNotifyBarTextCol), strNotNull(notifyData.statusbarText.toString()));
        cv.put(mContext.getString(R.string.dbNotifyLightFlgCol), booleanToStr(notifyData.light));
        cv.put(mContext.getString(R.string.dbNotifyLightColorCol), notifyData.lightColor);
        cv.put(mContext.getString(R.string.dbNotifyLightOnCol), notifyData.lightOn);
        cv.put(mContext.getString(R.string.dbNotifyLightOffCol), notifyData.lightOff);
        cv.put(mContext.getString(R.string.dbNotifyVibrateFlgCol), booleanToStr(notifyData.vibrate));
        cv.put(mContext.getString(R.string.dbNotifyVibrateDefFlgCol), booleanToStr(notifyData.vibrateDefault));
        //cv.put(spaContext.getString(R.string.dbNotifyVibratePatternCol), notifyData.vibratePattern);
        cv.put(mContext.getString(R.string.dbNotifySoundDefFlgCol), booleanToStr(notifyData.soundDefault));
        cv.put(mContext.getString(R.string.dbNotifySoundFlgCol), booleanToStr(notifyData.sound));
        if (notifyData.soundFile.toString().length() > 0)
        	cv.put(mContext.getString(R.string.dbNotifySoundFileCol), strNotNull(notifyData.soundFile.toString()));

        if (EpicFeedConsts.DEBUG_DB) 
        	Log.i(TAG, TAG2 + "updateNotificationEntry: Notification data for: PhoneID:" 
        		+ notifyData.phoneID);
    	long msgID = 0;
    	String[] whereArgs = {notifyData.phoneID};
    	try {
    		msgID = appDB.update(mContext.getString(R.string.dbNotifyTableName), 
    				cv,
    				mContext.getString(R.string.dbNotifyPhoneIDCol) + " = ? ",
    				whereArgs
    				);
    	} catch (Exception e) {
    		if (EpicFeedConsts.DEBUG_DB) 
    			Log.i(TAG, TAG2 + "updateNotificationEntry: Error executing Text Entry SQL");
    		if (EpicFeedConsts.DEBUG_DB) 
    			Log.i(TAG, TAG2 + "Error e:" + e.toString());
    		return -1;
    	}
      if (EpicFeedConsts.DEBUG_DB) 
    		Log.i(TAG, TAG2 + "updateNotificationEntry: Row updated");
      if (EpicFeedConsts.DEBUG_DB) 
    		Log.i(TAG, TAG2 + "updateNotificationEntry: notifyData.statusbarIcon: " 
    				+ notifyData.statusbarIcon);
    	return msgID;
    }
    
    public long updateDraftMsg(FeedTextMsg tmpMsg) {
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "updateDraftMsg: Updating draft entry...");
        ContentValues cv = new ContentValues();
       	cv.put(mContext.getString(R.string.dbTxtPhoneIDCol), strNotNull(tmpMsg.msgPhoneID));
       	cv.put(mContext.getString(R.string.dbTxtMsgCol), strNotNull(tmpMsg.msgTxt));

        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "updateDraftMsg: Draft msg data for: PhoneID:" 
        		+ tmpMsg.msgPhoneID);
    	long msgID = 0;
    	String[] whereArgs = {tmpMsg.msgPhoneID};
    	try {
    		msgID = appDB.update(mContext.getString(R.string.dbTxtTableName), 
    				cv,
    				mContext.getString(R.string.dbTxtPhoneIDCol) + " = ? "
    				+ " AND "
    	        	+ mContext.getString(R.string.dbTxtFlgsCol) + " & "
    	        	+ FeedTextMsg.DRAFT_MSG_FLG,
    				whereArgs
    				);
    	} catch (Exception e) {
    		if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "updateDraftMsg: Error executing Text Entry SQL");
    		if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "Error e:" + e.toString());
    		return -1;
    	}
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "updateDraftMsg: Row updated");
    	return msgID;
    }
    
    public long updateMsgFlgs(FeedTextMsg tmpMsg) {
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "updateMsgFlgs: Updating flags entry...");
        ContentValues cv = new ContentValues();
       	cv.put(mContext.getString(R.string.dbTxtFlgsCol), tmpMsg.msgFlgs);

        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "updateMsgFlgs: Msg data for: PhoneID:" 
        		+ tmpMsg.msgPhoneID);
    	long msgID = 0;
    	String[] whereArgs = {tmpMsg.msgID};
    	try {
    		msgID = appDB.update(mContext.getString(R.string.dbTxtTableName), 
    				cv,
    				mContext.getString(R.string.dbTxtIDCol) + " = ? ",
    				whereArgs
    				);
    	} catch (Exception e) {
    		if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "updateMsgFlgs: Error executing Text Entry SQL");
    		if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "Error e:" + e.toString());
    		return -1;
    	}
        if (EpicFeedConsts.DEBUG_TEXT_MSG_LIST) Log.i(TAG, TAG2 + "updateMsgFlgs: Row updated, calling onDelivered");
        if (tmpMsg.deliveryUpdate&&mCheckMessageListener != null) 
        	mCheckMessageListener.onDeliveredMsg();
        if (tmpMsg.sendUpdate&&mCheckMessageListener != null) mCheckMessageListener.onSentMsg();
    	return msgID;
    }
    
    public boolean updateContact(EpicContact contact) {
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "updateContact: updateContact");
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "updateContact: " + contact.phoneID + " ; contactFlags: " + contact.secCd);
        ContentValues cv = new ContentValues();
        cv.put(mContext.getString(R.string.dbPhoneTypeCDCol), strNotNull(contact.type));
        cv.put(mContext.getString(R.string.dbPhoneSecFlgCol), strNotNull(contact.secCd+""));
        cv.put(mContext.getString(R.string.dbPhoneNameCol), strNotNull(contact.name));
        cv.put(mContext.getString(R.string.dbPhoneFirstNameCol), strNotNull(contact.firstName));
        cv.put(mContext.getString(R.string.dbPhoneLastNameCol), strNotNull(contact.lastName));
        cv.put(mContext.getString(R.string.dbPhoneNameIDCol), strNotNull(contact.nameId));
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "updateContact: Clearing new msgs: PhoneID:" + contact.phoneID);
    	long contactID = 0;
    	try {
        	String[] whereArgs = {contact.phoneID};
        	contactID = appDB.update(mContext.getString(R.string.dbPhoneIDTableName) , 
    				cv,
    				mContext.getString(R.string.dbPhoneIDCol) + " = ? ",
    				whereArgs
    				);
    	} catch (Exception e) {
    		if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "updateContact: Error executing New Msg clear SQL");
    		if (EpicFeedConsts.DEBUG_DB) e.printStackTrace();
    		return false;
    	}
    	return (contactID > 0);
    }
    
    public boolean deletePhoneID(String pID) {
    	if (pID == null) return false;
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "deletePhoneID: Deleting text msg entry...");
        String DELETE_STR = 
        	"DELETE FROM " 
        	+ mContext.getString(R.string.dbPhoneIDTableName) 
        	+ " WHERE "
        	+ mContext.getString(R.string.dbPhoneIDCol) + " = '" + pID + "'; COMMIT;";
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "deletePhoneID: Deleting: PhoneID:" + pID);
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "deletePhoneID: SQL : " + DELETE_STR);
    	boolean deleted = false;
    	try {
    		deleted = execSQLString(DELETE_STR);
    	} catch (Exception e) {
    		if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "deletePhoneID: Error executing PhoneID Delete SQL");
    		if (EpicFeedConsts.DEBUG_DB) e.printStackTrace();
    	}
    	return deleted;
    }
    
    public boolean deleteNotification(String pID) {
    	if (pID == null) return false;
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "deleteNotification: Deleting notification entry for: " + pID);
        String DELETE_STR = 
        	"DELETE FROM " 
        	+ mContext.getString(R.string.dbNotifyTableName) 
        	+ " WHERE "
        	+ mContext.getString(R.string.dbNotifyPhoneIDCol) + " = '" + pID + "'; COMMIT;";
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "deleteNotification: Deleting: PhoneID:" + pID);
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "deleteNotification: SQL : " + DELETE_STR);
    	boolean deleted = false;
    	try {
    		deleted = execSQLString(DELETE_STR);
    	} catch (Exception e) {
    		if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "deleteNotification: Error executing PhoneID Delete SQL");
    		if (EpicFeedConsts.DEBUG_DB) e.printStackTrace();
    	}
    	return deleted;
    }
    
    public boolean deleteAllPhoneID() {
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "deleteAllPhoneID: Deleting text msg entry...");
        String DELETE_STR = 
        	"DELETE FROM " + mContext.getString(R.string.dbPhoneIDTableName) + "; COMMIT;";
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "deleteAllPhoneID: SQL : " + DELETE_STR);
    	boolean deleted = false;
    	try {
    		deleted = execSQLString(DELETE_STR);
    	} catch (Exception e) {
    		if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "deleteAllPhoneID: Error executing PhoneID Delete SQL");
    	}
    	return deleted;
    }
    
    public boolean deleteLogEntry() {
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "deleteLogEntry: Deleting log entry...");
        String DELETE_STR = 
        	"DELETE FROM " + mContext.getString(R.string.dbLogTableName) + "; COMMIT;";
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "deleteLogEntry: Log entry deleting all.");
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "deleteLogEntry: SQL : " + DELETE_STR);
    	boolean deleted = false;
    	try {
    		deleted = execSQLString(DELETE_STR);
    	} catch (Exception e) {
    		if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "deleteLogEntry: Error executing Log Entry SQL");
    	}
    	return deleted;
    }
    
    public FeedTextMsg[] getContactMessages(String pID, boolean includeBlocked, boolean asc) throws SQLException {
    	//---retrieves a particular phone conversation---
    	if (pID == null) pID = "0";
    	if (pID.length() <1) pID = "0";
    	String msgOrder = " ASC";
    	if (!asc) msgOrder = " DESC";
    	Cursor spaCursor;
    	if (includeBlocked) 
	    	spaCursor =
	        	appDB.query(true, mContext.getString(R.string.dbTxtTableName), new String[] {
	            	mContext.getString(R.string.dbTxtPhoneIDCol),
	            	mContext.getString(R.string.dbTxtTimestampCol),
	            	mContext.getString(R.string.dbTxtMsgCol),
	            	mContext.getString(R.string.dbTxtFlgsCol),
	            	mContext.getString(R.string.dbTxtSenderFlgCol),
	                mContext.getString(R.string.dbTxtIDCol)
	                }, 
	                mContext.getString(R.string.dbTxtPhoneIDCol) + "= '" + pID + "'"
	                + " AND " 
	                + mContext.getString(R.string.dbTxtFlgsCol) + " & "
	                + ~FeedTextMsg.DRAFT_MSG_FLG, 
	                null,
	                null, 
	                null, 
	                mContext.getString(R.string.dbTxtIDCol) + msgOrder, 
	                null);
    	else
	    	spaCursor =
	        	appDB.query(true, mContext.getString(R.string.dbTxtTableName), new String[] {
	            	mContext.getString(R.string.dbTxtPhoneIDCol),
	            	mContext.getString(R.string.dbTxtTimestampCol),
	            	mContext.getString(R.string.dbTxtMsgCol),
	            	mContext.getString(R.string.dbTxtFlgsCol),
	                mContext.getString(R.string.dbTxtIDCol)
	                }, 
	                mContext.getString(R.string.dbTxtPhoneIDCol) + "= '" + pID + "'"
	                + " AND " 
	                + mContext.getString(R.string.dbTxtFlgsCol) + " & "
	                + ~FeedTextMsg.DRAFT_MSG_FLG + " & "
	                + ~FeedTextMsg.BLOCKED_MSG_FLG, 
	                null,
	                null, 
	                null, 
	                mContext.getString(R.string.dbTxtIDCol) + msgOrder, 
	                null);

        if (spaCursor != null) {
        	spaCursor.moveToFirst();
        } else return null;
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "getContactMessages: New SQL Cursor - execTxtQuerySQL() - pID:" + pID);
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "getContactMessages: New SQL Cursor #rows:" + spaCursor.getCount());
    	FeedTextMsg[] msgs = new FeedTextMsg[spaCursor.getCount()];
        int count = 0;
    	while (!spaCursor.isAfterLast()){
    		msgs[count] = new FeedTextMsg();
            if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "getContactMessages: Cursor data: dbTxtIDCol:" 
            		+ spaCursor.getString(spaCursor.getColumnIndex(mContext.getString(R.string.dbTxtIDCol))));
        	msgs[count].msgID = spaCursor.getString(spaCursor.getColumnIndex(mContext.getString(R.string.dbTxtIDCol)));
        	msgs[count].msgPhoneID = spaCursor.getString(spaCursor.getColumnIndex(mContext.getString(R.string.dbTxtPhoneIDCol)));
        	msgs[count].msgFlgs = spaCursor.getInt(spaCursor.getColumnIndex(mContext.getString(R.string.dbTxtFlgsCol)));
        	msgs[count].msgTxt = spaCursor.getString(spaCursor.getColumnIndex(mContext.getString(R.string.dbTxtMsgCol)));
        	try {
        		Calendar c = Calendar.getInstance();
        		c.setTimeInMillis(spaCursor.getLong(spaCursor.getColumnIndex(mContext.getString(R.string.dbTxtTimestampCol))));
        		msgs[count].msgDate = c.getTime();
        		if (EpicFeedConsts.DEBUG_DB) {
        			java.text.DateFormat df = java.text.DateFormat.getDateTimeInstance(java.text.DateFormat.SHORT, java.text.DateFormat.SHORT);
        			Log.i(TAG, TAG2 + "getContactMessages: msgID:" + df.format(msgs[count].msgTimestamp));
        		}

        	} catch (Exception e) {
                if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "getContactMessages: Error getting date: msgID:" + msgs[count].msgID);        		
        	}
        	count++;
        	spaCursor.moveToNext();
        }
    	spaCursor.close();
        return msgs;
    }
    
    public FeedTextMsg getMessage(String msgId) throws SQLException {
    	//---retrieves a particular phone conversation---
    	if (msgId == null) return new FeedTextMsg();
    	Cursor spaCursor;
    	spaCursor =
        	appDB.query(true, mContext.getString(R.string.dbTxtTableName), new String[] {
            	mContext.getString(R.string.dbTxtPhoneIDCol),
            	mContext.getString(R.string.dbTxtTimestampCol),
            	mContext.getString(R.string.dbTxtMsgCol),
            	mContext.getString(R.string.dbTxtFlgsCol),
            	mContext.getString(R.string.dbTxtSenderFlgCol),
                mContext.getString(R.string.dbTxtIDCol)
                }, 
                mContext.getString(R.string.dbTxtIDCol) + "= '" + msgId + "'", 
                null,
                null, 
                null, 
                mContext.getString(R.string.dbTxtIDCol) + " DESC", 
                null);
        if (spaCursor != null) {
        	spaCursor.moveToFirst();
        } else return new FeedTextMsg();
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "getMessage: New SQL Cursor - getMessage() - msgId:" + msgId);
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "getMessage: New SQL Cursor #rows:" + spaCursor.getCount());
    	FeedTextMsg msg = new FeedTextMsg();
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "getMessage: Cursor data: dbTxtIDCol:" 
        		+ spaCursor.getString(spaCursor.getColumnIndex(mContext.getString(R.string.dbTxtIDCol))));
    	msg.msgID = spaCursor.getString(spaCursor.getColumnIndex(mContext.getString(R.string.dbTxtIDCol)));
    	msg.msgPhoneID = spaCursor.getString(spaCursor.getColumnIndex(mContext.getString(R.string.dbTxtPhoneIDCol)));
    	msg.msgFlgs = spaCursor.getInt(spaCursor.getColumnIndex(mContext.getString(R.string.dbTxtFlgsCol)));
    	msg.msgTxt = spaCursor.getString(spaCursor.getColumnIndex(mContext.getString(R.string.dbTxtMsgCol)));
    	try {
    		Calendar c = Calendar.getInstance();
    		c.setTimeInMillis(spaCursor.getLong(spaCursor.getColumnIndex(mContext.getString(R.string.dbTxtTimestampCol))));
    		msg.msgDate = c.getTime();
    		if (EpicFeedConsts.DEBUG_DB) {
    			java.text.DateFormat df = java.text.DateFormat.getDateTimeInstance(java.text.DateFormat.SHORT, java.text.DateFormat.SHORT);
    			Log.i(TAG, TAG2 + "getMessage: msgID:" + df.format(msg.msgTimestamp));
    		}

    	} catch (Exception e) {
            if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "getContactMessages: Error getting date: msgID:" + msg.msgID);        		
    	}
    	//spaCursor.moveToNext();
    	spaCursor.close();
        return msg;
    }
    
    public String[] getContactOrder() throws SQLException {
    	//---retrieves the contact list order of contacts---
    	Cursor spaCursor =
	        	appDB.query(false, mContext.getString(R.string.dbTxtTableName), new String[] {
	            	mContext.getString(R.string.dbTxtPhoneIDCol),
	            	"MAX(" + mContext.getString(R.string.dbTxtIDCol) + ") AS MSG_ID_MAX",
	            	"MAX(" + mContext.getString(R.string.dbTxtFlgsCol) + " & "
	            	+ FeedTextMsg.NEW_MSG_FLG
	            	+ ") AS MSG_NEW"
	                }, 
	                mContext.getString(R.string.dbTxtFlgsCol) + " & "
	                + ~FeedTextMsg.DRAFT_MSG_FLG + " & "
	                + ~FeedTextMsg.BLOCKED_MSG_FLG, 
	                null,
	                mContext.getString(R.string.dbTxtPhoneIDCol), 
	                null, 
	                "MSG_NEW DESC, " +
	                "MSG_ID_MAX DESC", 
	                null);

        if (spaCursor != null) {
        	spaCursor.moveToFirst();
        } else return null;
        if (EpicFeedConsts.DEBUG_DB) 
        	Log.i(TAG, TAG2 + "getContactOrder: New SQL Cursor #rows:" + spaCursor.getCount());
        int count = 0;
    	String[] contacts = new String[spaCursor.getCount()];
    	while (!spaCursor.isAfterLast()){
    		String pID = spaCursor.getString(spaCursor.getColumnIndex(mContext.getString(R.string.dbTxtPhoneIDCol)));
    		if (pID != null) {
    			contacts[count] = pID;
            	count++;
    		}
            if (EpicFeedConsts.DEBUG_DB) 
            	Log.i(TAG, TAG2 + "getContactOrder: Cursor data: dbTxtPhoneIDCol:" 
            		+ spaCursor.getString(spaCursor.getColumnIndex(mContext.getString(R.string.dbTxtPhoneIDCol))));
        	spaCursor.moveToNext();
        }
    	if (contacts.length != count) {
    		String[] tmpContacts = new String[count];
    		count = 0;
    		for (int i = 0; i < contacts.length; i++) {
    			if (contacts[i] != null) {
    				tmpContacts[count] = contacts[i];
        			count++;
    			}
    		}
    		contacts = tmpContacts;
    	}
    	spaCursor.close();
        return contacts;
    }
    
    public FeedTextMsg[] execTxtQuerySQL(String pID, boolean asc) throws SQLException {
    	return getContactMessages(pID, false, asc);
    }
    
    public FeedTextMsg execTxtDraftQuerySQL(String pID) throws SQLException {
    	//---retrieves a particular phone conversation---
    	if (pID == null) pID = "0";
    	if (pID.length() <1) pID = "0";
    	Cursor spaCursor =
        	appDB.query(true, mContext.getString(R.string.dbTxtTableName), new String[] {
            	mContext.getString(R.string.dbTxtPhoneIDCol),
            	mContext.getString(R.string.dbTxtTimestampCol),
            	mContext.getString(R.string.dbTxtMsgCol),
            	mContext.getString(R.string.dbTxtFlgsCol),
                mContext.getString(R.string.dbTxtIDCol)
                }, 
                mContext.getString(R.string.dbTxtPhoneIDCol) + " = '" + pID + "'" 
                + " AND " 
                + mContext.getString(R.string.dbTxtFlgsCol) + " & "
                + FeedTextMsg.DRAFT_MSG_FLG, 
                null,
                null, 
                null, 
                mContext.getString(R.string.dbTxtIDCol) + " DESC", 
                null);
    	FeedTextMsg draftMsg = new FeedTextMsg();
    	draftMsg.setDraftMsg(true);
    	draftMsg.msgPhoneID = pID;
    	draftMsg.msgTxt = "";
        if (spaCursor != null) {
        	spaCursor.moveToFirst();
        } else return draftMsg; 
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "execTxtDraftQuerySQL() - pID:" + pID);
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "execTxtDraftQuerySQL #rows:" + spaCursor.getCount());
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "execTxtDraftQuerySQL: Data: dbTxtIDCol:" 
        		+ spaCursor.getString(spaCursor.getColumnIndex(mContext.getString(R.string.dbTxtIDCol))));
        draftMsg.msgID = spaCursor.getString(spaCursor.getColumnIndex(mContext.getString(R.string.dbTxtIDCol)));
        draftMsg.msgPhoneID = spaCursor.getString(spaCursor.getColumnIndex(mContext.getString(R.string.dbTxtPhoneIDCol)));
        draftMsg.msgFlgs = spaCursor.getInt(spaCursor.getColumnIndex(mContext.getString(R.string.dbTxtFlgsCol)));
        draftMsg.msgTxt = spaCursor.getString(spaCursor.getColumnIndex(mContext.getString(R.string.dbTxtMsgCol)));
    	try {
    		Calendar c = Calendar.getInstance();
    		c.setTimeInMillis(spaCursor.getLong(spaCursor.getColumnIndex(mContext.getString(R.string.dbTxtTimestampCol))));
    		draftMsg.msgDate = c.getTime();
    		if (EpicFeedConsts.DEBUG_DB) {
    			java.text.DateFormat df = java.text.DateFormat.getDateTimeInstance(java.text.DateFormat.SHORT, java.text.DateFormat.SHORT);
    			Log.i(TAG, TAG2 + "Date: msgID:" + df.format(draftMsg.msgTimestamp));
    		}

    	} catch (Exception e) {
            if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "execTxtDraftQuerySQL: Error getting date: msgID:" + draftMsg.msgID);        		
    	}
    	spaCursor.close();
        return draftMsg;
    }
    
    public List<EpicContact> allContacts() {
    	return execContactQuerySQL();
    }
     
    public List<EpicContact> execContactQuerySQL() throws SQLException {
    	//---retrieves a particular phone conversation---
    	Cursor spaCursor =
        	appDB.query(true, mContext.getString(R.string.dbPhoneIDTableName), new String[] {
            	mContext.getString(R.string.dbPhoneIDCol),
            	mContext.getString(R.string.dbPhoneTypeCDCol),
            	mContext.getString(R.string.dbPhoneSecFlgCol),
            	mContext.getString(R.string.dbPhoneNameCol),
            	mContext.getString(R.string.dbPhoneFirstNameCol),
            	mContext.getString(R.string.dbPhoneLastNameCol),
            	mContext.getString(R.string.dbPhoneNameIDCol)
                }, 
                null, 
                null,
                null, 
                null, 
                mContext.getString(R.string.dbPhoneIDCol) + " DESC", 
                null);
        if (spaCursor != null) {
        	spaCursor.moveToFirst();
        } else return new ArrayList<EpicContact>();
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "New SQL Cursor - All contacts - #rows:" + spaCursor.getCount());
    	ArrayList<EpicContact> contacts = new ArrayList<EpicContact>();
    	while (!spaCursor.isAfterLast()){
    		EpicContact tmpContact = new EpicContact(
    				spaCursor.getString(spaCursor.getColumnIndex(mContext.getString(R.string.dbPhoneIDCol))));
    		tmpContact.name = spaCursor.getString(spaCursor.getColumnIndex(mContext.getString(R.string.dbPhoneNameCol)));
        	if (tmpContact.name == null) tmpContact.name = "";
    		tmpContact.firstName = spaCursor.getString(spaCursor.getColumnIndex(mContext.getString(R.string.dbPhoneFirstNameCol)));
        	if (tmpContact.firstName == null) tmpContact.firstName = "";
    		tmpContact.lastName = spaCursor.getString(spaCursor.getColumnIndex(mContext.getString(R.string.dbPhoneLastNameCol)));
        	if (tmpContact.lastName == null) tmpContact.lastName = "";
        	tmpContact.nameId = spaCursor.getString(spaCursor.getColumnIndex(mContext.getString(R.string.dbPhoneNameIDCol)));
        	if (tmpContact.nameId == null) tmpContact.nameId = "";
        	String flagsStr = spaCursor.getString(spaCursor.getColumnIndex(mContext.getString(R.string.dbPhoneSecFlgCol)));
            int contactFlags = 0;
        	if (flagsStr.equals("Y")) contactFlags = EpicContact.SPA_ID_FLG;
        	else if (flagsStr.equals("B")) contactFlags = EpicContact.BLOCK_MSGS_FLG;
        	else if (flagsStr.equals("N")) contactFlags = 0;
        	else if (flagsStr.length() < 1) contactFlags = 0;
        	else contactFlags = Integer.valueOf(flagsStr);
            tmpContact.setSpaId(
           			(EpicContact.SPA_ID_FLG & contactFlags) == EpicContact.SPA_ID_FLG);
            tmpContact.secCd = contactFlags;
            if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "execContactQuerySQL: " + tmpContact.phoneID 
            		+ " ; saved blocked msgs: " + tmpContact.saveBlockedMsgs() + " ; contactFlags: " + contactFlags);
        	contacts.add(tmpContact);
        	spaCursor.moveToNext();
        }
    	spaCursor.close();
        return contacts;
    }
     
    public EpicContact getContact(String pID) throws SQLException {
    	pID = PhoneNumberUtils.stripSeparators(pID);
    	pID = pID.replaceAll("[\\D]", "");	// replace any nondigit character
    	if (pID.length() <1) return new EpicContact("");
    	//---retrieves a particular phone conversation---
    	Cursor spaCursor =
        	appDB.query(true, mContext.getString(R.string.dbPhoneIDTableName), new String[] {
            	mContext.getString(R.string.dbPhoneIDCol),
            	mContext.getString(R.string.dbPhoneTypeCDCol),
            	mContext.getString(R.string.dbPhoneSecFlgCol),
            	mContext.getString(R.string.dbPhoneNameCol),
            	mContext.getString(R.string.dbPhoneFirstNameCol),
            	mContext.getString(R.string.dbPhoneLastNameCol),
            	mContext.getString(R.string.dbPhoneNameIDCol)
                }, 
                mContext.getString(R.string.dbPhoneIDCol) + " = '" + pID + "'", 
                null,
                null, 
                null, 
                mContext.getString(R.string.dbPhoneIDCol) + " DESC", 
                null);
    	EpicContact contact = new EpicContact(pID);
        if (spaCursor != null) {
        	spaCursor.moveToFirst();
        } else return contact;
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "getContact: Cursor - Get contact: " + pID);
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "getContact: Cursor #rows:" + spaCursor.getCount());
    	if (spaCursor.getCount() < 1) return contact;
    	if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "getContact: Cursor data: dbPhoneIDCol:" 
        	+ spaCursor.getString(spaCursor.getColumnIndex(mContext.getString(R.string.dbPhoneIDCol))));
    	contact.name = spaCursor.getString(spaCursor.getColumnIndex(mContext.getString(R.string.dbPhoneNameCol)));
        if (contact.name == null) contact.name = "";
		contact.firstName = spaCursor.getString(spaCursor.getColumnIndex(mContext.getString(R.string.dbPhoneFirstNameCol)));
    	if (contact.firstName == null) contact.firstName = "";
		contact.lastName = spaCursor.getString(spaCursor.getColumnIndex(mContext.getString(R.string.dbPhoneLastNameCol)));
    	if (contact.lastName == null) contact.lastName = "";
        contact.nameId = spaCursor.getString(spaCursor.getColumnIndex(mContext.getString(R.string.dbPhoneNameIDCol)));
        if (contact.nameId == null) contact.nameId = "";
    	String flagsStr = spaCursor.getString(spaCursor.getColumnIndex(mContext.getString(R.string.dbPhoneSecFlgCol)));
        int contactFlags = 0;
    	if (flagsStr.equals("Y")) contactFlags = EpicContact.SPA_ID_FLG;
    	else if (flagsStr.equals("B")) contactFlags = EpicContact.BLOCK_MSGS_FLG;
    	else if (flagsStr.equals("N")) contactFlags = 0;
    	else if (flagsStr.length() < 1) contactFlags = 0;
    	else contactFlags = Integer.valueOf(flagsStr);
        contact.setSpaId(
    			(EpicContact.SPA_ID_FLG & contactFlags) == EpicContact.SPA_ID_FLG);
        contact.secCd = contactFlags;
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "getContact: " + pID + " ; contactFlags: " + contact.secCd);
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "getContact: " + pID + " ; saved blocked msgs: " + contact.saveBlockedMsgs());
    	spaCursor.close();
        return contact;
    }
     
    public List<ContactFeed> getContactFeeds(String pID) throws SQLException {
    	pID = PhoneNumberUtils.stripSeparators(pID);
    	pID = pID.replaceAll("[\\D]", "");
    	if (pID.length() <1) {
    		return new ArrayList<ContactFeed>();
    	}
    	//---retrieves a particular phone conversation---
    	Cursor spaCursor =
        	appDB.query(true, ContactFeed.DB_TABLE_NAME, 
        		ContactFeed.DB_TABLE_COLUMNS, 
                ContactFeed.DB_TABLE_COLUMNS[ContactFeed.PHONE_ID] + " = '" + pID + "'", 
                null,
                null, 
                null, 
                ContactFeed.DB_TABLE_COLUMNS[ContactFeed.FEED_ACTIVITY_TS] + " DESC", 
                null);
    	ArrayList<ContactFeed> contactFeeds = new ArrayList<ContactFeed>();
        if (spaCursor == null) return contactFeeds;
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "getContactFeeds: Cursor Get getContactFeeds: " + pID + " #rows:" + spaCursor.getCount());
    	if (spaCursor.getCount() < 1) {
    		spaCursor.close();
    		return contactFeeds;
    	}
    	while (spaCursor.moveToNext()) {
	    	if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "getContactFeeds: Cursor feed: feedType:" 
	        	+ spaCursor.getString(spaCursor.getColumnIndex(ContactFeed.DB_TABLE_COLUMNS[ContactFeed.FEED_TYPE])));
	    	ContactFeed contactFeed = new ContactFeed(spaCursor.getString(
	    			spaCursor.getColumnIndex(ContactFeed.DB_TABLE_COLUMNS[ContactFeed.PHONE_ID])));
	    	contactFeed.setFeedId(spaCursor.getLong(
	    			spaCursor.getColumnIndex(ContactFeed.DB_TABLE_COLUMNS[ContactFeed._ID])));
	    	contactFeed.setFeedType(spaCursor.getString(
	    			spaCursor.getColumnIndex(ContactFeed.DB_TABLE_COLUMNS[ContactFeed.FEED_TYPE])));
	    	contactFeed.setFeedSystemId(spaCursor.getString(
	    			spaCursor.getColumnIndex(ContactFeed.DB_TABLE_COLUMNS[ContactFeed.FEED_ID])));
	    	contactFeed.setFeedLoginId(spaCursor.getString(
	    			spaCursor.getColumnIndex(ContactFeed.DB_TABLE_COLUMNS[ContactFeed.USER_ID])));
	    	contactFeed.setPasswordTxt(spaCursor.getString(
	    			spaCursor.getColumnIndex(ContactFeed.DB_TABLE_COLUMNS[ContactFeed.PASSWORD_TXT])));
	    	contactFeed.setFeedFlgs(spaCursor.getInt(
	    			spaCursor.getColumnIndex(ContactFeed.DB_TABLE_COLUMNS[ContactFeed.FEED_FLGS])));
	    	contactFeed.setFeedUpdates(spaCursor.getInt(
	    			spaCursor.getColumnIndex(ContactFeed.DB_TABLE_COLUMNS[ContactFeed.FEED_UPDATE_COUNT])));
	    	contactFeed.setFeedRecentActivityTimestamp(spaCursor.getLong(
	    			spaCursor.getColumnIndex(ContactFeed.DB_TABLE_COLUMNS[ContactFeed.FEED_ACTIVITY_TS])));
	    	contactFeed.setFeedLastCheckedTimestamp(spaCursor.getLong(
	    			spaCursor.getColumnIndex(ContactFeed.DB_TABLE_COLUMNS[ContactFeed.FEED_CHECKED_TS])));
	    	contactFeeds.add(contactFeed);
    	}
    	spaCursor.close();
        return contactFeeds;
    }
     
    public SpaTextNotification getNotification(String pID) throws SQLException {
    	//---retrieves a particular phone conversation---
    	Cursor spaCursor =
        	appDB.query(true, mContext.getString(R.string.dbNotifyTableName), new String[] {
            	mContext.getString(R.string.dbNotifyPhoneIDCol),
            	mContext.getString(R.string.dbNotifyBarFlgCol),
            	mContext.getString(R.string.dbNotifyBarDefIconFlgCol),
            	mContext.getString(R.string.dbNotifyBarTickerCol),
            	mContext.getString(R.string.dbNotifyBarTitleCol),
            	mContext.getString(R.string.dbNotifyBarTextCol),
            	mContext.getString(R.string.dbNotifyBarIconIDCol),
            	mContext.getString(R.string.dbNotifyBarTickerMsgFlgCol),
            	mContext.getString(R.string.dbNotifyBarDefNotifyFlgCol),
            	mContext.getString(R.string.dbNotifyBarDefTickerFlgCol),
            	mContext.getString(R.string.dbNotifyBarDefTitleFlgCol),
            	mContext.getString(R.string.dbNotifyBarDefTextFlgCol),
            	mContext.getString(R.string.dbNotifyLightFlgCol),
            	mContext.getString(R.string.dbNotifyLightColorCol),
            	mContext.getString(R.string.dbNotifyLightOnCol),
            	mContext.getString(R.string.dbNotifyLightOffCol),
            	mContext.getString(R.string.dbNotifyVibrateFlgCol),
            	mContext.getString(R.string.dbNotifyVibrateDefFlgCol),
            	mContext.getString(R.string.dbNotifyVibratePatternCol),
            	mContext.getString(R.string.dbNotifySoundDefFlgCol),
            	mContext.getString(R.string.dbNotifySoundFlgCol),
            	mContext.getString(R.string.dbNotifySoundFileCol)
                }, 
                mContext.getString(R.string.dbNotifyPhoneIDCol) + " = '" + pID + "'", 
                null,
                null, 
                null, 
                null, 
                null);
    	SpaTextNotification notifyOptions = new SpaTextNotification(pID);
        if (spaCursor != null) {
        	spaCursor.moveToFirst();
        } else return notifyOptions;
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "New SQL Cursor - Get notification: " + pID);
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "New SQL Cursor #rows:" + spaCursor.getCount());
    	if (spaCursor.getCount() < 1) {
    		spaCursor.close();
    		return notifyOptions;
    	}
    	if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "Cursor data: dbNotifyPhoneIDCol:" 
        	+ spaCursor.getString(spaCursor.getColumnIndex(mContext.getString(R.string.dbNotifyPhoneIDCol))));
    	notifyOptions.statusbar = strToBoolean(
    			spaCursor.getString(spaCursor.getColumnIndex(mContext.getString(R.string.dbNotifyBarFlgCol))));
    	if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "Cursor data: Default Icon:" 
            	+ spaCursor.getString(spaCursor.getColumnIndex(mContext.getString(R.string.dbNotifyBarDefIconFlgCol))));
    	notifyOptions.statusbarDefaultIcon = strToBoolean(
    			spaCursor.getString(spaCursor.getColumnIndex(mContext.getString(R.string.dbNotifyBarDefIconFlgCol))));
    	notifyOptions.statusbarDefaultNotification = strToBoolean(
    			spaCursor.getString(spaCursor.getColumnIndex(mContext.getString(R.string.dbNotifyBarDefNotifyFlgCol))));
    	notifyOptions.statusbarTickerUseTextMsg = strToBoolean(
    			spaCursor.getString(spaCursor.getColumnIndex(mContext.getString(R.string.dbNotifyBarTickerMsgFlgCol))));
    	notifyOptions.statusbarDefaultTicker = strToBoolean(
    			spaCursor.getString(spaCursor.getColumnIndex(mContext.getString(R.string.dbNotifyBarDefTickerFlgCol))));
    	notifyOptions.statusbarDefaultTitle = strToBoolean(
    			spaCursor.getString(spaCursor.getColumnIndex(mContext.getString(R.string.dbNotifyBarDefTitleFlgCol))));
    	notifyOptions.statusbarDefaultText = strToBoolean(
    			spaCursor.getString(spaCursor.getColumnIndex(mContext.getString(R.string.dbNotifyBarDefTextFlgCol))));
    	notifyOptions.statusbarTicker = 
    			spaCursor.getString(spaCursor.getColumnIndex(mContext.getString(R.string.dbNotifyBarTickerCol)));
    	notifyOptions.statusbarTitle = 
    			spaCursor.getString(spaCursor.getColumnIndex(mContext.getString(R.string.dbNotifyBarTitleCol)));
    	notifyOptions.statusbarText = 
    			spaCursor.getString(spaCursor.getColumnIndex(mContext.getString(R.string.dbNotifyBarTextCol)));
    	notifyOptions.light = strToBoolean(
    			spaCursor.getString(spaCursor.getColumnIndex(mContext.getString(R.string.dbNotifyLightFlgCol))));
    	notifyOptions.vibrate = strToBoolean(
    			spaCursor.getString(spaCursor.getColumnIndex(mContext.getString(R.string.dbNotifyVibrateFlgCol))));
    	notifyOptions.vibrateDefault = strToBoolean(
    			spaCursor.getString(spaCursor.getColumnIndex(mContext.getString(R.string.dbNotifyVibrateDefFlgCol))));
    	notifyOptions.soundDefault = strToBoolean(
    			spaCursor.getString(spaCursor.getColumnIndex(mContext.getString(R.string.dbNotifySoundDefFlgCol))));
    	notifyOptions.sound = strToBoolean(
    			spaCursor.getString(spaCursor.getColumnIndex(mContext.getString(R.string.dbNotifySoundFlgCol))));
    	notifyOptions.statusbarIcon = 
			spaCursor.getInt(spaCursor.getColumnIndex(mContext.getString(R.string.dbNotifyBarIconIDCol)));
        if (EpicFeedConsts.DEBUG_DB) 
        	Log.i(TAG, TAG2 + "getNotification: notifyOptions.statusbarIcon" + notifyOptions.statusbarIcon);
    	notifyOptions.lightOn = 
			spaCursor.getInt(spaCursor.getColumnIndex(mContext.getString(R.string.dbNotifyLightOnCol)));
    	notifyOptions.lightOff = 
			spaCursor.getInt(spaCursor.getColumnIndex(mContext.getString(R.string.dbNotifyLightOffCol)));
    	//place holder to get when encoded
        //cv.put(spaContext.getString(R.string.dbNotifyVibratePatternCol), notifyData.vibratePattern);
    	notifyOptions.soundFile = Uri.parse(
			spaCursor.getString(spaCursor.getColumnIndex(mContext.getString(R.string.dbNotifySoundFileCol))));
    	spaCursor.close();
        return notifyOptions;
    }
     
    public Cursor execLogQuerySQL() throws SQLException {
        //---retrieves all log entries---
        Cursor spaCursor =
        	appDB.query(mContext.getString(R.string.dbLogTableName), new String[] { 
            		mContext.getString(R.string.dbLogIDCol),
                    mContext.getString(R.string.dbLogTimestampCol),
                	mContext.getString(R.string.dbLogDescrCol)
            	},   
            	null, 
            	null, 
            	null, 
            	null, 
            	mContext.getString(R.string.dbLogIDCol) + " DESC");  
        String sqlstr = "SELECT " 
       		+ mContext.getString(R.string.dbLogIDCol) + ", "
       		+ mContext.getString(R.string.dbLogTimestampCol) + ", "
    		+ mContext.getString(R.string.dbLogDescrCol) + " "
    		+ " FROM " 
        	+ mContext.getString(R.string.dbLogTableName) 
        	+ " ORDER BY " + mContext.getString(R.string.dbLogIDCol) + " DESC";
    	spaCursor = 
        	appDB.rawQuery(sqlstr, 
            	null);  

    	if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, "execLogQuerySQL: SQL: " + sqlstr);
        if (spaCursor != null) {
        	spaCursor.moveToFirst();
        }
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "execLogQuerySQL: New SQL Cursor - execLogQuerySQL()");
        return spaCursor;
    }
 
    public int execMsgCount(String pID) throws SQLException {
    	if (pID == null) return 0;
    	if (pID.length() < 1) return 0;
        //---retrieves count of all message for a contact---
    	Cursor spaCursor =
        	appDB.rawQuery("SELECT COUNT(*) FROM " 
            	+ mContext.getString(R.string.dbTxtTableName) + " WHERE "
            	+ mContext.getString(R.string.dbTxtPhoneIDCol) + " = '" + pID + "'"
            	+ " AND " 
                + mContext.getString(R.string.dbTxtFlgsCol) + " & "
                + ~FeedTextMsg.DRAFT_MSG_FLG, 
            	null);  
    	spaCursor.moveToFirst();
    	int count = spaCursor.getInt(0);
    	spaCursor.close();
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "execMsgCount: msgCount: " + count);
        return count;
    }
 
    public int execBlockedMsgCount(String pID) throws SQLException {
    	if (pID == null) return 0;
    	if (pID.length() < 1) return 0;
        //---retrieves count of all message for a contact---
    	Cursor spaCursor =
        	appDB.rawQuery("SELECT COUNT(*) FROM " 
            	+ mContext.getString(R.string.dbTxtTableName) + " WHERE "
            	+ mContext.getString(R.string.dbTxtPhoneIDCol) + " = '" + pID + "'"
            	+ " AND " 
                + mContext.getString(R.string.dbTxtFlgsCol) + " & "
                + FeedTextMsg.BLOCKED_MSG_FLG, 
            	null);  
    	spaCursor.moveToFirst();
    	int count = spaCursor.getInt(0);
    	spaCursor.close();
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "execBlockedMsgCount: blockedMsgCount: " + count);
        return count;
    }
 
    public int execAllUnreadMsgCount() throws SQLException {
        //---retrieves count of all unread messages---
    	Cursor spaCursor =
        	appDB.rawQuery("SELECT COUNT(*) FROM " 
            	+ mContext.getString(R.string.dbTxtTableName) + " WHERE "
                + mContext.getString(R.string.dbTxtFlgsCol) + " & "
                + FeedTextMsg.NEW_MSG_FLG, 
            	null);  
    	spaCursor.moveToFirst();
    	int count = spaCursor.getInt(0);
    	spaCursor.close();
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "execAllUnreadMsgCount: newMsgs: " + count);
        return count;
    }
 
    public int[] execNewMsgCount(String pID) throws SQLException {
    	int[] newMsgs = {0,0,0,0};
    	if (pID == null) return newMsgs;
    	if (pID.length() < 1) return newMsgs;
        //---retrieves count of all unread messages for a contact---
    	Cursor spaCursor =
        	appDB.rawQuery("SELECT COUNT(*) FROM " 
            	+ mContext.getString(R.string.dbTxtTableName) + " WHERE "
            	+ mContext.getString(R.string.dbTxtPhoneIDCol) + " = '" + pID + "'"
            	+ " AND " 
                + mContext.getString(R.string.dbTxtFlgsCol) + " & "
                + FeedTextMsg.NEW_MSG_FLG 
            	+ " AND " 
                + mContext.getString(R.string.dbTxtFlgsCol) + " & "
                + FeedTextMsg.RECEIVED_FLG, 
            	null);  
    	spaCursor.moveToFirst();
    	newMsgs[0] = spaCursor.getInt(0);
    	spaCursor.close();
        //---retrieves count of all missed calls for a contact---
    	spaCursor =
        	appDB.rawQuery("SELECT COUNT(*) FROM " 
            	+ mContext.getString(R.string.dbTxtTableName) + " WHERE "
            	+ mContext.getString(R.string.dbTxtPhoneIDCol) + " = '" + pID + "'"
            	+ " AND " 
                + mContext.getString(R.string.dbTxtFlgsCol) + " & "
                + FeedTextMsg.NEW_MSG_FLG
            	+ " AND " 
                + mContext.getString(R.string.dbTxtFlgsCol) + " & "
                + FeedTextMsg.CALL_LOG, 
            	null);  
    	spaCursor.moveToFirst();
    	newMsgs[1] = spaCursor.getInt(0);
    	spaCursor.close();
        //---retrieves count of send errors for a contact---
    	spaCursor =
        	appDB.rawQuery("SELECT COUNT(*) FROM " 
            	+ mContext.getString(R.string.dbTxtTableName) + " WHERE "
            	+ mContext.getString(R.string.dbTxtPhoneIDCol) + " = '" + pID + "'"
            	+ " AND " 
                + mContext.getString(R.string.dbTxtFlgsCol) + " & "
                + FeedTextMsg.NEW_MSG_FLG
            	+ " AND " 
                + mContext.getString(R.string.dbTxtFlgsCol) + " & "
                + FeedTextMsg.SEND_ERR_FLG, 
            	null);  
    	spaCursor.moveToFirst();
    	newMsgs[2] = spaCursor.getInt(0);
    	spaCursor.close();
        //---retrieves count of all delivery errors for a contact---
    	spaCursor =
        	appDB.rawQuery("SELECT COUNT(*) FROM " 
            	+ mContext.getString(R.string.dbTxtTableName) + " WHERE "
            	+ mContext.getString(R.string.dbTxtPhoneIDCol) + " = '" + pID + "'"
            	+ " AND " 
                + mContext.getString(R.string.dbTxtFlgsCol) + " & "
                + FeedTextMsg.NEW_MSG_FLG
            	+ " AND " 
                + mContext.getString(R.string.dbTxtFlgsCol) + " & "
                + FeedTextMsg.DELIVERY_ERR_FLG, 
            	null);  
    	spaCursor.moveToFirst();
    	newMsgs[3] = spaCursor.getInt(0);
    	spaCursor.close();
        if (EpicFeedConsts.DEBUG_DB) 
        	Log.i(TAG, TAG2 + "execNewMsgCount: newMsgs: " + newMsgs[0]
        	+ " new missed/blocked calls:" + newMsgs[1]);
        return newMsgs;
    }
    
    public boolean containsPhoneID(String pID) {
    	pID = PhoneNumberUtils.stripSeparators(pID);
    	pID = pID.replaceAll("[\\D]", "");
    	if (pID.length() <1) return false;
    	return containsPhoneID(pID, false);
    }
 
    public boolean containsPhoneID(String pID, boolean sysCall) throws SQLException {
    	if (!sysCall) {
	    	if (pID == null) pID = "0";
	    	pID = PhoneNumberUtils.stripSeparators(pID);
	    	pID = pID.replaceAll("[\\D]", "");
	    	if (pID.length() <1) return false;
    	}
        //---retrieves all contact entries---
        Cursor spaCursor =
        	appDB.query(
        		true, 
        		mContext.getString(R.string.dbPhoneIDTableName), 
        		new String[] {
        			mContext.getString(R.string.dbPhoneIDCol)
	            }, 
	            mContext.getString(R.string.dbPhoneIDCol) + " = '" + pID + "'", 
	            null,
	            null, 
	            null, 
	            mContext.getString(R.string.dbPhoneIDCol) + " ASC", 
	            null
            );
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "containsPhoneID: New SQL Cursor - containsPhoneID()");
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "containsPhoneID: containsPhoneID(): " + pID);
        if (spaCursor != null) {
        	if (spaCursor.getCount() > 0) {
        		spaCursor.close();
                if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "containsPhoneID: true");
        		return true;
        	} else {
        		spaCursor.close();
                if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "containsPhoneID: false");
        		return false;
        	}
        } else {
            if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "containsPhoneID: false");
        	return false;
        }
    }
 
    public boolean containsContactFeed(ContactFeed feed) throws SQLException {
        //---retrieves all feed entries---
        Cursor spaCursor =
        	appDB.query(
        		true, 
        		ContactFeed.DB_TABLE_NAME, 
        		new String[] {
        			ContactFeed.DB_TABLE_COLUMNS[ContactFeed.FEED_ID]
	            }, 
	            ContactFeed.DB_TABLE_COLUMNS[ContactFeed.PHONE_ID] + "= '" + feed.getPhoneId() + "' AND " +
	            ContactFeed.DB_TABLE_COLUMNS[ContactFeed.FEED_TYPE] + " = '" + feed.getFeedType() + "'", 
	            null,
	            null, 
	            null, 
	            mContext.getString(R.string.dbPhoneIDCol) + " ASC", 
	            null
            );
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "containsContactFeed: New SQL Cursor - containsContactFeed()");
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "containsContactFeed: " + feed.getPhoneId() + " : " + feed.getFeedType());
        if (spaCursor != null) {
        	if (spaCursor.getCount() > 0) {
        		spaCursor.close();
                if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "containsContactFeed: true");
        		return true;
        	} else {
        		spaCursor.close();
                if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "containsContactFeed: false");
        		return false;
        	}
        } else {
            if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "containsContactFeed: false");
        	return false;
        }
    }
 
    public boolean containsBlockedID(String pID, boolean sysCall) throws SQLException {
    	if (!sysCall) {
	    	if (pID == null) pID = "0";
	    	if (pID.length() <1) pID = "0";
    	}
        //---retrieves all contact entries---
        Cursor spaCursor =
        	appDB.query(
        		true, 
        		mContext.getString(R.string.dbPhoneIDTableName), 
        		new String[] {
        			mContext.getString(R.string.dbPhoneIDCol)
	            }, 
	            mContext.getString(R.string.dbPhoneIDCol) + "= '" + pID + "'"
            	+ " AND " 
                + mContext.getString(R.string.dbPhoneSecFlgCol) + " & "
                + EpicContact.BLOCK_CALLS, 
	            null,
	            null, 
	            null, 
	            mContext.getString(R.string.dbPhoneIDCol) + " ASC", 
	            null
            );
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "containsBlockedID: New SQL Cursor - containsPhoneID()");
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "containsBlockedID: containsPhoneID(): " + pID);
        if (spaCursor != null) {
        	if (spaCursor.getCount() > 0) {
        		spaCursor.close();
                if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "containsBlockedID: true");
        		return true;
        	} else {
        		spaCursor.close();
                if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "containsBlockedID: false");
        		return false;
        	}
        } else {
            if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "containsBlockedID: false");
        	return false;
        }
    }
 
    public boolean containsSpaID(String pID) throws SQLException {
    	if (pID == null) pID = "0";
    	if (pID.length() <1) pID = "0";
        //---retrieves all log entries---
        Cursor spaCursor =
        	appDB.query(
        		true, 
        		mContext.getString(R.string.dbPhoneIDTableName), 
        		new String[] {
        			mContext.getString(R.string.dbPhoneIDCol)
	            }, 
	            mContext.getString(R.string.dbPhoneIDCol) + "= '" + pID + "'"
	            + " AND "
	            + mContext.getString(R.string.dbPhoneSecFlgCol) + " = 'Y' ", 
	            null,
	            null, 
	            null, 
	            mContext.getString(R.string.dbPhoneIDCol) + " ASC", 
	            null
            );
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "containsPhoneID: New SQL Cursor - containsPhoneID()");
        if (spaCursor != null) {
        	if (spaCursor.getCount() > 0) {
       		spaCursor.close();
        		return true;
        	} else {
        		spaCursor.close();
        		return false;
        	}
        } else {
        	return false;
        }
    }
 
    public boolean containsBlockedIDold(String pID) throws SQLException {
    	if (pID == null) pID = "0";
    	if (pID.length() <1) pID = "0";
        //---retrieves all log entries---
        Cursor spaCursor =
        	appDB.query(
        		true, 
        		mContext.getString(R.string.dbPhoneIDTableName), 
        		new String[] {
        			mContext.getString(R.string.dbPhoneIDCol)
	            }, 
	            mContext.getString(R.string.dbPhoneIDCol) + "= '" + pID + "'"
	            + " AND "
	            + mContext.getString(R.string.dbPhoneSecFlgCol) + " = 'B' ", 
	            null,
	            null, 
	            null, 
	            mContext.getString(R.string.dbPhoneIDCol) + " ASC", 
	            null
            );
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "containsPhoneID: New SQL Cursor - containsPhoneID()");
        if (spaCursor != null) {
        	if (spaCursor.getCount() > 0) {
        		spaCursor.close();
        		return true;
        	} else {
        		spaCursor.close();
        		return false;
        	}
        } else {
        	return false;
        }
    }
 
    public boolean containsNotification(String pID) throws SQLException {
    	if (pID == null) pID = "0";
    	if (pID.length() <1) pID = "0";
        //---retrieves all log entries---
        Cursor spaCursor =
        	appDB.query(
        		true, 
        		mContext.getString(R.string.dbNotifyTableName), 
        		new String[] {
        			mContext.getString(R.string.dbNotifyPhoneIDCol)
	            }, 
	            mContext.getString(R.string.dbNotifyPhoneIDCol) + "= '" + pID + "'",
	            null,
	            null, 
	            null, 
	            mContext.getString(R.string.dbNotifyPhoneIDCol) + " ASC", 
	            null
            );
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "containsNotification: New SQL Cursor");
        if (spaCursor != null) {
        	if (spaCursor.getCount() > 0) {
        		spaCursor.close();
        		return true;
        	} else {
        		spaCursor.close();
        		return false;
        	}
        } else {
        	return false;
        }
    }
 
    public boolean containsMsg(String msgID) throws SQLException {
        //---determines if it has a msg entry---
        Cursor spaCursor =
        	appDB.query(
        		true, 
        		mContext.getString(R.string.dbTxtTableName), 
        		new String[] {
        			mContext.getString(R.string.dbTxtIDCol)
	            }, 
	            mContext.getString(R.string.dbTxtIDCol) + "= '" + msgID + "'",
	            null,
	            null, 
	            null, 
	            null, 
	            null
            );
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "containsMsg: New SQL Cursor");
        if (spaCursor != null) {
        	if (spaCursor.getCount() > 0) {
        		spaCursor.close();
        		return true;
        	} else {
        		spaCursor.close();
        		return false;
        	}
        } else {
        	return false;
        }
    }
 
    public boolean containsDraft(String pID) throws SQLException {
    	if (pID == null) pID = "0";
    	if (pID.length() <1) pID = "0";
        //---retrieves all log entries---
        Cursor spaCursor =
        	appDB.query(
        		true, 
        		mContext.getString(R.string.dbTxtTableName), 
        		new String[] {
        			mContext.getString(R.string.dbTxtPhoneIDCol)
	            }, 
	            mContext.getString(R.string.dbTxtPhoneIDCol) + "= '" + pID + "'" 
	            + " AND " 
                + mContext.getString(R.string.dbTxtFlgsCol) + " & "
                + FeedTextMsg.DRAFT_MSG_FLG, 
	            null,
	            null, 
	            null, 
	            mContext.getString(R.string.dbTxtPhoneIDCol) + " ASC", 
	            null
            );
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "containsDraft: New SQL Cursor");
        if (spaCursor != null) {
        	if (spaCursor.getCount() > 0) {
        		spaCursor.close();
        		return true;
        	} else {
        		spaCursor.close();
        		return false;
        	}
        } else {
        	return false;
        }
    }
 
	public String comparePhoneID(String pID) throws SQLException {
        //---retrieves phone entries that match according to Android---
        Cursor spaCursor =
        	appDB.query(
        		true, 
        		mContext.getString(R.string.dbPhoneIDTableName), 
        		new String[] {
        			mContext.getString(R.string.dbPhoneIDCol)
	            }, 
	            null, 
	            null,
	            null, 
	            null, 
	            mContext.getString(R.string.dbPhoneIDCol) + " ASC", 
	            null
            );
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "comparePhoneID: New SQL Cursor - allPhoneID()");
        String tmpID;
        if (spaCursor != null) {
    		spaCursor.moveToFirst();
            if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "comparePhoneID: New SQL Cursor - # ids: " + spaCursor.getCount());
        	for (int i = 0; i < spaCursor.getCount(); i++) {
        		tmpID = spaCursor.getString(0);
        		if (PhoneNumberUtils.compare(tmpID, pID)) {
        			spaCursor.close();
        			return tmpID;
        		}
                if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "comparePhoneID: New SQL Cursor - Got id: " + tmpID);
        		if (!spaCursor.isLast()) spaCursor.moveToNext();
        	}
        } else {
        	return "";
        }
		spaCursor.close();
		return "";
    }
 
    public String[] allPhoneID() throws SQLException {
        //---retrieves all phone ID entries---
        Cursor spaCursor =
        	appDB.query(
        		true, 
        		mContext.getString(R.string.dbPhoneIDTableName), 
        		new String[] {
        			mContext.getString(R.string.dbPhoneIDCol)
	            }, 
	            null, 
	            null,
	            null, 
	            null, 
	            mContext.getString(R.string.dbPhoneNameCol) + " ASC," + 
	            mContext.getString(R.string.dbPhoneIDCol) + " ASC", 
	            null
            );
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "allPhoneID: New SQL Cursor - allPhoneID()");
        String[] pIDs;
        if (spaCursor != null) {
    		spaCursor.moveToFirst();
        	pIDs = new String[spaCursor.getCount()];
            if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "allPhoneID: New SQL Cursor - # ids: " + spaCursor.getCount());
        	for (int i = 0; i < spaCursor.getCount(); i++) {
        		pIDs[i] = spaCursor.getString(0);
                if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "allPhoneID: New SQL Cursor - Got id: " + pIDs[i]);
        		if (!spaCursor.isLast()) spaCursor.moveToNext();
        	}
        } else {
        	return new String[0];
        }
		spaCursor.close();
		return pIDs;
    }
 
    public String[] allSpaID() throws SQLException {
        //---retrieves all log entries---
        Cursor spaCursor =
        	appDB.query(
        		true, 
        		mContext.getString(R.string.dbPhoneIDTableName), 
        		new String[] {
        			mContext.getString(R.string.dbPhoneIDCol)
	            }, 
	            mContext.getString(R.string.dbPhoneSecFlgCol) + " = 'Y' ", 
	            null,
	            null, 
	            null, 
	            mContext.getString(R.string.dbPhoneIDCol) + " ASC", 
	            null
            );
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "allSpaID: New SQL Cursor - allPhoneID()");
        String[] pIDs;
        if (spaCursor != null) {
    		spaCursor.moveToFirst();
        	pIDs = new String[spaCursor.getCount()];
            if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "allSpaID: New SQL Cursor - # ids: " + spaCursor.getCount());
        	for (int i = 0; i < spaCursor.getCount(); i++) {
        		pIDs[i] = spaCursor.getString(0);
                if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "allSpaID: New SQL Cursor - Got id: " + pIDs[i]);
        		if (!spaCursor.isLast()) spaCursor.moveToNext();
        	}
        } else {
        	return new String[0];
        }
		spaCursor.close();
		return pIDs;
    }
 
    public String firstPhoneID() throws SQLException {
        //---retrieves all log entries---
        Cursor spaCursor =
        	appDB.query(
        		true, 
        		mContext.getString(R.string.dbPhoneIDTableName), 
        		new String[] {
        			mContext.getString(R.string.dbPhoneIDCol)
	            }, 
	            null, 
	            null,
	            null, 
	            null, 
	            mContext.getString(R.string.dbPhoneIDCol) + " ASC", 
	            null
            );
        if (EpicFeedConsts.DEBUG_DB) Log.i(TAG, TAG2 + "containsPhoneID: New SQL Cursor - containsPhoneID()");
        if (spaCursor != null) {
        	if (spaCursor.getCount() > 0) {
        		spaCursor.moveToFirst();
        		String pid = spaCursor.getString(0);
        		spaCursor.close();
        		return pid;
        	} else {
        		spaCursor.close();
        		return  "";
        	}
        } else {
        	return "";
        }
    }
    
    public boolean isOpen() {
    	return appDB.isOpen();
    }
    
    public void setCheckMessageListener(CheckMessageListener checkMessageListener) {
        if (EpicFeedConsts.DEBUG_TEXT_MSG_LIST) Log.i(TAG, TAG2 + "setCheckMessageListener: setting listener");
    	mCheckMessageListener = checkMessageListener;
        if (EpicFeedConsts.DEBUG_TEXT_MSG_LIST) Log.i(TAG, TAG2 + "setCheckMessageListener: mCheckMessageListener is null:" + (mCheckMessageListener == null));
    }
    
    public void removeCheckMessageListener() {
        if (EpicFeedConsts.DEBUG_TEXT_MSG_LIST) Log.i(TAG, TAG2 + "setCheckMessageListener: removing listener");
    	mCheckMessageListener = null;
    }
    
	public interface CheckMessageListener {
		public abstract void onNewMsg();
		public abstract void onSentMsg();
		public abstract void onDeliveredMsg();
	}

    public void setContactListListener(ContactListListener contactListListener) {
    	mContactListListener = contactListListener;
    }
    
    public void removeContactListListener() {
    	mContactListListener = null;
    }
    
	public interface ContactListListener {
		public abstract void onNewMsgs();
	}

 }