package com.sdmmllc.epicfeed.data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import twitter4j.DirectMessage;
import twitter4j.PagableResponseList;
import twitter4j.Paging;
import twitter4j.ResponseList;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.User;
import android.util.Log;

import com.sdmmllc.epicfeed.data.FeedContactList.FeedContactListComparator;
import com.sdmmllc.epicfeed.model.Twitter11;
import com.sdmmllc.epicfeed.utils.EpicFeedConsts;
import com.sdmmllc.epicfeed.view.FeedMessageListAdapter;
import com.sdmmllc.epicfeed.view.FeedTextMsg;

public class TwitterData {
	public final int NUM = 20, NUMBER = 45;
	
	public final String TAG = "TwitterData";
	
	//public ArrayList<User> friendsArray = new ArrayList<User>();
	public static User[] twitterFriends,
	                     twitterFollowers;
	public User[] userTwtFriends;
	public FeedContactList userTwtContacts;
	
	public boolean rateLimitSDMFlag = false,
			rateLimitRDMFlag = false,
			rateLimitMTLFlag = false,
			rateLimitFTLFlag = false,
		    rateLimitGTFFlag = false,
		    rateLimitGMFlag = false, 
		    rateLimitTwtFlag = false,
		    rateLimitMyTwtFlag = false,
		    rateLimitMeFlag = false,
		    rateLimitFriendFlag = false,
		    rateLimitBSDMFlag = false,
		    rateLimitBRDMFlag = false,
		    rateLimitBTCFlag = false,
		    rateLimitOneFriendFlag = false,
		    rateLimitGCOFlag = false,
		    rateLimitGetFollowersFlag = false,
		    rateLimitBuildFollowersFlag = false;
	
	public boolean rateLimitStat() {
		boolean rateLimit = rateLimitSDMFlag || rateLimitRDMFlag || rateLimitMTLFlag || rateLimitFTLFlag || 
				rateLimitGTFFlag || rateLimitGMFlag || rateLimitTwtFlag || rateLimitMyTwtFlag || 
				rateLimitMeFlag || rateLimitFriendFlag || rateLimitBSDMFlag || rateLimitBRDMFlag ||
				rateLimitBTCFlag || rateLimitOneFriendFlag || rateLimitGCOFlag || rateLimitGetFollowersFlag ||
				rateLimitBuildFollowersFlag;
		
		if (EpicFeedConsts.DEBUG_TWT) Log.i(TAG, "Rate Limit Reached: " + rateLimit +
				" rateLimitSDMFlag: " + rateLimitSDMFlag +
				" rateLimitRDMFlag: " + rateLimitRDMFlag +
				" rateLimitMTLFlag: " + rateLimitMTLFlag +
				"\nrateLimitFTLFlag: " + rateLimitFTLFlag +
				" rateLimitGTFFlag: " + rateLimitGTFFlag +
				" rateLimitGMFlag: " + rateLimitGMFlag +
				" rateLimitTwtFlag: " + rateLimitTwtFlag +
				"\nrateLimitMyTwtFlag: " + rateLimitMyTwtFlag +
				" rateLimitMeFlag: " + rateLimitMeFlag +
				" rateLimitFriendFlag: " + rateLimitFriendFlag +
				" rateLimitBSDMFlag: " + rateLimitBSDMFlag +
				"\nrateLimitBRDMFlag: " + rateLimitBRDMFlag +
				" rateLimitBTCFlag: " + rateLimitBTCFlag +
				" rateLimitOneFriendFlag: " + rateLimitOneFriendFlag +
				" rateLimitGCOFlag: " + rateLimitGCOFlag +
				"\nrateLimitBuildFollowersFlag: " + rateLimitBuildFollowersFlag +
				" rateLimitGetFollowersFlag: " + rateLimitGetFollowersFlag);
		
		return rateLimit;
	}
	
	private List<FeedTextMsg> txtMsgs = Collections.synchronizedList(new ArrayList<FeedTextMsg>());
	
	private EpicContact twtContact;
	
	public EpicContact self = new EpicContact("me");
	
	private Twitter twitter;
	
	public TwitterData(Twitter11 twitter11, EpicContact contact) {
		twtContact = contact;
		mTwitter11 = twitter11;
		twitter = twitter11.twitter;
	}
	
	private TwitterDataListener twtListener;
	
	public TwitterDataListener getTwitterDataListener() {
    	return twtListener;
    }
    
    public void setTwitterDataListener(TwitterDataListener listener) {
    	twtListener = listener;
    }
    
    public boolean isTwtDataReady() {
    	boolean twtDataReady = !getTwitterFriendsRunning && !getTweetsRunning && !buildMeRunning && !buildFriendRunning &&
    			!buildFriendsTimelineRunning && !buildSentDirectMessagesRunning && !getSentDMsRunning && !getMyselfRunning  &&
    			!getMyTweetsRunning && !buildMyTimelineRunning && !buildReceivedDirectMessageRunning && !getRecievedDMsRunning
    			&& !mySentDMsRunning && !myRecievedDMsRunning && !otherPersonTweetsRunning && !mySentTweetsRunning &&
    			!getOneFriendDataRunning && !buildOneFriendRunning && !getChosenOneRunning && !getTwitterFollowersRunning &&
    			!buildFollowersRunning;
    	if (EpicFeedConsts.DEBUG_TWT) Log.i(TAG, "isTwtDataReady: " + twtDataReady +
    			" getTwitterFriendsRunning: " + getTwitterFriendsRunning +
    			" getTweetsRunning: " + getTweetsRunning +
    			" buildMeRunning: " + buildMeRunning +
    			" buildFriendRunning: " + buildFriendRunning +
    			" buildFriendsTimelineRunning: " + buildFriendsTimelineRunning +
    			" buildDirectMessagesRunning: " + buildSentDirectMessagesRunning +
    			" getDMsRunning: " + getSentDMsRunning +
    			" getMyselfRunning: " + getMyselfRunning +
    			" getMyTweetsRunning: " + getMyTweetsRunning +
    			" buildMyTimelineRunning: " + buildMyTimelineRunning +
    			" buildReceivedDirectMessageRunning: " + buildReceivedDirectMessageRunning +
    			" getRecievedDMsRunning: " + getRecievedDMsRunning +
    			" mySentDMsRunning: " + mySentDMsRunning +
    			" myRecievedDMsRunning: " + myRecievedDMsRunning +
    			" otherPersonTweetsRunning: " + otherPersonTweetsRunning +
    			" mySentTweetsRunning: " + mySentTweetsRunning + 
    			" getOneFriendDataRunning: " + getOneFriendDataRunning +
    			" buildOneFriendRunning: " + buildOneFriendRunning +
    			" getChosenOneRunning: " + getChosenOneRunning +
    			" getTwitterFollowersRunning: " + getTwitterFollowersRunning +
    			" buildFollowersRunning: " + buildFollowersRunning);
    	return twtDataReady;
    }
    
    private Twitter11 mTwitter11;
    
    public Twitter11 getTwitter11() {
    	return mTwitter11;
    }
    
    public void setTwitter11(Twitter11 twitter11) {
    	this.mTwitter11 = twitter11;
    }
    
    public List<FeedTextMsg> getTwtMsgs() {
    	return txtMsgs;
	}
    
    public void clearTwtMsgs() {
    	txtMsgs.clear();
	}
    
    private boolean getSentDMsRunning = false;
    
    private void getSentDMs() { 
    	getSentDMsRunning = true;
    	rateLimitSDMFlag = false;
    	new Thread(new Runnable() {
        	@Override
        	public void run() {
    			ResponseList<DirectMessage>	sdms;
    			ArrayList<DirectMessage> newSDMsgs = new ArrayList<DirectMessage>();
    			int n = 1;
    			Paging sdmPaging = new Paging(n);
    			
    			try {
    				do {
    					sdms = twitter.getSentDirectMessages(sdmPaging);
    					
    					for(DirectMessage dm : sdms) {
    						if (String.valueOf(dm.getRecipientId()).equals(
    								twtContact.getFeed(ContactFeed.FEED_TYPE_TWITTER).getFeedSystemId())) {
    							if(newSDMsgs.size() < NUMBER) {
    			    	    		newSDMsgs.add(dm);
    			    	    	}
    						}
			    	    }
    					
    					sdmPaging.setPage(++n);
    				} while (sdms.size() == NUM && newSDMsgs.size() < NUMBER);
    				
    			} catch (TwitterException te) {
    				Log.i(TAG, "*  *  *  *  * Exception S dmRunnable >> " + te.getErrorMessage());
    				rateLimitSDMFlag = true;
    				//Log.i(TAG, "*  *  *  *  * Cause >> " + te.getCause());
    				//te.printStackTrace();
    			}
    			
    			DirectMessage[] messages = newSDMsgs.toArray(new DirectMessage[newSDMsgs.size()]);
    			buildSentDirectMessages(messages);
    			getSentDMsRunning = false;
        	}
        }, "Get S DM's").start();
	}
    
    private boolean getRecievedDMsRunning = false;
    
    private void getRecievedDMs() {
    	getRecievedDMsRunning = true;
    	rateLimitRDMFlag = false;
    	new Thread(new Runnable() {
        	@Override
        	public void run() {
    			ResponseList<DirectMessage>	rdms;
    			ArrayList<DirectMessage> newRDMsgs = new ArrayList<DirectMessage>();
    			int n = 1;
    			Paging rdmPaging = new Paging(n);
    			
    			try {
		    	    do {
		    	    	rdms = twitter.getDirectMessages(rdmPaging);
		    	    	
		    	    	for(DirectMessage dm : rdms) {
    		    	    	if(String.valueOf(dm.getSenderId()).equals(
    		    	    			twtContact.getFeed(ContactFeed.FEED_TYPE_TWITTER).getFeedSystemId())) {
    		    	    		if(newRDMsgs.size() < NUMBER) {
    		    	    			newRDMsgs.add(dm);
    		    	    		}
    		    	    	}
    		    	    }
		    	    	
		    	    	rdmPaging.setPage(++n);
    			    } while (rdms.size() == NUM && newRDMsgs.size() < NUMBER);
		    	    
    			} catch (TwitterException te) {
    				Log.i(TAG, "*  *  *  *  * Exception R dmRunnable >> " + te.getErrorMessage());
    				rateLimitRDMFlag = true;
    				//Log.i(TAG, "*  *  *  *  * Cause >> " + te.getCause());
    				//te.printStackTrace();
    			}
    			
    			DirectMessage[] messages = newRDMsgs.toArray(new DirectMessage[newRDMsgs.size()]);
    			buildReceivedDirectMessage(messages);
    			getRecievedDMsRunning = false;
        	}
        }, "Get R DM's").start();
    }
    
    private boolean getTwitterFriendsRunning = false;
    
    private void getTwitterFriends(final TwitterDataListener listener) {
    	getTwitterFriendsRunning = true;
    	rateLimitGTFFlag = false;
    	new Thread(new Runnable() {
    		
    		@Override
    		public void run() {
    			PagableResponseList<User> friends;
    			long cursor = -1;
    			int index = 0, j = 0;
    			
    			try {
    				long me = twitter.getId();
    				int numberFriends = twitter.showUser(me).getFriendsCount();
    				
    				if(numberFriends % 20 != 0)
    					index = numberFriends / 20 + 1;
    				else
    					index = numberFriends / 20;
    				
    				twitterFriends = new User[numberFriends];
    				
    		        for(int i = 0; i < index; i++) {
    		    	    friends = twitter.getFriendsList(me, cursor); 
    		    	
    		    	    for(User a : friends) {
    		    	    	twitterFriends[j] = a;
    		    	    	//friendsArray.add(a);
    		    	    	j++;
    		    	    }
    		    	
    		    	    if(friends.hasNext()) 
    		    	        cursor = friends.getNextCursor();
    		        }
    		        
    		        /*for (int k = 0; k < j; k++) {
    		        	twitterFriends[k] = friendsArray.get(k);
     		        }*/
    		        
    		        getTwitterFriendsRunning = false;
    			} catch (TwitterException te) {
    				Log.i(TAG, "*  *  *  *  * Exception FriendsRunnable >> " + te.getErrorMessage());
    				rateLimitGTFFlag = true;
    				//Log.i(TAG, "*  *  *  *  * Cause >> " + te.getCause());
    				//te.printStackTrace();
    			}
    			Log.i(TAG, "+++++++++++++++++++++ twitterFriends is null >> " + (twitterFriends == null));
    			buildFriend(twitterFriends, listener);
    		}
    	}, "Get Twitter Friends").start();
    	
    }
    
    private boolean getTwitterFollowersRunning = false;
    
    public void getTwitterFollowers(final Contact.UpdateListener listener) {
    	getTwitterFollowersRunning = true;
    	rateLimitGetFollowersFlag = false;
    	new Thread(new Runnable() {
    		
    		@Override
    		public void run() {
    			PagableResponseList<User> friends;
    			long cursor = -1;
    			int index = 0, j = 0;
    			
    			try {
    				long me = twitter.getId();
    				int numberFriends = twitter.showUser(me).getFriendsCount();
    				
    				if(numberFriends % 20 != 0)
    					index = numberFriends / 20 + 1;
    				else
    					index = numberFriends / 20;
    				
    				twitterFollowers = new User[numberFriends];
    				
    		        for(int i = 0; i < index; i++) {
    		    	    friends = twitter.getFriendsList(me, cursor); 
    		    	
    		    	    for(User a : friends) {
    		    	    	twitterFollowers[j] = a;
    		    	    	j++;
    		    	    }
    		    	
    		    	    if(friends.hasNext()) 
    		    	        cursor = friends.getNextCursor();
    		        }
    		        
    		        getTwitterFollowersRunning = false;
    			} catch (TwitterException te) {
    				Log.i(TAG, "*  *  *  *  * Exception FriendsRunnable >> " + te.getErrorMessage());
    				rateLimitGetFollowersFlag = true;
    			}
    			Log.i(TAG, "+++++++++++++++++++++ twitterFollowers is null >> " + (twitterFollowers == null));
    			buildFollowers(twitterFollowers, listener);
    		}
    	}, "Get Twitter Friends").start();
    	
    }
    
    private boolean getMyselfRunning = false;
    
    private void getMyself(final TwitterDataListener listener) {
    	getMyselfRunning = true;
    	rateLimitGMFlag = false;
    	new Thread(new Runnable() {
    		
    		@Override
    		public void run() {
    			try {
    				buildMe(twitter.showUser(twitter.getId()), listener); 
    				getMyselfRunning = false;
    			} catch (TwitterException e) {
    				Log.i(TAG, "*  *  *  *  *  Exception getMyselfRunnable >> " + e.getErrorMessage());
    				rateLimitGMFlag = true;
    				//Log.i(TAG, "*  *  *  *  *  Cause >> " + e.getCause());
    				//e.printStackTrace();
    			}
    		}
    	}, "Get Myself").start();
    }
    
    private boolean getChosenOneRunning = false;
    
    private void getChosenOne(final TwitterDataListener listener, final EpicContact friend) {
    	getChosenOneRunning = true;
    	rateLimitGCOFlag = false;
    	new Thread(new Runnable() {
    		
    		@Override
    		public void run() {
    			try {
    				buildOneFriend(twitter.showUser(Long.valueOf(friend.getFeed(ContactFeed.FEED_TYPE_TWITTER).
    						getFeedSystemId()).longValue()), listener, friend);
    				getChosenOneRunning = false;
    			} catch (TwitterException e) {
    				Log.i(TAG, "++++++++++++++++++ Exception getChosenOneRunnable >> " + e.getErrorMessage());
    				rateLimitGCOFlag = true;
    				//Log.i(TAG, "*  *  *  *  *  Cause >> " + e.getCause());
    				//e.printStackTrace();
    			}
    		}
    	}, "Get Chosen One").start();
    }
    
    private boolean getTweetsRunning = false;
	
	private void getTweets() {  
		getTweetsRunning = true;
		rateLimitTwtFlag = false;
		new Thread(new Runnable() {
			public void run() {
				ResponseList<Status> userTweets;
				ArrayList<Status> tweets = new ArrayList<Status>();
				int n = 1;
				Paging paging = new Paging(n, NUM); // NUM controls number of tweets received per user
				
				try {
					do {
						userTweets = twitter.getUserTimeline(Long.valueOf(twtContact.getFeed(
								ContactFeed.FEED_TYPE_TWITTER).getFeedSystemId()).longValue(), paging);
						
						for(Status b : userTweets) {
					    	if(tweets.size() < NUMBER) {
					    		tweets.add(b);
					    	}
					     }
						
						paging.setPage(++n);
					} while (userTweets.size() == NUM && tweets.size() < NUMBER);
					
				} catch (TwitterException te) {
					Log.i(TAG, "*  *  *  *  * Exception tweetsRunnable >> " + te.getErrorMessage());
					rateLimitTwtFlag = true;
					//Log.i(TAG, "*  *  *  *  * Cause >> " + te.getCause());
					//te.printStackTrace();
				}
				
				Status[] statuses = tweets.toArray(new Status[tweets.size()]);
				buildFriendsTimeline(statuses);
				getTweetsRunning = false;
			}
		}, "Get Tweets").start();
	}
	
	private boolean getMyTweetsRunning = false;
	
	private void getMyTweets() {
		getMyTweetsRunning = true;
		rateLimitMyTwtFlag = false;
		new Thread(new Runnable() {
			public void run() {
				ResponseList<Status> myTweets;
				ArrayList<Status> tweets = new ArrayList<Status>();
				int n = 1;
				Paging paging = new Paging(n, NUM);
				
				try {
					do {
						myTweets = twitter.getUserTimeline(twitter.getId(), paging);
						
						for(Status c : myTweets) {
							if(tweets.size() < NUMBER) {
								tweets.add(c);
							}
						}
						
						paging.setPage(++n);
					} while (myTweets.size() <= NUM && tweets.size() != NUMBER);
					
				} catch (TwitterException te) {
					Log.i(TAG, "*  *  *  *  * Exception getMyTweets >> " + te.getErrorMessage());
					rateLimitMyTwtFlag = true;
					//Log.i(TAG, "*  *  *  *  * Cause >> " + te.getCause());
					//te.printStackTrace();
				}
				
				Status[] statuses = tweets.toArray(new Status[tweets.size()]);
				buildMyTimeline(statuses);
				getMyTweetsRunning = false;
			}
		}, "Get My Tweets").start();
	}
	
	private boolean buildMeRunning = false;
	
	private void buildMe(User resu, TwitterDataListener listener) {
		buildMeRunning = true;
		rateLimitMeFlag = false;
    	try {
    	    if (resu != null) {
        		self.nameId = resu.getName();
        		self.name = resu.getScreenName();
        		self.fbID = String.valueOf(resu.getId());
        		ContactFeed feed = new ContactFeed(String.valueOf(resu.getId()));
        		feed.setFeedSystemId(String.valueOf(resu.getId()));
        		feed.setFeedType(ContactFeed.FEED_TYPE_TWITTER);
        		feed.profileURL = resu.getMiniProfileImageURL();
        		self.addFeed(feed);
        	}
    	    buildMeRunning = false;
    	}
    	catch (Exception e) {
    		Log.i(TAG, "*  *  *  *  * Exception buidMe >> " + e.getMessage());
    		rateLimitMeFlag = true;
    		//e.printStackTrace();
    	}
    	
    	if (listener != null) {
        	listener.onUpdate();
        	Log.i(TAG, "*  *  *  *  *  isTwtDataReady() >> " + isTwtDataReady());
        	if (isTwtDataReady()) 
        		listener.onComplete();
        }
    }
    
    public void getMe(final TwitterDataListener tdlistener) {
    	if (EpicFeedConsts.DEBUG_TWT) Log.w(TAG, "+++++++ getTwitterMe() ++++++++++");
    	getMyself(tdlistener);
    }
    
    private boolean buildFriendRunning = false;
    
    private void buildFriend(User[] users, TwitterDataListener listener) {
    	buildFriendRunning = true;
    	rateLimitFriendFlag = false;
    	userTwtContacts = new FeedContactList();
    	    
    	if (null != users) {
    		for(int i = 0; i < users.length; i++) {
        		try {
            		EpicContact twtFriend = new EpicContact(String.valueOf(
        					users[i].getId())); 
            		twtFriend.nameId = users[i].getName();
            		twtFriend.name = users[i].getScreenName();
            		twtFriend.fbID = String.valueOf(users[i].getId()); // added for Act_ContactList in getContact method
            		ContactFeed feed = new ContactFeed(String.valueOf(users[i].getId()));
            		feed.setFeedSystemId(String.valueOf(users[i].getId()));
            		feed.setFeedType(ContactFeed.FEED_TYPE_TWITTER);
            		feed.profileURL = users[i].getMiniProfileImageURL();
            		twtFriend.addFeed(feed);
            		userTwtContacts.add(twtFriend);
            		if (EpicFeedConsts.DEBUG_TWT) Log.w(TAG, "added contact: +++++++ " + twtFriend.name + " ++++++++++");
            		buildFriendRunning = false;
            	} catch (Exception e) {
                	Log.i(TAG, "*  *  *  *  * Exception buildFriend >> " + e.getMessage());
                	rateLimitFriendFlag = true;
                	//e.printStackTrace();
                } finally {
                	Collections.sort(userTwtContacts, FeedContactListComparator.getComparator(FeedContactListComparator.DISPLAY_NAME));
                }
        	}
    	} else {
    		rateLimitFriendFlag = true;
    	}
    	
    	if (listener != null) {
        	listener.onUpdate();
        	Log.i(TAG, "*  *  *  *  *  isTwtDataReady() >> " + isTwtDataReady());
        	if (isTwtDataReady()) 
        		listener.onComplete();
        }	
    }
    
    private boolean buildFollowersRunning = false;
    
    private void buildFollowers(User[] users, Contact.UpdateListener listener) {
    	buildFollowersRunning = true;
    	rateLimitBuildFollowersFlag = false;
    	userTwtFriends = users;
    	
    	buildFollowersRunning = false;
    	if (twtListener != null) {
	    	if (isTwtDataReady()) twtListener.onComplete();
	    }
    	FeedContactList.getContactsList(listener);  
    }
    
    private boolean buildOneFriendRunning = false;
    
    private User chosenOne;
    
    private void buildOneFriend(User user, TwitterDataListener listener, EpicContact twtFriend) {
    	buildOneFriendRunning = true;
    	rateLimitOneFriendFlag = false;
    	userTwtContacts = new FeedContactList();
    	   
    	if (null != user) {
    		try {
        		chosenOne = user;
    			Log.i(TAG, "+++++++++++++++++ chosenOne >> " + chosenOne.toString());
        		twtFriend.nameId = chosenOne.getName();
        		twtFriend.name = chosenOne.getScreenName();
        		twtFriend.fbID = String.valueOf(chosenOne.getId()); // added for Act_ContactList in getContact method
        		ContactFeed feed = new ContactFeed(String.valueOf(chosenOne.getId()));
        		feed.setFeedSystemId(String.valueOf(chosenOne.getId()));
        		feed.setFeedType(ContactFeed.FEED_TYPE_TWITTER);
        		feed.profileURL = chosenOne.getMiniProfileImageURL();
        		twtFriend.addFeed(feed);
        		userTwtContacts.add(twtFriend);
        		if (EpicFeedConsts.DEBUG_TWT) Log.w(TAG, "added this chosenOne: +++++++ " + twtFriend.name + " ++++++++++");
        		buildOneFriendRunning = false;
        	} catch (Exception e) {
            	Log.i(TAG, "*  *  *  *  * Exception buildOneFriend >> " + e.getMessage());
            	rateLimitOneFriendFlag = true;
            	e.printStackTrace();
            } finally {
            	Collections.sort(userTwtContacts, FeedContactListComparator.getComparator(FeedContactListComparator.DISPLAY_NAME));
            }
    	}
    	
    	if (listener != null) {
        	listener.onUpdate();
        	Log.i(TAG, "*  *  *  *  *  isTwtDataReady() >> " + isTwtDataReady());
        	if (isTwtDataReady()) 
        		listener.onComplete();
        }	
    } 
    
    private boolean getOneFriendDataRunning = false;
    
    public void getOneFriendData(final TwitterDataListener listener, final EpicContact friend) {
    	getOneFriendDataRunning = true;
    	
		getChosenOne(listener, friend);
    	
    	getOneFriendDataRunning = false;
    }
    
    public void getTwitterFriend(final TwitterDataListener tdlistener) {
    	if (EpicFeedConsts.DEBUG_TWT) Log.w(TAG, "+++++++ getTwitterFriend() ++++++++++");
    	getTwitterFriends(tdlistener);
    }
    
    private boolean buildFriendsTimelineRunning = false;
    
    private void buildFriendsTimeline(Status[] statuses) {
    	buildFriendsTimelineRunning = true;
    	rateLimitFTLFlag = false;
    	
    	if(statuses != null && statuses.length > 0) {
    		try {
    			List<FeedTextMsg> twtFriendStatus = new ArrayList<FeedTextMsg>();
    			for(int i = 0; i < statuses.length; i++) {
    				FeedTextMsg msg = new FeedTextMsg();
    				msg.setMsgType(FeedMessageListAdapter.RECEIVED_TWEET);
    				msg.setMsgTypeFlg(FeedTextMsg.TYPE_TWEET);
    				msg.msgID = i + "";
    				msg.feedMsgID = String.valueOf(statuses[i].getId());
    				msg.msgTxt = statuses[i].getText();
    				msg.msgDate = statuses[i].getCreatedAt();
    				msg.msgForwards = statuses[i].getRetweetCount();
    				msg.msgLikes = statuses[i].getFavoriteCount();
    				msg.setReceivedMsg(true);
    				//Log.i(TAG, "*  *  *  *  * Friend Timeline copy over >> " + msg.msgID + " : " + msg.msgTxt);
    				
    				twtFriendStatus.add(msg);
    				
    			}
    			
    			txtMsgs.addAll(twtFriendStatus);
    			buildFriendsTimelineRunning = false;
    			if(twtListener != null) 
    				twtListener.onUpdate();
    		} catch (Exception e) {
    			Log.i(TAG, "*  *  *  *  * Exception buildFriendsTimeline >> " + e.getMessage());
    			rateLimitFTLFlag = true;
    			//e.printStackTrace();
    		}
    	}
    }
    
    private boolean buildMyTimelineRunning = false;
    
    private void buildMyTimeline(Status[] statuses) {
    	buildMyTimelineRunning = true;
    	rateLimitMTLFlag = false;
    	if(statuses != null && statuses.length > 0) {
    		try {
    			List<FeedTextMsg> twtFriendStatus = new ArrayList<FeedTextMsg>();
    			for(int i = 0; i < statuses.length; i++) {
    				FeedTextMsg msg = new FeedTextMsg();
    				msg.setMsgType(FeedMessageListAdapter.SENT_TWEET);
    				msg.setMsgTypeFlg(FeedTextMsg.TYPE_TWEET);
    				msg.msgID = i + "";
    				msg.feedMsgID = String.valueOf(statuses[i].getId());
    				msg.msgTxt = statuses[i].getText();
    				msg.msgDate = statuses[i].getCreatedAt();
    				msg.msgForwards = statuses[i].getRetweetCount();
    				msg.msgLikes = statuses[i].getFavoriteCount();
    				msg.setSentFlg(true);
					msg.setSentMsg(true);
    				//Log.i(TAG, "*  *  *  *  * My Timeline copy over >> " + msg.msgID + " : " + msg.msgTxt);
    				
    				twtFriendStatus.add(msg);
    			}
    			
    			txtMsgs.addAll(twtFriendStatus);
    			buildMyTimelineRunning = false;
    			if(twtListener != null) 
    				twtListener.onUpdate();
    		} catch (Exception e) {
    			Log.i(TAG, "*  *  *  *  * Exception buildMyTimeline >> " + e.getMessage());
    			rateLimitMTLFlag = true;
    			//e.printStackTrace();
    		}
    	}
    }
    
    private boolean buildSentDirectMessagesRunning = false;
    
    private void buildSentDirectMessages(DirectMessage[] dirMsg) {
    	buildSentDirectMessagesRunning = true;
    	rateLimitBSDMFlag = false;
    	if(dirMsg != null && dirMsg.length > 0) {
    		try {
    			List<FeedTextMsg> twtDirMsg = new ArrayList<FeedTextMsg>();
    			for(int j = 0; j < dirMsg.length; j++) {
    				FeedTextMsg msg = new FeedTextMsg();
    				msg.setMsgType(FeedMessageListAdapter.SENT_TWITTER_MSG);
    				msg.setMsgTypeFlg(FeedTextMsg.TYPE_TWITTER_MSG);
    				msg.msgID = j + "";
    				msg.feedMsgID = String.valueOf(dirMsg[j].getId());
    				msg.msgTxt = dirMsg[j].getText();
    				msg.msgDate = dirMsg[j].getCreatedAt();
    				msg.setSentFlg(true);
					msg.setSentMsg(true);
                    //Log.i(TAG, "*  *  *  *  * Sent Direct Messages copy over >> " + msg.msgID + " : " + msg.msgTxt);
    				
    				twtDirMsg.add(msg);
    			}
    			
    			txtMsgs.addAll(twtDirMsg);
    			buildSentDirectMessagesRunning = false;
    			if(twtListener != null) 
    				twtListener.onUpdate();
    		} catch (Exception e) {
    			Log.i(TAG, "*  *  *  *  * Exception buildSDirectMessages >> " + e.getMessage());
    			rateLimitBSDMFlag = true;
    			//e.printStackTrace();
    		}
    	}
    }
    
    private boolean buildReceivedDirectMessageRunning = false;
    
    private void buildReceivedDirectMessage(DirectMessage[] dirMsg) {
    	buildReceivedDirectMessageRunning = true;
    	rateLimitBRDMFlag = false;
    	if(dirMsg != null && dirMsg.length > 0) {
    		try {
    			List<FeedTextMsg> twtDirMsg = new ArrayList<FeedTextMsg>();
    			for(int j = 0; j < dirMsg.length; j++) {
    				FeedTextMsg msg = new FeedTextMsg();
    				msg.setMsgType(FeedMessageListAdapter.RECEIVED_TWITTER_MSG);
    				msg.setMsgTypeFlg(FeedTextMsg.TYPE_TWITTER_MSG);
    				msg.msgID = j + "";
    				msg.feedMsgID = String.valueOf(dirMsg[j].getId());
    				msg.msgTxt = dirMsg[j].getText();
    				msg.msgDate = dirMsg[j].getCreatedAt();
    				msg.setReceivedMsg(true);
                    //Log.i(TAG, "*  *  *  *  * Received Direct Messages copy over >> " + msg.msgID + " : " + msg.msgTxt);
    				
    				twtDirMsg.add(msg);
    			}
    			
    			txtMsgs.addAll(twtDirMsg);
    			buildReceivedDirectMessageRunning = false;
    			if(twtListener != null) 
    				twtListener.onUpdate();
    		} catch (Exception e) {
    			Log.i(TAG, "*  *  *  *  * Exception buildRDirectMessages >> " + e.getMessage());
    			rateLimitBRDMFlag = true;
    			//e.printStackTrace();
    		}
    	}
    } 
    
    private boolean mySentDMsRunning = false;
    private boolean myRecievedDMsRunning = false;
    private boolean otherPersonTweetsRunning = false;
    private boolean mySentTweetsRunning = false;
    
    public void getTwitterData() {
    	if(mTwitter11 == null) {
    		Log.w(TAG, "Twitter session is NULL - cannot get Twitter data");
    		return;
    	}
    	
    	mySentDMsRunning = true;
    	getSentDMs();
    	mySentDMsRunning = false;
    	
    	myRecievedDMsRunning = true;
    	getRecievedDMs();
    	myRecievedDMsRunning = false;
    	
    	otherPersonTweetsRunning = true;
    	getTweets();
    	otherPersonTweetsRunning = false;
    	
    	mySentTweetsRunning = true;
    	getMyTweets();
    	mySentTweetsRunning = false;
    }
} 