package com.sdmmllc.epicfeed.data;

public interface FacebookDataListener {
	public void onUpdate();
	public void onComplete();
}
