package com.sdmmllc.epicfeed.data;

public interface TwitterDataListener {
	public void onUpdate();
	public void onComplete();
}
