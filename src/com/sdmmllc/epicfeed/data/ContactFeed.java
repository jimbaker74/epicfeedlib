package com.sdmmllc.epicfeed.data;


public class ContactFeed {
	
	public static String 
		FEED_TYPE_FACEBOOK 	= "facebook",
		FEED_TYPE_LINKEDIN 	= "linkedIn",
		FEED_TYPE_TWITTER 	= "twitter",
		FEED_TYPE_PINTEREST	= "pinterest";

	public static String DB_TABLE_NAME = "feed_user_tbl";
	public static String[] DB_TABLE_COLUMNS = {
		"_id",
		"phone_id",
		"feed_type",
		"user_id",
		"password_txt",
		"feed_id",
		"feed_update_cnt",
		"feed_flgs",
		"feed_checked_timestamp",
		"feed_activity_timestamp"
	};
	
	public static int _ID 				= 0;
	public static int PHONE_ID			= 1;
	public static int FEED_TYPE			= 2;
	public static int USER_ID			= 3;
	public static int PASSWORD_TXT		= 4;
	public static int FEED_ID			= 5;
	public static int FEED_UPDATE_COUNT	= 6;
	public static int FEED_FLGS			= 7;
	public static int FEED_CHECKED_TS	= 8;
	public static int FEED_ACTIVITY_TS	= 9;

	public static String[] DB_CREATE_COLUMNS = {
       	DB_TABLE_COLUMNS[_ID] + " integer primary key autoincrement, ",
        DB_TABLE_COLUMNS[PHONE_ID] + " TEXT NOT NULL, ",
        DB_TABLE_COLUMNS[FEED_TYPE] + " TEXT, ",
        DB_TABLE_COLUMNS[USER_ID] + " TEXT, ",
        DB_TABLE_COLUMNS[PASSWORD_TXT] + " TEXT, ", 
        DB_TABLE_COLUMNS[FEED_ID] + " TEXT NOT NULL, ", 
        DB_TABLE_COLUMNS[FEED_UPDATE_COUNT] + " INTEGER NOT NULL DEFAULT \"0\", ", 
        DB_TABLE_COLUMNS[FEED_FLGS] + " INTEGER NOT NULL DEFAULT \"0\", ",
        DB_TABLE_COLUMNS[FEED_CHECKED_TS] + " INTEGER NOT NULL DEFAULT \"0\", ", 
        DB_TABLE_COLUMNS[FEED_ACTIVITY_TS] + " INTEGER NOT NULL DEFAULT \"0\" " 
	};
	
	private String 
		feedSystemId = "",
		feedLoginId = "",
		phoneId = "",
		feedType = "",
		passwordTxt = "";
	
	public String
		profileURL = "";
	
	private Integer
		feedUpdates = 0;
	
	private Long
		feedId = Long.valueOf(0),
		feedLastCheckedTimestamp = Long.valueOf(0),
		feedRecentActivityTimestamp = Long.valueOf(0);
	
	private int feedFlgs = 0;

	public ContactFeed(String contactId) {
		phoneId = contactId;
	}
	
	public String getFeedLoginId() {
		return feedLoginId;
	}

	public void setFeedLoginId(String feedLoginId) {
		this.feedLoginId = feedLoginId;
	}

	public String getPhoneId() {
		return phoneId;
	}

	public void setPhoneId(String phoneId) {
		if (phoneId != null) this.phoneId = phoneId;
	}

	public String getFeedType() {
		return feedType;
	}

	public void setFeedType(String feedType) {
		if (feedType != null) this.feedType = feedType;
	}

	public String getPasswordTxt() {
		return passwordTxt;
	}

	public void setPasswordTxt(String passwordTxt) {
		this.passwordTxt = passwordTxt;
	}

	public String getFeedSystemId() {
		return feedSystemId;
	}

	public void setFeedSystemId(String feedId) {
		if (feedId != null) this.feedSystemId = feedId;
	}

	public Long getFeedId() {
		return feedId;
	}

	public void setFeedId(Long feedContactId) {
		this.feedId = feedContactId;
	}

	public int getFeedUpdates() {
		synchronized (feedUpdates) {
			return feedUpdates;
		}
	}

	public void setFeedUpdates(int feedUpdates) {
		if (feedUpdates >= 0) 
			synchronized (this.feedUpdates) {
				this.feedUpdates = feedUpdates;
			}
	}
	
	public void incrementFeedUpates() {
		synchronized (feedUpdates) {
			feedUpdates++;
		}
	}

	public int getFeedFlgs() {
		return feedFlgs;
	}

	public void setFeedFlgs(int feedFlgs) {
		this.feedFlgs = feedFlgs;
	}

	public Long getFeedLastCheckedTimestamp() {
		return feedLastCheckedTimestamp;
	}

	public void setFeedLastCheckedTimestamp(Long feedLastCheckedTimestamp) {
		if (feedLastCheckedTimestamp > 0)
			this.feedLastCheckedTimestamp = feedLastCheckedTimestamp;
	}

	public Long getFeedRecentActivityTimestamp() {
		return feedRecentActivityTimestamp;
	}

	public void setFeedRecentActivityTimestamp(
			Long feedRecentActivityTimestamp) {
		if (feedRecentActivityTimestamp > 0)
			this.feedRecentActivityTimestamp = feedRecentActivityTimestamp;
	}
	
	public ContactFeed clone() {
		ContactFeed retval = new ContactFeed("");
		
		retval.feedSystemId = this.feedSystemId;
		retval.feedLoginId = this.feedLoginId;
		retval.phoneId = this.phoneId;
		retval.feedType = this.feedType;
	    retval.passwordTxt = this.passwordTxt;
	    retval.profileURL = this.profileURL;
	    retval.feedUpdates = this.feedUpdates;
	    retval.feedId = this.feedId;
	    retval.feedLastCheckedTimestamp = this.feedLastCheckedTimestamp;
	    retval.feedRecentActivityTimestamp = this.feedRecentActivityTimestamp;
	    
	    return retval;
	}
	
	public String toString() {
		return "ContactFeed: " +
				" feedSystemId: " + feedSystemId +
				" feedLoginId: " + feedLoginId + 
				" phoneId: " + phoneId +
				"\n feedType: " + feedType +
				" passwordTxt: " + passwordTxt +
				" profileURL: " + profileURL + 
				" feedUpdates: " + feedUpdates +
				" feedId: " + feedId +
				"\n feedLastCheckedTimestamp: " + feedLastCheckedTimestamp +
				" feedRecentActivityTimestamp: " + feedRecentActivityTimestamp +
				" feedFlgs: " + feedFlgs;
		
	}
}
