package com.sdmmllc.epicfeed.data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Hashtable;

import android.database.Cursor;
import android.net.Uri;
import android.util.Log;

import com.sdmmllc.epicfeed.EpicFeedController;
import com.sdmmllc.epicfeed.utils.EpicFeedConsts;

public class FeedContactList extends ArrayList<EpicContact>{
	
	public static final String TAG = "FeedContactList";
	
    private static final long serialVersionUID = 1L;
    
    private String searchTerm = "";
    private boolean caseSensitive = false;
    
	public static ArrayList<String> phoneIds = new ArrayList<String>();
    public static Hashtable<String, String> threadIds = new Hashtable<String, String>();
    public static Hashtable<String, Long> threadOrder = new Hashtable<String, Long>();
    
    public FeedContactList search(String search) {
    	searchTerm = search;
    	FeedContactList results = new FeedContactList();
    	if (caseSensitive || !(searchTerm.toLowerCase().equals(searchTerm) ||
    			searchTerm.toUpperCase().equals(searchTerm))) {
	    	for (EpicContact contact : this) {
	    		if (contact.name.contains(searchTerm)) {
	    			results.add(contact);
	    		}
	    	}
    	} else {
    		String lowerTerm = searchTerm.toLowerCase();
        	for (EpicContact contact : this) {
        		if (contact.name.toLowerCase().contains(lowerTerm)) {
        			results.add(contact);
        		}
        	}
    	}
    	return results;
    }
    
    public FeedContactList getSearchList() {
    	return search(searchTerm);
    }
    
    public void setSearch(String term) {
    	searchTerm = term;
    }

    public FeedContactList sort(FeedContactListComparator comparator) {
        Collections.sort(this, comparator);
        return (FeedContactList) this.clone();
    }
    
    public static void getContactsList(Contact.UpdateListener listener) {
    	try {
       	    phoneIds = new ArrayList<String>();
    	    threadIds = new Hashtable<String, String>();
    	    threadOrder = new Hashtable<String, Long>();
    		Uri mSmsinboxQueryUri = Uri.parse("content://sms");
            Cursor cursor1 = EpicFeedController.getContext().getContentResolver().query(mSmsinboxQueryUri,
            		new String[] { "_id", "date", "thread_id", "address" }, 
            		null, 
            		null, 
            		"date DESC");
            String[] columns = new String[] { "thread_id", "address", "date"};

            Contact.addListener(listener);
            if (cursor1.getCount() > 0) {
                while (cursor1.moveToNext()){
                    String address = cursor1.getString(cursor1.getColumnIndex(columns[1]));
                    if (!phoneIds.contains(address)) {
                		if (EpicFeedConsts.DEBUG_FB) Log.d(TAG, "************** Adding phone number:" + address);
                		if (EpicFeedConsts.DEBUG_FB) Log.d(TAG, "************** thread ID:" + cursor1.getString(cursor1.getColumnIndex(columns[0])));
                    	phoneIds.add(address);
                    	threadIds.put(address, cursor1.getString(cursor1.getColumnIndex(columns[0])));
                    	threadOrder.put(address, cursor1.getLong(cursor1.getColumnIndex(columns[2])));
                    }
                }
            }		
            cursor1.close();
    		if (EpicFeedConsts.DEBUG_FB) Log.d(TAG, "************** Text threads found phoneIds.size()" + phoneIds.size());

    		
    		ContactList contacts = ContactList.getByNumbers(phoneIds, false);
    		for (Contact oneContact : contacts) {
        		if (EpicFeedConsts.DEBUG_FB) Log.d(TAG, "************** ContactList contact Name and Number:" + oneContact.getNameAndNumber());
    		}
    	} catch (Throwable t) {
    		t.printStackTrace();
    	}
    }

	public enum FeedContactListComparator implements Comparator<EpicContact> {
	    FIRST_NAME {
	        public int compare(EpicContact o1, EpicContact o2) {
	            return o1.firstName.compareTo(o2.firstName);
	        }},
	        
	    LAST_NAME {
	        public int compare(EpicContact o1, EpicContact o2) {
	            return o1.lastName.compareTo(o2.lastName);
	        }},
	        
	    DISPLAY_NAME {
	        public int compare(EpicContact o1, EpicContact o2) {
	            return o1.name.compareTo(o2.name);
	        }},

	    THREAD_DATE {
	        public int compare(EpicContact o1, EpicContact o2) {
	            return o1.threadDt.compareTo(o2.threadDt);
	        }};

	    public static Comparator<EpicContact> descending(final Comparator<EpicContact> other) {
	        return new Comparator<EpicContact>() {
	            public int compare(EpicContact o1, EpicContact o2) {
	                return -1 * other.compare(o1, o2);
	            }
	        };
	    }

	    public static Comparator<EpicContact> getComparator(final FeedContactListComparator... multipleOptions) {
	        return new Comparator<EpicContact>() {
	            public int compare(EpicContact o1, EpicContact o2) {
	                for (FeedContactListComparator option : multipleOptions) {
	                    int result = option.compare(o1, o2);
	                    if (result != 0) {
	                        return result;
	                    }
	                }
	                return 0;
	            }
	        };
	    }
	}

}
