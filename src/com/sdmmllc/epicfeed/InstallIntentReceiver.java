package com.sdmmllc.epicfeed;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.util.Log;

import com.sdmm.epicfeed.R;
import com.sdmmllc.epicfeed.ui.Act_Authenticate;
import com.sdmmllc.epicfeed.utils.EpicFeedConsts;
import com.sdmmllc.superdupersmsmanager.sdk.SDSmsManager;

public class InstallIntentReceiver extends BroadcastReceiver {

	String TAG = "InstallIntentReceiver";
	ScanPkgList scanList;
	Context ctx;
	PackageManager pkgMgr;
	NotificationManager nm;
	String installPkgName = "";
	
	@Override
	public void onReceive(Context context, Intent intent) {
    	if (EpicFeedConsts.DEBUG_INSTALL_RECEIVER) 
			Log.i(TAG, "received intent for add/changed/replaced/install");
	    ctx = context;
	    pkgMgr = context.getPackageManager();
	    nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
	    installPkgName = intent.getDataString();
	    installPkgName = installPkgName.substring(installPkgName.indexOf(":")+1, installPkgName.length());
    	if (EpicFeedConsts.DEBUG_INSTALL_RECEIVER) 
    		Log.i(TAG, "new scan for:" + installPkgName);
		scanList = new ScanPkgList();
		// add the app - returns true if added (non-system packages are not added
		if (scanList.addInstalledApp(pkgMgr, installPkgName)) {
			scanList.scanForSMS(pkgMgr);
	    	if (EpicFeedConsts.DEBUG_INSTALL_RECEIVER) 
	    		Log.i(TAG, "scanned : scanList.has999App(): " + scanList.has999App()
					+ " scanList.hasOver999App(): " + scanList.hasOver999App());
	       	if (EpicFeedConsts.DEBUG_HANGOUTS) Log.e(TAG, "onReceive scanList.warnHangoutsInstalled");
			if (!SDSmsManager.hasKitKat() && 
					((!scanList.hasThisSpaApp(ctx) && (scanList.has999App()|scanList.hasOver999App()))
					|| scanList.warnHangoutsInstalled(ctx))) {
		    	if (EpicFeedConsts.DEBUG_INSTALL_RECEIVER) 
		    		Log.i(TAG, "found high-priority install");
				SharedPreferences settings = ctx.getSharedPreferences(EpicFeedConsts.SCAN_FILE, 0);
				SharedPreferences.Editor editor = settings.edit();
				editor.putBoolean(EpicFeedConsts.warnInstall, true);
				editor.putString(EpicFeedConsts.warnPackage, installPkgName);
				editor.commit();
		    	sendNotification(ctx);
			}
	    	if (EpicFeedConsts.DEBUG_INSTALL_RECEIVER) 
	    		Log.i(TAG, "scanned for SMS apps");
		}
		if (scanList.warnHangoutsInstalled(ctx) && !SDSmsManager.hasKitKat()) {
			sendNotification(ctx);
		}
	}

	private  void sendNotification(Context context) {
		Intent notificationIntent = new Intent(context, Act_Authenticate.class);
		notificationIntent.putExtra(EpicFeedConsts.AUTH_STATUS, false);
		notificationIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                | Intent.FLAG_ACTIVITY_CLEAR_TOP);		
		PendingIntent contentIntent = PendingIntent.getActivity(context, 0, notificationIntent, 0);
		int icon = R.drawable.notification_icon;
		Notification notification;
		CharSequence tickerText = context.getString(R.string.statusNotificationTicker);
		CharSequence contentTitle = context.getString(R.string.statusNotificationTitle);
		CharSequence contentText = context.getString(R.string.statusNotificationText);
		long when = System.currentTimeMillis();
		notification = new Notification(icon, tickerText, when);
		notification.flags |= Notification.FLAG_AUTO_CANCEL;
		notification.setLatestEventInfo(context, contentTitle, contentText, contentIntent);
		nm.notify(R.string.smsNotificationID, notification);
	}
	
}
