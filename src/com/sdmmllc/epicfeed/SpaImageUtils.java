package com.sdmmllc.epicfeed;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Iterator;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Debug;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.sdmmllc.epicfeed.utils.EpicFeedConsts;

@SuppressLint("UseSparseArrays")
public class SpaImageUtils {
	
	public static String TAG = "SpaImageUtils";
	public static int JELLY_BEAN = 16;

	public static int getByteCount(Bitmap bitmap) {
	    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB_MR1) {
	        return bitmap.getRowBytes() * bitmap.getHeight();
	    } else {
	        return bitmap.getByteCount();
	    }
	}
	
	public static void setBackgroundNull(View v) {
		if (v == null) return;
		SpaImageUtilsV1.setBackgroundDrawableNull(v);
	}
	
	public static void setBackgroundDrawable(View v, int resId) {
		if (v == null) return;
		SpaImageUtilsV1.setBackgroundDrawable(v, resId);
	}
	
	public static void setImageDrawable(ImageView v, int resId) {
		if (v == null) return;
		v.setImageDrawable(SpaDrawables.getInstance().get(resId));
	}
	
	public static void setBackgroundDrawable(View v, Drawable sd) {
		if (v == null) return;
		SpaImageUtilsV1.setBackgroundDrawable(v, sd);
	}
	
	public static boolean checkBitmapFitsInMemory(long bmpwidth, long bmpheight, double d) {
	    long reqsize=(long)(bmpwidth*bmpheight*d);
	    long allocNativeHeap = Debug.getNativeHeapAllocatedSize();

	    final long heapPad=(long) Math.max(4*1024*1024,Runtime.getRuntime().maxMemory()*0.1);
	    
	    if ((reqsize + allocNativeHeap + heapPad) >= Runtime.getRuntime().maxMemory()) {
	        return false;
	    }
	    return true;

	}
	
	public static Drawable decodeDrawable(Context ctx, int res, int height, int width){
		return decodeResource(ctx, res, height, width, false).getCurrent(); 
		
	}
	
	public static Drawable getScaledDrawable(Context c, Drawable d, int height, int width) {
		Bitmap b = ((BitmapDrawable)d).getBitmap();
		
		double scale = 1.0;
		int scaleWidth = b.getWidth(), scaleHeight = b.getHeight();
		if (width > 0 && scaleWidth > width) scale = width/scaleWidth;
		if (height > 0 && scaleHeight > height) scale = height/scaleHeight;
		
		return new BitmapDrawable(c.getResources(),
				Bitmap.createScaledBitmap(b, (int)scale*scaleWidth, (int)scale*scaleHeight, true));
	}
	
	public static Bitmap scaleBitmap(Bitmap orig, int height, int width) {

		int bmHeight = orig.getHeight();
		int bmWidth = orig.getWidth();
		
		int scale = 1;
		if (bmHeight > height || bmWidth > width) {
			int IMAGE_MAX_SIZE = height;
			if ((bmWidth - width) > (bmHeight - height)) {
				IMAGE_MAX_SIZE = width;
			}
			int IMAGE_MAX_ADJ = bmHeight;
			if ((bmWidth - width) > (bmHeight - height)) {
				IMAGE_MAX_ADJ = bmWidth;
			}
			
			scale = (int)Math.pow(2, (int) Math.round(Math.log(IMAGE_MAX_SIZE / 
					(double) IMAGE_MAX_ADJ) / Math.log(0.5)));
		}
		
		return Bitmap.createScaledBitmap(orig, scale*bmWidth, scale*bmHeight, true);

	}
	
	public static BitmapDrawable decodeResource(Context ctx, int res, int height, int width, boolean animation){
		BitmapDrawable b = null;

		//Decode image size
		BitmapFactory.Options o = new BitmapFactory.Options();
		o.inJustDecodeBounds = true;

		BitmapFactory.decodeResource(ctx.getResources(), res, o);

		//Decode with inSampleSize
		BitmapFactory.Options o2 = new BitmapFactory.Options();
		o2.inSampleSize = calculateInSampleSize(o, width, height, animation);
		o2.inPurgeable = true;
		b = new BitmapDrawable(ctx.getResources(), BitmapFactory.decodeResource(ctx.getResources(), res, o2));

		return b;
	}
	
	public static Bitmap decodeBitmap(Context ctx, int res, int height, int width, boolean animation){
		//Decode image size
		BitmapFactory.Options o = new BitmapFactory.Options();
		o.inJustDecodeBounds = true;

		BitmapFactory.decodeResource(ctx.getResources(), res, o);

		//Decode with inSampleSize
		BitmapFactory.Options o2 = new BitmapFactory.Options();
		o2.inSampleSize = calculateInSampleSize(o, width, height, animation);
		o2.inPurgeable = true;

		return BitmapFactory.decodeResource(ctx.getResources(), res, o2);
	}
	
	public static void imageGC() {
		System.gc();
		Runtime.getRuntime().gc();
	}
	
	public static int calculateScale(BitmapFactory.Options o, int width, int height) {
		int scale = 1;
		if (o.outHeight > height || o.outWidth > width) {
			int IMAGE_MAX_SIZE = height;
			if ((o.outWidth - width) > (o.outHeight - height)) {
				IMAGE_MAX_SIZE = width;
			}
			int IMAGE_MAX_ADJ = o.outHeight;
			if ((o.outWidth - width) > (o.outHeight - height)) {
				IMAGE_MAX_ADJ = o.outWidth;
			}
			
			scale = (int)Math.pow(2, (int) Math.round(Math.log(IMAGE_MAX_SIZE / 
					(double) IMAGE_MAX_ADJ) / Math.log(0.5)));
		}
		return scale;
	}

	public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight, boolean animation) {
	    // Raw height and width of image
	    final int height = options.outHeight;
	    final int width = options.outWidth;
	    int inSampleSize = 1;
	
	    if (height > reqHeight || width > reqWidth) {
	
	        final int halfHeight = height / 2;
	        final int halfWidth = width / 2;
	
	        // Calculate the largest inSampleSize value that is a power of 2 and keeps both
	        // height and width larger than the requested height and width.
	        while ((halfHeight / inSampleSize) > reqHeight
	                && (halfWidth / inSampleSize) > reqWidth) {
	            inSampleSize *= 2;
	        }
	    }
	
	    //if (animation) inSampleSize = 2*inSampleSize;
	    return inSampleSize;
	}
	
	private static void createFakeBitmap(int width, int height) {
		Bitmap.Config conf = Bitmap.Config.ARGB_8888;
		WeakReference<Bitmap> bm = new WeakReference<Bitmap>(Bitmap.createBitmap(width, height, conf));
	   	if (EpicFeedConsts.DEBUG_OOME) Log.i(TAG, "Fake bitmap actual memory use: " + getByteCount(bm.get()));
	   	bm.get().recycle();
		bm = null;
	}
	
	public synchronized static Bitmap decodeResource(Resources resources, int resId, int reqWidth, int reqHeight, boolean animation) {
		// First decode with inJustDecodeBounds=true to check dimensions
	    final BitmapFactory.Options options = new BitmapFactory.Options();
	    options.inJustDecodeBounds = true;
	    BitmapFactory.decodeResource(resources, resId, options);

	    if (EpicFeedConsts.DEBUG_OOME) Log.i(TAG, "Requested image size before resampling: " + options.outHeight * options.outWidth * 4);
		
	    // Calculate inSampleSize
	    options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight, animation);
	    
	    if (EpicFeedConsts.DEBUG_OOME) Log.i(TAG, "Requested image size after resampling to " + options.inSampleSize + 
	    		": " + options.outHeight * options.outWidth * 2.25);

	    if ((EpicFeedConsts.DEBUG_OOME) && !checkBitmapFitsInMemory(options.outWidth, options.outHeight, 4)) {
	    	Log.i(TAG, "*** Headed for a OOM crash right now!! ***");
	    }
	    // Decode bitmap with inSampleSize set
	    options.inJustDecodeBounds = false;
		options.inPurgeable = true;
		//if (animation) options.inSampleSize = 2;

	    try {
	    	if (animation) createFakeBitmap(options.outWidth/2, options.outHeight/2);
	    	else createFakeBitmap(options.outWidth, options.outHeight);
	    	imageGC();
	    } catch (OutOfMemoryError E) {
	    	E.printStackTrace();
	    	imageGC();
	    	OOMEListener.getInstance().outOfMemory();
	    	if (EpicFeedConsts.DEBUG_OOME) Log.i(TAG, "Out of memory creating fake bitmap");
	    	return null;
	    }

	    Bitmap b = null;
	    try {
	    	b = BitmapFactory.decodeResource(resources, resId, options);
	    } catch (OutOfMemoryError E) {
	    	E.printStackTrace();
	    	imageGC();
	    	OOMEListener.getInstance().outOfMemory();
	    	if (EpicFeedConsts.DEBUG_OOME) Toast.makeText(EpicFeedController.getContext(), "Out of memory", Toast.LENGTH_SHORT).show();
	    	return null;
	    }
    	if (EpicFeedConsts.DEBUG_OOME) Toast.makeText(EpicFeedController.getContext(), "Actual memory use: " + getByteCount(b), Toast.LENGTH_SHORT).show();
	    return b;
	}
    
	public synchronized static Bitmap decodeResource(Resources resources, int resId, boolean animation) {
		// First decode with inJustDecodeBounds=true to check dimensions
	    final BitmapFactory.Options options = new BitmapFactory.Options();
	    options.inJustDecodeBounds = true;
	    BitmapFactory.decodeResource(resources, resId, options);

	    if (EpicFeedConsts.DEBUG_OOME) Log.i(TAG, "Requested image size before resampling: " + options.outHeight * options.outWidth * 2.25);
		
	    // pixel density... looks like 2.25 on Kindle
	    //if (!checkBitmapFitsInMemory(options.outWidth, options.outHeight, 2.25)) {
		if ((EpicFeedConsts.DEBUG_OOME) && !checkBitmapFitsInMemory(options.outWidth, options.outHeight, 4)) {
	    	Log.i(TAG, "*** Headed for a OOM crash right now!! ***");
	    }
	    // Decode bitmap with inSampleSize set
	    options.inJustDecodeBounds = false;
		options.inPurgeable = true;
		//if (animation) options.inSampleSize = 2;

		try {
	    	if (animation) createFakeBitmap(options.outWidth/2, options.outHeight/2);
	    	else createFakeBitmap(options.outWidth, options.outHeight);
	    	imageGC();
	    } catch (OutOfMemoryError E) {
	    	E.printStackTrace();
	    	imageGC();
	    	OOMEListener.getInstance().outOfMemory();
	    	if (EpicFeedConsts.DEBUG_OOME) Log.i(TAG, "Out of memory creating fake bitmap");
	    	return null;
	    }

		Bitmap b = BitmapFactory.decodeResource(resources, resId, options);
    	if (EpicFeedConsts.DEBUG_OOME) Toast.makeText(EpicFeedController.getContext(), "Actual memory use: " + getByteCount(b), Toast.LENGTH_LONG).show();
	    return b;
	}
    
	public synchronized static Bitmap drawableToBitmap (Drawable drawable) {
	    if (drawable instanceof BitmapDrawable) {
	        return ((BitmapDrawable)drawable).getBitmap();
	    }

	    Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Config.ARGB_8888);
	    Canvas canvas = new Canvas(bitmap); 
	    drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
	    drawable.draw(canvas);

	    return bitmap;
	}
	
	public static abstract class WeakReferenceStorage<Key, Value> {
		private static HashMap<Integer, WeakReference<Bitmap>> objectsHash = new HashMap<Integer, WeakReference<Bitmap>>();
		private int imageCount = 0;

		public synchronized Bitmap get(Integer key) {
		    if (objectsHash.containsKey(key)) {
		        WeakReference<Bitmap> ref = objectsHash.get(key);
		        if (ref.get() == null) {
					if (EpicFeedConsts.DEBUG_OOME) Log.i(TAG, "Using referenced but not stored SpaDrawables");
		            objectsHash.put(key, new WeakReference<Bitmap>(createValueForKey(key)));
		            return (Bitmap)objectsHash.get(key).get();
		        } else {
					if (EpicFeedConsts.DEBUG_OOME) Log.i(TAG, "Using stored SpaDrawables");
		            return (Bitmap)ref.get();
		        }
		    } else {
				if (EpicFeedConsts.DEBUG_OOME) Log.i(TAG, "Creating new SpaDrawables");
		        objectsHash.put(key, new WeakReference<Bitmap>(createValueForKey(key)));
		        imageCount++;
				if (EpicFeedConsts.DEBUG_OOME) Log.i(TAG, "SpaBitmaps.get() size: " + objectsHash.size() + " imageCount:" + imageCount);
		        return (Bitmap)objectsHash.get(key).get();
		    }
		}
		
		protected synchronized void checkRecycled(Integer resId) {
		    if (EpicFeedConsts.DEBUG_OOME) Log.i(TAG, "Checking SpaBitmaps for recycled image: " + resId);
			if (objectsHash.containsKey(resId) && objectsHash.get(resId).get() != null 
					&& ((android.graphics.Bitmap)(objectsHash.get(resId).get())).isRecycled())
			{
			    if (EpicFeedConsts.DEBUG_OOME) Log.i(TAG, "Removing SpaBitmaps recycled image: " + resId);
				objectsHash.remove(resId);
			}
		}

		protected synchronized void remove(Bitmap sd) {
			WeakReference<Bitmap> ref;
			for(Iterator<WeakReference<Bitmap>> iter = objectsHash.values().iterator(); iter.hasNext();) {
				ref = iter.next();
				if (ref.get() != null && ref.get().equals(sd)) {
					iter.remove();
					imageCount--;
					if (EpicFeedConsts.DEBUG_OOME) Log.i(TAG, "SpaBitmaps size: " + objectsHash.size() + " imageCount:" + imageCount);
					if (EpicFeedConsts.DEBUG_OOME) Log.i(TAG, "Removing SpaBitmaps");
				}
			}
		}

		public synchronized void destroy() {
			if (EpicFeedConsts.DEBUG_OOME) Log.i(TAG, "destroy SpaBitmaps size: " + objectsHash.size());
	        imageCount = 0;
			objectsHash = new HashMap<Integer, WeakReference<Bitmap>>();
		}

		public synchronized void recycle() {
			if (EpicFeedConsts.DEBUG_OOME) Log.i(TAG, "recycle SpaBitmaps");
			objectsHash = new HashMap<Integer, WeakReference<Bitmap>>();
		}
		
		protected abstract Bitmap createValueForKey(Integer key);
	}
	
	public static class SpaBitmaps extends WeakReferenceStorage<Integer, Bitmap>{
		private static SpaBitmaps instance = null;

		private Resources resources;

		public SpaBitmaps(Resources resources) {
		    super();
		    this.resources = resources;
		}

		public synchronized static SpaBitmaps getInstance() {
		    if (instance == null) {
		        instance = new SpaBitmaps(EpicFeedController.getContext().getResources());
		    }
		    return instance;
		}
		
		public synchronized Bitmap get(Integer resId) {
			checkRecycled(resId);
			return super.get(resId);
		}

		@Override
		protected synchronized Bitmap createValueForKey(Integer resId) {
			Bitmap b;
		    try {
		    	b = BitmapFactory.decodeResource(resources, resId);
			   	if (EpicFeedConsts.DEBUG_OOME) Log.i(TAG, "SpaBitmaps.createValueForKey resId:" + resId + " bitmap actual memory use: " + getByteCount(b));
			    return b;
		    } catch (OutOfMemoryError E) {
		    	E.printStackTrace();
		    	imageGC();
		    	OOMEListener.getInstance().outOfMemory();
		    	if (EpicFeedConsts.DEBUG_OOME) Toast.makeText(EpicFeedController.getContext(), "Out of memory", Toast.LENGTH_SHORT).show();
		    	return null;
		    }
		}
	}
	
	// bitmap drawables
	public static abstract class DrawablesStorage<Key, Value>{
		private static HashMap<Integer, WeakReference<Drawable>> objectsHash = new HashMap<Integer, WeakReference<Drawable>>();
		private int imageCount = 0;

		public synchronized Drawable get(Integer key) {
		    if (EpicFeedConsts.DEBUG_OOME) Log.i(TAG, "DrawablesStorage get() called");
		    if (objectsHash.containsKey(key)) {
		        WeakReference<Drawable> ref = objectsHash.get(key);
		        if (ref.get() == null) {
					if (EpicFeedConsts.DEBUG_OOME) Log.i(TAG, "Using referenced but not stored SpaDrawables");
		            objectsHash.put(key, new WeakReference<Drawable>(createValueForKey(key)));
		            return (Drawable)objectsHash.get(key).get();
		        } else {
					if (EpicFeedConsts.DEBUG_OOME) Log.i(TAG, "Using stored SpaDrawables");
		            return (Drawable)ref.get();
		        }
		    } else {
				if (EpicFeedConsts.DEBUG_OOME) Log.i(TAG, "Creating new SpaDrawables");
		        objectsHash.put(key, new WeakReference<Drawable>(createValueForKey(key)));
		        imageCount++;
				if (EpicFeedConsts.DEBUG_OOME) Log.i(TAG, "SpaDrawables.get() size: " + objectsHash.size() + " imageCount:" + imageCount);
		        return (Drawable)objectsHash.get(key).get();
		    }
		}

		public synchronized Drawable getScaled(Integer key, int width, int height, boolean animation) {
		    if (EpicFeedConsts.DEBUG_OOME) Log.i(TAG, "DrawablesStorage getScaled() called");
		    if (objectsHash.containsKey(key)) {
		        WeakReference<Drawable> ref = objectsHash.get(key);
		        if (ref.get() == null) {
					if (EpicFeedConsts.DEBUG_OOME) Log.i(TAG, "Using reference but not stored scaled SpaDrawables");
		            objectsHash.put(key, new WeakReference<Drawable>(createScaledValueForKey(key, width, height, animation)));
		            return (Drawable)objectsHash.get(key).get();
		        } else {
					if (EpicFeedConsts.DEBUG_OOME) Log.i(TAG, "Using stored scaled SpaDrawables");
		            return (Drawable)ref.get();
		        }
		    } else {
				if (EpicFeedConsts.DEBUG_OOME) Log.i(TAG, "Creating new scaled SpaDrawables");
		        objectsHash.put(key, new WeakReference<Drawable>(createScaledValueForKey(key, width, height, animation)));
		        imageCount++;
				if (EpicFeedConsts.DEBUG_OOME) Log.i(TAG, "SpaDrawables.getScaled() size: " + objectsHash.size() + " imageCount:" + imageCount);
		        return (Drawable)objectsHash.get(key).get();
		    }
		}

		protected void checkRecycled(Integer resId) {
		    if (EpicFeedConsts.DEBUG_OOME) Log.i(TAG, "Checking DrawablesStorage for recycled image: " + resId);
			if (objectsHash.containsKey(resId) && objectsHash.get(resId).get() != null 
					&& BitmapDrawable.class.isAssignableFrom(objectsHash.get(resId).get().getClass())
					&& ((BitmapDrawable)(objectsHash.get(resId).get())).getBitmap() != null
					&& ((BitmapDrawable)(objectsHash.get(resId).get())).getBitmap().isRecycled())
			{
			    if (EpicFeedConsts.DEBUG_OOME) Log.i(TAG, "Removing DrawablesStorage recycled image: " + resId);
				objectsHash.remove(resId);
			}
		}

		public synchronized void destroy() {
			if (EpicFeedConsts.DEBUG_OOME) Log.i(TAG, "destroy SpaDrawables size: " + objectsHash.size());
	        imageCount = 0;
			objectsHash = new HashMap<Integer, WeakReference<Drawable>>();
		}

		protected synchronized void remove(Drawable sd) {
			WeakReference<Drawable> ref;
			for(Iterator<WeakReference<Drawable>> iter = objectsHash.values().iterator(); iter.hasNext();) {
				ref = iter.next();
				if (ref.get() != null && ref.get().equals(sd)) {
					iter.remove();
					imageCount--;
					if (EpicFeedConsts.DEBUG_OOME) Log.i(TAG, "SpaDrawables size: " + objectsHash.size() + " imageCount:" + imageCount);
				}
			}
		}

		public synchronized void recycle() {
			if (EpicFeedConsts.DEBUG_OOME) Log.i(TAG, "destroy SpaDrawables size: " + objectsHash.size());
			if (EpicFeedConsts.DEBUG_OOME) Log.i(TAG, "recycle SpaDrawables");
			objectsHash = new HashMap<Integer, WeakReference<Drawable>>();
		}
		
		protected abstract Drawable createValueForKey(Integer key);
		protected abstract Drawable createScaledValueForKey(Integer key, int width, int height, boolean animation);
	}
	
	public static class SpaDrawables extends DrawablesStorage<Integer, Drawable>{
		private static SpaDrawables instance = null;

		private Resources resources;

		public SpaDrawables(Resources resources) {
		    super();
		    this.resources = resources;
		}

		public synchronized static SpaDrawables getInstance() {
		    if (instance == null) {
		        instance = new SpaDrawables(EpicFeedController.getContext().getResources());
		    }
		    return instance;
		}

		@Override
		public synchronized Drawable get(Integer resId) {
		    if (EpicFeedConsts.DEBUG_OOME) Log.i(TAG, "Checking SpaDrawables for recycled image: " + resId);
			checkRecycled(resId);
			return super.get(resId);
		}

		@Override
		public synchronized Drawable getScaled(Integer resId, int width, int height, boolean animation) {
		    if (EpicFeedConsts.DEBUG_OOME) Log.i(TAG, "Checking SpaDrawables for recycled image: " + resId);
			checkRecycled(resId);
			return super.getScaled(resId, width, height, animation);
		}

		@Override
		protected synchronized Drawable createValueForKey(Integer resId) {
			Drawable d = null; 
			try {
				d = resources.getDrawable(resId);
		    } catch (OutOfMemoryError E) {
		    	imageGC();
		    	E.printStackTrace();
		    	OOMEListener.getInstance().outOfMemory();
		    	if (EpicFeedConsts.DEBUG_OOME) Toast.makeText(EpicFeedController.getContext(), "Out of memory", Toast.LENGTH_LONG).show();
		    }
		   	if (EpicFeedConsts.DEBUG_OOME) Log.i(TAG, "SpaDrawables.createValueForKey resId:" + resId + " bitmap actual memory use: " + getByteCount(((BitmapDrawable)d).getBitmap()));
		    return d; 
		}
		
		@Override
		protected synchronized Drawable createScaledValueForKey(Integer resId, int width, int height, boolean animation) {
			Bitmap b = decodeResource(resources, resId, width, height, animation);
			if (b == null) return null;
			BitmapDrawable bd = null;
			try {
				bd = new BitmapDrawable(resources, Bitmap.createScaledBitmap(b, width, height, true));
		    } catch (OutOfMemoryError E) {
		    	imageGC();
		    	E.printStackTrace();
		    	OOMEListener.getInstance().outOfMemory();
		    	if (EpicFeedConsts.DEBUG_OOME) Toast.makeText(EpicFeedController.getContext(), "Out of memory", Toast.LENGTH_LONG).show();
		    }
		   	if (EpicFeedConsts.DEBUG_OOME) Log.i(TAG, "SpaDrawables.createScaledValueForKey resId:" + resId + " bitmap actual memory use: " + getByteCount(b));
			b = null;
		    return bd;
		}
	}
	
	public static class OOMEListener extends SpaImageUtilsAdapter {
		private static SpaImageUtilsListener mActListener;
		private static OOMEListener instance = null;
		
		public OOMEListener() {
			super();
		}
		
		public static OOMEListener getInstance() {
		    if (instance == null) {
		        instance = new OOMEListener();
		    }
		    return instance;
		}
		
		@Override
		public void outOfMemory() {
			if (mActListener != null) mActListener.OOMEEvent();
			
		}

		@Override
		public void setActListener(SpaImageUtilsListener actListener) {
			mActListener = actListener;
		}
	}
	
}
