package com.sdmmllc.epicfeed;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.util.Log;

import com.sdmmllc.epicfeed.data.EpicContact;
import com.sdmmllc.epicfeed.utils.EpicFeedConsts;

public class SpaContactNewAPI {
	String TAG = "SpaContactNewAPI";
	ContentResolver cr;
	Context contactCtx;
	Uri contactsUri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
	String rowOrder = ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC";

	public SpaContactNewAPI (Context ctx) {
		contactCtx = ctx;
		cr = ctx.getContentResolver();
	}

	@SuppressWarnings("unused")
	private InputStream openPhoto(long contactId) {
		Uri contactUri = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, contactId);
		return ContactsContract.Contacts.openContactPhotoInputStream(cr, contactUri);
	}

   	
   	@SuppressWarnings("unused")
	private InputStream openPhotoOld(long contactId) {
    	Uri contactUri = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, contactId);
    	Uri photoUri = Uri.withAppendedPath(contactUri, ContactsContract.Contacts.PHOTO_ID);
    	Cursor cursor = cr.query(photoUri,
    			new String[] {ContactsContract.Contacts.PHOTO_ID}, null, null, null);
    	if (cursor == null) {
    		return null;
    	}
    	
    	try {
    		if (cursor.moveToFirst()) {
    			byte[] data = cursor.getBlob(0);
    			if (data != null) {
    				return new ByteArrayInputStream(data);
    			}
    		}
    	} finally {
    		cursor.close();
    	}
    	return null;
    }

	public EpicContact[] getContactData(Cursor cur){
    	List<EpicContact> tmpContacts = new ArrayList<EpicContact>();
    	if (EpicFeedConsts.debugLevel > 5) Log.i(TAG, "getContactData: number of contacts: " + cur.getCount());
		int count = 0;
		int numContacts = cur.getCount();
		if (EpicFeedConsts.debugLevel > 8) Log.i(TAG, "getContactData: getting contacts");
		String contactID, contactName, phoneType;
    	if (cur.moveToFirst()) {
    		int idColumn = cur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID);
    		int nameColumn = cur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
    		if (EpicFeedConsts.debugLevel > 8) Log.i(TAG, "getContactData: contact info count:" + numContacts);
    		for (int i = 0; i < numContacts; i++) {
    			phoneType = cur.getString(cur.getColumnIndex(
    					ContactsContract.CommonDataKinds.Phone.DATA2));
	    		if (cur.getString(idColumn) != null) contactID = cur.getString(idColumn).trim();
	    		else contactID = "";
	    		if (cur.getString(nameColumn) != null) contactName = cur.getString(nameColumn).trim();
	    		else contactName = "";
	    		try {
	    			EpicContact tmpContact = new EpicContact(cur.getString(
	    					cur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)));
	        		if (phoneType != null) {
		    			// check for the phone number type - Phone.DATA2 / Phone.TYPE
		    			int phoneTypeInt = 0;
		    			try {
		    				phoneTypeInt = Integer.parseInt(cur.getString(cur.getColumnIndex(
			    					ContactsContract.CommonDataKinds.Phone.DATA2)));
		    			} catch (Exception e) {
		    				e.printStackTrace();
		    			}
		    			switch (phoneTypeInt) {
			    			case ContactsContract.CommonDataKinds.Phone.TYPE_HOME: 
			    				tmpContact.type = "Home";
			    				break;
			    			case ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE: 
			    				tmpContact.type = "Mobile";
			    				break;
			    			case ContactsContract.CommonDataKinds.Phone.TYPE_WORK: 
			    				tmpContact.type = "Work";
			    				break;
			    			case ContactsContract.CommonDataKinds.Phone.TYPE_FAX_WORK: 
			    				tmpContact.type = "Fax Work";
			    				break;
			    			case ContactsContract.CommonDataKinds.Phone.TYPE_FAX_HOME: 
			    				tmpContact.type = "Fax Home";
			    				break;
			    			case ContactsContract.CommonDataKinds.Phone.TYPE_PAGER: 
			    				tmpContact.type = "Pager";
			    				break;
			    			case ContactsContract.CommonDataKinds.Phone.TYPE_OTHER: 
			    				tmpContact.type = 
			    					cur.getString(cur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.LABEL));
			    				break;
			    			case ContactsContract.CommonDataKinds.Phone.TYPE_CAR: 
			    				tmpContact.type = "Car";
			    				break;
			    			case ContactsContract.CommonDataKinds.Phone.TYPE_COMPANY_MAIN: 
			    				tmpContact.type = "Company Main";
			    				break;
			    			case ContactsContract.CommonDataKinds.Phone.TYPE_MAIN: 
			    				tmpContact.type = "Main";
			    				break;
			    			case ContactsContract.CommonDataKinds.Phone.TYPE_OTHER_FAX: 
			    				tmpContact.type = "Other Fax";
			    				break;
			    			case ContactsContract.CommonDataKinds.Phone.TYPE_WORK_MOBILE: 
			    				tmpContact.type = "Work Mobile";
			    				break;
			    			case ContactsContract.CommonDataKinds.Phone.TYPE_WORK_PAGER: 
			    				tmpContact.type = "Work Pager";
			    				break;
			    			default: 
			    				tmpContact.type = "";
		    			}
						if (tmpContact.type == null) tmpContact.type = ""; 
		    			
		    			if (EpicFeedConsts.debugLevel > 5) Log.i(TAG, "getContactData: getting contact: " + tmpContact.name);
		    			if (EpicFeedConsts.debugLevel > 9) Log.i(TAG, "getContactData: contact number: " + tmpContact.phoneID);
		    			if (EpicFeedConsts.debugLevel > 9) Log.i(TAG, "getContactData: contact type: " + tmpContact.type);
		    			if (EpicFeedConsts.debugLevel > 9) Log.i(TAG, "getContactData: contact datatype: " + 
		        				Integer.parseInt(cur.getString(cur.getColumnIndex(
				    					ContactsContract.CommonDataKinds.Phone.DATA2))));
	        		} else
	    				tmpContact.type = "";
	        			
		    		if (EpicFeedConsts.debugLevel > 7) Log.i(TAG, "getContactData: contact name: " + contactName);
	    			// Get the field values
	    			tmpContact.name = contactName;
	    			tmpContact.nameId = contactID;
	    			tmpContact.exists = false;
	    			//tmpContact.photo = Drawable.createFromStream(openPhoto(cur.getInt(idColumn)), "photo");
	    			if (tmpContact.phoneID != null && tmpContact.phoneID.length() > 0) {
		    			tmpContacts.add(tmpContact);
	    				count++;
	    			}
	    			if (EpicFeedConsts.debugLevel > 7) Log.i(TAG, "onCreate: " + tmpContacts.size() + " total contacts");
	    		} catch (Exception e) {
	    			// some data was invalid, skip contact...
	    		}
	    		cur.moveToNext();
    		}
    		cur.deactivate();
    		cur.close();
    	}
    	
    	// get only contacts that have phone numbers
    	EpicContact[] retContacts = new EpicContact[count];
    	if (count == 0) return retContacts;
    	count = 0;
    	// null contact set
    	if (tmpContacts != null)
	    	for (int i = 0; i < tmpContacts.size(); i++) {
	    		// null contact
	    		if (tmpContacts.get(i) != null &
	    				// null phone
	    				tmpContacts.get(i).phoneID != null &
	    				// zero length phone
	    				tmpContacts.get(i).phoneID.length() > 0) {
	    			retContacts[count] = tmpContacts.get(i);
	    			count++;
	    		}
	    	}
    	return retContacts;
    }
}
