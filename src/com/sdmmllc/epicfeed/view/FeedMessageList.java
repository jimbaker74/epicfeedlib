package com.sdmmllc.epicfeed.view;

import java.util.Comparator;
import java.util.List;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.ArrayAdapter;

import com.sdmm.epicfeed.R;
import com.sdmmllc.epicfeed.utils.EpicFeedConsts;

public class FeedMessageList {
	
	public static final String TAG = "SpaTextSMSList";
	
    public String listTitle;
    public List<FeedTextMsg> txtMsgList;
    public ArrayAdapter<FeedTextMsg> msgListAA;
    public boolean fakeMsgList = false;
    public int index;

    public FeedMessageList(int idx, String title, List<FeedTextMsg> msgList) {
    	index = idx;
    	listTitle = title;
    	txtMsgList = msgList;
    }
    
    public void addMsg(FeedTextMsg tmpMsg) {
    	txtMsgList.add(tmpMsg);
    	//if (msgListAA != null) msgListAA.add(tmpMsg);
    }
    
    public void getAA(Context ctx, LayoutInflater mInf) {
    	msgListAA = getArrayAdapter(ctx, mInf, txtMsgList);
		if (EpicFeedConsts.debugLevel > 10) Log.i(TAG, "getAA: got adapter"); 
    }
    
	private ArrayAdapter<FeedTextMsg> getArrayAdapter(Context context, final LayoutInflater mInflater, List<FeedTextMsg> mList) {
		if (EpicFeedConsts.debugLevel > 7) Log.i(TAG, "getArrayAdapter: getting adapter"); 
		return new ArrayAdapter<FeedTextMsg>(context, R.layout.smsitem, mList){
			
			
		};
	}
	
	public enum FeedListComparator implements Comparator<FeedTextMsg> {
	    TIMESTAMP {
	        public int compare(FeedTextMsg o1, FeedTextMsg o2) {
	            return Long.valueOf(o1.msgTimestamp).compareTo(o2.msgTimestamp);
	        }},
	        
	    DATE {
	        public int compare(FeedTextMsg o1, FeedTextMsg o2) {
	            return o1.msgDate.compareTo(o2.msgDate);
	        }},
	        
	    ID {
	        public int compare(FeedTextMsg o1, FeedTextMsg o2) {
	            return o1.msgID.compareTo(o2.msgID);
	        }};

	    public static Comparator<FeedTextMsg> decending(final Comparator<FeedTextMsg> other) {
	        return new Comparator<FeedTextMsg>() {
	            public int compare(FeedTextMsg o1, FeedTextMsg o2) {
	                return -1 * other.compare(o1, o2);
	            }
	        };
	    }

	    public static Comparator<FeedTextMsg> getComparator(final FeedListComparator... multipleOptions) {
	        return new Comparator<FeedTextMsg>() {
	            public int compare(FeedTextMsg o1, FeedTextMsg o2) {
	                for (FeedListComparator option : multipleOptions) {
	                    int result = option.compare(o1, o2);
	                    if (result != 0) {
	                        return result;
	                    }
	                }
	                return 0;
	            }
	        };
	    }
	}
}
