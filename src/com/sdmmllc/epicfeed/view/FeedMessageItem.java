package com.sdmmllc.epicfeed.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

public class FeedMessageItem extends RelativeLayout {
	
	private Context mContext;
	private AttributeSet mAttrs;
	private int mDefStyle = 0;
	private FeedTextMsg textMsg;
	private int messageType = 0;

	public FeedMessageItem(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		mContext = context;
	}

	public FeedMessageItem(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		mContext = context;
		mAttrs = attrs;
	}

	public FeedMessageItem(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		// TODO Auto-generated constructor stub
		mContext = context;
		mAttrs = attrs;
		mDefStyle = defStyle;
	}
	
	public FeedTextMsg getMsg() {
		return textMsg;
	}
	
	public int getMessageType() {
		return messageType;
	}
	
	public void setMessageType(int type) {
		messageType = type;
	}

}
