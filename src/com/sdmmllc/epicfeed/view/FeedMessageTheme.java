package com.sdmmllc.epicfeed.view;

import android.content.Context;

import com.sdmm.epicfeed.R;
import com.sdmmllc.epicfeed.utils.EpicFeedConsts;

public class FeedMessageTheme {

	public String themeSelect, themeDescr, themeDescrShort;
	private Context spaContext;
	public int sendColor, receiveColor, sendTextColor, receiveTextColor,
		textWhite, textBlack, textGray, textLightGray,
		headerTextColor, headerColor, textSize, sendImg, receiveImg,
		sendImgTop, sendImgBottom, sendImgMiddle, sendImgFull,
		receiveImgTop, receiveImgBottom, receiveImgMiddle, receiveImgFull,
		
		fbMyPostImgBg, fbContactPostImgBg, fbMyPostTextColor, fbContactPostTextColor,
		fbMyPostBgColor, fbContactPostBgColor;
	public boolean useTheme = false, threadAscending = true;
	
	public FeedMessageTheme (Context ctx) {
		spaContext = ctx;
    	textWhite = spaContext.getResources().getColor(R.color.instWhite);
    	textBlack = spaContext.getResources().getColor(R.color.instBlack);
    	textGray = spaContext.getResources().getColor(R.color.instVeryDarkGray);
    	textLightGray = spaContext.getResources().getColor(R.color.instVeryLightGray);
    	headerTextColor = textWhite;
    	headerColor = R.color.instVeryDarkGray;
    	textSize = EpicFeedConsts.txt_med;
	}
	
	public void setTextSize(int newSize) {
		if (newSize == EpicFeedConsts.txt_sml) {
			this.textSize = EpicFeedConsts.txt_sml;
		}
		else if (newSize == EpicFeedConsts.txt_lrg) {
			this.textSize = EpicFeedConsts.txt_lrg;
		} else {
			this.textSize = EpicFeedConsts.txt_med;
		}
	}

	public void setTextSize(String textSize) {
		if (textSize.equals(EpicFeedConsts.TEXT_SM)) {
			this.textSize = EpicFeedConsts.txt_sml;
		}
		else if (textSize.equals(EpicFeedConsts.TEXT_LRG)) {
			this.textSize = EpicFeedConsts.txt_lrg;
		} else {
			this.textSize = EpicFeedConsts.txt_med;
		}
	}
	
	public int getHeaderTextSize() {
		if (this.textSize == EpicFeedConsts.txt_sml) {
			return EpicFeedConsts.txt_sml;
		} else if (this.textSize == EpicFeedConsts.txt_lrg) {
			return EpicFeedConsts.txt_lrg;
		} else {
			return EpicFeedConsts.txt_med;
		}
	}
	
	public void setTheme(String theme) {
		themeSelect = theme;
    	if (themeSelect.equals(EpicFeedConsts.STANDARD_THEME)) {
	    	sendColor = R.color.instDarkBlue;
	    	receiveColor = R.color.instDarkRed;
	    	sendImg = R.drawable.sent_yellow;
	    	receiveImg = R.drawable.recd_orange;
	    	sendImgTop = R.drawable.sent_darkred_top;
	    	sendImgBottom = R.drawable.sent_darkred_bottom;
	    	sendImgMiddle = R.drawable.sent_darkred_middle;
	    	sendImgFull = R.drawable.sent_darkred_full;
	    	receiveImgTop = R.drawable.recd_darkblue_top;
	    	receiveImgBottom = R.drawable.recd_darkblue_bottom;
	    	receiveImgMiddle = R.drawable.recd_darkblue_middle;
	    	receiveImgFull = R.drawable.recd_darkblue_full;
	    	sendTextColor = textLightGray;
	    	receiveTextColor = textWhite;
	    	headerTextColor = textLightGray;
	    	headerColor = R.color.instGray;
	    	themeDescrShort = spaContext.getString(R.string.smsTextThemeStandardTxtShort);
	    	themeDescr = spaContext.getString(R.string.smsTextThemeBtnTxt);
	    	
	    	fbMyPostImgBg = R.drawable.sent_lgtblue;
	    	fbContactPostImgBg = R.drawable.sent_lgtblue; 
	    	fbMyPostTextColor = R.color.instBlue; 
	    	fbContactPostTextColor = R.color.instBlack;
			fbMyPostBgColor = R.color.instLightGray;
			fbContactPostBgColor = R.color.instWhite;
    	} else if (themeSelect.equals(EpicFeedConsts.PINK_THEME)) {
	    	sendColor = R.color.instPink;
	    	receiveColor = R.color.instPurple;
	    	sendImgTop = R.drawable.sent_purple_top;
	    	sendImgBottom = R.drawable.sent_purple_bottom;
	    	sendImgMiddle = R.drawable.sent_purple_middle;
	    	sendImgFull = R.drawable.sent_purple_full;
	    	receiveImgTop = R.drawable.recd_pink_top;
	    	receiveImgBottom = R.drawable.recd_pink_bottom;
	    	receiveImgMiddle = R.drawable.recd_pink_middle;
	    	receiveImgFull = R.drawable.recd_pink_full;
	    	sendTextColor = textLightGray;
	    	receiveTextColor = textWhite;
	    	headerTextColor = textBlack;
	    	headerColor = R.color.instLightPink;
	    	themeDescrShort = spaContext.getString(R.string.smsTextThemeBtnPinkTxtShort);
	    	themeDescr = spaContext.getString(R.string.smsTextThemeBtnPinkTxt);
	    	
	    	fbMyPostImgBg = R.drawable.sent_lgtblue;
	    	fbContactPostImgBg = R.drawable.sent_lgtblue; 
	    	fbMyPostTextColor = R.color.instBlue; 
	    	fbContactPostTextColor = R.color.instBlack;
			fbMyPostBgColor = R.color.instLightGray;
			fbContactPostBgColor = R.color.instWhite;
    	} else if (themeSelect.equals(EpicFeedConsts.BLUE_THEME)) {
	    	sendColor = R.color.instBlue;
	    	receiveColor = R.color.instDarkBlue;
	    	sendImgTop = R.drawable.sent_deepblue_top;
	    	sendImgBottom = R.drawable.sent_deepblue_bottom;
	    	sendImgMiddle = R.drawable.sent_deepblue_middle;
	    	sendImgFull = R.drawable.sent_deepblue_full;
	    	receiveImgTop = R.drawable.recd_blue_top;
	    	receiveImgBottom = R.drawable.recd_blue_bottom;
	    	receiveImgMiddle = R.drawable.recd_blue_middle;
	    	receiveImgFull = R.drawable.recd_blue_full;
	    	sendTextColor = textLightGray;
	    	receiveTextColor = textWhite;
	    	headerTextColor = textBlack;
	    	headerColor = R.color.instLightBlue;
	    	themeDescrShort = spaContext.getString(R.string.smsTextThemeBtnBlueTxtShort);
	    	themeDescr = spaContext.getString(R.string.smsTextThemeBtnBlueTxt);
	    	
	    	fbMyPostImgBg = R.drawable.sent_lgtblue;
	    	fbContactPostImgBg = R.drawable.sent_lgtblue; 
	    	fbMyPostTextColor = R.color.instBlue; 
	    	fbContactPostTextColor = R.color.instBlack;
			fbMyPostBgColor = R.color.instLightGray;
			fbContactPostBgColor = R.color.instWhite;
    	} else if (themeSelect.equals(EpicFeedConsts.GREEN_THEME)) {
	    	sendColor = R.color.instGreen;
	    	receiveColor = R.color.instDarkGreen;
	    	sendImgTop = R.drawable.sent_darkgreen_top;
	    	sendImgBottom = R.drawable.sent_darkgreen_bottom;
	    	sendImgMiddle = R.drawable.sent_darkgreen_middle;
	    	sendImgFull = R.drawable.sent_darkgreen_full;
	    	receiveImgTop = R.drawable.recd_green_top;
	    	receiveImgBottom = R.drawable.recd_green_bottom;
	    	receiveImgMiddle = R.drawable.recd_green_middle;
	    	receiveImgFull = R.drawable.recd_green_full;
	    	sendTextColor = textWhite;
	    	receiveTextColor = textLightGray;
	    	headerTextColor = textBlack;
	    	headerColor = R.color.instLightGreen;
	    	themeDescrShort = spaContext.getString(R.string.smsTextThemeBtnGreenTxtShort);
	    	themeDescr = spaContext.getString(R.string.smsTextThemeBtnGreenTxt);
	    	
	    	fbMyPostImgBg = R.drawable.sent_lgtblue;
	    	fbContactPostImgBg = R.drawable.sent_lgtblue; 
	    	fbMyPostTextColor = R.color.instBlue; 
	    	fbContactPostTextColor = R.color.instBlack;
			fbMyPostBgColor = R.color.instLightGray;
			fbContactPostBgColor = R.color.instWhite;
    	} else if (themeSelect.equals(EpicFeedConsts.DARK_THEME)) {
	    	sendColor = R.color.instDarkGray;
	    	receiveColor = R.color.instGray;
	    	sendImgTop = R.drawable.sent_gray_top;
	    	sendImgBottom = R.drawable.sent_gray_bottom;
	    	sendImgMiddle = R.drawable.sent_gray_middle;
	    	sendImgFull = R.drawable.sent_gray_full;
	    	receiveImgTop = R.drawable.recd_darkgray_top;
	    	receiveImgBottom = R.drawable.recd_darkgray_bottom;
	    	receiveImgMiddle = R.drawable.recd_darkgray_middle;
	    	receiveImgFull = R.drawable.recd_darkgray_full;
	    	sendTextColor = textWhite;
	    	receiveTextColor = textLightGray;
	    	headerTextColor = textWhite;
	    	headerColor = R.color.instBlack;
	    	themeDescrShort = spaContext.getString(R.string.smsTextThemeBtnDarkTxtShort);
	    	themeDescr = spaContext.getString(R.string.smsTextThemeBtnDarkTxt);
	    	
	    	fbMyPostImgBg = R.drawable.sent_lgtblue;
	    	fbContactPostImgBg = R.drawable.sent_lgtblue; 
	    	fbMyPostTextColor = R.color.instBlue; 
	    	fbContactPostTextColor = R.color.instBlack;
			fbMyPostBgColor = R.color.instLightGray;
			fbContactPostBgColor = R.color.instWhite;
    	} else if (themeSelect.equals(EpicFeedConsts.LIGHT_THEME)) {
	    	sendColor = R.color.instLightGray;
	    	receiveColor = R.color.instVeryLightGray;
	    	sendImgTop = R.drawable.sent_verylightgray_top;
	    	sendImgBottom = R.drawable.sent_verylightgray_bottom;
	    	sendImgMiddle = R.drawable.sent_verylightgray_middle;
	    	sendImgFull = R.drawable.sent_verylightgray_full;
	    	receiveImgTop = R.drawable.recd_medgray_top;
	    	receiveImgBottom = R.drawable.recd_medgray_bottom;
	    	receiveImgMiddle = R.drawable.recd_medgray_middle;
	    	receiveImgFull = R.drawable.recd_medgray_full;
	    	sendTextColor = textGray;
	    	receiveTextColor = textBlack;
	    	headerTextColor = textGray;
	    	headerColor = R.color.instWhite;
	    	themeDescrShort = spaContext.getString(R.string.smsTextThemeBtnLightTxtShort);
	    	themeDescr = spaContext.getString(R.string.smsTextThemeBtnLightTxt);

	    	fbMyPostImgBg = R.drawable.sent_lgtblue;
	    	fbContactPostImgBg = R.drawable.sent_lgtblue; 
	    	fbMyPostTextColor = R.color.instBlue; 
	    	fbContactPostTextColor = R.color.instBlack;
			fbMyPostBgColor = R.color.instLightGray;
			fbContactPostBgColor = R.color.instWhite;
    	} else if (themeSelect.equals(EpicFeedConsts.CLASSIC_STANDARD_THEME)) {
	    	sendColor = R.color.instDarkBlue;
	    	receiveColor = R.color.instDarkRed;
	    	sendImgTop = R.drawable.classic_sent_darkred_top;
	    	sendImgBottom = R.drawable.classic_sent_darkred_bottom;
	    	sendImgMiddle = R.drawable.classic_sent_darkred_middle;
	    	sendImgFull = R.drawable.classic_sent_darkred_full;
	    	receiveImgTop = R.drawable.classic_recd_darkblue_top;
	    	receiveImgBottom = R.drawable.classic_recd_darkblue_bottom;
	    	receiveImgMiddle = R.drawable.classic_recd_darkblue_middle;
	    	receiveImgFull = R.drawable.classic_recd_darkblue_full;
	    	sendTextColor = textLightGray;
	    	receiveTextColor = textWhite;
	    	headerTextColor = textLightGray;
	    	headerColor = R.color.instGray;
	    	themeDescrShort = spaContext.getString(R.string.smsTextClassicThemeStandardTxtShort);
	    	themeDescr = spaContext.getString(R.string.smsTextClassicThemeBtnTxt);
	    	
	    	fbMyPostImgBg = R.drawable.sent_lgtblue;
	    	fbContactPostImgBg = R.drawable.sent_lgtblue; 
	    	fbMyPostTextColor = R.color.instBlue; 
	    	fbContactPostTextColor = R.color.instBlack;
			fbMyPostBgColor = R.color.instLightGray;
			fbContactPostBgColor = R.color.instWhite;
    	} else if (themeSelect.equals(EpicFeedConsts.CLASSIC_PINK_THEME)) {
	    	sendColor = R.color.instPink;
	    	receiveColor = R.color.instPurple;
	    	sendImgTop = R.drawable.classic_sent_purple_top;
	    	sendImgBottom = R.drawable.classic_sent_purple_bottom;
	    	sendImgMiddle = R.drawable.classic_sent_purple_middle;
	    	sendImgFull = R.drawable.classic_sent_purple_full;
	    	receiveImgTop = R.drawable.classic_recd_pink_top;
	    	receiveImgBottom = R.drawable.classic_recd_pink_bottom;
	    	receiveImgMiddle = R.drawable.classic_recd_pink_middle;
	    	receiveImgFull = R.drawable.classic_recd_pink_full;
	    	sendTextColor = textLightGray;
	    	receiveTextColor = textWhite;
	    	headerTextColor = textBlack;
	    	headerColor = R.color.instLightPink;
	    	themeDescrShort = spaContext.getString(R.string.smsTextClassicThemeBtnPinkTxtShort);
	    	themeDescr = spaContext.getString(R.string.smsTextClassicThemeBtnPinkTxt);
	    	
	    	fbMyPostImgBg = R.drawable.sent_lgtblue;
	    	fbContactPostImgBg = R.drawable.sent_lgtblue; 
	    	fbMyPostTextColor = R.color.instBlue; 
	    	fbContactPostTextColor = R.color.instBlack;
			fbMyPostBgColor = R.color.instLightGray;
			fbContactPostBgColor = R.color.instWhite;
    	} else if (themeSelect.equals(EpicFeedConsts.CLASSIC_BLUE_THEME)) {
	    	sendColor = R.color.instBlue;
	    	receiveColor = R.color.instDarkBlue;
	    	sendImgTop = R.drawable.classic_sent_deepblue_top;
	    	sendImgBottom = R.drawable.classic_sent_deepblue_bottom;
	    	sendImgMiddle = R.drawable.classic_sent_deepblue_middle;
	    	sendImgFull = R.drawable.classic_sent_deepblue_full;
	    	receiveImgTop = R.drawable.classic_recd_blue_top;
	    	receiveImgBottom = R.drawable.classic_recd_blue_bottom;
	    	receiveImgMiddle = R.drawable.classic_recd_blue_middle;
	    	receiveImgFull = R.drawable.classic_recd_blue_full;
	    	sendTextColor = textLightGray;
	    	receiveTextColor = textWhite;
	    	headerTextColor = textBlack;
	    	headerColor = R.color.instLightBlue;
	    	themeDescrShort = spaContext.getString(R.string.smsTextClassicThemeBtnBlueTxtShort);
	    	themeDescr = spaContext.getString(R.string.smsTextClassicThemeBtnBlueTxt);
	    	
	    	fbMyPostImgBg = R.drawable.sent_lgtblue;
	    	fbContactPostImgBg = R.drawable.sent_lgtblue; 
	    	fbMyPostTextColor = R.color.instBlue; 
	    	fbContactPostTextColor = R.color.instBlack;
			fbMyPostBgColor = R.color.instLightGray;
			fbContactPostBgColor = R.color.instWhite;
    	} else if (themeSelect.equals(EpicFeedConsts.CLASSIC_GREEN_THEME)) {
	    	sendColor = R.color.instGreen;
	    	receiveColor = R.color.instDarkGreen;
	    	sendImgTop = R.drawable.classic_sent_darkgreen_top;
	    	sendImgBottom = R.drawable.classic_sent_darkgreen_bottom;
	    	sendImgMiddle = R.drawable.classic_sent_darkgreen_middle;
	    	sendImgFull = R.drawable.classic_sent_darkgreen_full;
	    	receiveImgTop = R.drawable.classic_recd_green_top;
	    	receiveImgBottom = R.drawable.classic_recd_green_bottom;
	    	receiveImgMiddle = R.drawable.classic_recd_green_middle;
	    	receiveImgFull = R.drawable.classic_recd_green_full;
	    	sendTextColor = textWhite;
	    	receiveTextColor = textLightGray;
	    	headerTextColor = textBlack;
	    	headerColor = R.color.instLightGreen;
	    	themeDescrShort = spaContext.getString(R.string.smsTextClassicThemeBtnGreenTxtShort);
	    	themeDescr = spaContext.getString(R.string.smsTextClassicThemeBtnGreenTxt);
	    	
	    	fbMyPostImgBg = R.drawable.sent_lgtblue;
	    	fbContactPostImgBg = R.drawable.sent_lgtblue; 
	    	fbMyPostTextColor = R.color.instBlue; 
	    	fbContactPostTextColor = R.color.instBlack;
			fbMyPostBgColor = R.color.instLightGray;
			fbContactPostBgColor = R.color.instWhite;
    	} else if (themeSelect.equals(EpicFeedConsts.CLASSIC_DARK_THEME)) {
	    	sendColor = R.color.instDarkGray;
	    	receiveColor = R.color.instGray;
	    	sendImgTop = R.drawable.classic_sent_gray_top;
	    	sendImgBottom = R.drawable.classic_sent_gray_bottom;
	    	sendImgMiddle = R.drawable.classic_sent_gray_middle;
	    	sendImgFull = R.drawable.classic_sent_gray_full;
	    	receiveImgTop = R.drawable.classic_recd_darkgray_top;
	    	receiveImgBottom = R.drawable.classic_recd_darkgray_bottom;
	    	receiveImgMiddle = R.drawable.classic_recd_darkgray_middle;
	    	receiveImgFull = R.drawable.classic_recd_darkgray_full;
	    	sendTextColor = textWhite;
	    	receiveTextColor = textLightGray;
	    	headerTextColor = textWhite;
	    	headerColor = R.color.instBlack;
	    	themeDescrShort = spaContext.getString(R.string.smsTextClassicThemeBtnDarkTxtShort);
	    	themeDescr = spaContext.getString(R.string.smsTextClassicThemeBtnDarkTxt);
	    	
	    	fbMyPostImgBg = R.drawable.sent_lgtblue;
	    	fbContactPostImgBg = R.drawable.sent_lgtblue; 
	    	fbMyPostTextColor = R.color.instBlue; 
	    	fbContactPostTextColor = R.color.instBlack;
			fbMyPostBgColor = R.color.instLightGray;
			fbContactPostBgColor = R.color.instWhite;
    	} else if (themeSelect.equals(EpicFeedConsts.CLASSIC_LIGHT_THEME)) {
	    	sendColor = R.color.instLightGray;
	    	receiveColor = R.color.instVeryLightGray;
	    	sendImgTop = R.drawable.classic_sent_verylightgray_top;
	    	sendImgBottom = R.drawable.classic_sent_verylightgray_bottom;
	    	sendImgMiddle = R.drawable.classic_sent_verylightgray_middle;
	    	sendImgFull = R.drawable.classic_sent_verylightgray_full;
	    	receiveImgTop = R.drawable.classic_recd_medgray_top;
	    	receiveImgBottom = R.drawable.classic_recd_medgray_bottom;
	    	receiveImgMiddle = R.drawable.classic_recd_medgray_middle;
	    	receiveImgFull = R.drawable.classic_recd_medgray_full;
	    	sendTextColor = textGray;
	    	receiveTextColor = textBlack;
	    	headerTextColor = textGray;
	    	headerColor = R.color.instWhite;
	    	themeDescrShort = spaContext.getString(R.string.smsTextClassicThemeBtnLightTxtShort);
	    	themeDescr = spaContext.getString(R.string.smsTextClassicThemeBtnLightTxt);
	    	
	    	fbMyPostImgBg = R.drawable.sent_lgtblue;
	    	fbContactPostImgBg = R.drawable.sent_lgtblue; 
	    	fbMyPostTextColor = R.color.instBlue; 
	    	fbContactPostTextColor = R.color.instBlack;
			fbMyPostBgColor = R.color.instLightGray;
			fbContactPostBgColor = R.color.instWhite;
    	}
	}
}
