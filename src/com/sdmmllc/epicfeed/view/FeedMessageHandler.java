package com.sdmmllc.epicfeed.view;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Telephony;
import android.telephony.PhoneNumberUtils;
import android.util.Log;

import com.facebook.Session;
import com.sdmm.epicfeed.R;
import com.sdmmllc.epicfeed.EpicFeedController;
import com.sdmmllc.epicfeed.data.ContactFeed;
import com.sdmmllc.epicfeed.data.EpicContact;
import com.sdmmllc.epicfeed.data.EpicDB;
import com.sdmmllc.epicfeed.data.EpicDBConn;
import com.sdmmllc.epicfeed.data.FacebookData;
import com.sdmmllc.epicfeed.data.FacebookDataListener;
import com.sdmmllc.epicfeed.data.TwitterData;
import com.sdmmllc.epicfeed.data.TwitterDataListener;
import com.sdmmllc.epicfeed.model.FacebookConnectionModel;
import com.sdmmllc.epicfeed.model.Twitter11;
import com.sdmmllc.epicfeed.utils.EpicFeedConsts;
import com.sdmmllc.epicfeed.utils.EpicMessageUtils;
import com.sdmmllc.epicfeed.view.FeedMessageList.FeedListComparator;

public class FeedMessageHandler extends Thread {
	
	public static final String TAG = "FeedMessageHandler";
	
	public int counter = 0;
	public FeedTextMsg[] arrayMsgs;
	private FeedTextMsg[] feedMsgs, textSmsMsgs, textMmsMsgs;
	private Context mContext;
	private EpicDB epicDB;
	public EpicContact contact, me;
    private FeedMessageTheme themeOptions;
    private FeedMessageHandlerListener fmhListener;
    private FacebookConnectionModel mFacebookConn;
    private Twitter11 mTwitter11;
    private FacebookData mFacebookData;
    private TwitterData mTwitterData;
    private SharedPreferences settings;
    private Bundle savedState;
    private WeakReference<Activity> wrAct;
    private Boolean sendInProgress = false, updateHandlerinProgress = false;
    private boolean firstLoad = false;
    public static String 
					FEED_TYPE_FACEBOOK 	= "facebook",
					FEED_TYPE_LINKEDIN 	= "linkedIn",
					FEED_TYPE_TWITTER 	= "twitter",
					FEED_TYPE_PINTEREST	= "pinterest";
    
    public FacebookDataListener fbDataListener = new FacebookDataListener() {

		@Override
		public void onUpdate() {
			synchronized (sendInProgress) {
				synchronized (updateHandlerinProgress) {
					if (sendInProgress||updateHandlerinProgress) {
				        haveMoreMsgsToGet = true;
					} else getMessages();
				}
			}
		}

		@Override
		public void onComplete() {
			if (mFacebookData != null && mFacebookData.isFbDataReady()) haveMoreMsgsToGet = false;
		}
		
	};
	
	public TwitterDataListener twtDataListener = new TwitterDataListener() {
		
		@Override
		public void onUpdate() {
			synchronized (sendInProgress) {
				synchronized (updateHandlerinProgress) {
					if (sendInProgress||updateHandlerinProgress) {
						haveMoreMsgsToGet = true;
					} else getMessages();
				}
			}
		}
		
		@Override
		public void onComplete() {
			if (mTwitterData != null && mTwitterData.isTwtDataReady()) haveMoreMsgsToGet = false;
		}
	};
	
	public void init(Context context, EpicContact otherPerson, EpicContact meContact, FeedMessageTheme theme,
			Bundle savedInstanceState, WeakReference<Activity> act) {
		mContext = context;
		epicDB = EpicFeedController.getEpicDB(mContext.getApplicationContext());
		contact = otherPerson;
		me = meContact;
		themeOptions = theme;
		savedState = savedInstanceState;
		wrAct = act;
		
		settings = mContext.getSharedPreferences(EpicFeedConsts.PREFS_NAME, 0);
		Activity activity = wrAct.get();
		
		mFacebookConn = new FacebookConnectionModel(act);
		mTwitter11 = new Twitter11(activity, R.string.app_name, settings);
		mTwitter11.setupTwitter();
		
		mFacebookData = new FacebookData(mFacebookConn.setupFB(savedState), contact);
		mTwitterData = new TwitterData(mTwitter11, contact);
		
		start();
		
		getSocialNetworkData();
		
		firstLoadMessages(contact);
	}
	
	public void getSocialNetworkData() {
        for (ContactFeed feed : contact.contactFeeds) {
			
			if(feed.getFeedType().equals(FEED_TYPE_FACEBOOK)) {
				mFacebookData = new FacebookData(mFacebookConn.setupFB(savedState), contact);
				mFacebookData.getMyData(new FacebookDataListener() {
					@Override
					public void onUpdate() {
						me = mFacebookData.me;
						Log.i(TAG, "================== Got me data: " + me.name);
						messagesReturn();
					}

					@Override
					public void onComplete() {
						
					}
				});
				if (!contact.phoneID.equals("me")) mFacebookData.getOneFriendData(new FacebookDataListener() {
					@Override
					public void onUpdate() {
						Log.i(TAG, "================== Got contact data: " + contact.name);
						messagesReturn();
					}

					@Override
					public void onComplete() {
						
					}
				}, contact);
			} 
			
			if (feed.getFeedType().equals(FEED_TYPE_TWITTER)) {								
				mTwitterData = new TwitterData(mTwitter11, contact);
			    mTwitterData.getMe(new TwitterDataListener() {
				    @Override 
				    public void onUpdate() {
					    me = mTwitterData.self;
					    Log.i(TAG, "================== Got me data: " + me.nameId);
					    messagesReturn();
				    }
								
				    @Override
				    public void onComplete() {
					    
				    }
			    });
							
			    if (!contact.phoneID.equals("me")) 
			        mTwitterData.getOneFriendData(new TwitterDataListener() {
					    @Override
					    public void onUpdate() {
						    Log.i(TAG, "================== Got contact data: " + contact.nameId);
						    messagesReturn();
					    }
									
					    @Override
					    public void onComplete() {
						    
						}
				    }, contact);
			} 
		}
	}
	
	public void clearMsgs() {
		
		EpicDBConn conn = epicDB.open(false);
		final EpicContact oldContact = epicDB.getContact(contact.phoneID);
		oldContact.notifyOptions = epicDB.getNotification(contact.phoneID);
		oldContact.setContactFeeds(epicDB.getContactFeeds(contact.phoneID));
		epicDB.close(conn);
		
		conn = epicDB.open(true);
		
		epicDB.deleteContactFeed(oldContact.getFeed(ContactFeed.FEED_TYPE_FACEBOOK));
    	contact.removeFeed(oldContact.getFeed(ContactFeed.FEED_TYPE_FACEBOOK));
    	mFacebookData.clearFbMsgs();
		epicDB.deleteContactFeed(oldContact.getFeed(ContactFeed.FEED_TYPE_TWITTER));
    	contact.removeFeed(oldContact.getFeed(ContactFeed.FEED_TYPE_TWITTER));
    	mTwitterData.clearTwtMsgs();
    	epicDB.closeConn(conn);
    	
    	epicDB.close(conn);
	}
	
	public void startOn() {
		Session.getActiveSession().addCallback(mFacebookConn.getStatusCallback());
    	if (mFacebookData != null) mFacebookData.setFacebookDataListener(fbDataListener);
    	if (mTwitterData != null) mTwitterData.setTwitterDataListener(twtDataListener);
	}
	
	public boolean isFeedMessageHandlerReady() {
		boolean FeedMessageHandlerReady = !runRunning;
		return FeedMessageHandlerReady;
	}
	
	private boolean runRunning = false;
	private Boolean doneGettingMessages = false;
	private Boolean haveMoreMsgsToGet = false;
	
	public void run() {
		runRunning = true;
		if (mContext == null) {
			Log.w(TAG, "did not initialize properly");
			return;
		}
		
		while (!doneGettingMessages) {
			if (haveMoreMsgsToGet) {
				getMessages();
			}
			try {
				synchronized(this) {
					wait(250);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		runRunning = false;
	}

	private void getMessages() {
		doneGettingMessages = true;
		EpicDBConn tmpConn = epicDB.open(true);
		feedMsgs = 
				epicDB
				.getContactMessages(
						contact.phoneID, 
						false, 
						themeOptions.
						threadAscending);
		if (feedMsgs.length > 0) epicDB.clearNewMsgs(contact.phoneID);
		epicDB.close(tmpConn);
		
		boolean needSms = textSmsMsgs == null || textSmsMsgs.length < 1;
		boolean needMms = textMmsMsgs == null || textMmsMsgs.length < 1;
		
		if (needSms || needMms) {
			Uri mSmsinboxQueryUri = Uri.parse("content://sms");
	        Cursor cursor1 = mContext.getContentResolver().query(mSmsinboxQueryUri,new String[] { "_id", "thread_id", "address", "person", "date", "date_sent", "body", "type" }, null, null, null);
	        String[] columns = new String[] { "address", "person", "date", "date_sent", "body", "type", "thread_id", "_id" };
			if (EpicFeedConsts.DEBUG_FB) Log.d(TAG, "************** Text cursor.getCount()" + cursor1.getCount());
			String threadId = "";
	        if (cursor1.getCount() > 0) {
	        	ArrayList<FeedTextMsg> allTxtMsgs = new ArrayList<FeedTextMsg>();
	            while (cursor1.moveToNext()){
	                String address = cursor1.getString(cursor1.getColumnIndex(columns[0]));
	
	                if(PhoneNumberUtils.compare(address, contact.phoneID)){ //put your number here
						FeedTextMsg newMsg = new FeedTextMsg();
						int msgType = cursor1.getInt(cursor1.getColumnIndex(columns[5]));
						newMsg.msgTimestamp = cursor1.getLong(cursor1.getColumnIndex(columns[2]));
						if (msgType == Telephony.Sms.MESSAGE_TYPE_SENT) {
							newMsg.setSentFlg(true);
							newMsg.setSentMsg(true);
						} else {
							newMsg.setReceivedMsg(true);
						}
				        newMsg.msgDate = new Date(newMsg.msgTimestamp);
						newMsg.msgTxt = cursor1.getString(cursor1.getColumnIndex(columns[4]));
						newMsg.feedThreadID = cursor1.getString(cursor1.getColumnIndex(columns[6]));
						threadId = newMsg.feedThreadID;
						newMsg.feedMsgID = cursor1.getString(cursor1.getColumnIndex(columns[7]));
						if (EpicFeedConsts.DEBUG_FB) Log.d(TAG, "Text msg ******** type Sent: " + newMsg.isSent() + " body = " + newMsg.msgTxt);
						allTxtMsgs.add(newMsg);
	                }
	            }
	    		if (EpicFeedConsts.DEBUG_FB) Log.d(TAG, "************** Found " + allTxtMsgs.size() + " for contact: " + contact.phoneID);
		        textSmsMsgs = allTxtMsgs.toArray(new FeedTextMsg[0]);
	        }		
	        cursor1.close();
	        
			Uri mMmsinboxQueryUri = Uri.parse("content://mms");
	        Cursor cursor2 = mContext.getContentResolver().query(mMmsinboxQueryUri,new String[] { "_id", "date", "msg_box", "read", "thread_id" }, null, null, null);
	        //String[] columns2 = new String[] { "address", "person", "date", "date_sent", "body", "type", "thread_id", "_id" };
	        String[] columns2 = new String[] {"_id", "date", "msg_box", "read", "thread_id"};
	        
			if (EpicFeedConsts.DEBUG_FB) Log.d(TAG, "************** MMS Text cursor.getCount(): " + cursor2.getCount());
	        if (cursor2.getCount() > 0) {
	        	ArrayList<FeedTextMsg> allTxtMsgs = new ArrayList<FeedTextMsg>();
	            while (cursor2.moveToNext()){
	            	int mmsId = cursor2.getInt(cursor2.getColumnIndex(columns2[0]));
	                //String address = getAddressNumber(mmsId);
	
	                //if(PhoneNumberUtils.compare(address, contact.phoneID)){ //put your number here
	                if(cursor2.getString(cursor2.getColumnIndex(columns2[4])).equals(threadId)){ //put your number here
						FeedTextMsg newMsg = new FeedTextMsg();
			            String body = "";
						String selectionPart = "mid=" + mmsId;
						Uri uri = Uri.parse("content://mms/part");
						Cursor cursor = mContext.getContentResolver().query(uri, null,
						    selectionPart, null, null);
						if (cursor.moveToFirst()) {
						    do {
						        String partId = cursor.getString(cursor.getColumnIndex("_id"));
						        String type = cursor.getString(cursor.getColumnIndex("ct"));
						        if ("text/plain".equals(type)) {
						            String data = cursor.getString(cursor.getColumnIndex("_data"));
						            if (data != null) {
						                Uri partURI = Uri.parse("content://mms/part/" + partId);
						                InputStream is = null;
						                StringBuilder sb = new StringBuilder();
						                try {
						                    is = mContext.getContentResolver().openInputStream(partURI);
						                    if (is != null) {
						                        InputStreamReader isr = new InputStreamReader(is, "UTF-8");
						                        BufferedReader reader = new BufferedReader(isr);
						                        String temp = reader.readLine();
						                        while (temp != null) {
						                            sb.append(temp);
						                            temp = reader.readLine();
						                        }
						                    }
						                } catch (IOException e) {}
						                finally {
						                    if (is != null) {
						                        try {
						                            is.close();
						                        } catch (IOException e) {}
						                    }
						                }
						                body = sb.toString();
						            } else {
						                body = cursor.getString(cursor.getColumnIndex("text"));
						            }
						        }
						    } while (cursor.moveToNext());
						    cursor.close();
						}
						
						int msgType = cursor2.getInt(cursor2.getColumnIndex(columns2[2]));
						newMsg.msgTimestamp = cursor2.getLong(cursor2.getColumnIndex(columns2[1])) * 1000;
						if (msgType == Telephony.Mms.MESSAGE_BOX_SENT) {
							newMsg.setSentFlg(true);
							newMsg.setSentMsg(true);
						} else {
							newMsg.setReceivedMsg(true);
						}
						newMsg.setMMS(true);
				        newMsg.msgDate = new Date(newMsg.msgTimestamp);
						newMsg.msgTxt = body;
						newMsg.feedThreadID = cursor2.getString(cursor2.getColumnIndex(columns2[4]));
						newMsg.feedMsgID = cursor2.getString(cursor2.getColumnIndex(columns2[0]));
						if (EpicFeedConsts.DEBUG_FB) Log.d(TAG, "Text msg ******** type MMS Sent: " + newMsg.isSent() + " body = " + newMsg.msgTxt);
						allTxtMsgs.add(newMsg);
	                }
	            }
	    		if (EpicFeedConsts.DEBUG_FB) Log.d(TAG, "************** Found MMS " + allTxtMsgs.size() + " for contact: " + contact.phoneID);
				textMmsMsgs = allTxtMsgs.toArray(new FeedTextMsg[0]);
	        }		
	        cursor2.close();
		}
		feedMsgs = EpicMessageUtils.concat(feedMsgs, textSmsMsgs);
		//feedMsgs = EpicMessageUtils.concat(feedMsgs, textMmsMsgs);
	        
		if (mFacebookData != null && mFacebookData.getFbMsgs().size() > 0) {
        	FeedTextMsg[] newMsgs = mFacebookData.getFbMsgs().toArray(new FeedTextMsg[mFacebookData.getFbMsgs().size()]);
        	int newCount = newMsgs.length;
        	for (FeedTextMsg msg1 : newMsgs) {
        		if (msg1.getTargetMsgID() == null) msg1.setTargetMsgID("");
        		if (msg1.getTargetMsgID().length() > 0) {
					newCount--;
        			for (FeedTextMsg msg2 : newMsgs) {
        				if (msg2.feedMsgID.equals(msg1.getTargetMsgID())) {
        					msg2.linkedMsgs.add(msg1);
        					Log.i(TAG, "linking msg1 " + msg1.feedMsgID + " to msg2 " + msg2.feedMsgID);
        				}
        			}
        		}
        	}
    		Log.i(TAG, "msg1 newCount: " + newCount);
        	FeedTextMsg[] removeMsgs = new FeedTextMsg[newCount];
        	newCount = 0;
        	for (FeedTextMsg msg1 : newMsgs) {
        		if (!(msg1.getTargetMsgID().length() > 0)) {
        			removeMsgs[newCount] = msg1;
        			newCount++;
        		}
        	}
    		Log.i(TAG, "before concat newMsgs.length: " + newMsgs.length);
    		Log.i(TAG, "before concat removeMsgs.length: " + removeMsgs.length);
    		Log.i(TAG, "before concat txtMsgs.length: " + feedMsgs.length);
        	feedMsgs = EpicMessageUtils.concat(feedMsgs, removeMsgs);
    		Log.i(TAG, "after concat txtMsgs.length: " + feedMsgs.length);
        }
		
		if (mTwitterData != null && mTwitterData.getTwtMsgs().size() > 0) {
    		if (mTwitterData.rateLimitStat() == false) {
    			FeedTextMsg[] newMsgs = mTwitterData.getTwtMsgs().toArray(new FeedTextMsg[mTwitterData.getTwtMsgs().size()]);
            	int newCount = newMsgs.length;
            	for (FeedTextMsg msg1 : newMsgs) {
            		if (msg1.getTargetMsgID() == null) msg1.setTargetMsgID("");
            		if (msg1.getTargetMsgID().length() > 0) {
    					newCount--;
            			for (FeedTextMsg msg2 : newMsgs) {
            				if (msg2.feedMsgID.equals(msg1.getTargetMsgID())) {
            					msg2.linkedMsgs.add(msg1);
            					Log.i(TAG, "linking msg1 " + msg1.feedMsgID + " to msg2 " + msg2.feedMsgID);
            				}
            			}
            		}
            	}
        		Log.i(TAG, "msg1 newCount: " + newCount);
            	FeedTextMsg[] removeMsgs = new FeedTextMsg[newCount];
            	newCount = 0;
            	for (FeedTextMsg msg1 : newMsgs) {
            		if (!(msg1.getTargetMsgID().length() > 0)) {
            			removeMsgs[newCount] = msg1;
            			newCount++;
            		}
            	}
        		Log.i(TAG, "before concat newMsgs.length: " + newMsgs.length);
        		Log.i(TAG, "before concat removeMsgs.length: " + removeMsgs.length);
        		Log.i(TAG, "before concat txtMsgs.length: " + feedMsgs.length);
            	feedMsgs = EpicMessageUtils.concat(feedMsgs, removeMsgs);
        		Log.i(TAG, "after concat txtMsgs.length: " + feedMsgs.length);
    		} else {
    			FeedTextMsg[] newMsgs = new FeedTextMsg[1];
    			newMsgs[0] = new FeedTextMsg();
    			newMsgs[0].msgTxt = mContext.getString(R.string.sysTwitterRateLimit);
    			
    			feedMsgs = EpicMessageUtils.concat(feedMsgs, newMsgs);
    		}
        }
        
		ArrayList<FeedTextMsg> msgList = new ArrayList<FeedTextMsg>(Arrays.asList(feedMsgs));
        Collections.sort(msgList, FeedListComparator.getComparator(FeedListComparator.DATE));
        if (!themeOptions.threadAscending) Collections.reverse(msgList);
        feedMsgs = msgList.toArray(feedMsgs);

    	if (EpicFeedConsts.debugLevel > 5) Log.i(TAG, "getMessages: # txt msgs: " + feedMsgs.length);
		if (feedMsgs.length < 1) {
			feedMsgs = new FeedTextMsg[1];
			feedMsgs[0] = new FeedTextMsg();
			feedMsgs[0].msgTxt = mContext.getString(R.string.sysMsgNoMsgs);
			feedMsgs[0].noMessages = true;
			feedMsgs[0].setInfoMsg(true);
		} else {
			feedMsgs[0].noMessages = false;
			// set flags for display
			if (feedMsgs.length == 1) {
				feedMsgs[0].setInfoMsg(false);
				feedMsgs[0].endResponse = true;
				feedMsgs[0].startResponse = true;
			} else {
				feedMsgs[0].setInfoMsg(false);
				feedMsgs[0].startResponse = true;
				for (int i = 1; i < feedMsgs.length; i++) {
					feedMsgs[i].setInfoMsg(false);
					// call logs should start and end
					if (feedMsgs[i].isCallLog()) {
						feedMsgs[i-1].endResponse = true;
						feedMsgs[i].startResponse = true;
					} else
					// it is a msg, and if the sent flag is not the same, then the speaker has changed
					if (feedMsgs[i].isSentMsg()^feedMsgs[i-1].isSentMsg()|
							feedMsgs[i-1].isCallLog()) {
						feedMsgs[i-1].endResponse = true;
						feedMsgs[i].startResponse = true;
					}
                	//if (EpicFeedConsts.DEBUG_MSG_LIST_ACT) Log.i(TAG, "getMessages: start or end reponse - msg: " 
                	//		+ txtMsgs[i].msgID + " ; start: " + txtMsgs[i].startResponse 
                	//		+ " ; end: " + txtMsgs[i].endResponse);  
                	//if (EpicFeedConsts.DEBUG_MSG_LIST_ACT) Log.i(TAG, "getMessages: receivedMsgFlg: " 
                	//		+ txtMsgs[i].isReceivedMsg() + " ; callLogFlg: " + txtMsgs[i].isCallLog() 
                	//		+ " ; new msg: " + txtMsgs[i].isNewMsg());  
				}
				feedMsgs[feedMsgs.length -1].endResponse = true;
			}
		}
		
		me = me.clone();
		
		arrayMsgs = new FeedTextMsg[feedMsgs.length];
		System.arraycopy(feedMsgs, 0, arrayMsgs, 0, feedMsgs.length);
		doneGettingMessages = false;

		if (fmhListener != null) {
        	if (isFeedMessageHandlerReady()) 
        		fmhListener.onComplete();
        	else fmhListener.onUpdate();
		}
	}
	
	public FeedMessageHandlerListener getFeedMessageHandler() {
		return fmhListener;
	}
	
	public void setFeedMessageHandler(FeedMessageHandlerListener listener) {
		fmhListener = listener;
	}
	
	public void firstLoadMessages(EpicContact ec) {
		if (mFacebookData != null && ec.getFeed(FEED_TYPE_FACEBOOK) != null) mFacebookData.getFacebookData();
		if(mTwitterData != null && ec.getFeed(FEED_TYPE_TWITTER) != null) mTwitterData.getTwitterData();
		
		synchronized (sendInProgress) {
			synchronized (updateHandlerinProgress) {
				if (sendInProgress||updateHandlerinProgress) {
					haveMoreMsgsToGet = true;
				}
			}
		}
        firstLoad = true;
	}
	
	public void messagesReturn() {
		synchronized (sendInProgress) {
			synchronized (updateHandlerinProgress) {
				if ((!updateHandlerinProgress
						)&&(!sendInProgress)) 
					getMessages();
			}
		}
	}
	
	public void resetActConfig() {
		for (ContactFeed feed : contact.contactFeeds) {
			if (feed.getFeedType().equals(FEED_TYPE_FACEBOOK)) {
				mFacebookConn = new FacebookConnectionModel(wrAct);
				mFacebookConn.setupFB(savedState);
			}
			
			if (feed.getFeedType().equals(FEED_TYPE_TWITTER)) {
				mTwitter11 = new Twitter11(wrAct.get(), R.string.app_name, settings);
				mTwitter11.setupTwitter();
			}
		}
	}
	
	public void oneStopOn() {
		if (mFacebookData != null) mFacebookData.setFacebookDataListener(null);
		if (mTwitterData != null) mTwitterData.setTwitterDataListener(null);
	}
	
	public void twoStopOn() {
		Session.getActiveSession().removeCallback(mFacebookConn.getStatusCallback());    
	}
}