package com.sdmmllc.epicfeed.view;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;

import com.sdmmllc.epicfeed.data.EpicContact;
import com.sdmmllc.epicfeed.utils.EpicFeedConsts;

public class FeedMessageListGroup {

	String TAG = "SpaTextSMSListGroup";
    public static int[] msgGrouping = {-1,0,1,2,3,4,5,6,13,10000};
    public long[] msgGroupingDates = new long[msgGrouping.length];
    public static String[] msgGroupTitle = {"No Messages", "Today", "Yesterday", "", "", "", "", "", 
    		"Last Week", "Over 2 Weeks Old"};
    public FeedMessageListHelper[] msgGroup = new FeedMessageListHelper[msgGrouping.length];
    private Calendar cal1 = Calendar.getInstance(), 
		cal2 = Calendar.getInstance();
    public int msgTextSize = EpicFeedConsts.txt_med;
    
    private Context spaContext;
    private LayoutInflater spaInflater;
    private boolean setupGroupingDates = false;
    private EpicContact mContact, me;
    
    public FeedMessageListGroup(Context ctx, LayoutInflater mInf, EpicContact contact, EpicContact meContact) {
    	spaContext = ctx;
    	spaInflater = mInf;
    	setupGroupingDates();
    	mContact = contact;
    	me = meContact;
    }
    
    private void setupGroupingDates() {
		cal1 = Calendar.getInstance();
		cal1.set(Calendar.HOUR_OF_DAY, 0);
		cal1.set(Calendar.MINUTE, 0);
		cal1.set(Calendar.SECOND, 0);
		cal1.set(Calendar.MILLISECOND, 0);
		//start a day in advance - then work backward
		cal1.add(Calendar.DAY_OF_YEAR, 1);
    	msgGroupingDates[0] = cal1.getTimeInMillis();
		for (int i = 0; i < msgGrouping.length - 1 ; i++) {
			cal1.add(Calendar.DAY_OF_YEAR, msgGrouping[i] - msgGrouping[i+1]);
	    	msgGroupingDates[i+1] = cal1.getTimeInMillis();
			//Log.i(TAG, "setupGroupingDates: gcal1.set: YEAR:" + cal1.get(Calendar.YEAR) + 
			//		" - MONTH:" + cal1.get(Calendar.MONTH) + " - DAY:" + cal1.get(Calendar.DAY_OF_MONTH) +
			//		" - offset (msgGrouping[i] - msgGrouping[i+1]): " + (msgGrouping[i] - msgGrouping[i+1]));
			//Log.i(TAG, "MsgGroupingDates[i+1] = " + msgGroupingDates[i] + 
			//		" - cal1.getTimeInMillis(): " + cal1.getTimeInMillis());
		}
		setupGroupingDates = true;
    }

    public void addList(int idx, String title, List<FeedTextMsg> msgList) {
    	if ((msgList == null)||(msgList.size() < 1)) return;
    	if (title.length() < 1) {
    		Calendar cal = Calendar.getInstance();
    		cal.setTime(msgList.get(0).msgDate);
    	    java.text.SimpleDateFormat df = new SimpleDateFormat("EEEE '('MMM d')'");
    		title = df.format(cal.getTime());
    	}
    	FeedMessageListHelper tmpList = new FeedMessageListHelper(idx, title, msgList);
    	tmpList.msgTextSize = this.msgTextSize;
    	tmpList.getAA(spaContext, spaInflater);
    	tmpList.msgListAA.setContact(mContact);
    	tmpList.msgListAA.setMe(me);
    	msgGroup[idx] = tmpList;
		if (EpicFeedConsts.debugLevel > 8) Log.i(TAG, "addList: got adapter and list"); 
    }
    
    public void removeList(int idx) {
    	msgGroup[idx] = null;
    }
    
    public int getCount() {
    	int retVal = 0;
    	for (int i = 0; i < msgGroup.length; i++) {
    		if (msgGroup[i] != null) retVal += msgGroup[i].txtMsgList.size() + 1;
    	}
    	return retVal;
    }
    
    public int getMsgCount() {
    	int retVal = 0;
    	for (int i = 1; i < msgGroup.length; i++) {
    		if (msgGroup[i] != null) retVal += msgGroup[i].txtMsgList.size();
    	}
    	return retVal;
    }
    
    public int getMsgGroup(String msgID) {
    	for (int i = 0; i < msgGroup.length; i++) {
    		if (msgGroup[i] != null) {
    			for (int j = 0; j < msgGroup[i].txtMsgList.size(); j++) {
    				if (msgID.equals(msgGroup[i].txtMsgList.get(j).msgID)) return i;
    			}
    		}
    	}
    	return -1;
    }
    
    public FeedMessageListHelper getGroup(int idx) {
    	for (int i = 0; i < msgGroup.length; i++) {
    		if (msgGroup[i] != null) {
    			if (msgGroup[i].index == idx) return msgGroup[i];
    		}
    	}
    	return null;
    }
    
    public boolean hasGroup(int groupNum) {
    	if (msgGroup[groupNum] != null) return true;
    	else return false;
    }
    
    public int getGroupForMsg(FeedTextMsg tmpMsg) {
		cal1 = Calendar.getInstance();
		if ((!setupGroupingDates)||(cal1.get(Calendar.DATE)!=cal2.get(Calendar.DATE))) {
			setupGroupingDates();
			cal2 = Calendar.getInstance();
		}
		cal1.setTime(tmpMsg.msgDate);
		//Log.i(TAG, "cal1.set: YEAR:" + cal1.get(Calendar.YEAR) + 
		//		" - MONTH:" + cal1.get(Calendar.MONTH) + " - DAY:" + cal1.get(Calendar.DAY_OF_MONTH));
		//Log.i(TAG, "gcal2.set: YEAR:" + (tmpMsg.msgTimestamp.getYear()+1900) +
		//		" - MONTH:" + tmpMsg.msgTimestamp.getMonth() + " - DAY:" + tmpMsg.msgTimestamp.getDate());
		long msgTime = cal1.getTimeInMillis();
		if (msgTime >= msgGroupingDates[0]) msgTime = msgGroupingDates[0] - 1;
		else if (msgTime < msgGroupingDates[msgGroupingDates.length - 1]) msgTime = msgGroupingDates[0] - 1;
		int groupIdx = 0;
		while (msgGroupingDates[groupIdx] > msgTime) groupIdx++;
		return groupIdx;
    }
    
    public int getMsgIndex(String msgID, int group) {
		if (msgGroup[group] != null) {
			for (int j = 0; j < msgGroup[group].txtMsgList.size(); j++) {
				if (msgID.equals(msgGroup[group].txtMsgList.get(j).msgID)) return j;
			}
		}
    	return -1;
    }
    
    public void insert(FeedTextMsg tmpMsg, int msgGroupIdx) {
    	if (msgGroupIdx < 0) msgGroupIdx = getGroupForMsg(tmpMsg);
    	if (hasGroup(msgGroupIdx)) msgGroup[msgGroupIdx].txtMsgList.add(tmpMsg);
    	else {
    		ArrayList<FeedTextMsg> tmpMsgList = new ArrayList<FeedTextMsg>();
    		tmpMsgList.add(tmpMsg);
        	addList(msgGroupIdx, msgGroupTitle[msgGroupIdx], tmpMsgList);
    		if (EpicFeedConsts.debugLevel > 9) Log.i(TAG, "insert: new list, got adapter and list"); 
    	}
    }

    public void remove(FeedTextMsg tmpMsg) {
    	int msgGroupIdx = getMsgGroup(tmpMsg.msgID);
    	if (msgGroupIdx < 0) return;
    	else {
    		if (msgGroup[msgGroupIdx].txtMsgList.size() > 1) {
    			msgGroup[msgGroupIdx].txtMsgList.remove(getMsgIndex(tmpMsg.msgID, msgGroupIdx));
    			msgGroup[msgGroupIdx].getAA(spaContext, spaInflater);
    		} else {
    			msgGroup[msgGroupIdx].msgListAA.clear();
    			msgGroup[msgGroupIdx].txtMsgList.clear();
    			msgGroup[msgGroupIdx].getAA(spaContext, spaInflater);
    		}
    	}
    }

    public void clear() {
    	for (int i = 0; i < msgGrouping.length; i++) {
    		if (msgGroup[i] != null) {
				msgGroup[i].msgListAA.clear();
	    		msgGroup[i].txtMsgList.clear();
    			msgGroup[i].getAA(spaContext, spaInflater);
    		}
    	}
    }
}
