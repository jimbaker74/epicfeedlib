package com.sdmmllc.epicfeed.view;

public interface FeedMessageHandlerListener {
	public void onUpdate();
	public void onComplete();
}
