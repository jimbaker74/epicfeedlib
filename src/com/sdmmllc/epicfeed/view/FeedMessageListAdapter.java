package com.sdmmllc.epicfeed.view;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.regex.Pattern;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.text.Html;
import android.text.TextUtils;
import android.text.util.Linkify;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sdmm.epicfeed.R;
import com.sdmmllc.epicfeed.EpicFeedController;
import com.sdmmllc.epicfeed.data.ContactFeed;
import com.sdmmllc.epicfeed.data.EpicContact;
import com.sdmmllc.epicfeed.model.FacebookConnectionModel;
import com.sdmmllc.epicfeed.utils.EpicEllipsizingTextView;
import com.sdmmllc.epicfeed.utils.EpicFeedConsts;
import com.sdmmllc.epicfeed.utils.EpicImageAdapter;
import com.sdmmllc.epicfeed.utils.EpicImageUtils.EpicBitmaps;
import com.sdmmllc.epicfeed.utils.EpicSmileyParser;

public class FeedMessageListAdapter extends ArrayAdapter<FeedTextMsg> {

	public static final String TAG = "FeedMessageListAdapter";
	
	public static final int SENT_TEXT_MSG 			= 0;
	public static final int RECEIVED_TEXT_MSG 		= 1;
	public static final int SENT_MMS_MSG 			= 2;
	public static final int RECEIVED_MMS_MSG 		= 3;
	public static final int SENT_FB_POST 			= 4;
	public static final int RECEIVED_FB_POST 		= 5;
	public static final int SENT_FB_MSG				= 6;
	public static final int RECEIVED_FB_MSG			= 7;
	public static final int SENT_TWITTER_MSG		= 8;
	public static final int RECEIVED_TWITTER_MSG	= 9;
	public static final int SENT_TWEET 				= 10;
	public static final int RECEIVED_TWEET			= 11;
	public static final int SENT_LINKEDIN_POST		= 12;
	public static final int RECEIVED_LINKEDIN_POST 	= 13;
	public static final int SENT_LINKEDIN_MSG 		= 14;
	public static final int RECEIVED_LINKEDIN_MSG 	= 15;

	public static final int VIEW_COUNT = 16;

	private String themeOption = EpicFeedConsts.STANDARD_THEME;
	private FeedMessageTheme themeColors;
	private EpicSmileyParser mSmileyParser;
	private Pattern phonePattern = Pattern.compile("(^|([[\\D]&&[^[[a-z][A-Z]]]])+?)(\\d{3})?([[\\D]&&[^[[a-z][A-Z]]]])?(\\d{3})([[\\D]&&[^[[a-z][A-Z]]]])?(\\d{4})");
    private Context mContext;
    private LayoutInflater mInflater;
    private List<FeedTextMsg> txtMsgList;
    private EpicContact mContact, me;
    private Drawable downloadImg;
    private int imgHeight = 0, imgWidth = 0, avatarHeight = 0;
    private float textSize = 12;

    private Calendar 
    	cal1 = Calendar.getInstance(), 
    	cal2 = Calendar.getInstance();
    
	private int index, 
		msgTextSize = EpicFeedConsts.txt_med,
		defResourceId = 0, defTextViewResourceId = 0;
	
    static java.text.DateFormat df;
    private java.text.SimpleDateFormat standardDf;

    public FeedMessageListAdapter(Context context, int resource) {
		super(context, resource);
		init(context);
		txtMsgList = new ArrayList<FeedTextMsg>();
		defResourceId = resource;
	}

	public FeedMessageListAdapter(Context context, int resource,
			int textViewResourceId) {
		super(context, resource, textViewResourceId);
		init(context);
		txtMsgList = new ArrayList<FeedTextMsg>();
		defResourceId = resource;
		defTextViewResourceId = resource;
	}

	public FeedMessageListAdapter(Context context, int resource,
			int textViewResourceId, List<FeedTextMsg> objects) {
		super(context, resource, textViewResourceId, objects);
		init(context);
		txtMsgList = objects;
		defResourceId = resource;
		defTextViewResourceId = textViewResourceId;
	}

	public FeedMessageListAdapter(Context context, int resource,
			int textViewResourceId, FeedTextMsg[] objects) {
		super(context, resource, textViewResourceId, objects);
		init(context);
		txtMsgList = Arrays.asList(objects);
		defResourceId = resource;
		defTextViewResourceId = textViewResourceId;
	}

	public FeedMessageListAdapter(Context context, int textViewResourceId,
			List<FeedTextMsg> objects) {
		super(context, textViewResourceId, objects);
		init(context);
		txtMsgList = objects;
		defTextViewResourceId = textViewResourceId;
	}

	public FeedMessageListAdapter(Context context, int resource, FeedTextMsg[] objects, EpicContact contact) {
		super(context, resource, objects);
		init(context);
		txtMsgList = Arrays.asList(objects);
		defResourceId = resource;
		mContact = contact;
	}
	
	private void init(Context ctx) {
    	mSmileyParser = EpicFeedController.getSmileyParser();
    	cal1 = Calendar.getInstance();
    	
    	standardDf = new java.text.SimpleDateFormat("h:mm a MMM dd");
    	standardDf.setCalendar(cal1);
    	mContext = ctx;
    	SharedPreferences settings = mContext.getSharedPreferences(EpicFeedConsts.PREFS_NAME, 0);
		if (!settings.contains(EpicFeedConsts.THEME_SELECT)) {
			if (ctx.getResources().getBoolean(R.bool.pinkThread)) themeOption = EpicFeedConsts.PINK_THEME;
			else if (ctx.getResources().getBoolean(R.bool.greenThread)) themeOption = EpicFeedConsts.GREEN_THEME;
			else themeOption = settings.getString(EpicFeedConsts.THEME_SELECT, EpicFeedConsts.DEFAULT_THEME);
		} else themeOption = settings.getString(EpicFeedConsts.THEME_SELECT, EpicFeedConsts.DEFAULT_THEME);
    	themeColors = new FeedMessageTheme(mContext);
    	themeColors.setTheme(themeOption);
    	mInflater = (LayoutInflater) mContext.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
    	downloadImg = mContext.getResources().getDrawable(R.drawable.btn_image_download);
    	imgHeight = (int) (downloadImg.getIntrinsicHeight() * 1.5);
    	imgWidth = (int) (downloadImg.getIntrinsicWidth() * 1.5);
    	avatarHeight = (int) (mContext.getResources().getDrawable(R.drawable.sample_avatar).getIntrinsicHeight());
    }
	
	public void setContact(EpicContact contact) {
		mContact = contact;
	}
	
	public void setMe(EpicContact meContact) {
		me = meContact;
	}

	@Override
	public int getViewTypeCount() {
		return VIEW_COUNT;
	}
	
	@Override
	public int getCount() {
		return txtMsgList.size();
	}
	
	@Override
	public int getItemViewType(int position) {
		return txtMsgList.get(position).getMessageType();
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		return getTextMessage(position, convertView, parent);
	}
	
	private View getTextMessage(final int position, View convertView, ViewGroup parent) {
		View row;
		
		final FeedTextMsg tmpMsg = getItem(position); 
		// check message type for container
		int itemType = getItem(position).getMessageTypeFlg(); 
		if (itemType == FeedTextMsg.TYPE_FB_MSG) {
			if (tmpMsg.isReceivedMsg()) {
				row = mInflater.inflate(R.layout.feed_item_fb_msg_received, parent, false);
				ImageView badge = (ImageView)row.findViewById(R.id.fbMsgReceivedFeedBadge);
				badge.setTag(position);
				downloadAvatar(mContact.getFeed(ContactFeed.FEED_TYPE_FACEBOOK).profileURL, badge, position);
			} else {
				row = mInflater.inflate(R.layout.feed_item_fb_msg_sent, parent, false);
				ImageView badge = (ImageView)row.findViewById(R.id.fbMsgSentFeedBadge);
				badge.setTag(position);
				if (me.getFeeds() != null) {
					if (me.getFeeds().size() > 1) {
						for (ContactFeed feed : me.getFeeds()) {
							if (feed.getFeedType() == ContactFeed.FEED_TYPE_FACEBOOK) {
								downloadAvatar(me.getFeed(ContactFeed.FEED_TYPE_FACEBOOK).profileURL, badge, position);
							}
							// Added for time being to see if a potential short term solution to data being in the 
							// wrong place problem
							// TODO
							if (feed.getFeedType() == ContactFeed.FEED_TYPE_TWITTER) {
								Log.i(TAG, "+++++ TWITTER data in FACEBOOK MSG block +++++");
								downloadAvatar(me.getFeed(ContactFeed.FEED_TYPE_TWITTER).profileURL, badge, position);
							}
						}
					} else {
						if (me.getFeed(ContactFeed.FEED_TYPE_FACEBOOK) != null) {
							downloadAvatar(me.getFeed(ContactFeed.FEED_TYPE_FACEBOOK).profileURL, badge, position);
						}
					}
				}
			}
		} else if (itemType == FeedTextMsg.TYPE_FB_STATUS) {
			if (tmpMsg.isReceivedMsg()) {
				row = mInflater.inflate(R.layout.feed_item_fb_received, parent, false);
				ImageView badge = (ImageView)row.findViewById(R.id.fbReceivedStatusFeedBadge);
				badge.setTag(position);
				downloadAvatar(mContact.getFeed(ContactFeed.FEED_TYPE_FACEBOOK).profileURL, badge, position);
				final EpicEllipsizingTextView text = (EpicEllipsizingTextView)row.findViewById(R.id.fbReceivedStatusText);
				final View newRow = row;
				setOnClickLaunchFb(newRow, tmpMsg);
				EpicEllipsizingTextView.EllipsizeListener el = new EpicEllipsizingTextView.EllipsizeListener () {
					@Override
					public void ellipsizeStateChanged(boolean ellipsized) {
						newRow.setOnClickListener(null);
						setOnClickFbExpand(newRow, text, tmpMsg);
						text.removeEllipsizeListener(this);
					}
				};
				text.addEllipsizeListener(el);
			} else {
				row = mInflater.inflate(R.layout.feed_item_fb_sent, parent, false);
				ImageView badge = (ImageView)row.findViewById(R.id.fbSentStatusFeedBadge);
				badge.setTag(position);
				Log.i(TAG, "-------------- in TYPE_FB_STATUS --------------");
				Log.i(TAG, "-------------- me: " + me.toString());
				Log.i(TAG, "-------------- me.getFeeds().size(): " + me.getFeeds().size());
				if (me.getFeeds() != null) {
					if (me.getFeeds().size() > 1) {
						for (ContactFeed feed : me.getFeeds()) {
						    Log.i(TAG, "-------------- feed: " + feed.toString());
						    if (feed.getFeedType() == ContactFeed.FEED_TYPE_FACEBOOK) {
						    	downloadAvatar(me.getFeed(ContactFeed.FEED_TYPE_FACEBOOK).profileURL, badge, position);
						    }
						    
						    // Added for time being to see if a potential short term solution to data being in the 
							// wrong place problem
						    // TODO
							if (feed.getFeedType() == ContactFeed.FEED_TYPE_TWITTER) {
								Log.i(TAG, "+++++ TWITTER data in FACEBOOK STATUS block +++++");
								downloadAvatar(me.getFeed(ContactFeed.FEED_TYPE_TWITTER).profileURL, badge, position);
							}
						}
					} else {
						if (me.getFeed(ContactFeed.FEED_TYPE_FACEBOOK) != null) {
							downloadAvatar(me.getFeed(ContactFeed.FEED_TYPE_FACEBOOK).profileURL, badge, position);
						}
					}
				}
				
				final EpicEllipsizingTextView text = (EpicEllipsizingTextView)row.findViewById(R.id.fbSentStatusText);
				final View newRow = row;
				setOnClickLaunchFb(newRow, tmpMsg);
				EpicEllipsizingTextView.EllipsizeListener el = new EpicEllipsizingTextView.EllipsizeListener () {
					@Override
					public void ellipsizeStateChanged(boolean ellipsized) {
						newRow.setOnClickListener(null);
						setOnClickFbExpand(newRow, text, tmpMsg);
						text.removeEllipsizeListener(this);
					}
				};
				text.addEllipsizeListener(el);
			}
		} else if (itemType == FeedTextMsg.TYPE_FB_PHOTO) {
			if (tmpMsg.isReceivedMsg()) {
				row = mInflater.inflate(R.layout.feed_item_fb_photo_received, parent, false);
				ImageView badge = (ImageView)row.findViewById(R.id.fbReceivedPhotoFeedBadge);
				badge.setTag(position);
				downloadAvatar(mContact.getFeed(ContactFeed.FEED_TYPE_FACEBOOK).profileURL, badge, position);
				final EpicEllipsizingTextView text = (EpicEllipsizingTextView)row.findViewById(R.id.fbReceivedPhotoText);
				final View newRow = row;
				setOnClickLaunchFb(newRow, tmpMsg);
				EpicEllipsizingTextView.EllipsizeListener el = new EpicEllipsizingTextView.EllipsizeListener () {
					@Override
					public void ellipsizeStateChanged(boolean ellipsized) {
						newRow.setOnClickListener(null);
						setOnClickFbExpand(newRow, text, tmpMsg);
						text.removeEllipsizeListener(this);
					}
				};
				text.addEllipsizeListener(el);
			} else {
				row = mInflater.inflate(R.layout.feed_item_fb_photo_sent, parent, false);
				ImageView badge = (ImageView)row.findViewById(R.id.fbSentPhotoFeedBadge);
				badge.setTag(position);
				Log.i(TAG, "---------------- in TYPE_FB_PHOTO -----------------");
				Log.i(TAG, "-------------- me: " + me.toString());
				Log.i(TAG, "-------------- me.getFeeds().size(): " + me.getFeeds().size());
				if (me.getFeeds() != null) {
					if (me.getFeeds().size() > 1) {
						for (ContactFeed feed : me.getFeeds()) {
						    Log.i(TAG, "-------------- feed: " + feed.toString());
						    if (feed.getFeedType() == ContactFeed.FEED_TYPE_FACEBOOK) {
						    	downloadAvatar(me.getFeed(ContactFeed.FEED_TYPE_FACEBOOK).profileURL, badge, position);
						    }
						    
						    // Added for time being to see if a potential short term solution to data being in the 
							// wrong place problem
						    // TODO
							if (feed.getFeedType() == ContactFeed.FEED_TYPE_TWITTER) {
								Log.i(TAG, "+++++ TWITTER data in FACEBOOK PHOTO block +++++");
								downloadAvatar(me.getFeed(ContactFeed.FEED_TYPE_TWITTER).profileURL, badge, position);
							}
						}
					} else {
						if (me.getFeed(ContactFeed.FEED_TYPE_FACEBOOK) != null) {
							downloadAvatar(me.getFeed(ContactFeed.FEED_TYPE_FACEBOOK).profileURL, badge, position);
						}
					}
				}
				
				final EpicEllipsizingTextView text = (EpicEllipsizingTextView)row.findViewById(R.id.fbSentPhotoText);
				final View newRow = row;
				setOnClickLaunchFb(newRow, tmpMsg);
				EpicEllipsizingTextView.EllipsizeListener el = new EpicEllipsizingTextView.EllipsizeListener () {
					@Override
					public void ellipsizeStateChanged(boolean ellipsized) {
						newRow.setOnClickListener(null);
						setOnClickFbExpand(newRow, text, tmpMsg);
						text.removeEllipsizeListener(this);
					}
				};
				text.addEllipsizeListener(el);
			}
		} else if (itemType == FeedTextMsg.TYPE_TWITTER_MSG) {
			if (tmpMsg.isReceivedMsg()) {
				row = mInflater.inflate(R.layout.feed_item_twitter_msg_received, parent, false);
				ImageView badge = (ImageView)row.findViewById(R.id.twitterMsgReceivedFeedBadge);
				badge.setTag(position);
				downloadAvatar(mContact.getFeed(ContactFeed.FEED_TYPE_TWITTER).profileURL, badge, position);
			} else {
				row = mInflater.inflate(R.layout.feed_item_twitter_msg_sent, parent, false);
				ImageView badge = (ImageView)row.findViewById(R.id.twitterMsgSentFeedBadge);
				badge.setTag(position);
				Log.i(TAG, "-------------- in TYPE_TWITTER_MSG --------------");
				Log.i(TAG, "-------------- me: " + me.toString());
				Log.i(TAG, "-------------- me.getFeeds().size(): " + me.getFeeds().size());
				if (me.getFeeds() != null) {
					if (me.getFeeds().size() > 1) {
						for (ContactFeed feed : me.getFeeds()) {
						    Log.i(TAG, "-------------- feed: " + feed.toString());
						    if (feed.getFeedType() == ContactFeed.FEED_TYPE_TWITTER) {
						    	downloadAvatar(me.getFeed(ContactFeed.FEED_TYPE_TWITTER).profileURL, badge, position);
						    }
						    
						    // Added for time being to see if a potential short term solution to data being in the 
							// wrong place problem
						    // TODO
							if (feed.getFeedType() == ContactFeed.FEED_TYPE_FACEBOOK) {
								Log.i(TAG, "+++++ FACEBOOK data in TWITTER MSG block +++++");
								downloadAvatar(me.getFeed(ContactFeed.FEED_TYPE_FACEBOOK).profileURL, badge, position);
							}
						}
					} else {
						if (me.getFeed(ContactFeed.FEED_TYPE_TWITTER) != null) {
							downloadAvatar(me.getFeed(ContactFeed.FEED_TYPE_TWITTER).profileURL, badge, position);
						}
					}
				}
			}
		} else if (itemType == FeedTextMsg.TYPE_TWEET) {
			if (tmpMsg.isReceivedMsg()) {
				row = mInflater.inflate(R.layout.feed_item_twitter_tweet_received, parent, false);
				ImageView badge = (ImageView)row.findViewById(R.id.twitterTweetReceivedFeedBadge);
				badge.setTag(position);
				downloadAvatar(mContact.getFeed(ContactFeed.FEED_TYPE_TWITTER).profileURL, badge, position);
			} else {
				row = mInflater.inflate(R.layout.feed_item_twitter_tweet_sent, parent, false);
				ImageView badge = (ImageView)row.findViewById(R.id.twitterTweetSentFeedBadge);
				badge.setTag(position);
				if (me.getFeeds() != null) {
					if (me.getFeeds().size() > 1) {
						for (ContactFeed feed : me.getFeeds()) {
							if (feed.getFeedType() == ContactFeed.FEED_TYPE_TWITTER) {
								downloadAvatar(me.getFeed(ContactFeed.FEED_TYPE_TWITTER).profileURL, badge, position);
							}
							
							// Added for time being to see if a potential short term solution to data being in the 
							// wrong place problem
							// TODO
							if (feed.getFeedType() == ContactFeed.FEED_TYPE_FACEBOOK) {
								Log.i(TAG, "+++++ FACEBOOK data in TWITTER TWEET block +++++");
								downloadAvatar(me.getFeed(ContactFeed.FEED_TYPE_FACEBOOK).profileURL, badge, position);
							}
						}
					} else {
						if (me.getFeed(ContactFeed.FEED_TYPE_TWITTER) != null) {
							downloadAvatar(me.getFeed(ContactFeed.FEED_TYPE_TWITTER).profileURL, badge, position);
						}
					}
				}
			}
		} else if (tmpMsg.isSentMsg()) {
			row = mInflater.inflate(R.layout.feed_item_sms_sent, parent, false);
			if (me != null && me.mAvatar != null) {
				((ImageView)row.findViewById(R.id.smsSentFeedBadge)).setImageDrawable(me.mAvatar);
			}
			if (!tmpMsg.isMMS()) 
				((HorizontalScrollView)row.findViewById(R.id.smsSentFeedImageScrollLayout)).setVisibility(View.GONE);
			else {
				((HorizontalScrollView)row.findViewById(R.id.smsSentFeedImageScrollLayout)).setVisibility(View.VISIBLE);
				ImageView image = (ImageView)row.findViewById(R.id.smsSentFeedImage);
				image.setImageBitmap(getOneMMSImage(tmpMsg.feedMsgID));
			}
			row.setClickable(true);
			row.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					Intent i = new Intent(Intent.ACTION_VIEW,
							Uri.parse("content://mms-sms/conversations/" + tmpMsg.feedThreadID));                
					mContext.startActivity(i);
				}
			});
		} else {
			row = mInflater.inflate(R.layout.feed_item_sms_received, parent, false);
			if (mContact.mAvatar != null) {
				((ImageView)row.findViewById(R.id.smsReceivedFeedBadge)).setImageDrawable(mContact.mAvatar);
			}
			if (!tmpMsg.isMMS()) 
				((HorizontalScrollView)row.findViewById(R.id.smsReceivedFeedImageScrollLayout)).setVisibility(View.GONE);
			else {
				((HorizontalScrollView)row.findViewById(R.id.smsReceivedFeedImageScrollLayout)).setVisibility(View.VISIBLE);
				ImageView image = (ImageView)row.findViewById(R.id.smsReceivedFeedImage);
				image.setImageBitmap(getOneMMSImage(tmpMsg.feedMsgID));
			}
			row.setClickable(true);
			row.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					Intent i = new Intent(Intent.ACTION_VIEW,
							Uri.parse("content://mms-sms/conversations/" + tmpMsg.feedThreadID));                
					mContext.startActivity(i);
				}
			});
		}
		
		cal2.setTime(tmpMsg.msgDate);

		String txtMsg = tmpMsg.makeBig(TextUtils.htmlEncode(tmpMsg.msgTxt));
		// is a Facebook message
		if (tmpMsg.isFbType()) {
			// is a Facebook Direct Message
			if (itemType == FeedTextMsg.TYPE_FB_MSG) {
				if (tmpMsg.isSentMsg()) {
					TextView tv = (TextView) row.findViewById(R.id.fbMsgSentText);
					tv.setTextSize(msgTextSize);
					TextView timestampTv = (TextView) row.findViewById(R.id.fbMsgSentTimestamp);
					timestampTv.setText(standardDf.format(cal2.getTime()));
					tv.setText(mSmileyParser.addSmileySpans(Html.fromHtml(txtMsg + "<br/>" 
							+ tmpMsg.makeSmall(mContext.getString(R.string.fb_msg))))
							,  TextView.BufferType.SPANNABLE);
			        Linkify.addLinks(tv, Linkify.WEB_URLS|Linkify.EMAIL_ADDRESSES|Linkify.MAP_ADDRESSES);
			        Linkify.addLinks(tv, phonePattern, "tel:");
				} else {
					TextView tv = (TextView) row.findViewById(R.id.fbMsgReceivedText);
					tv.setTextSize(msgTextSize);
					TextView timestampTv = (TextView) row.findViewById(R.id.fbMsgReceivedTimestamp);
					timestampTv.setText(standardDf.format(cal2.getTime()));
					tv.setText(mSmileyParser.addSmileySpans(Html.fromHtml(txtMsg + "<br/>" 
							+ tmpMsg.makeSmall(mContext.getString(R.string.fb_msg))))
							,  TextView.BufferType.SPANNABLE);
			        Linkify.addLinks(tv, Linkify.WEB_URLS|Linkify.EMAIL_ADDRESSES|Linkify.MAP_ADDRESSES);
			        Linkify.addLinks(tv, phonePattern, "tel:");
				}
			// Is a Facebook Photo or Status
			} else {
				if (tmpMsg.isSentMsg()) {
					if (tmpMsg.isFbStatus() && tmpMsg.linkedMsgs.size() == 0) {
						TextView tv = (TextView) row.findViewById(R.id.fbSentStatusText);
						tv.
						    setTextSize(
						    		msgTextSize);
						TextView timestampTv = (TextView) row.findViewById(R.id.fbSentStatusTimestamp);
						timestampTv.setText(standardDf.format(cal2.getTime()));
						tv.setText(mSmileyParser.addSmileySpans(Html.fromHtml(txtMsg + "<br/>" 
								+ tmpMsg.makeSmall(mContext.getString(R.string.fb_status))))
								,  TextView.BufferType.SPANNABLE);
						((TextView)row.findViewById(R.id.fbSentStatusLikes)).setText(tmpMsg.msgLikes+"");
						((TextView)row.findViewById(R.id.fbSentStatusComments)).setText(tmpMsg.msgComments+"");
				        Linkify.addLinks(tv, Linkify.WEB_URLS|Linkify.EMAIL_ADDRESSES|Linkify.MAP_ADDRESSES);
				        Linkify.addLinks(tv, phonePattern, "tel:");
					} else if (tmpMsg.isFbStatus() && tmpMsg.linkedMsgs.size() > 0) {
						TextView tv = (TextView) row.findViewById(R.id.fbSentStatusText);
						tv.setTextSize(msgTextSize);
						TextView timestampTv = (TextView) row.findViewById(R.id.fbSentStatusTimestamp);
						timestampTv.setText(standardDf.format(cal2.getTime()));
						LinearLayout photos = (LinearLayout)row.findViewById(R.id.fbSentPhotoLinearLayout);
						photos.removeAllViews();
						for (FeedTextMsg photoMsg : tmpMsg.linkedMsgs) {
					    	if (bitmaps.hasUrl(photoMsg.msgPicURL)) {
								setImage(photoMsg, row, photos);
					    	} else {
					    		setDownloadImage(photoMsg, row, photos, position);
					    	}
						}
						if (txtMsg.length() > 0)
							tv.setText(mSmileyParser.addSmileySpans(Html.fromHtml(
									txtMsg + "<br/>" 
									+ tmpMsg.makeSmall(mContext.getString(R.string.fb_photo))))
									,  TextView.BufferType.SPANNABLE);
						else tv.setVisibility(View.GONE);
						((TextView)row.findViewById(R.id.fbSentStatusLikes)).setText(tmpMsg.msgLikes+"");
						((TextView)row.findViewById(R.id.fbSentStatusComments)).setText(tmpMsg.msgComments+"");
				        Linkify.addLinks(tv, Linkify.WEB_URLS|Linkify.EMAIL_ADDRESSES|Linkify.MAP_ADDRESSES);
				        Linkify.addLinks(tv, phonePattern, "tel:");
					} else if (tmpMsg.isFbPhoto()) {
						TextView tv = (TextView) row.findViewById(R.id.fbSentPhotoText);
						tv.setTextSize(msgTextSize);
						TextView timestampTv = (TextView) row.findViewById(R.id.fbSentPhotoTimestamp);
						timestampTv.setText(standardDf.format(cal2.getTime()));
						LinearLayout photos = (LinearLayout)row.findViewById(R.id.fbSentPhotoLinearLayout);
						photos.removeAllViews();
				    	if (bitmaps.hasUrl(tmpMsg.msgPicURL)) {
							setImage(tmpMsg, row, photos);
				    	} else {
				    		setDownloadImage(tmpMsg, row, photos, position);
				    	}
						if (txtMsg.length() > 0)
							tv.setText(mSmileyParser.addSmileySpans(Html.fromHtml(
									txtMsg + "<br/>" 
									+ tmpMsg.makeSmall(mContext.getString(R.string.fb_photo))))
									,  TextView.BufferType.SPANNABLE);
						else tv.setVisibility(View.GONE);
						((TextView)row.findViewById(R.id.fbSentPhotoLikes)).setText(tmpMsg.msgLikes+"");
						((TextView)row.findViewById(R.id.fbSentPhotoComments)).setText(tmpMsg.msgComments+"");
				        Linkify.addLinks(tv, Linkify.WEB_URLS|Linkify.EMAIL_ADDRESSES|Linkify.MAP_ADDRESSES);
				        Linkify.addLinks(tv, phonePattern, "tel:");
					}						
				} else {
					if (tmpMsg.isFbStatus() && tmpMsg.linkedMsgs.size() == 0) {
						TextView tv = (TextView) row.findViewById(R.id.fbReceivedStatusText);
						tv.setTextSize(msgTextSize);
						TextView timestampTv = (TextView) row.findViewById(R.id.fbReceivedStatusTimestamp);
						timestampTv.setText(standardDf.format(cal2.getTime()));
						tv.setText(mSmileyParser.addSmileySpans(Html.fromHtml(txtMsg + "<br/>" 
								+ tmpMsg.makeSmall(mContext.getString(R.string.fb_status))))
								,  TextView.BufferType.SPANNABLE);
						((TextView)row.findViewById(R.id.fbReceivedStatusLikes)).setText(tmpMsg.msgLikes+"");
						((TextView)row.findViewById(R.id.fbReceivedStatusComments)).setText(tmpMsg.msgComments+"");
				        Linkify.addLinks(tv, Linkify.WEB_URLS|Linkify.EMAIL_ADDRESSES|Linkify.MAP_ADDRESSES);
				        Linkify.addLinks(tv, phonePattern, "tel:");
					} else if (tmpMsg.isFbStatus() && tmpMsg.linkedMsgs.size() > 0) {
						TextView tv = (TextView) row.findViewById(R.id.fbReceivedStatusText);
						tv.setTextSize(msgTextSize);
						TextView timestampTv = (TextView) row.findViewById(R.id.fbReceivedStatusTimestamp);
						timestampTv.setText(standardDf.format(cal2.getTime()));
						LinearLayout photos = (LinearLayout)row.findViewById(R.id.fbReceivedPhotoLinearLayout);
						photos.removeAllViews();
						for (FeedTextMsg photoMsg : tmpMsg.linkedMsgs) {
					    	if (bitmaps.hasUrl(photoMsg.msgPicURL)) {
								setImage(photoMsg, row, photos);
					    	} else {
					    		setDownloadImage(photoMsg, row, photos, position);
					    	}
						}
						if (txtMsg.length() > 0)
							tv.setText(mSmileyParser.addSmileySpans(Html.fromHtml(
									txtMsg + "<br/>" 
									+ tmpMsg.makeSmall(mContext.getString(R.string.fb_photo))))
									,  TextView.BufferType.SPANNABLE);
						else tv.setVisibility(View.GONE);
						((TextView)row.findViewById(R.id.fbReceivedStatusLikes)).setText(tmpMsg.msgLikes+"");
						((TextView)row.findViewById(R.id.fbReceivedStatusComments)).setText(tmpMsg.msgComments+"");
				        Linkify.addLinks(tv, Linkify.WEB_URLS|Linkify.EMAIL_ADDRESSES|Linkify.MAP_ADDRESSES);
				        Linkify.addLinks(tv, phonePattern, "tel:");
					} else if (tmpMsg.isFbPhoto()) {
						TextView tv = (TextView) row.findViewById(R.id.fbReceivedPhotoText);
						tv.setTextSize(msgTextSize);
						TextView timestampTv = (TextView) row.findViewById(R.id.fbReceivedPhotoTimestamp);
						timestampTv.setText(standardDf.format(cal2.getTime()));
						LinearLayout photos = (LinearLayout)row.findViewById(R.id.fbReceivedPhotoLinearLayout);
						photos.removeAllViews();
				    	if (bitmaps.hasUrl(tmpMsg.msgPicURL)) {
							setImage(tmpMsg, row, photos);
				    	} else {
				    		setDownloadImage(tmpMsg, row, photos, position);
				    	}
						if (txtMsg.length() > 0)
							tv.setText(mSmileyParser.addSmileySpans(Html.fromHtml(
									txtMsg + "<br/>" 
									+ tmpMsg.makeSmall(mContext.getString(R.string.fb_photo))))
									,  TextView.BufferType.SPANNABLE);
						else tv.setVisibility(View.GONE);
						((TextView)row.findViewById(R.id.fbReceivedPhotoLikes)).setText(tmpMsg.msgLikes+"");
						((TextView)row.findViewById(R.id.fbReceivedPhotoComments)).setText(tmpMsg.msgComments+"");
				        Linkify.addLinks(tv, Linkify.WEB_URLS|Linkify.EMAIL_ADDRESSES|Linkify.MAP_ADDRESSES);
				        Linkify.addLinks(tv, phonePattern, "tel:");
					}						
				}
			}
		} else if(tmpMsg.isTwtType()) {
			if(itemType == FeedTextMsg.TYPE_TWITTER_MSG) {
				if (tmpMsg.isSentMsg()) {
					TextView tv = (TextView) row.findViewById(R.id.twitterMsgSentText);
					tv.setTextSize(msgTextSize); // null here 20140711 1318
					TextView timestampTv = (TextView) row.findViewById(R.id.twitterMsgSentTimestamp);
					timestampTv.setText(standardDf.format(cal2.getTime()));
					tv.setText(mSmileyParser.addSmileySpans(Html.fromHtml(txtMsg + "<br/>" 
							+ tmpMsg.makeSmall(mContext.getString(R.string.twitter_msg))))
							,  TextView.BufferType.SPANNABLE);
			        Linkify.addLinks(tv, Linkify.WEB_URLS|Linkify.EMAIL_ADDRESSES|Linkify.MAP_ADDRESSES);
			        Linkify.addLinks(tv, phonePattern, "tel:");
				} else {
					TextView tv = (TextView) row.findViewById(R.id.twitterMsgReceivedText);
					tv.setTextSize(msgTextSize);
					TextView timestampTv = (TextView) row.findViewById(R.id.twitterMsgReceivedTimestamp);
					timestampTv.setText(standardDf.format(cal2.getTime()));
					tv.setText(mSmileyParser.addSmileySpans(Html.fromHtml(txtMsg + "<br/>" 
							+ tmpMsg.makeSmall(mContext.getString(R.string.twitter_msg))))
							,  TextView.BufferType.SPANNABLE);
			        Linkify.addLinks(tv, Linkify.WEB_URLS|Linkify.EMAIL_ADDRESSES|Linkify.MAP_ADDRESSES);
			        Linkify.addLinks(tv, phonePattern, "tel:");
				}
			} 
			
			if(itemType == FeedTextMsg.TYPE_TWEET) {
				if (tmpMsg.isSentMsg()) {
					TextView tv = (TextView) row.findViewById(R.id.twitterTweetSentText);
					tv.setTextSize(msgTextSize);
					TextView timestampTv = (TextView) row.findViewById(R.id.twitterTweetSentTimestamp);
					timestampTv.setText(standardDf.format(cal2.getTime()));
					tv.setText(mSmileyParser.addSmileySpans(Html.fromHtml(txtMsg + "<br/>" 
							+ tmpMsg.makeSmall(mContext.getString(R.string.twitter_tweet))))
							,  TextView.BufferType.SPANNABLE);
					//((TextView)row.findViewById(R.id.fbSentStatusLikes)).setText(tmpMsg.msgLikes+"");
					//((TextView)row.findViewById(R.id.fbSentStatusComments)).setText(tmpMsg.msgComments+"");
			        Linkify.addLinks(tv, Linkify.WEB_URLS|Linkify.EMAIL_ADDRESSES|Linkify.MAP_ADDRESSES);
			        Linkify.addLinks(tv, phonePattern, "tel:");
				} else {
					TextView tv = (TextView) row.findViewById(R.id.twitterTweetReceivedText);
					tv.
					    setTextSize(// null here 20140711 0936
					    		msgTextSize);
					TextView timestampTv = (TextView) row.findViewById(R.id.twitterTweetReceivedTimestamp);
					timestampTv.setText(standardDf.format(cal2.getTime()));
					tv.setText(mSmileyParser.addSmileySpans(Html.fromHtml(txtMsg + "<br/>" 
							+ tmpMsg.makeSmall(mContext.getString(R.string.twitter_tweet))))
							,  TextView.BufferType.SPANNABLE);
					//((TextView)row.findViewById(R.id.fbSentStatusLikes)).setText(tmpMsg.msgLikes+"");
					//((TextView)row.findViewById(R.id.fbSentStatusComments)).setText(tmpMsg.msgComments+"");
			        Linkify.addLinks(tv, Linkify.WEB_URLS|Linkify.EMAIL_ADDRESSES|Linkify.MAP_ADDRESSES);
			        Linkify.addLinks(tv, phonePattern, "tel:");
				}
			}
		} else if (tmpMsg.isSentMsg()|tmpMsg.isCallLogOutgoing()) {
			TextView tv = (TextView) row.findViewById(R.id.smsSentText);
			tv.setTextSize(msgTextSize);
			TextView timestampTv = (TextView) row.findViewById(R.id.smsSentTimestamp);
			timestampTv.setText(standardDf.format(cal2.getTime()));
			if (tmpMsg.hasSendErr()) {
				tv.setText(mSmileyParser.addSmileySpans(Html.fromHtml(txtMsg + "<br/>" 
						+ tmpMsg.makeSmall(mContext.getString(R.string.sysMsgNotSent))))
						,  TextView.BufferType.SPANNABLE);
			} else if (tmpMsg.hasDeliveryErr()) {
				tv.setText(mSmileyParser.addSmileySpans(Html.fromHtml(txtMsg + "<br/>" 
						+ tmpMsg.makeSmall(mContext.getString(R.string.sysMsgNotDelivered))))
						,  TextView.BufferType.SPANNABLE);
			} else 
			// check for forbidden messages, and change the message if it is after 8 hours 
			if (tmpMsg.isForbiddenMsg() & ((cal1.getTimeInMillis() - cal2.getTimeInMillis()) > 8*60*60*1000)) {
				tv.setText(mSmileyParser.addSmileySpans(Html.fromHtml(txtMsg + "<br/>" 
					+ tmpMsg.makeSmall(mContext.getString(R.string.sysMsgNotSentForbidden))))
					,  TextView.BufferType.SPANNABLE);
			} else 
				// check for call logs 
				if (tmpMsg.isCallLog()) {
				tv.setText(mSmileyParser.addSmileySpans(Html.fromHtml(txtMsg))
					,  TextView.BufferType.SPANNABLE);
			} else {
				if (tmpMsg.isDelivered())
					//TODO fix "sent" and "delivered" status indicators
					tv.setText(mSmileyParser.addSmileySpans(Html.fromHtml(txtMsg))
						,  TextView.BufferType.SPANNABLE);
				else if (tmpMsg.isSent())
					tv.setText(mSmileyParser.addSmileySpans(Html.fromHtml(txtMsg))
						,  TextView.BufferType.SPANNABLE);
				else
					tv.setText(mSmileyParser.addSmileySpans(Html.fromHtml(txtMsg)) 
							,  TextView.BufferType.SPANNABLE);
			}
        	if (EpicFeedConsts.debugLevel > 10) Log.i(TAG, "ArrayAdapter: setting sent images for msg");  
            Linkify.addLinks(tv, Linkify.WEB_URLS|Linkify.EMAIL_ADDRESSES|Linkify.MAP_ADDRESSES);
            Linkify.addLinks(tv, phonePattern, "tel:");
		} else if (tmpMsg.isInfoMsg() && tmpMsg.noMessages) {
			TextView tv = (TextView) row.findViewById(R.id.smsReceivedText);
			tv.setTextSize(msgTextSize);
			TextView timestampTv = (TextView) row.findViewById(R.id.smsReceivedTimestamp);
			timestampTv.setText(standardDf.format(cal2.getTime()));
			tv.setText(Html.fromHtml(txtMsg)
					,  TextView.BufferType.SPANNABLE);
		} else {
			TextView tv = (TextView) row.findViewById(R.id.smsReceivedText);
			tv.
			setTextSize(// null here 20140710 1336
					msgTextSize);
			TextView timestampTv = (TextView) row.findViewById(R.id.smsReceivedTimestamp);
			timestampTv.setText(standardDf.format(cal2.getTime()));
			// check for call logs 
			if (tmpMsg.isCallLog()&!tmpMsg.isCallLogMissed()&!tmpMsg.isCallLogBlocked()) {
				tv.setText(Html.fromHtml(txtMsg)
					, TextView.BufferType.SPANNABLE);
			} else
				tv.setText(mSmileyParser.addSmileySpans(Html.fromHtml(txtMsg))
						,  TextView.BufferType.SPANNABLE);
            Linkify.addLinks(tv, Linkify.WEB_URLS|Linkify.EMAIL_ADDRESSES|Linkify.MAP_ADDRESSES);
            Linkify.addLinks(tv, phonePattern, "tel:");
		}
		return row;
	}
	
	private void setOnClickLaunchFb(View row, final FeedTextMsg tmpMsg) {
		row.setClickable(true);
		row.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				EpicFeedController.getContext().startActivity(getFbIntent(tmpMsg));
			}
		});
	}
	
	private void setOnClickFbExpand(final View row, final TextView tv, final FeedTextMsg tmpMsg) {
		tv.setClickable(true);
		OnClickListener listener = new OnClickListener() {
			@Override
			public void onClick(View v) {
				tv.setMaxLines(20);
				tv.setOnClickListener(null);
				tv.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						EpicFeedController.getContext().startActivity(getFbIntent(tmpMsg));
					}
				});
				tv.invalidate();
				setOnClickLaunchFb(row, tmpMsg);
			}
		};
		tv.setOnClickListener(listener);
	}
	
	private Intent getFbIntent(FeedTextMsg textMsg) {
		Intent fbIntent = FacebookConnectionModel.getFbIntent("facebook.com/photo");
		return fbIntent;
	}

	private EpicBitmaps bitmaps = EpicBitmaps.getInstance();
	
	private void setImage(final FeedTextMsg photoMsg, View row, LinearLayout photos) {
		final ImageView photo = new ImageView(row.getContext());
		photo.setVisibility(View.VISIBLE);
		photo.setLayoutParams(new LayoutParams(
				LayoutParams.MATCH_PARENT,
				LayoutParams.MATCH_PARENT));
		Bitmap b = bitmaps.get(photoMsg.msgPicURL);
		int width = Math.min(imgWidth, (int)(((long)b.getWidth() * (long)imgHeight) / (long)b.getHeight()));
		photo.setScaleType(ScaleType.CENTER_CROP);
		photo.getLayoutParams().height = imgHeight;
		photo.getLayoutParams().width = width;
		photo.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				mContext.startActivity(getFbIntent(photoMsg));
			}
		});
		final RelativeLayout rl = new RelativeLayout(row.getContext());
		rl.setBackgroundResource(R.drawable.bg_mms);
		rl.setLayoutParams(new RelativeLayout.LayoutParams(
				b.getWidth() * (imgHeight / b.getHeight()),
				LayoutParams.WRAP_CONTENT));
		rl.getLayoutParams().height = imgHeight;
		rl.getLayoutParams().width = width;
		rl.addView(photo);
		photos.addView(rl);
		photo.setImageBitmap(b);
		photo.setClickable(true);
	}
	
	private void setDownloadImage(final FeedTextMsg photoMsg, final View row, final LinearLayout photos, final int position) {
		final ImageView photo = new ImageView(row.getContext());
		photo.setVisibility(View.VISIBLE);
		photo.setLayoutParams(new LayoutParams(
				LayoutParams.MATCH_PARENT,
				LayoutParams.MATCH_PARENT));
		photo.setImageDrawable(downloadImg);
		photo.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				downloadImg(photoMsg.msgPicURL, row, photos, position);
			}
		});
		photos.addView(photo);
		photo.setClickable(true);
	}
	
	private synchronized void downloadImg(final String imgURL, final View row, final LinearLayout photos, final int id) {
    	if (imgURL == null || 
    			imgURL.length() < 5 ||
    			photos == null) return;
    	final int height = imgHeight;
    	if (bitmaps.hasUrl(imgURL)) {
    		setImage(getItem(id), row, photos);
			return;
    	}
		final String key = photos.getTag()+"";
		EpicImageAdapter listener = new EpicImageAdapter() {
			@Override
			public void onLoaded(Bitmap b, String imgUrl) {
				if (imgUrl.equals(imgURL)) {
					photos.removeAllViews();
		    		setImage(getItem(id), row, photos);
					done = true;
				}
			}
		};
		bitmaps.get(imgURL, key, listener);
    }
	
	private synchronized void downloadAvatar(final String imgURL, final ImageView image, final int id) {
    	if (imgURL == null || 
    			imgURL.length() < 5 ||
    			image == null) return;
    	if (bitmaps.hasUrl(imgURL)) {
			image.setImageDrawable(new BitmapDrawable(mContext.getResources(), bitmaps.get(imgURL)));
			image.setScaleType(ScaleType.CENTER_CROP);
			image.getLayoutParams().height = avatarHeight;
			image.getLayoutParams().width = avatarHeight;
			return;
    	}
		final String key = image.getTag()+"";
		EpicImageAdapter listener = new EpicImageAdapter() {
			@Override
			public void onLoaded(Bitmap b, String imgUrl) {
				if (imgUrl.equals(imgURL)) {
					image.setImageDrawable(new BitmapDrawable(mContext.getResources(), b));
					image.setScaleType(ScaleType.CENTER_CROP);
					image.getLayoutParams().height = avatarHeight;
					image.getLayoutParams().width = avatarHeight;
					done = true;
				}
			}
		};
		bitmaps.get(imgURL, key, listener);
    }
	
	private Bitmap getOneMMSImage (String mmsId) {
		String selectionPart = "mid=" + mmsId;
		Uri uri = Uri.parse("content://mms/part");
		Cursor cPart = mContext.getContentResolver().query(uri, null,
		    selectionPart, null, null);
		if (cPart.moveToFirst()) {
		    do {
		        String partId = cPart.getString(cPart.getColumnIndex("_id"));
		        String type = cPart.getString(cPart.getColumnIndex("ct"));
		        if ("image/jpeg".equals(type) || "image/bmp".equals(type) ||
		                "image/gif".equals(type) || "image/jpg".equals(type) ||
		                "image/png".equals(type)) {
		        	// should create an array
		            //Bitmap bitmap = getMmsImage(partId);
		        	cPart.close();
		            return bitmaps.getMmsImage(partId, imgHeight);
		        }
		    } while (cPart.moveToNext());
		}
		return null;
	}
	
}
