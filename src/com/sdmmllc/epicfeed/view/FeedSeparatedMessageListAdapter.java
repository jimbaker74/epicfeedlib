package com.sdmmllc.epicfeed.view;

import java.util.LinkedHashMap;
import java.util.Map;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.sdmm.epicfeed.R;
import com.sdmmllc.epicfeed.utils.EpicFeedConsts;

public class FeedSeparatedMessageListAdapter extends BaseAdapter {

	public String TAG = "SpaSeparatedListAdapter";
	public final Map<String, Adapter> sections = new LinkedHashMap<String, Adapter>();
	public final ArrayAdapter<String> headers;
	public final static int TYPE_SECTION_HEADER = 0;
	public int limitCount = 50;
	FeedMessageTheme themeOptions;
	LayoutInflater mInflater;

	public FeedSeparatedMessageListAdapter(Context context, FeedMessageTheme newTheme) {
		themeOptions = newTheme;
		mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		headers = new ArrayAdapter<String>(context, R.layout.list_header) {
			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
		    	if (EpicFeedConsts.debugLevel > 9) Log.i(TAG, "getHeaderAdapter: getView: position: " + position);
				View row;
				if (null == convertView)  row = mInflater.inflate(R.layout.list_header, parent, false);
				else row = convertView;
				TextView tv = (TextView) row.findViewById(R.id.list_header_title);
				tv.setText(getItem(position));
				if (EpicFeedConsts.debugLevel > 9) 
					Log.i(TAG, "getHeaderAdapter: setText: getItem(position): " + getItem(position));
				if (themeOptions.useTheme) {
					tv.setTextSize(themeOptions.getHeaderTextSize());
					if (themeOptions.getHeaderTextSize() == EpicFeedConsts.txt_sml) {
						tv.setPadding(0, 0, 0, 0);
						if (EpicFeedConsts.debugLevel > 9) 
							Log.i(TAG, "getHeaderAdapter: setText: SpaTextConsts.txt_sml");
					} else if (themeOptions.getHeaderTextSize() == EpicFeedConsts.txt_lrg) {
						tv.setPadding(0, 2, 0, 2);
						if (EpicFeedConsts.debugLevel > 9) 
							Log.i(TAG, "getHeaderAdapter: setText: SpaTextConsts.txt_lrg");
					} else {
						tv.setPadding(0, 1, 0, 1);
						if (EpicFeedConsts.debugLevel > 9) 
							Log.i(TAG, "getHeaderAdapter: setText: SpaTextConsts.txt_med");
					}
					tv.setTextColor(themeOptions.headerTextColor);
					tv.setBackgroundResource(themeOptions.headerColor);
				}
				return row;
			};
		};
	}

	public void addSection(String section, Adapter adapter) {
		this.headers.add(section);
		this.sections.put(section, adapter);
	}

	public void removeSection(String section, Adapter adapter) {
		this.headers.remove(section);
		this.sections.remove(section);
	}

	@Override
	public Object getItem(int position) {
		for(Object section : this.sections.keySet()) {
			Adapter adapter = sections.get(section);
			int size = adapter.getCount() + 1;
			if (themeOptions.threadAscending) {
				// check if position inside this section
				if(position == (size - 1)) return section;
				if(position < (size - 1)) return adapter.getItem(position);
			} else {
				// check if position inside this section
				if(position == 0) return section;
				if(position < size) return adapter.getItem(position - 1);
			}

			// otherwise jump into next section
			position -= size;
		}
		return null;
	}

	@Override
	public int getCount() {
		// total together all sections, plus one for each section header
		int total = 0;
		for(Adapter adapter : this.sections.values())
			total += adapter.getCount() + 1;
		if (total > limitCount) return limitCount;
		return total;
	}

	public int getTotalCount() {
		// total together all sections, plus one for each section header
		int total = 0;
		for(Adapter adapter : this.sections.values())
			total += adapter.getCount() + 1;
		return total;
	}

	@Override
	public int getViewTypeCount() {
		// assume that headers count as one, then total all sections
		int total = 1;
		for(Adapter adapter : this.sections.values())
			total += adapter.getViewTypeCount();
		return total;
	}

	// *   *   *   *   *   Look here Mr. Fuentes!!!!!!!!!!
	@Override
	public int getItemViewType(int position) {
		int type = 1;
		for(Object section : this.sections.keySet()) {
			//type = 1; // changed 20140729 JF: temp solution for ArrayIndexOutOfBounds error for twitter
			Adapter adapter = sections.get(section);
			int size = adapter.getCount() + 1;
			if (themeOptions.threadAscending) {
				// check if position inside this section
				if(position == (size - 1)) return TYPE_SECTION_HEADER;
				if(position < (size - 1)) return type + adapter.getItemViewType(position);
			} else {
				// check if position inside this section
				if(position == 0) return TYPE_SECTION_HEADER;
				if(position < size) return type + adapter.getItemViewType(position - 1);
			}
			// otherwise jump into next section
			position -= size;
			type += adapter.getViewTypeCount();
		}
		return -1;
	}

	public boolean areAllItemsSelectable() {
		return false;
	}

	@Override
	public boolean isEnabled(int position) {
		return (getItemViewType(position) != TYPE_SECTION_HEADER);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		int sectionnum = 0;
		for(Object section : this.sections.keySet()) {
			Adapter adapter = sections.get(section);
			int size = adapter.getCount() + 1;

			if (themeOptions.threadAscending) {
				// check if position inside this section
				if(position == (size - 1)) return headers.getView(sectionnum, convertView, parent);
				if(position < (size - 1)) return adapter.getView(position, convertView, parent);
			} else {
				// check if position inside this section
				if(position == 0) return headers.getView(sectionnum, convertView, parent);
				if(position < size) return adapter.getView(position - 1, convertView, parent);
			}
			// otherwise jump into next section
			position -= size;
			sectionnum++;
		}
		return null;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	public FeedTextMsg getMsg(int position) {
		for(Object section : this.sections.keySet()) {
			Adapter adapter = sections.get(section);
			int size = adapter.getCount() + 1;
			if (themeOptions.threadAscending) {
				// check if position inside this section
				if(position < (size - 1)) return (FeedTextMsg) 
					adapter.getItem(position);
			} else {
				// check if position inside this section
				if(position < size) return (FeedTextMsg) 
					adapter.getItem(position - 1);
			}

			// otherwise jump into next section
			position -= (size - 1);
		}
		return null;
	}
}
