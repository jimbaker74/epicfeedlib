package com.sdmmllc.epicfeed.view;

import java.util.Calendar;
import java.util.List;
import java.util.regex.Pattern;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;

import com.sdmm.epicfeed.R;
import com.sdmmllc.epicfeed.EpicFeedController;
import com.sdmmllc.epicfeed.utils.EpicFeedConsts;
import com.sdmmllc.epicfeed.utils.EpicSmileyParser;

public class FeedMessageListHelper {
	String TAG = "SpaTextSMSList";
	public int index, msgTextSize = EpicFeedConsts.txt_med;
    public String listTitle;
    public List<FeedTextMsg> txtMsgList;
    public FeedMessageListAdapter msgListAA;
    public boolean fakeMsgList = false;
    private Context spaContext;
	private String themeOption = EpicFeedConsts.STANDARD_THEME;
	private FeedMessageTheme themeColors;
	private EpicSmileyParser mSmileyParser;
	private Pattern phonePattern = Pattern.compile("(^|([[\\D]&&[^[[a-z][A-Z]]]])+?)(\\d{3})?([[\\D]&&[^[[a-z][A-Z]]]])?(\\d{3})([[\\D]&&[^[[a-z][A-Z]]]])?(\\d{4})");

    private int 
		textRecdLeftMargin = 5, textRecdRightMargin = -1,
		textSentLeftMargin = -1, textSentRightMargin = 5,
		textBtmMargin = 1, textTopMargin = 1; 
    private Calendar cal1 = Calendar.getInstance(), 
	cal2 = Calendar.getInstance();
    static java.text.DateFormat df;

    public FeedMessageListHelper(int idx, String title, List<FeedTextMsg> msgList) {
    	index = idx;
    	listTitle = title;
    	txtMsgList = msgList;
    	mSmileyParser = EpicFeedController.getSmileyParser();
    }
    
    public void addMsg(FeedTextMsg tmpMsg) {
    	txtMsgList.add(tmpMsg);
    	//if (msgListAA != null) msgListAA.add(tmpMsg);
    }
    
    public void getAA(Context ctx, LayoutInflater mInf) {
    	cal1 = Calendar.getInstance();
    	spaContext = ctx;
    	SharedPreferences settings = spaContext.getSharedPreferences(EpicFeedConsts.PREFS_NAME, 0);
		if (!settings.contains(EpicFeedConsts.THEME_SELECT)) {
			if (ctx.getResources().getBoolean(R.bool.pinkThread)) themeOption = EpicFeedConsts.PINK_THEME;
			else if (ctx.getResources().getBoolean(R.bool.greenThread)) themeOption = EpicFeedConsts.GREEN_THEME;
			else themeOption = settings.getString(EpicFeedConsts.THEME_SELECT, EpicFeedConsts.DEFAULT_THEME);
		} else themeOption = settings.getString(EpicFeedConsts.THEME_SELECT, EpicFeedConsts.DEFAULT_THEME);
    	themeColors = new FeedMessageTheme(spaContext);
    	themeColors.setTheme(themeOption);
    	msgListAA = getArrayAdapter(ctx, mInf, txtMsgList);
		if (EpicFeedConsts.debugLevel > 10) Log.i(TAG, "getAA: got adapter"); 
    }
    
	private FeedMessageListAdapter getArrayAdapter(Context context, final LayoutInflater mInflater, List<FeedTextMsg> mList) {
		if (EpicFeedConsts.debugLevel > 7) Log.i(TAG, "getArrayAdapter: getting adapter"); 
		return new FeedMessageListAdapter(context, R.layout.spa_text_sms_list_item, mList);
	}
}
