package com.sdmmllc.epicfeed.ads;

import java.lang.ref.WeakReference;
import java.util.Calendar;

import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;

import com.mopub.mobileads.MoPubErrorCode;
import com.mopub.mobileads.MoPubView;
import com.mopub.mobileads.MoPubView.BannerAdListener;
import com.sdmm.epicfeed.R;

public class MoPubAdListener implements BannerAdListener {
	String TAG = "MoPubAdListener", activityName;
	
	public boolean needAd = true, tryLocation = false;
    public String actName = "";
    public long lastAdTime = 0, requestCount = 0, adCount = 0, adTotalTime = 0, pauseCount = 0, adClickCnt = 0;

    private WeakReference<Activity> adAct;
    private RelativeLayout adLayout;

    public MoPubAdListener(String name, WeakReference<Activity> actRef, RelativeLayout layout) {
		actName = name;
		adAct = actRef;
		adLayout = layout;
	}
	
	public void pauseAds() {
		lastAdTime = 0;
		pauseCount++;
	}
	
	public boolean needAd() {
		if (((Calendar.getInstance().getTimeInMillis() - lastAdTime) > 
			MoPubAdHandler.waitTime)
			||needAd
			||tryLocation) return true;
		else return false;
	}
	
	@Override
	public void onBannerClicked(MoPubView arg0) {
		adClickCnt++;
		Log.i("Ads", "SPA: ad clicked " + actName);
	}

	@Override
	public void onBannerCollapsed(MoPubView arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onBannerExpanded(MoPubView arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onBannerFailed(final MoPubView adView, MoPubErrorCode arg1) {
		needAd = true;
		requestCount++;
		adView.getActivity().runOnUiThread(new Runnable() {
			@Override
			public void run() {
				adView.setVisibility(View.GONE);
			}
		});
	}

	@Override
	public void onBannerLoaded(final MoPubView adView) {
		needAd = false;
		long adTime = Calendar.getInstance().getTimeInMillis();
		if (lastAdTime > 0) adTotalTime += adTime - lastAdTime;
		lastAdTime = adTime;
		adCount++;
		requestCount++;
		adView.getActivity().runOnUiThread(new Runnable() {
			@Override
			public void run() {
				if (adView == null || adLayout == null) return;
				adView.setVisibility(View.VISIBLE);
				Log.i("Ads", "SPA: new ad height:" + adView.getHeight());
				if (adView.getHeight() > adAct.get().getResources().getDimension(R.dimen.adBuffer)) {
					LayoutParams params = new LayoutParams(adView.getWidth(), 
							adAct.get().getResources().getDimensionPixelSize(R.dimen.adBuffer));
					adView.setLayoutParams(params);
					adLayout.requestLayout();
					Log.i("Ads", "SPA: adjusted ad height:" + adView.getHeight());
				}
			}
		});
		Log.i(TAG, "SPA: received new ad " + actName);
	}

}
