package com.sdmmllc.epicfeed.ads;

import com.amazon.device.ads.Ad;
import com.amazon.device.ads.AdError;
import com.amazon.device.ads.AdListener;
import com.amazon.device.ads.AdProperties;

public interface AmazonAdListener extends AdListener {
	
	public void onAdCollapsed(Ad ad);
	public void onAdDismissed(Ad ad);
	public void onAdExpanded(Ad ad);
	public void onAdFailedToLoad(Ad ad, AdError error);
	public void onAdLoaded(Ad ad, AdProperties adProperties);
	
}
