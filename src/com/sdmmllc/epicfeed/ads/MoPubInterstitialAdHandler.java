package com.sdmmllc.epicfeed.ads;

import java.lang.ref.WeakReference;

import android.util.Log;

import com.mopub.mobileads.MoPubInterstitial;
import com.sdmmllc.epicfeed.ui.Act_FeedMessageList;

public class MoPubInterstitialAdHandler implements Runnable {
	String TAG = "InterstitialAdHandler", activityName;
	public static int waitTime = 10000, displayWait = 5000;
	private WeakReference<Act_FeedMessageList> act;
	private MoPubInterstitial mInterstitial;
	private boolean getAd = true, reloadAd = false, isShowing = false;
	private int waitCount = 1;
	private ReturnInterstitialAdListener adListener;

	public MoPubInterstitialAdHandler(WeakReference<Act_FeedMessageList> activity, ReturnInterstitialAdListener listener, MoPubInterstitial ad) {
		act = activity;
		adListener = listener;
		mInterstitial = ad;
	}
	
	public void getAd(boolean status) {
		getAd = status;
	}
	
	public void reloadAd(boolean status) {
		reloadAd = status;
	}
	
	public void isShowing(boolean showing) {
		isShowing = showing;
	}
	
	private long getNotifyWaitTime() {
		return waitTime*waitCount;
	}

	@Override
	public void run() {
	    //if (SpaTextConsts.debugLevel > 7) 
			Log.i(TAG, "run(): starting Interstitial AdHandler");
		// wait for adView to initialize
		synchronized (this) {
			try {
				wait(displayWait*3);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	    //if (SpaTextConsts.debugLevel > 7) 
			Log.i(TAG, "run(): getting new interstitial ad");
		waitCount = 1;
		mInterstitial.getActivity().runOnUiThread(loadAd);
		while (getAd) {
			try {
				synchronized (this) {
					wait(waitTime*waitCount);
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			while (mInterstitial.isReady() && !isShowing) {
				if (adListener.checkNotifyAd() > getNotifyWaitTime()) {
					Log.i(TAG, "run(): notification of Interstitial Ad");
					adListener.notifyNewAd();
					// wait to check for ad display
					try {
						synchronized (this) {
							wait(displayWait);
						}
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					if (adListener.checkDisplayAd() > (getNotifyWaitTime() + displayWait)) {
						Log.i(TAG, "run(): displaying Interstitial Ad");
						adListener.displayAd();
						isShowing = true;
						waitCount = 1;
					} else {
						if (waitCount < 4) waitCount++;
					}
				}
				try {
					synchronized (this) {
						wait(waitTime*waitCount);
					}
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				Log.i(TAG, "run(): waiting for Interstitial ad");
			}
			if (reloadAd) {
				mInterstitial.getActivity().runOnUiThread(loadAd);
				reloadAd = false;
			}
		}
	};
	
	private Runnable loadAd = new Runnable() {

		@Override
		public void run() {
			mInterstitial.load();
		}
		
	};
	
	public interface ReturnInterstitialAdListener {
		public abstract long checkNotifyAd();
		public abstract void notifyNewAd();
		public abstract long checkDisplayAd();
		public abstract void displayAd();
	}
}
