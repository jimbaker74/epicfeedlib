package com.sdmmllc.epicfeed.ads;

import java.lang.ref.WeakReference;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;

import com.mopub.mobileads.MoPubView;
import com.sdmm.epicfeed.R;
import com.sdmmllc.epicfeed.CacheManager;
import com.sdmmllc.epicfeed.utils.EpicFeedConsts;

public class MoPubAdHandler implements Runnable {
	String TAG = "AdHandler", activityName;
	public static int waitTime = 20000, waitSplit = 5;
	private MoPubView mMMAdView;
	private MoPubAdListener mMMAdListener;
	private ReturnAdListener mReturnAdListener;
    private RelativeLayout adLayout;
	private Timer timer;
	private TimerTask adTimer;
	private WeakReference<Activity> spaContext;
	boolean haveLocation = false, skipLocation = false,
		adRequestReady = false, loadAd = false;
    public Boolean isBeingDestroyed = false, checkAd = true;
	private Boolean pauseAds = false, doNotWait = false, timerRunning = false, doNotDestroy = true;
    private Thread adCheckerThread;
    private boolean solitaireLicense = false, maleApp = false, femaleApp = false;

    private boolean pauseAdCheck = false, destroyAdCheck = false;
	
	public MoPubAdHandler(Activity ctx, String name, MoPubView ad, RelativeLayout layout, MoPubAdListener listener, 
			ReturnAdListener returnAdListener) {
		spaContext = new WeakReference<Activity>(ctx);
		activityName = name;
		mMMAdView = ad;
		mMMAdListener = listener;
		adLayout = layout;
		mReturnAdListener = returnAdListener;
		solitaireLicense = ctx.getApplicationContext().getResources().getBoolean(R.bool.solitaireLicense);
		maleApp = ctx.getApplicationContext().getResources().getBoolean(R.bool.maleApp);
		femaleApp = ctx.getApplicationContext().getResources().getBoolean(R.bool.femaleApp);
	}

	public void clearCache() {
	    if (EpicFeedConsts.DEBUG_AD_HANDLER) 
			Log.i(TAG, "clearCache: clearing cache");
		if ((adTimer != null)|(mMMAdView != null)|(adRequestReady)) {
			synchronized (isBeingDestroyed) {
				if (!isBeingDestroyed) {
					//mMMAdView.halt();
					loadAd = true;
				}
			}
		}
		try {
			CacheManager.trimCache(spaContext.get());
		} catch (Exception e) {
			e.printStackTrace();
		}
		synchronized (isBeingDestroyed) {
			if (!isBeingDestroyed) mMMAdListener.needAd = true;
		}
	}
	
	@Override
	public void run() {
		if (solitaireLicense) {
			return;
		}
	    if (EpicFeedConsts.DEBUG_AD_HANDLER) 
			Log.i(TAG, "run(): starting AdHandler");
    	int locCount = 0, waitCount = 0;
		timer = new Timer(TAG + " Timer:" + activityName, true);
		adTimer = getListenerTimerTask();
		timer.scheduleAtFixedRate(adTimer, 0, waitTime);
		timerRunning = true;
		// wait for adView to initialize
		synchronized (this) {
			try {
				wait(200);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	    if (EpicFeedConsts.DEBUG_ADS) 
			Log.i(TAG, "run(): getting new ad request");
		needAd();
	    if (EpicFeedConsts.DEBUG_ADS) 
			Log.i(TAG, "thread - start(): AdHandler Size Check Thread : " + activityName);
		adCheckerThread = new Thread(null, adSizeChecker, "AdHandler Size Check Thread : " + activityName);
		adCheckerThread.start();
		while (checkAd && timer != null) {
			synchronized (doNotDestroy) {
				doNotDestroy = true;
				if (!pauseAds) {
	    			if (mMMAdListener.needAd()
	    					||(EpicFeedConsts.DEBUG_AD_HANDLER)) {
	    			    if (EpicFeedConsts.DEBUG_AD_HANDLER) 
	    					Log.i(TAG, "run(): need new Ad: " 
	    							+ "  threadAdListener.needAd():" + mMMAdListener.needAd());
	    			    if (EpicFeedConsts.DEBUG_AD_HANDLER) 
	    		    		mMMAdListener.needAd = true;
	    			    if (EpicFeedConsts.DEBUG_AD_HANDLER) 
	    		    		Log.i(TAG, "getAd thread run(): AdMob getting new ad...");
	        		    mReturnAdListener.onGetNewAd();
	    			} else {
	    			    if (EpicFeedConsts.DEBUG_AD_HANDLER) 
	    					Log.i(TAG, "run(): did not need Ad");
	    			}
	    			// check for location status
	    			if (!haveLocation) locCount++;
	    			// if location was not obtained, try again after 5 minutes
	    			if (locCount > 15) {
	    				locCount = 0;
	    				skipLocation = false;
	    				mMMAdListener.tryLocation = true;
	    				adRequestReady = false;
	    			}
	    			// skip if location cannot be obtained
	    			else if (locCount > 1) skipLocation = true;
				} else {
    			    if (EpicFeedConsts.DEBUG_AD_HANDLER) 
						Log.i(TAG, "run: ads are paused");
				}
				doNotDestroy = false;
			}
			try {
				waitCount = 0;
				synchronized (this) {
					while (waitCount < waitSplit) {
						wait(waitTime/waitSplit);
						if (doNotWait) {
							waitCount = waitSplit;
							doNotWait = false;
						}
						else waitCount++;
						if (mMMAdView == null || adLayout == null) return;
						mMMAdView.getActivity().runOnUiThread(adViewVisible);
					}
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	};
	
	private Runnable adViewVisible = new Runnable() {
		@Override
		public void run() {
			if (solitaireLicense) {
				return;
			}
			synchronized (isBeingDestroyed) {
				if (isBeingDestroyed) return;
				if (mMMAdView == null) return;
				mMMAdView.setVisibility(View.VISIBLE);
			    if (EpicFeedConsts.DEBUG_ADS) 
			    	Log.i("Ads", "SPA: new ad height:" + mMMAdView.getHeight());
				//if (mMMAdView.getHeight() > spaContext.get().getResources().getDimension(R.dimen.adBuffer)) {
					LayoutParams params = new LayoutParams(mMMAdView.getWidth(), 
							spaContext.get().getResources().getDimensionPixelSize(R.dimen.adBuffer));
					mMMAdView.setLayoutParams(params);
					adLayout.requestLayout();
				    if (EpicFeedConsts.DEBUG_ADS) 
				    	Log.i("Ads", "SPA: adjusted ad height:" + mMMAdView.getHeight());
				//}
			}
		}
	};
	
	// this will check for the layout size after the screen is rendered...
	// not currently using this; assume the initial call to adView.setVisibility(View.GONE) will suffice
   	private Runnable adSizeChecker = new Runnable() {

		@Override
		public void run() {
			if (solitaireLicense) {
				return;
			}
			while (!destroyAdCheck) {
				try {
					synchronized (this) {
						if (pauseAdCheck) wait(2000);
						wait(1000);
					}
				} catch (Exception e) {
					if (EpicFeedConsts.DEBUG_ADS) 
						Log.i(TAG, "adSizeChecker: wait error...");
				}
				if (!pauseAdCheck) {
			    	if (EpicFeedConsts.DEBUG_ADS && (adLayout != null) &&(mMMAdView != null) ) 
			    		Log.i(TAG, "adSizeChecker: ad layout height: " + adLayout.getHeight());
			    	if (EpicFeedConsts.DEBUG_ADS && (adLayout != null) &&(mMMAdView != null) ) 
			    		Log.i(TAG, "adSizeChecker: ad view height: " + mMMAdView.getHeight());
					if ((adLayout != null) && (adLayout.getHeight() > EpicFeedConsts.MAX_TALL_AD_HEIGHT) && (mMMAdView != null) ) 
						mMMAdView.getActivity().runOnUiThread(
							new Runnable() {
								@Override
								public void run() {
							    	if (EpicFeedConsts.DEBUG_ADS) 
							    		Log.i(TAG, "adSizeChecker: removing ad layout...");
									if (mMMAdView != null) mMMAdView.setVisibility(View.GONE);
								}
							});
				}
			}
		}
	};

	public void stopAds() {
		pauseAdCheck = true;
		pauseAds = true;
	}
	
	private Runnable destroyAd = new Runnable() {
		@Override
		public void run() {
			mMMAdView.destroy();
		}
	};
	
	public void destroyAds() {
		pauseAds = true;
		destroyAdCheck = true;
		synchronized (doNotDestroy) {
			while (doNotDestroy) {
				try {
					wait(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			mMMAdListener = null;
			if (mMMAdView != null) {
				mMMAdView.getActivity().runOnUiThread(destroyAd);
				mMMAdView = null;
			}
		}
	}
	
	public void pauseAds() {
		if (solitaireLicense) {
			return;
		}
		pauseAdCheck = true;
	    if (EpicFeedConsts.DEBUG_AD_HANDLER) 
			Log.i(TAG, "pauseAds: pausing...");
		synchronized (pauseAds) {
			pauseAds = true;
		}
		synchronized (doNotDestroy) {
			doNotDestroy = true;
			mMMAdListener.pauseAds();
			doNotDestroy = false;
		}
	}
	
	public void resumeAds() {
		if (solitaireLicense) {
			return;
		}
	    if (EpicFeedConsts.DEBUG_AD_HANDLER) 
			Log.i(TAG, "resumeAds: resuming...");
		synchronized (pauseAds) {
			pauseAds = false;
		}
		doNotWait = true;
		pauseAdCheck = false;
	}
	
	private Runnable loadNewAd = new Runnable() {

		@Override
		public void run() {
			if (solitaireLicense) {
				return;
			}
		    synchronized (doNotDestroy) {
		    	if (mMMAdView == null) return; 
		    	doNotDestroy = true;
				if (femaleApp) mMMAdView.setKeywords("m_gender:f");
				if (maleApp) mMMAdView.setKeywords("m_gender:m");
		        adRequestReady = true;
		        mMMAdView.loadAd();
		        doNotDestroy = false;
		    }
		}
		
	};
	
	public boolean needAd() {
		if (solitaireLicense) {
			return false;
		}
	    if (EpicFeedConsts.DEBUG_AD_HANDLER) 
    		Log.i(TAG, "needAd: starting...");
	    // MoPub automatically gets location data... removed MMAd Location code
	    if (EpicFeedConsts.DEBUG_AD_HANDLER) 
    		Log.i(TAG, "needAd: setting gender...");
    	if (mMMAdView == null) return false; 
    	mMMAdView.getActivity().runOnUiThread(loadNewAd);
        return true;
	}

	public TimerTask getListenerTimerTask() {
		return new TimerTask() { 
			@Override
			public void run() { 
				if (solitaireLicense) {
					return;
				}
				try {
				} catch (Exception e) {
					e.printStackTrace();
    			    if (EpicFeedConsts.DEBUG_AD_HANDLER) 
    			    	Log.i(TAG, "Call log processing error...");
				}
				if (mMMAdListener != null && mMMAdListener.needAd) {
					if (mMMAdView == null||mReturnAdListener == null) return;
					needAd();
				}
			} 
		};
	}
    
    public void setAdView (MoPubView adView) {
	    if (EpicFeedConsts.DEBUG_AD_HANDLER) 
    		Log.i(TAG, "setAdView: needed new AdView");
    	stopAdTimer();
    	mMMAdView = adView;
    	//mMMAdView.loadAd();
    	startAdTimer();
    }
    
    public MoPubView getAdView() {
    	return mMMAdView;
    }
	
    public void setAdListener (MoPubAdListener adListener) {
	    if (EpicFeedConsts.DEBUG_AD_HANDLER) 
    		Log.i(TAG, "setAdlistener: needed new AdListener");
    	stopAdTimer();
    	mMMAdListener = adListener;
    	startAdTimer();
    }
    
    public MoPubAdListener getAdListener() {
    	return mMMAdListener;
    }
	
    public void setReturnAdListener (ReturnAdListener returnAdListener) {
	    if (EpicFeedConsts.DEBUG_AD_HANDLER) 
    		Log.i(TAG, "setReturnAdListener: needed new ReturnAdListener");
    	stopAdTimer();
    	mReturnAdListener = returnAdListener;
    	startAdTimer();
    }
    
    public ReturnAdListener getReturnAdListener() {
    	return mReturnAdListener;
    }
	
    public void stopAdTimer() {
	    if (EpicFeedConsts.DEBUG_AD_HANDLER) 
    		Log.i(TAG, "stopTimer: stopping...");
    	if (timer != null) {
    		timer.cancel();
    		timerRunning = false;
    	}
    }
    
    public void startAdTimer() {
    	if (!timerRunning) {
    		timer = new Timer(TAG + " startTimer:" + activityName, true);
			adTimer = getListenerTimerTask();
			timer.scheduleAtFixedRate(adTimer, 0, waitTime);
		    if (EpicFeedConsts.DEBUG_AD_HANDLER) 
				Log.i(TAG, "startTimer: started");
		}
    }

	public interface ReturnAdListener {
		public abstract void onGetNewAd();
	}
}
