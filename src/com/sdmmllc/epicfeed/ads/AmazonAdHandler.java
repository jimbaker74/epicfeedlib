package com.sdmmllc.epicfeed.ads;

import java.lang.ref.WeakReference;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;

import com.amazon.device.ads.Ad;
import com.amazon.device.ads.AdError;
import com.amazon.device.ads.AdLayout;
import com.amazon.device.ads.AdProperties;
import com.amazon.device.ads.AdRegistration;

public class AmazonAdHandler implements Runnable {
	
	String TAG = "AmazonAdHandler", activityName;
    private final String appKey = "e7b94ee81b5f4f8c9e0ecec59037923a";
    private WeakReference<Activity> amazonAdAct;
    private AdLayout amazonAdLayout;
    private Context amazonContext;
    private AttributeSet amazonAttributeSet;
    
    private AmazonAdListener myListener = new AmazonAdListener() {
    	
    	@Override
    	public void onAdCollapsed(Ad ad) {
    		
    	}
    	
    	@Override
    	public void onAdDismissed(Ad ad) {
    		
    	}
    	
    	@Override
    	public void onAdExpanded(Ad ad) {
    		
    	}
    	
    	@Override
    	public void onAdFailedToLoad(Ad ad, AdError error) {
    		
    	}
    	
    	@Override
    	public void onAdLoaded(Ad ad, AdProperties adProperties) {
    		
    	}
    };
    
    public void init(Activity activity, String string, Context context, AttributeSet attrs) {
    	AdRegistration.setAppKey(appKey);
    	amazonAdAct = new WeakReference<Activity>(activity); 
    	activityName = string;
    	amazonContext = context;
    	amazonAttributeSet = attrs;
    	
    	amazonAdLayout = new AdLayout(amazonContext, amazonAttributeSet);
    	amazonAdLayout.setListener(myListener);
    	amazonAdLayout.setTimeout(45000);
    }
	   
	@Override
	public void run() {
		
	}
}
