package com.sdmmllc.epicfeed.ui;

import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.ClipboardManager;
import android.text.Html;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.QuickContactBadge;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockListActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.flurry.android.FlurryAgent;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;
import com.mopub.mobileads.MoPubErrorCode;
import com.mopub.mobileads.MoPubInterstitial;
import com.mopub.mobileads.MoPubInterstitial.InterstitialAdListener;
import com.mopub.mobileads.MoPubView;
import com.sdmm.epicfeed.R;
import com.sdmmllc.epicfeed.CacheManager;
import com.sdmmllc.epicfeed.EpicFeedController;
import com.sdmmllc.epicfeed.FlurryConsts;
import com.sdmmllc.epicfeed.SpaTextConfig;
import com.sdmmllc.epicfeed.SpaTextNotification;
import com.sdmmllc.epicfeed.SpaUserAccount;
import com.sdmmllc.epicfeed.SpaUserAccountAdapter;
import com.sdmmllc.epicfeed.SpaUserAccountListener;
import com.sdmmllc.epicfeed.ads.MoPubAdHandler;
import com.sdmmllc.epicfeed.ads.MoPubAdListener;
import com.sdmmllc.epicfeed.ads.MoPubInterstitialAdHandler;
import com.sdmmllc.epicfeed.data.Contact;
import com.sdmmllc.epicfeed.data.ContactFeed;
import com.sdmmllc.epicfeed.data.EpicContact;
import com.sdmmllc.epicfeed.data.EpicDB;
import com.sdmmllc.epicfeed.data.EpicDBConn;
import com.sdmmllc.epicfeed.utils.EpicFeedConsts;
import com.sdmmllc.epicfeed.utils.EpicGAConsts;
import com.sdmmllc.epicfeed.view.FeedMessageHandler;
import com.sdmmllc.epicfeed.view.FeedMessageHandlerListener;
import com.sdmmllc.epicfeed.view.FeedMessageListGroup;
import com.sdmmllc.epicfeed.view.FeedMessageListHelper;
import com.sdmmllc.epicfeed.view.FeedMessageTheme;
import com.sdmmllc.epicfeed.view.FeedSeparatedMessageListAdapter;
import com.sdmmllc.epicfeed.view.FeedTextMsg;

@SuppressWarnings("deprecation")
public class Act_FeedMessageList extends SherlockListActivity implements OnScrollListener, InterstitialAdListener {

	public static String TAG = "Act_FeedMessageList";
	
	private EpicDB epicDB;
	private Context spaContext;
	private WeakReference<Activity> spaActivity;
	private FeedTextMsg[] feedMsgs, textSmsMsgs, textMmsMsgs;
	private TextView actionbarTitle;
	private RelativeLayout adLayout;
	private MoPubView adView;
	private MoPubAdListener adListener;
	private MoPubAdHandler adHandler;
	private MoPubAdHandler.ReturnAdListener returnAdListener;
	private MoPubInterstitialAdHandler interstitialAdHandler;
	private MoPubInterstitialAdHandler.ReturnInterstitialAdListener returnInterstitialAdListener;
    private Thread adHandlerThread, interstitialAdHandlerThread, authMonitorThread;
    private Boolean sendInProgress = false, updateUIinProgress = false;
    private boolean 
		scrollListenerSet = false, initialLoadDone = false;
	public static int singleMsgLength = 140;
    private LayoutInflater mInflater;
    private SpaUserAccount mSpaUserAccount;
    private SpaUserAccountListener accountListener;
    private FeedMessageTheme themeOptions;
    private FeedSeparatedMessageListAdapter txtSepMsgAA;
    private FeedMessageListGroup txtMsgListGroup;
    private long pageStartTime = 0, pageTotalTime = 0;
    private int pageViews = 0, msgsSent = 0;
    private static java.text.DateFormat df;
    private EpicDB.CheckMessageListener checkMessageListener;
    private ProgressDialog waitDialog, exportDialog;
	private final Handler UiHandler = new Handler();
	private Bundle savedState;
	private EpicContact contact;
	private EpicContact me;
	//private boolean lsOrientation = false;
	private SharedPreferences settings;
	private SharedPreferences.Editor editor;
    private boolean scrollSelectionSet = false;
    private long scrollSelectionId = 0;
    private Boolean runAuthMonitor = false, userInteraction = false, showInterstitial = false, delayInterstitial = false;
	private MoPubInterstitial mInterstitial;
    private LinearLayout progressBarLayout;
    private TextView progressBarLbl;
    private ProgressBar progressSpinner;
    private int scrollLocation = 0;
    private ListView messageList;
    private EasyTracker easyTracker;
    private FeedMessageHandler mFeedMessageHandler = new FeedMessageHandler();
    //private int SOCIAL_TYPE;
    public static String 
					FEED_TYPE_FACEBOOK 	= "facebook",
					FEED_TYPE_LINKEDIN 	= "linkedIn",
					FEED_TYPE_TWITTER 	= "twitter",
					FEED_TYPE_PINTEREST	= "pinterest";
	
	public FeedMessageHandlerListener fmhListener = new FeedMessageHandlerListener() {
		
		@Override
		public void onUpdate() {
			synchronized (sendInProgress) {
				synchronized (updateUIinProgress) {
					if (sendInProgress||updateUIinProgress) {
				        Thread thread = new Thread(null, UiWait(), "UiWait");
				        thread.start();
					}
					else UiHandler.post(UiUpdate());
				}
			}
		}
		
		@Override
		public void onComplete() {
			if (mFeedMessageHandler.isFeedMessageHandlerReady()) {
		        Thread thread = new Thread(null, UiWait(), "UiWait");
		        thread.start();
			}
		}
	};
    
    private Runnable authMonitor = new Runnable() {
    	private void cleanUp() {
    		try {
	            synchronized (userInteraction) {
	                if (userInteraction) {
	                	userInteraction = false;
	                	mSpaUserAccount.updateAuthActivity();
	                }
	            }
    		} catch (Exception e) {
    			// do nothing - the referencing process must have been killed
    		}
    	}
    	
        @Override
        public void run() {
            while(runAuthMonitor) {
                try {
                	Thread.sleep(5000);
                } catch (InterruptedException e) {
                	cleanUp();
                    Thread.currentThread().interrupt();
                } finally {
                	cleanUp();
                }
                try {
		            synchronized (userInteraction) {
	                    if (userInteraction) {
	                    	userInteraction = false;
	                    	mSpaUserAccount.updateAuthActivity();
	                    }
	                }
                } catch (Exception e) {
                	// do nothing - try again later
                }
            }
        }
    };
    
    private void updateAuthActivity() {
    	synchronized (userInteraction) {
    		userInteraction = true;
    	}
    }

  @Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater menuInfl = this.getSupportMenuInflater();
		menuInfl.inflate(R.menu.thread_menu, menu);
		return true;
	}
	
    public void launchMenu(View view){
        this.getWindow().openPanel(Window.FEATURE_OPTIONS_PANEL, new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_MENU));
        this.getWindow().openPanel(Window.FEATURE_OPTIONS_PANEL, new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_MENU));
    }
    
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
        updateAuthActivity();
		int itemId = item.getItemId();
		if (itemId == R.id.threadHelp) {
			helpDialog();
			return true;
		} else if (itemId == R.id.editTheme) {
			themeSelect(false);
			return true;
		} else if (itemId == R.id.editClassicTheme) {
			themeSelect(true);
			return true;
		} else if (itemId == R.id.unlinkFromNetwork) {
			unlinkFromNetwork();
			return true;
		} else if (itemId == R.id.editMsgOrder) {
			threadSettings();
			return true;
		} else if (itemId == R.id.callContact) {
			callContact(null);
			return true;
		} else if (itemId == android.R.id.home) {
			finish();
			return true;
		}
		return false;
	}
	
	public void selectInputMethod() {
        updateAuthActivity();
		InputMethodManager imm = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
        	imm.showInputMethodPicker();
        } else {
            Toast.makeText(this, R.string.sysInputMethodNull, Toast.LENGTH_LONG).show();
        }
	}

	public void helpDialog () {
		closeOptionsMenu();
		easyTracker.send(MapBuilder
                .createEvent(EpicFeedConsts.gat_Button,     	// Event category (required)
                		EpicFeedConsts.gat_Click,			  	// Event action (required)
                		EpicFeedConsts.gatTextListHelp,	   			// Event label
                        1L)            						// Event value
                .build()
            );
		final View helpTextView = mInflater.inflate(R.layout.helpview, null);
		final TextView helpText = (TextView)helpTextView.findViewById(R.id.helptext);
		helpText.setText(Html.fromHtml(
				getText(R.string.threadHelp).toString()));
		AlertDialog ad = new AlertDialog.Builder(spaContext)
        .setTitle(R.string.threadHelpHeader)
        .setView(helpTextView)
        .setNeutralButton(getString(R.string.sysDone), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                updateAuthActivity();
            	dialog.cancel();
            }
        })
        .create();
		ad.show();
	}
	
	public void launchSettings(View v) {
		threadSettings();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	    super.onActivityResult(requestCode, resultCode, data);
	    if (EpicFeedConsts.DEBUG_REDNOTE) Log.i(TAG, "onActivityResult: (SpaTextConsts.REDNOTE_MESSAGE == requestCode) =" + (EpicFeedConsts.REDNOTE_MESSAGE == requestCode));
	    if (EpicFeedConsts.DEBUG_REDNOTE) Log.i(TAG, "onActivityResult: (resultCode == RESULT_OK) =" + (resultCode == RESULT_OK));
	}
	
	public void unlinkFromNetwork() {
		easyTracker.send(MapBuilder
                .createEvent(EpicFeedConsts.gat_Button,     	// Event category (required)
                		EpicFeedConsts.gat_Click,			  	// Event action (required)
                		EpicFeedConsts.gatUnlinkNetwork,	   			// Event label
                        1L)            						// Event value
                .build()
            );
		
		EpicDBConn conn = epicDB.open(false);
		final EpicContact oldContact = epicDB.getContact(contact.phoneID);
		oldContact.notifyOptions = epicDB.getNotification(contact.phoneID);
		oldContact.setContactFeeds(epicDB.getContactFeeds(contact.phoneID));
		epicDB.close(conn);

    	AlertDialog ad;
    	final View helpTextView = mInflater.inflate(R.layout.linkview, null);
    	final TextView titleTxt = (TextView)helpTextView.findViewById(R.id.linksviewTitle);
		final TextView nameTxt1 = (TextView)helpTextView.findViewById(R.id.linksviewLink1Name);
		final TextView nameTxt2 = (TextView)helpTextView.findViewById(R.id.linksviewLink2Name);
		ImageView linkImg = (ImageView)helpTextView.findViewById(R.id.linksviewLinksImg);
		linkImg.setImageDrawable(getResources().getDrawable(R.drawable.img_unlink));
		if (EpicFeedConsts.DEBUG_FB) 
			Log.i(TAG, "itemClick: Connect Contact: " + contact.phoneID +
				" to FB user: " + oldContact.getNameOrPhoneId());
		titleTxt.setText(getString(R.string.linksviewUnlinkTitleTxt));
		nameTxt1.setText(contact.getNameOrPhoneId());
		nameTxt2.setText(oldContact.getNameOrPhoneId());
		ad = new AlertDialog.Builder(spaContext)
        .setTitle(getString(R.string.actFeedMessageUnlinkTitleTxt)+
				" " + contact.getNameOrPhoneId() + "?")
        .setView(helpTextView)
		.setPositiveButton(spaContext.getString(R.string.actFeedMessageUnlinkActionTxt), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int id) {
                updateAuthActivity();
            	dialog.cancel();
            	
            	mFeedMessageHandler.clearMsgs();
            	updateUI();
			}
		})
		.setNegativeButton(spaContext.getString(R.string.sysNo), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int id) {
    	        mSpaUserAccount.updateAuthActivity();
				dialog.cancel();
			}
		})
        .create();
		ad.show();
	}

	public void callContact(View v) {
		easyTracker.send(MapBuilder
                .createEvent(EpicFeedConsts.gat_Button,     	// Event category (required)
                		EpicFeedConsts.gat_Click,			  	// Event action (required)
                		EpicFeedConsts.gatTextListCall,	   			// Event label
                        1L)            						// Event value
                .build()
            );
        updateAuthActivity();
    	Intent callContactIntent = new Intent(Intent.ACTION_CALL);
    	callContactIntent.setData(Uri.parse("tel:"+contact.phoneID));
    	callContactIntent.putExtra("pID", contact.phoneID);
    	startActivityForResult(callContactIntent, EpicFeedConsts.CONTACTCALL);
	}

	public void configurationOptions() {
	   	Intent configIntent = new Intent(this, SpaTextConfig.class);
		startActivityForResult(configIntent, EpicFeedConsts.CONFIG);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		CacheManager.trimCache(this);
		Contact.init(this.getApplicationContext());
		mSpaUserAccount = EpicFeedController.getSpaUserAccount(this.getApplicationContext());
        settings = getSharedPreferences(EpicFeedConsts.PREFS_NAME, 0);
		if (!checkAuth(getIntent())) finish();
		easyTracker = EasyTracker.getInstance(this);
		FlurryAgent.onStartSession(this, getString(R.string.flurryAppKey));
		FlurryAgent.onPageView();
    	EpicFeedController.getGaTracker(this.getApplicationContext()).set(Fields.SCREEN_NAME, TAG);
    	
    	//SOCIAL_TYPE = getIntent().getIntExtra(EpicFeedConsts.SOCIAL_NETWORK, 0);
    	
    	mSpaUserAccount.authenticated(new WeakReference<Activity>(this));
    	
		initVariables(savedInstanceState);
		
		getContact();
		
		setupTheme();
		mFeedMessageHandler.
	      init(
			spaContext, 
			contact, 
			me,
			themeOptions, 
			savedState,
			spaActivity);
		
		setupLayout();
		setupAdHandler();
		setupListAdapter();
        startAds();
		if (EpicFeedConsts.DEBUG_MSG_LIST_ACT) 
			Log.i(TAG, "onCreate: setFastScrollEnabled:" + getListView().isFastScrollEnabled());
        setResult(Activity.RESULT_OK);
        accountListener = new SpaUserAccountAdapter(new WeakReference<Activity>(this)) {
        	
    		@Override
    		public void timeout() {
    			mSpaUserAccount.authenticated(spaActivity);
    			stopAds();
    			destroyAds();
    			finish();
    		}
        	
        };
	}

	@Override
	public void onRestart() {
		super.onRestart();
		if (!checkAuth(getIntent())) finish();
		if (!mSpaUserAccount.authenticated(new WeakReference<Activity>(this))) finish();;
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		runAuthMonitor = true;
        if ((authMonitorThread == null)||(authMonitorThread.getState() == Thread.State.TERMINATED))
        	authMonitorThread = new Thread(null, authMonitor, "authMonitor Thread : " + TAG);
        if (authMonitorThread.getState() == Thread.State.NEW) {
        	authMonitorThread.start();
        }
		EasyTracker.getInstance(this).activityStart(this);  
		FlurryAgent.onStartSession(this, getString(R.string.flurryAppKey));
		FlurryAgent.onPageView();
    	FlurryAgent.logEvent(FlurryConsts.PAGE_VIEW_EVENT, FlurryConsts.getMap(FlurryConsts.PAGE_VIEW_EVENT, TAG));
    	
    	mFeedMessageHandler.setFeedMessageHandler(fmhListener);
        mFeedMessageHandler.startOn();
	}

	@Override
    public void onResume() {
		if (!mSpaUserAccount.authenticated(new WeakReference<Activity>(this))) {
			finish();
		}
		super.onResume();
        startAds();
        if (EpicFeedConsts.DEBUG_MSG_LIST_ACT) Log.i(TAG, "onResume: checking timeout...");
        mSpaUserAccount.setSpaUserAccountListener(accountListener);
        listenForMessages(true);
		if (EpicFeedConsts.DEBUG_TEXT_MSG_LIST) 
			Log.i(TAG, "onResume: listenForMessages called");
		clearNotification();
		if (initialLoadDone) updateUI();
		resumeAds();
		showInterstitial = true;
		pageStartTime = Calendar.getInstance().getTimeInMillis();
		pageViews++;
		messageList.post(new Runnable() {
	        @Override
	        public void run() {
	            // Select the last row so it will scroll into view...
	        	messageList.setSelection(scrollLocation);
	        }
	    });
    	easyTracker.send(MapBuilder
                .createEvent(EpicGAConsts.USER_ACTION_CATEGORY,     			// Event category (required)
                			EpicGAConsts.USER_MESSAGING,			  		// Event action (required)
                			EpicGAConsts.USER_VIEW_MESSAGE_LIST,	   			// Event label
                            1L)            						// Event value
                .build()
            );
	}

	public void initVariables(Bundle savedInstanceState) {
        savedState = savedInstanceState;
		spaContext = this;
		spaActivity = new WeakReference<Activity> (this);
        // get the database
		epicDB = EpicFeedController.getEpicDB(spaContext.getApplicationContext());
        mInflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		initialLoadDone = false;
	}

	public void getContact() {
		
		try {
			EpicDBConn tmpConn = epicDB.open(true);
			String phoneId = getIntent().getStringExtra("pID");
			Log.i(TAG, "--------- getContact() - viewing pID: " + phoneId);
			// This is potential problem of the twitter msgs being in the if blocks of facebook data
			// in FeedMessageListAdapter 
			// TODO
			me = new EpicContact("me");
			getMeTwitter(me);
			getMeFacebook(me);
			
			if ("me".equals(phoneId)) {
				contact = new EpicContact("me");
				if (contact.getFeed(FEED_TYPE_FACEBOOK) != null && contact.getFeed(FEED_TYPE_TWITTER) != null) {
					getMeTwitter(contact);
					getMeFacebook(contact);
				} else if(contact.getFeed(FEED_TYPE_FACEBOOK) != null) {
					contact = new EpicContact("me");
					getMeFacebook(contact);
					if (EpicFeedConsts.DEBUG_FB) Log.d(TAG, "************** Loaded contact: " + contact.phoneID);
					if (EpicFeedConsts.DEBUG_FB) Log.d(TAG, "************** contact feed size: " + contact.getFeeds().size());
					if (EpicFeedConsts.debugLevel > 5) Log.i(TAG, "onCreate: pID - " + contact.phoneID);
				} else if(contact.getFeed(FEED_TYPE_TWITTER) != null) {
					contact = new EpicContact("me");
					getMeTwitter(contact);
					if (EpicFeedConsts.DEBUG_TWT) Log.d(TAG, "************** Loaded contact: " + contact.phoneID);
					if (EpicFeedConsts.DEBUG_TWT) Log.d(TAG, "************** contact feed size: " + contact.getFeeds().size());
				} 
			} else {
				contact = epicDB.getContact(phoneId);
				contact.notifyOptions = epicDB.getNotification(contact.phoneID);
			 	contact.setContactFeeds(epicDB.getContactFeeds(contact.phoneID));
				Log.i(TAG, "--------- getContact() - pID: " + phoneId + " feedList size: " + contact.getFeeds().size());
				epicDB.close(tmpConn);
			}
			
		} catch (Exception e) {
			Log.i(TAG, "*  *  *  *  * getContact() Exception >> " + e.getMessage());
			e.printStackTrace();
		}
	}
	
	private void getMeTwitter(EpicContact contact) {
		ContactFeed newFeed = new ContactFeed("me");
		newFeed.setFeedType(ContactFeed.FEED_TYPE_TWITTER);
		contact.addFeed(newFeed);
	}
	
	private void getMeFacebook(EpicContact contact) {
		ContactFeed newFeed = new ContactFeed("me");
		newFeed.setFeedType(ContactFeed.FEED_TYPE_FACEBOOK);
		contact.addFeed(newFeed);
	}
	
    private QuickContactBadge mAvatarView;
    private QuickContactBadge mMeAvatarView;
    static private Drawable sDefaultContactImage, sDefaultMeContactImage;

    private void updateAvatarView() {
    	mAvatarView = new QuickContactBadge(spaContext);
        if (sDefaultContactImage == null) {
        	sDefaultContactImage = getResources().getDrawable(R.drawable.sample_avatar);
        }
        Drawable avatarDrawable;
        Contact contact = Contact.get(this.contact.phoneID, true);
        avatarDrawable = contact.getAvatar(spaContext, sDefaultContactImage);

        if (contact.existsInDatabase()) {
            mAvatarView.assignContactUri(contact.getUri());
        } else {
            mAvatarView.assignContactFromPhone(contact.getNumber(), true);
        }
        mAvatarView.setImageDrawable(avatarDrawable);
        this.contact.mAvatar = avatarDrawable;
        
    	mMeAvatarView = new QuickContactBadge(spaContext);
        if (sDefaultMeContactImage == null) {
        	sDefaultMeContactImage = getResources().getDrawable(R.drawable.sample_avatar_sent);
        }
        Drawable avatarMeDrawable;
        Contact meContact = Contact.getMe(false);
        avatarMeDrawable = meContact.getAvatar(spaContext, sDefaultMeContactImage);

        if (meContact.existsInDatabase()) {
            mMeAvatarView.assignContactUri(meContact.getUri());
        } else {
            mMeAvatarView.assignContactFromPhone(meContact.getNumber(), true);
        }
        mMeAvatarView.setImageDrawable(avatarMeDrawable);
        
        if (me != null) this.me.mAvatar = avatarMeDrawable;
       
    }

	public void setupLayout() {
		if (Configuration.ORIENTATION_LANDSCAPE == 
			this.getResources().getConfiguration().orientation) {
			setContentView(R.layout.smsthread_asc_ls);
			EpicFeedActionBar.actionbar_smsthread_asc_ls(new WeakReference<SherlockListActivity>(this));
			adLayout = (RelativeLayout)findViewById(R.id.smsLsAscAdLayout);
			adView = (MoPubView)findViewById(R.id.mmadviewSmsLsAsc);
	        actionbarTitle = (TextView)findViewById(R.id.hmtActionTitle);
		} else {
			setContentView(R.layout.act_feed_message_list);
			EpicFeedActionBar.actionbar_smsthread(new WeakReference<SherlockListActivity>(this));
			adLayout = (RelativeLayout)findViewById(R.id.smsAdLayout);
			adView = (MoPubView)findViewById(R.id.mmadviewSms);
	        actionbarTitle = (TextView)findViewById(R.id.hmtActionTitle);
	        progressBarLayout = (LinearLayout)findViewById(R.id.feedMessageListLoadProgressLayout);
	        progressBarLbl = (TextView)findViewById(R.id.feedMessageListLoadProgressTxt);
	        progressSpinner = (ProgressBar)findViewById(R.id.feedMessageListLoadProgressSpinner);
		}
		mInterstitial = new MoPubInterstitial(this, getString(R.string.mopubInterAdID));
	    mInterstitial.setInterstitialAdListener(this);
		adView.setAdUnitId(getString(R.string.mopubAdID));
		adView.setVisibility(View.GONE);
	}

	public void setupListAdapter() {
        setListAdapter(txtSepMsgAA);
        messageList = getListView();
        messageList.setTextFilterEnabled(false);
        /*messageList.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
				int position, long id) {
				itemShortTap(id, parent);
				}
			}
		);
        messageList.setOnItemLongClickListener(new OnItemLongClickListener() {
			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view,
				int position, long id) {
					easyTracker.send(MapBuilder
		                .createEvent(EpicFeedConsts.gat_MenuItem,     	// Event category (required)
		                		EpicFeedConsts.gat_Click,			  	// Event action (required)
		                		EpicFeedConsts.gatTextListLongHold,	   			// Event label
		                        1L)            						// Event value
		                .build()
		            );
					itemLongHold(id, parent);
					return true;
				}
			}
		);*/
		scrollLocation = messageList.getFirstVisiblePosition();
	}
	
	private Runnable notifyInterstitial = new Runnable() {

		@Override
		public void run() {
			if (showInterstitial) Toast.makeText(spaContext, "While you are waiting, we will display an ad that you can easily exit..." , Toast.LENGTH_LONG).show();
		}
		
	};
	
	public void setupAdHandler() {
		if (getResources().getBoolean(R.bool.solitaireLicense)) {
			return;
		}
        final WeakReference<Activity> adAct = new WeakReference<Activity>(this);
		if (adListener == null) adListener = new MoPubAdListener(TAG, adAct, adLayout);
		adView.setBannerAdListener(adListener);
		if (returnAdListener == null) {
			returnAdListener = new MoPubAdHandler.ReturnAdListener() {
				@Override
				public void onGetNewAd() {
					runOnUiThread(returnAd);
				}
			};
		}

		if (adHandler == null) {
			adHandler = new MoPubAdHandler(this, TAG + " null", adView, adLayout, adListener, returnAdListener);
		} else {
			if (!adHandler.getAdView().equals(adView)) {
				adHandler.setAdView(adView);
			}
			if (!adHandler.getAdListener().equals(adListener)) {
				adHandler.setAdListener(adListener);
			}
			if (!adHandler.getReturnAdListener().equals(returnAdListener)) {
				adHandler.setReturnAdListener(returnAdListener);
			}
		}

		if (returnInterstitialAdListener == null) 
			returnInterstitialAdListener = new MoPubInterstitialAdHandler.ReturnInterstitialAdListener() {

				@Override
				public long checkNotifyAd() {
					if ((mInterstitial == null) || !mInterstitial.isReady()) return 0;
	                if (userInteraction) {
	                	userInteraction = false;
	                	mSpaUserAccount.updateAuthActivity();
	                }
					return (System.currentTimeMillis() - mSpaUserAccount.getActivityTS());
				}

				@Override
				public void notifyNewAd() {
					runOnUiThread(notifyInterstitial);
				}

				@Override
				public long checkDisplayAd() {
	                if (userInteraction) {
	                	userInteraction = false;
	                	mSpaUserAccount.updateAuthActivity();
	                }
					return (System.currentTimeMillis() - mSpaUserAccount.getActivityTS());
				}

				@Override
				public void displayAd() {
					if (!isFinishing() && showInterstitial) {
						easyTracker.send(MapBuilder
				                .createEvent(EpicFeedConsts.gat_Info,     	// Event category (required)
				                		EpicFeedConsts.gat_Ads,			  	// Event action (required)
				                		EpicFeedConsts.gatInterstitialAdDisplayed,	   			// Event label
				                        1L)            						// Event value
				                .build()
				            );
						runOnUiThread(displayAd);
					}
				}
			
		};
		if (interstitialAdHandler == null)
			interstitialAdHandler = new MoPubInterstitialAdHandler(new WeakReference<Act_FeedMessageList>(this), returnInterstitialAdListener, mInterstitial);
	}
	
	private Runnable displayAd = new Runnable() {

		@Override
		public void run() {
			mInterstitial.show();					
		}
		
	};
	
	public void startAds() {
		if (getResources().getBoolean(R.bool.solitaireLicense)) {
			if (adView != null) adView.setVisibility(View.GONE);
			return;
		}
		adHandler.resumeAds();
		adHandler.checkAd = true;
        if ((adHandlerThread == null)||(adHandlerThread.getState() == Thread.State.TERMINATED))
            	adHandlerThread = new Thread(null, adHandler, "Ad Handler Thread : " + TAG);
        if (adHandlerThread.getState() == Thread.State.NEW) {
        	adHandlerThread.start();
        }
	    if (adHandlerThread.getState() == Thread.State.NEW) {
	    	adHandlerThread.start();
	    }
	    if ((interstitialAdHandlerThread == null)||(interstitialAdHandlerThread.getState() == Thread.State.TERMINATED)) {
	    	interstitialAdHandlerThread = new Thread(null, interstitialAdHandler, "Interstitial Ad Handler Thread : " + TAG);
	    }
		if (interstitialAdHandlerThread.getState() == Thread.State.NEW) {
			interstitialAdHandlerThread.start();
		}
	}

	public void setupTheme() {
		themeOptions = new FeedMessageTheme(spaContext);
		if (!settings.contains(EpicFeedConsts.THEME_SELECT)) {
			if (getResources().getBoolean(R.bool.pinkThread)) themeOptions.setTheme(EpicFeedConsts.PINK_THEME);
			else if (getResources().getBoolean(R.bool.greenThread)) themeOptions.setTheme(EpicFeedConsts.GREEN_THEME);
			else themeOptions.setTheme(settings.getString(EpicFeedConsts.THEME_SELECT,
					EpicFeedConsts.DEFAULT_THEME));
		} else themeOptions.setTheme(settings.getString(EpicFeedConsts.THEME_SELECT,
				EpicFeedConsts.DEFAULT_THEME));
		themeOptions.setTextSize(settings.getString(EpicFeedConsts.TXT_SIZE,
				EpicFeedConsts.TEXT_MED));
		themeOptions.threadAscending = settings.getString(EpicFeedConsts.MSG_ORDER,
				EpicFeedConsts.MSG_ASC_ORDER).equals(EpicFeedConsts.MSG_DESC_ORDER);
		themeOptions.useTheme = true;
	}

	public void resumeAds() {
		if (EpicFeedConsts.DEBUG_MSG_LIST_ACT) Log.i(TAG, "resumeAds: setting state flags");
		synchronized(updateUIinProgress){
			updateUIinProgress = false;
		}
		synchronized (sendInProgress) {
			sendInProgress = false;
		}
		if (adHandler != null) {
	        adHandler.resumeAds();
			adHandler.checkAd = true;
		}
	}
	
	public void listenForMessages(boolean start) {
		if (start) {
			if (checkMessageListener == null) checkMessageListener = getCheckMessageListener();
			epicDB.setCheckMessageListener(checkMessageListener);
			EpicDBConn tmpConn = epicDB.open(false);
			if (epicDB.execAllUnreadMsgCount() > 0) {
				clearNotification();
		        updateUI();
			}
			epicDB.close(tmpConn);
		} else {
			if (EpicFeedConsts.DEBUG_TEXT_MSG_LIST) 
				Log.i(TAG, "listenForMessages: removing message listener");
			epicDB.removeCheckMessageListener();
			checkMessageListener = null;
		}
	}
	
	private Runnable returnAd = new Runnable() {
		@Override
		public void run () {
			if (adHandler != null) {
				synchronized (adHandler.isBeingDestroyed) {
					if (adHandler.isBeingDestroyed) adListener.needAd = true;
				}
		    	if (EpicFeedConsts.DEBUG_MSG_LIST_ACT) 
		    		Log.i(TAG, "returnAd thread: AdMob got new ad...");
	    		Log.i("Ads", "SPA: requesting new ad " + TAG);
			} else {
	    		Log.i(TAG, "returnAd thread: adHandler dead");
			}
		}
	};

	private void threadSettings() {
		final String textSize = settings.getString(EpicFeedConsts.TXT_SIZE, 
				EpicFeedConsts.TEXT_MED);
		final boolean ascOrder = settings.getString(EpicFeedConsts.MSG_ORDER, 
				EpicFeedConsts.MSG_ASC_ORDER).equals(EpicFeedConsts.MSG_ASC_ORDER);
		final View settingsTextView = mInflater.inflate(R.layout.smsthread_settings, null);
		final RadioButton msgAscBtn = (RadioButton)settingsTextView.findViewById(R.id.threadSettingsMsgOrderAscRadio);
		final RadioButton msgDescBtn = (RadioButton)settingsTextView.findViewById(R.id.threadSettingsMsgOrderDescRadio);
		final RadioButton txtSizeSmlBtn = (RadioButton)settingsTextView.findViewById(R.id.threadSettingsTextSizeSmlRadio);
		final RadioButton txtSizeMedBtn = (RadioButton)settingsTextView.findViewById(R.id.threadSettingsTextSizeMedRadio);
		final RadioButton txtSizeLrgBtn = (RadioButton)settingsTextView.findViewById(R.id.threadSettingsTextSizeLrgRadio);
		txtSizeSmlBtn.setTextSize(EpicFeedConsts.txt_sml);
		txtSizeMedBtn.setTextSize(EpicFeedConsts.txt_med);
		txtSizeLrgBtn.setTextSize(EpicFeedConsts.txt_lrg);
		if (ascOrder) {
			msgAscBtn.setChecked(true);
		} else {
			msgDescBtn.setChecked(true);
		}
		if (textSize.equals(EpicFeedConsts.TEXT_MED)) txtSizeMedBtn.setChecked(true);
		else if (textSize.equals(EpicFeedConsts.TEXT_SM)) txtSizeSmlBtn.setChecked(true);
		else if (textSize.equals(EpicFeedConsts.TEXT_LRG)) txtSizeLrgBtn.setChecked(true);
		
		AlertDialog ad = new AlertDialog.Builder(spaContext)
        .setTitle(R.string.threadSettingsTitleTxt)
        .setView(settingsTextView)
        .setNeutralButton(getString(R.string.sysOK), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
    	        updateAuthActivity();
        		if (msgDescBtn.isChecked()) {
					easyTracker.send(MapBuilder
			                .createEvent(EpicFeedConsts.gat_MenuItem,     	// Event category (required)
			                		EpicFeedConsts.gat_Click,			  	// Event action (required)
			                		EpicFeedConsts.gatMsgOrderDesc,	   			// Event label
			                        1L)            						// Event value
			                .build()
			            );
	        		if (editor == null) editor = settings.edit();
	        		editor.putString(EpicFeedConsts.MSG_ORDER, EpicFeedConsts.MSG_DESC_ORDER);
	        		editor.commit();
	        		themeOptions.threadAscending = false;
        		} else {
					easyTracker.send(MapBuilder
			                .createEvent(EpicFeedConsts.gat_MenuItem,     	// Event category (required)
			                		EpicFeedConsts.gat_Click,			  	// Event action (required)
			                		EpicFeedConsts.gatMsgOrderAsc,	   			// Event label
			                        1L)            						// Event value
			                .build()
			            );
	        		if (editor == null) editor = settings.edit();
	        		editor.putString(EpicFeedConsts.MSG_ORDER, EpicFeedConsts.MSG_ASC_ORDER);
	        		editor.commit();
	        		themeOptions.threadAscending = true;
        		}
        		if (txtSizeSmlBtn.isChecked()) {
					easyTracker.send(MapBuilder
			                .createEvent(EpicFeedConsts.gat_MenuItem,     	// Event category (required)
			                		EpicFeedConsts.gat_Click,			  	// Event action (required)
			                		EpicFeedConsts.gatTxtSizeSml,	   			// Event label
			                        1L)            						// Event value
			                .build()
			            );
	        		if (editor == null) editor = settings.edit();
	        		editor.putString(EpicFeedConsts.TXT_SIZE, EpicFeedConsts.TEXT_SM);
	        		editor.commit();
					if (EpicFeedConsts.DEBUG_MSG_LIST_ACT) 
						Log.i(TAG, "threadSettings: Text Size " + EpicFeedConsts.TEXT_SM);
	        		themeOptions.setTextSize(EpicFeedConsts.txt_sml);
        		}
        		else if (txtSizeMedBtn.isChecked()) {
					easyTracker.send(MapBuilder
			                .createEvent(EpicFeedConsts.gat_MenuItem,     	// Event category (required)
			                		EpicFeedConsts.gat_Click,			  	// Event action (required)
			                		EpicFeedConsts.gatTxtSizeMed,	   			// Event label
			                        1L)            						// Event value
			                .build()
			            );
	        		if (editor == null) editor = settings.edit();
	        		editor.putString(EpicFeedConsts.TXT_SIZE, EpicFeedConsts.TEXT_MED);
	        		editor.commit();
					if (EpicFeedConsts.DEBUG_MSG_LIST_ACT) 
	        			Log.i(TAG, "threadSettings: Text Size " + EpicFeedConsts.TEXT_MED);
	        		themeOptions.setTextSize(EpicFeedConsts.txt_med);
        		}
        		else if (txtSizeLrgBtn.isChecked()) {
					easyTracker.send(MapBuilder
			                .createEvent(EpicFeedConsts.gat_MenuItem,     	// Event category (required)
			                		EpicFeedConsts.gat_Click,			  	// Event action (required)
			                		EpicFeedConsts.gatTxtSizeLrg,	   			// Event label
			                        1L)            						// Event value
			                .build()
			            );
	        		if (editor == null) editor = settings.edit();
	        		editor.putString(EpicFeedConsts.TXT_SIZE, EpicFeedConsts.TEXT_LRG);
	        		editor.commit();
					if (EpicFeedConsts.DEBUG_MSG_LIST_ACT) 
	        			Log.i(TAG, "threadSettings: Text Size " + EpicFeedConsts.TEXT_LRG);
	        		themeOptions.setTextSize(EpicFeedConsts.txt_lrg);
        		}
        		resetConfig();
        	}
        })
        .create();
		ad.show();
	}
    
	public void itemShortTap(final long messageId, final AdapterView<?> parent) {
		if (editor == null) editor = settings.edit();
		final FeedTextMsg tmpMsg = (FeedTextMsg)txtSepMsgAA.getItem((int)messageId);
    	if (tmpMsg.isInfoMsg()) return;
		final String messageTxt = tmpMsg.msgTxt;
		final String lockMsgDisplay;
		if (tmpMsg.isLockedMsg()) {
			lockMsgDisplay = getString(R.string.smsShortTapUnlock);
		} else {
			lockMsgDisplay = getString(R.string.smsShortTapLock);
		}

		ArrayList<String> shortTapOptions1 = new ArrayList<String>();
		shortTapOptions1.add(getString(R.string.smsShortTapCopyMsg));
		shortTapOptions1.add(lockMsgDisplay);
		shortTapOptions1.add(getString(R.string.smsShortTapDeleteMsg));
		shortTapOptions1.add(getString(R.string.smsShortTapCancel));
		shortTapOptions1.add(getString(R.string.smsLongHoldHelp));
		if (tmpMsg.hasSendErr()) {
			shortTapOptions1.add(getString(R.string.smsLongHoldResendMsg));
		}

		final String[] shortTapOptions = shortTapOptions1.toArray(new String[0]);

		AlertDialog ad = new AlertDialog.Builder(spaContext)
        .setTitle(R.string.smsShortTapOptionsTitle)
        .setItems(shortTapOptions, new DialogInterface.OnClickListener() {
            @SuppressWarnings("deprecation")
			public void onClick(DialogInterface dialog, int modeId) {
    	        updateAuthActivity();
            	AlertDialog ad;
            	EpicDBConn tmpConn;
            	final View helpTextView = mInflater.inflate(R.layout.helpview_chk,
                		parent, false);
    			final TextView infoText = (TextView)helpTextView.findViewById(R.id.helpText);
    			final CheckBox infoTextChk = (CheckBox)helpTextView.findViewById(R.id.helpCheck);
    			final float scale = spaContext.getResources().getDisplayMetrics().density;
    			infoTextChk.setPadding(infoTextChk.getPaddingLeft() + (int)(10.0f * scale + 0.5f), 
    					infoTextChk.getPaddingTop(), infoTextChk.getPaddingRight(), infoTextChk.getPaddingBottom());
            	switch (modeId) {
            	case 0 :
            		ClipboardManager cbManager = (ClipboardManager)getSystemService(CLIPBOARD_SERVICE);
            		cbManager.setText(messageTxt);
            		Toast.makeText(spaContext, "Copied: " + messageTxt, Toast.LENGTH_SHORT).show();
            		break;
            	case 1 :
            		tmpMsg.setLockMsg(!tmpMsg.isLockedMsg());
					if (EpicFeedConsts.debugLevel > 5) Log.i(TAG, "itemShortTap: Updating msg flags for phoneID:" + tmpMsg.msgPhoneID 
							+ " : Flags: " + tmpMsg.msgFlgs);
					tmpConn = epicDB.open(true);
					epicDB.updateMsgFlgs(tmpMsg);
					epicDB.close(tmpConn);
			        updateUI();
            		break;
            	case 2 :
					if (EpicFeedConsts.DEBUG_MSG_LIST_ACT) Log.i(TAG, "itemShortTap: Delete Text List# " + tmpMsg.msgID);
					tmpConn = epicDB.open(true);
					epicDB.deleteTxtEntry(tmpMsg.msgID);
					epicDB.close(tmpConn);
					if (EpicFeedConsts.debugLevel > 5) Log.i(TAG, "itemShortTap: Delete txtMsgAA Text:" + tmpMsg.msgTxt);
					if (EpicFeedConsts.debugLevel > 5) Log.i(TAG, "itemShortTap: Delete txtMsgAA msgID:" + tmpMsg.msgID);
					scrollSelectionSet = true;
					scrollSelectionId = messageId;
					txtMsgListGroup.remove(tmpMsg);
					txtSepMsgAA.notifyDataSetChanged();
			        updateUI();
            		Toast.makeText(spaContext, getString(R.string.sysMsgDeleted), Toast.LENGTH_SHORT).show();
            		break;
            	case 3 :
            		break;
            	case 4 :
            		infoTextChk.setVisibility(View.GONE);
            		infoText.setText(Html.fromHtml(
            				getText(R.string.smsShortTapHelpBody).toString()));
            		ad = new AlertDialog.Builder(spaContext)
                    .setTitle(R.string.smsShortTapHelpHeader)
                    .setView(helpTextView)
                    .setNeutralButton(getString(R.string.smsShortTapHelpDone), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            updateAuthActivity();
                        	dialog.cancel();
                        }
                    })
                    .create();
            		ad.show();
            		break;
            	case 5 :
    				synchronized (sendInProgress) {
    					sendInProgress = true;
    					EpicFeedController.smsHandler.sendTxtMsg(contact, tmpMsg.msgTxt);
    					sendInProgress = false;
    				}
			        updateUI();
			        msgsSent++;
            		break;
           		}
            }
        })
        .create();
		ad.show();

	}
	
	public void itemLongHold(long messageId, final AdapterView<?> parent) {
		if (editor == null) editor = settings.edit();
		final FeedTextMsg tmpMsg = (FeedTextMsg)txtSepMsgAA.getItem((int)messageId);
    	if (tmpMsg.isInfoMsg()) return;
		final String grpName = txtMsgListGroup.getGroup(
				txtMsgListGroup.getMsgGroup(tmpMsg.msgID)).listTitle;
		final String messageTxt = tmpMsg.msgTxt;
		final String lockMsgDisplay;
		if (tmpMsg.isLockedMsg()) {
			lockMsgDisplay = getString(R.string.smsLongHoldUnlock);
		} else {
			lockMsgDisplay = getString(R.string.smsLongHoldLock);
		}
		ArrayList<String> longHoldOptionsSelect1 = new ArrayList<String>();
		longHoldOptionsSelect1.add(getString(R.string.smsLongHoldCopyMsg));
		longHoldOptionsSelect1.add(lockMsgDisplay);
		longHoldOptionsSelect1.add(getString(R.string.smsLongHoldDeleteGroup) + " " + grpName);
		longHoldOptionsSelect1.add(getString(R.string.smsLongHoldDeleteThread));
		longHoldOptionsSelect1.add(getString(R.string.smsLongHoldEmailExport));
		longHoldOptionsSelect1.add(getString(R.string.smsLongHoldCancel));
		longHoldOptionsSelect1.add(getString(R.string.smsLongHoldHelp));
		if (tmpMsg.hasSendErr()) {
			longHoldOptionsSelect1.add(getString(R.string.smsLongHoldResendMsg));
		}

		final String[] longHoldOptions = longHoldOptionsSelect1.toArray(new String[0]);
		AlertDialog ad = new AlertDialog.Builder(spaContext)
        .setTitle(R.string.smsLongHoldOptionsTitle)
        .setItems(longHoldOptions, new DialogInterface.OnClickListener() {
            @SuppressWarnings("deprecation")
			public void onClick(DialogInterface dialog, int modeId) {
    	        updateAuthActivity();
            	AlertDialog ad;
            	final View helpTextView = mInflater.inflate(R.layout.helpview_chk, 
            			parent, false);
    			final TextView infoText = (TextView)helpTextView.findViewById(R.id.helpText);
    			final CheckBox infoTextChk = (CheckBox)helpTextView.findViewById(R.id.helpCheck);
    			final float scale = spaContext.getResources().getDisplayMetrics().density;
    			infoTextChk.setPadding(infoTextChk.getPaddingLeft() + (int)(10.0f * scale + 0.5f), 
    					infoTextChk.getPaddingTop(), infoTextChk.getPaddingRight(), infoTextChk.getPaddingBottom());
            	switch (modeId) {
            	case 0 :
            		ClipboardManager cbManager = (ClipboardManager)getSystemService(CLIPBOARD_SERVICE);
            		cbManager.setText(messageTxt);
            		Toast.makeText(spaContext, getString(R.string.smsTextCopiedToastMsg) 
            				+ messageTxt, Toast.LENGTH_SHORT).show();
            		break;
            	case 1 :
            		tmpMsg.setLockMsg(!tmpMsg.isLockedMsg());
					if (EpicFeedConsts.debugLevel > 5) Log.i(TAG, "itemLongHold: Updating msg flags for phoneID:" + tmpMsg.msgPhoneID 
							+ " : Flags: " + tmpMsg.msgFlgs);
					EpicDBConn tmpConn = epicDB.open(true);
					epicDB.updateMsgFlgs(tmpMsg);
					epicDB.close(tmpConn);
			        updateUI();
            		break;
            	case 2 :
        			infoTextChk.setPadding(infoTextChk.getPaddingLeft() + (int)(10.0f * scale + 0.5f), 
        					infoTextChk.getPaddingTop(), infoTextChk.getPaddingRight(), infoTextChk.getPaddingBottom());
        			infoTextChk.setText(spaContext.getString(R.string.deleteThreadGroupLockedTxt));
        			infoText.setText(Html.fromHtml(spaContext.getString(R.string.deleteThreadGroupHelp)
        					+ " " + grpName));
        			ad = new AlertDialog.Builder(spaContext)
        	        .setTitle(spaContext.getString(R.string.deleteThreadGroupHelpHeader))
        	        .setView(helpTextView)
					.setPositiveButton(spaContext.getString(R.string.deleteThreadGroupYesBtn), new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int id) {
					        updateAuthActivity();
		            		FeedMessageListHelper tmpList = txtMsgListGroup.getGroup(
		            				txtMsgListGroup.getMsgGroup(tmpMsg.msgID));
		            		int msgGroupIdx = txtMsgListGroup.getMsgGroup(tmpMsg.msgID);
		            		int count = 0, countDel = 0;
		            		Iterator<FeedTextMsg> tmpListIt = tmpList.txtMsgList.listIterator();
							EpicDBConn tmpConn = epicDB.open(true);
		            		while(tmpListIt.hasNext()) {
								FeedTextMsg tmpMsg = tmpListIt.next();
								if (EpicFeedConsts.DEBUG_MSG_LIST_ACT) Log.i(TAG, "itemLongHold: Delete Text# " + tmpMsg.msgID);
								if (!tmpMsg.isLockedMsg()|infoTextChk.isChecked()) {
									epicDB.deleteTxtEntry(tmpMsg.msgID);
									countDel++;
								}
								count++;
								if (EpicFeedConsts.debugLevel > 5) Log.i(TAG, "itemLongHold: Delete txtMsgAA Text:" + tmpMsg.msgTxt);
								if (EpicFeedConsts.debugLevel > 5) Log.i(TAG, "itemLongHold: Delete txtMsgAA msgID:" + tmpMsg.msgID);
		            		}
							if (epicDB.execMsgCount(tmpMsg.msgPhoneID) < 1) {
								txtMsgListGroup.clear();
								FeedTextMsg tmpMsg = new FeedTextMsg();
								tmpMsg.msgTxt = getString(R.string.sysMsgNoMsgs);
								tmpMsg.setInfoMsg(true);
								tmpMsg.noMessages = true;
								txtMsgListGroup.insert(tmpMsg, 0);
							}
							epicDB.close(tmpConn);
							if (countDel == count & msgGroupIdx >= 0)
								txtMsgListGroup.removeList(msgGroupIdx);
							txtSepMsgAA.notifyDataSetChanged();
					        updateUI();
						}
					})
					.setNegativeButton(spaContext.getString(R.string.deleteThreadGroupNoBtn), new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int id) {
					        updateAuthActivity();
							dialog.cancel();
						}
					})
			        .create();
					ad.show();
            		break;
            	case 3 :
        			infoTextChk.setPadding(infoTextChk.getPaddingLeft() + (int)(10.0f * scale + 0.5f), 
        					infoTextChk.getPaddingTop(), infoTextChk.getPaddingRight(), infoTextChk.getPaddingBottom());
        			infoTextChk.setText(spaContext.getString(R.string.deleteThreadLockedTxt));
        			infoText.setText(Html.fromHtml(spaContext.getString(R.string.deleteThreadHelp)));
        			ad = new AlertDialog.Builder(spaContext)
        	        .setTitle(spaContext.getString(R.string.deleteThreadHelpHeader))
        	        .setView(helpTextView)
					.setPositiveButton(spaContext.getString(R.string.deleteThreadYesBtn), new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int id) {
					        updateAuthActivity();
							String pID = contact.phoneID;
							if (EpicFeedConsts.debugLevel > 5) Log.i(TAG, "onItemLongClick.onClick: Deleting all text entries for phoneID:" + pID);
							EpicDBConn tmpConn = epicDB.open(true);
							epicDB.deleteTxtThread(pID, infoTextChk.isChecked());
							if (epicDB.execMsgCount(pID) < 1) {
								txtMsgListGroup.clear();
								FeedTextMsg tmpMsg = new FeedTextMsg();
								tmpMsg.msgTxt = getString(R.string.sysMsgNoMsgs);
								tmpMsg.setInfoMsg(true);
								tmpMsg.noMessages = true;
								txtMsgListGroup.insert(tmpMsg, 0);
							}
							epicDB.close(tmpConn);
					        updateUI();
						}
					})
					.setNegativeButton(spaContext.getString(R.string.deleteThreadNoBtn), new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int id) {
		        	        updateAuthActivity();
							dialog.cancel();
						}
					})
			        .create();
					ad.show();
            		break;
            	case 4 :
            		dialog.cancel();
            		break;
            	case 5 :
            		break;
            	case 6 :
            		infoTextChk.setVisibility(View.GONE);
            		infoText.setText(Html.fromHtml(
            				getText(R.string.threadLongHoldHelp).toString()));
            		ad = new AlertDialog.Builder(spaContext)
                    .setTitle(R.string.threadLongHoldHelpHeader)
                    .setView(helpTextView)
                    .setNeutralButton(getString(R.string.threadLongHoldHelpDone), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                	        mSpaUserAccount.updateAuthActivity();
                        	dialog.cancel();
                        }
                    })
                    .create();
            		ad.show();
            		break;
            	case 7 :
    				synchronized (sendInProgress) {
    					sendInProgress = true;
    					EpicFeedController.smsHandler.sendTxtMsg(contact, tmpMsg.msgTxt);
    					sendInProgress = false;
    				}
			        updateUI();
			        msgsSent++;
            		break;
            	}
            }
        })
        .create();
		ad.show();

	}
	
	public void themeSelect(View v) {
		themeSelect(false);
	}
	
	public void themeSelectClassic(View v) {
		themeSelect(true);
	}
	
	public void themeSelect(boolean classic) {
		if (!classic) {
			final String[] themeOptionNames = {
					getString(R.string.smsTextThemeStandard),
					getString(R.string.smsTextThemePink),
					getString(R.string.smsTextThemeBlue),
					getString(R.string.smsTextThemeGreen),
					getString(R.string.smsTextThemeDark),	
					getString(R.string.smsTextThemeLight)	
				};
	
			if (editor == null) editor = settings.edit();
			AlertDialog ad = new AlertDialog.Builder(spaContext)
	        .setTitle(R.string.smsTextThemeBtnTxt)
	        .setItems(themeOptionNames, new DialogInterface.OnClickListener() {
	            public void onClick(DialogInterface dialog, int modeId) {
	    	        updateAuthActivity();
	            	switch (modeId) {
	            	case 0 :
						easyTracker.send(MapBuilder
				                .createEvent(EpicFeedConsts.gat_MenuItem,     	// Event category (required)
				                		EpicFeedConsts.gat_Click,			  	// Event action (required)
				                		EpicFeedConsts.getStandardTheme,	   			// Event label
				                        1L)            						// Event value
				                .build()
				            );
	            		editor.putString(EpicFeedConsts.THEME_SELECT, EpicFeedConsts.STANDARD_THEME);
	                	themeOptions.setTheme(EpicFeedConsts.STANDARD_THEME);
	            		break;
	            	case 1 :
						easyTracker.send(MapBuilder
				                .createEvent(EpicFeedConsts.gat_MenuItem,     	// Event category (required)
				                		EpicFeedConsts.gat_Click,			  	// Event action (required)
				                		EpicFeedConsts.gatThemePink,	   			// Event label
				                        1L)            						// Event value
				                .build()
				            );
	            		editor.putString(EpicFeedConsts.THEME_SELECT, EpicFeedConsts.PINK_THEME);
	                	themeOptions.setTheme(EpicFeedConsts.PINK_THEME);
	            		break;
	            	case 2 :
						easyTracker.send(MapBuilder
				                .createEvent(EpicFeedConsts.gat_MenuItem,     	// Event category (required)
				                		EpicFeedConsts.gat_Click,			  	// Event action (required)
				                		EpicFeedConsts.gatThemeBlue,	   			// Event label
				                        1L)            						// Event value
				                .build()
				            );
	            		editor.putString(EpicFeedConsts.THEME_SELECT, EpicFeedConsts.BLUE_THEME);
	                	themeOptions.setTheme(EpicFeedConsts.BLUE_THEME);
	            		break;
	            	case 3 :
						easyTracker.send(MapBuilder
				                .createEvent(EpicFeedConsts.gat_MenuItem,     	// Event category (required)
				                		EpicFeedConsts.gat_Click,			  	// Event action (required)
				                		EpicFeedConsts.gatThemeGreen,	   			// Event label
				                        1L)            						// Event value
				                .build()
				            );
	            		editor.putString(EpicFeedConsts.THEME_SELECT, EpicFeedConsts.GREEN_THEME);
	                	themeOptions.setTheme(EpicFeedConsts.GREEN_THEME);
	            		break;
	            	case 4 :
						easyTracker.send(MapBuilder
				                .createEvent(EpicFeedConsts.gat_MenuItem,     	// Event category (required)
				                		EpicFeedConsts.gat_Click,			  	// Event action (required)
				                		EpicFeedConsts.gatThemeDark,	   			// Event label
				                        1L)            						// Event value
				                .build()
				            );
	            		editor.putString(EpicFeedConsts.THEME_SELECT, EpicFeedConsts.DARK_THEME);
	                	themeOptions.setTheme(EpicFeedConsts.DARK_THEME);
	            		break;
	            	case 5 :
						easyTracker.send(MapBuilder
				                .createEvent(EpicFeedConsts.gat_MenuItem,     	// Event category (required)
				                		EpicFeedConsts.gat_Click,			  	// Event action (required)
				                		EpicFeedConsts.gatThemeLight,	   			// Event label
				                        1L)            						// Event value
				                .build()
				            );
	            		editor.putString(EpicFeedConsts.THEME_SELECT, EpicFeedConsts.LIGHT_THEME);
	                	themeOptions.setTheme(EpicFeedConsts.LIGHT_THEME);
	            		break;
	            	}
	            	editor.commit();
	    	        updateUI();
	            }
	        })
	        .create();
			ad.show();
		} else {
			final String[] themeOptionNames = {
					getString(R.string.smsTextClassicThemeStandard),
					getString(R.string.smsTextClassicThemePink),
					getString(R.string.smsTextClassicThemeBlue),
					getString(R.string.smsTextClassicThemeGreen),
					getString(R.string.smsTextClassicThemeDark),	
					getString(R.string.smsTextClassicThemeLight)	
				};
	
			if (editor == null) editor = settings.edit();
			AlertDialog ad = new AlertDialog.Builder(spaContext)
	        .setTitle(R.string.smsTextClassicThemeBtnTxt)
	        .setItems(themeOptionNames, new DialogInterface.OnClickListener() {
	            public void onClick(DialogInterface dialog, int modeId) {
	    	        updateAuthActivity();
	            	switch (modeId) {
	            	case 0 :
						easyTracker.send(MapBuilder
				                .createEvent(EpicFeedConsts.gat_MenuItem,     	// Event category (required)
				                		EpicFeedConsts.gat_Click,			  	// Event action (required)
				                		EpicFeedConsts.getClassicThemeStandard,	   			// Event label
				                        1L)            						// Event value
				                .build()
				            );
	            		editor.putString(EpicFeedConsts.THEME_SELECT, EpicFeedConsts.CLASSIC_STANDARD_THEME);
	                	themeOptions.setTheme(EpicFeedConsts.CLASSIC_STANDARD_THEME);
	            		break;
	            	case 1 :
						easyTracker.send(MapBuilder
				                .createEvent(EpicFeedConsts.gat_MenuItem,     	// Event category (required)
				                		EpicFeedConsts.gat_Click,			  	// Event action (required)
				                		EpicFeedConsts.gatClassicThemePink,	   			// Event label
				                        1L)            						// Event value
				                .build()
				            );
	            		editor.putString(EpicFeedConsts.THEME_SELECT, EpicFeedConsts.CLASSIC_PINK_THEME);
	                	themeOptions.setTheme(EpicFeedConsts.CLASSIC_PINK_THEME);
	            		break;
	            	case 2 :
						easyTracker.send(MapBuilder
				                .createEvent(EpicFeedConsts.gat_MenuItem,     	// Event category (required)
				                		EpicFeedConsts.gat_Click,			  	// Event action (required)
				                		EpicFeedConsts.gatClassicThemeBlue,	   			// Event label
				                        1L)            						// Event value
				                .build()
				            );
	            		editor.putString(EpicFeedConsts.THEME_SELECT, EpicFeedConsts.CLASSIC_BLUE_THEME);
	                	themeOptions.setTheme(EpicFeedConsts.CLASSIC_BLUE_THEME);
	            		break;
	            	case 3 :
						easyTracker.send(MapBuilder
				                .createEvent(EpicFeedConsts.gat_MenuItem,     	// Event category (required)
				                		EpicFeedConsts.gat_Click,			  	// Event action (required)
				                		EpicFeedConsts.gatClassicThemeGreen,	   			// Event label
				                        1L)            						// Event value
				                .build()
				            );
	            		editor.putString(EpicFeedConsts.THEME_SELECT, EpicFeedConsts.CLASSIC_GREEN_THEME);
	                	themeOptions.setTheme(EpicFeedConsts.CLASSIC_GREEN_THEME);
	            		break;
	            	case 4 :
						easyTracker.send(MapBuilder
				                .createEvent(EpicFeedConsts.gat_MenuItem,     	// Event category (required)
				                		EpicFeedConsts.gat_Click,			  	// Event action (required)
				                		EpicFeedConsts.gatClassicThemeDark,	   			// Event label
				                        1L)            						// Event value
				                .build()
				            );
	            		editor.putString(EpicFeedConsts.THEME_SELECT, EpicFeedConsts.CLASSIC_DARK_THEME);
	                	themeOptions.setTheme(EpicFeedConsts.CLASSIC_DARK_THEME);
	            		break;
	            	case 5 :
						easyTracker.send(MapBuilder
				                .createEvent(EpicFeedConsts.gat_MenuItem,     	// Event category (required)
				                		EpicFeedConsts.gat_Click,			  	// Event action (required)
				                		EpicFeedConsts.gatClassicThemeLight,	   			// Event label
				                        1L)            						// Event value
				                .build()
				            );
	            		editor.putString(EpicFeedConsts.THEME_SELECT, EpicFeedConsts.CLASSIC_LIGHT_THEME);
	                	themeOptions.setTheme(EpicFeedConsts.CLASSIC_LIGHT_THEME);
	            		break;
	            	}
	            	editor.commit();
	    	        updateUI();
	            }
	        })
	        .create();
			ad.show();
		}

	}
	
	public boolean checkAuth(Intent appIntent) {
		if (appIntent.hasExtra(EpicFeedConsts.AUTH_STATUS)) {
			Boolean authPass = appIntent.getBooleanExtra(EpicFeedConsts.AUTH_STATUS, false);
			if (!authPass) {
				if (editor == null) editor = settings.edit();
				mSpaUserAccount.setLoggedIn(false);
			}
			return authPass;
		}
		return true;
	}
	
	public void clearNotification() {
		// do not clear notifications if the user timed out
		if (mSpaUserAccount.authenticated(new WeakReference<Activity>(this))) {
			Timer clrNotify = new Timer(TAG + " Clear Notification", true);
			clrNotify.schedule(new TimerTask() { 
				@Override
				public void run() { 
					if (contact.notifyOptions.statusbarDefaultIcon)
						((NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE))
						.cancel(SpaTextNotification.getIcon(0));
					else 
						((NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE))
						.cancel(SpaTextNotification.getIcon(contact.notifyOptions.statusbarIcon));
				}
			}, 500);
		}
	}
	
	public Runnable UiWait() {
		return new Runnable() {

			@Override
			public void run() {
				synchronized (this) {
					synchronized (sendInProgress) {
						synchronized (updateUIinProgress) {
							while (sendInProgress||updateUIinProgress) {
								synchronized (this) {
									try {
										wait(100);
									} catch (InterruptedException e) {
										e.printStackTrace();
									}
								}
							}
						}
					}
				}
				UiHandler.post(UiUpdate());
			}
			
		};
	}
	
	public Runnable UiUpdate() {
		return new Runnable() {

			@Override
			public void run() {
		        updateUI();
			}
			
		};
	}
	
	public EpicDB.CheckMessageListener getCheckMessageListener() {
		return new EpicDB.CheckMessageListener() {

			@Override
			public void onNewMsg() {
				if (mSpaUserAccount.authenticated(spaActivity)) {
					if (EpicFeedConsts.DEBUG_TEXT_MSG_LIST) 
						Log.i(TAG, "getCheckMessageListener: onNewMsg");
					synchronized (sendInProgress) {
						synchronized (updateUIinProgress) {
							if (sendInProgress||updateUIinProgress) {
						        Thread thread = new Thread(null, UiWait(), "UiWait");
						        thread.start();
							}
							else UiHandler.post(UiUpdate());
						}
					}
					clearNotification();
				}
			}
	
			@Override
			public void onSentMsg() {
				if (EpicFeedConsts.DEBUG_TEXT_MSG_LIST) 
					Log.i(TAG, "getCheckMessageListener: onSentMsg");
				synchronized (sendInProgress) {
					synchronized (updateUIinProgress) {
						if (sendInProgress||updateUIinProgress) {
					        Thread thread = new Thread(null, UiWait(), "UiWait");
					        thread.start();
						}
						else UiHandler.post(UiUpdate());
					}
				}
			}
	
			@Override
			public void onDeliveredMsg() {
				if (EpicFeedConsts.DEBUG_TEXT_MSG_LIST) 
					Log.i(TAG, "getCheckMessageListener: onDeliveredMsg");
				synchronized (sendInProgress) {
					synchronized (updateUIinProgress) {
						if (sendInProgress||updateUIinProgress) {
					        Thread thread = new Thread(null, UiWait(), "UiWait");
					        thread.start();
						}
						else UiHandler.post(UiUpdate());
					}
				}
			}
		};
	}
	
	private Bitmap getNextMmsImage(String mmsId) {
		String selectionPart = "mid=" + mmsId;
		Uri uri = Uri.parse("content://mms/part");
		Cursor cPart = getContentResolver().query(uri, null,
		    selectionPart, null, null);
		if (cPart.moveToFirst()) {
		    do {
		        String partId = cPart.getString(cPart.getColumnIndex("_id"));
		        String type = cPart.getString(cPart.getColumnIndex("ct"));
		        if ("image/jpeg".equals(type) || "image/bmp".equals(type) ||
		                "image/gif".equals(type) || "image/jpg".equals(type) ||
		                "image/png".equals(type)) {
		            //Bitmap bitmap = getMmsImage(partId);
		            return getMmsImage(partId);
		        }
		    } while (cPart.moveToNext());
		}
		return null;
	}
	
	private Bitmap getMmsImage(String _id) {
	    Uri partURI = Uri.parse("content://mms/part/" + _id);
	    InputStream is = null;
	    Bitmap bitmap = null;
	    try {
	        is = getContentResolver().openInputStream(partURI);
	        bitmap = BitmapFactory.decodeStream(is);
	    } catch (IOException e) {}
	    finally {
	        if (is != null) {
	            try {
	                is.close();
	            } catch (IOException e) {}
	        }
	    }
	    return bitmap;
	}
	
	private String getAddressNumber(int id) {
	    String selectionAdd = new String("msg_id=" + id);
	    String uriStr = MessageFormat.format("content://mms/{0}/addr", id);
	    Uri uriAddress = Uri.parse(uriStr);
	    Cursor cAdd = getContentResolver().query(uriAddress, null,
	        selectionAdd, null, null);
	    String name = null;
	    if (cAdd.moveToFirst()) {
	        do {
	            String number = cAdd.getString(cAdd.getColumnIndex("address"));
	            if (number != null) {
	                try {
	                    Long.parseLong(number.replace("-", ""));
	                    name = number;
	                } catch (NumberFormatException nfe) {
	                    if (name == null) {
	                        name = number;
	                    }
	                }
	            }
	        } while (cAdd.moveToNext());
	    }
	    if (cAdd != null) {
	        cAdd.close();
	    }
	    return name;
	}
	
	private void setActionBarTitle(FeedTextMsg[] ftMsg) {
		//EpicDBConn tmpConn = epicDB.open(true);
		//feedMsgs = epicDB.getContactMessages(contact.phoneID, false, themeOptions.threadAscending);
		//if (feedMsgs.length > 0) epicDB.clearNewMsgs(contact.phoneID);
		//epicDB.close(tmpConn);
		Log.i(TAG, "+++++ feedMsgs.length : " + feedMsgs.length + " +++++");
		if (ftMsg
				.length < 1) {
	        if (actionbarTitle != null) {
				if (contact.getNameOrPhoneId().length() < 16)
					actionbarTitle.setText(Html.fromHtml("<b>" + contact.getNameOrPhoneId() + "</b> " + getString(R.string.smsTextMsgCount0)));
				else actionbarTitle.setText(Html.fromHtml("<b>" + contact.getNameOrPhoneId().substring(0, 15) + "</b>... " + getString(R.string.smsTextMsgCount0)));
	        }
		} else {
			if (ftMsg.length == 1) {
		        if (actionbarTitle != null) {
					if (contact.getNameOrPhoneId().length() < 16)
						actionbarTitle.setText(Html.fromHtml("<b>" + contact.getNameOrPhoneId() + "</b> " 
								+ "(" + ftMsg.length + " " + getString(R.string.smsTextMsgCount1) + ")"));
					else actionbarTitle.setText(Html.fromHtml("<b>" + contact.getNameOrPhoneId().substring(0, 15) + "</b>... " 
							+ "(" + ftMsg.length + " " + getString(R.string.smsTextMsgCount1) + ")"));
		        }
			} else {
		        if (actionbarTitle != null) {
					if (contact.getNameOrPhoneId().length() < 16)
						actionbarTitle.setText(Html.fromHtml("<b>" + contact.getNameOrPhoneId() + "</b> " 
								+ "(" + ftMsg.length + " " + getString(R.string.smsTextMsgCountPlural) + ")"));
					else actionbarTitle.setText(Html.fromHtml("<b>" + contact.getNameOrPhoneId().substring(0, 15) + "</b>... " 
								+ "(" + ftMsg.length + " " + getString(R.string.smsTextMsgCountPlural) + ")"));
		        }
			}
		}
	}
	
	private void updateUI(){
		synchronized (updateUIinProgress) {
			updateUIinProgress = true;
			updateAvatarView();
			
			if (EpicFeedConsts.debugLevel > 5) Log.i(TAG, "updating UI");
	        if (mInflater == null) mInflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	        txtMsgListGroup = new FeedMessageListGroup(spaContext, mInflater, contact, me);
	        txtMsgListGroup.msgTextSize = themeOptions.textSize;
	    	txtSepMsgAA = new FeedSeparatedMessageListAdapter(spaContext, themeOptions);
	    	txtSepMsgAA.limitCount = 30000;
			ArrayList<FeedTextMsg> tmpMsgList = new ArrayList<FeedTextMsg>();
			if (EpicFeedConsts.DEBUG_MSG_LIST_ACT) Log.i(TAG, "checking message list");
			
			me = mFeedMessageHandler.me.clone();
			contact = mFeedMessageHandler.contact.clone();
			
			feedMsgs = new FeedTextMsg[mFeedMessageHandler.arrayMsgs.length];
			System.arraycopy(mFeedMessageHandler.arrayMsgs, 0, feedMsgs, 0, mFeedMessageHandler.arrayMsgs.length);	
			
			if (feedMsgs[0]
					.noMessages) {
				if (EpicFeedConsts.DEBUG_MSG_LIST_ACT) 
					Log.i(TAG, "checking message list: no messages");
				tmpMsgList.add(feedMsgs[0]);
				txtMsgListGroup.addList(0, FeedMessageListGroup.msgGroupTitle[0], tmpMsgList);
				txtSepMsgAA.addSection(txtMsgListGroup.getGroup(0).listTitle, 
						txtMsgListGroup.getGroup(0).msgListAA);
			} else {
				if (EpicFeedConsts.DEBUG_MSG_LIST_ACT) 
					Log.i(TAG, "checking message list: found messages");
				// skip the "no messages" title group
				txtMsgListGroup.insert(feedMsgs[0], -1);
				int msgGroupIdx = txtMsgListGroup.getGroupForMsg(feedMsgs[0]);
				for (int i = 1; i < feedMsgs.length; i++) {
					if (feedMsgs[i-1].msgDate.getDay() == feedMsgs[i].msgDate.getDay()) {
						txtMsgListGroup.insert(feedMsgs[i], msgGroupIdx);
					} else {
						txtMsgListGroup.insert(feedMsgs[i], -1);
						msgGroupIdx = txtMsgListGroup.getGroupForMsg(feedMsgs[i]);
					}
				}
				if (themeOptions.threadAscending) {
					FeedMessageListHelper[] tmpLists = txtMsgListGroup.msgGroup;
					for (int i = tmpLists.length; i > 0; i--) {
						if (tmpLists[i-1] != null) {
								txtSepMsgAA.addSection(tmpLists[i-1].listTitle, 
										tmpLists[i-1].msgListAA);
						}
					}
				} else {
					for (FeedMessageListHelper tmpList: txtMsgListGroup.msgGroup) {
						if (tmpList != null) {
								txtSepMsgAA.addSection(tmpList.listTitle, 
										tmpList.msgListAA);
						}
					}
				}
			}
			
			setActionBarTitle(feedMsgs);
	    	setListAdapter(txtSepMsgAA);
			txtSepMsgAA.notifyDataSetChanged();
			updateUIinProgress = false;
			if (!scrollListenerSet) {
				getListView().setOnScrollListener(this);
				scrollListenerSet = true;
			}

			if (scrollSelectionSet) {
				if (scrollSelectionId > (txtSepMsgAA.getCount()-1)) getListView().setSelection(txtSepMsgAA.getCount()-1);
				else getListView().setSelection((int)scrollSelectionId);
				scrollSelectionSet = false;
			} else {
				if (themeOptions.threadAscending) {
					getListView().setSelection(txtSepMsgAA.getCount()-1);
					getListView().setStackFromBottom(false);
				} else {
		    		getListView().setSelection(0);
					getListView().setStackFromBottom(true);
		    	}
			}
			if ((txtMsgListGroup.getMsgCount() > 30)&&
					!getListView().isFastScrollEnabled()) getListView().setFastScrollEnabled(true);
			if (EpicFeedConsts.DEBUG_MSG_LIST_ACT) 
				Log.i(TAG, "updateUI: txtMsgListGroup.getMsgCount(): " + txtMsgListGroup.getMsgCount()
						+ " setFastScrollEnabled:" + getListView().isFastScrollEnabled());
		}
    }
	
    @Override
    public boolean onTouchEvent(MotionEvent e) {
        synchronized (userInteraction) {
        	userInteraction = true;
        }
    	return super.onTouchEvent(e);
    }
    
    @Override
    public void onUserInteraction() {
        synchronized (userInteraction) {
        	userInteraction = true;
        }
    }
    
	public void onScroll(AbsListView view, int firstVisible, int visibleCount, int totalCount) {
		boolean loadMore =
				(firstVisible + visibleCount + 50 >= totalCount) && (txtMsgListGroup.getCount() > txtSepMsgAA.limitCount);
			if (loadMore) {
				txtSepMsgAA.limitCount += 5000;
				txtSepMsgAA.notifyDataSetChanged();
				if (EpicFeedConsts.DEBUG_MSG_LIST_ACT) 
					Log.i(TAG, "onScroll: loaded more: limit: " + txtSepMsgAA.limitCount);
			}
	}
	
	public void onScrollStateChanged (AbsListView view, int s) {
		
	}
	
	public void stopAds() {
		if (EpicFeedConsts.DEBUG_MSG_LIST_ACT) 
			Log.i(TAG, "stopAds: stopping..");
		if (adHandler != null) {
	        adHandler.pauseAds();
		}
        try {
			synchronized (adHandler.isBeingDestroyed) {
				if (!adHandler.isBeingDestroyed) adListener.needAd = false;
			}
        } catch (Exception e) {
        	// don't worry about an error
	    	if (EpicFeedConsts.DEBUG_MSG_LIST_ACT) {
				Log.i(TAG, "stopAds: adView.stopLoading: error");
				e.printStackTrace();
			}
        }
        if (interstitialAdHandler != null) 
        	interstitialAdHandler.getAd(false);
	    if (mInterstitial != null) mInterstitial.destroy();
	}
	
	public void destroyAds() {
		if (adView != null) {
			try {
				synchronized (adHandler.isBeingDestroyed) {
					adHandler.isBeingDestroyed = true;
				}
			} catch (Exception e) {
				// do nothing
			}
		}
        try {
			synchronized (adHandler.isBeingDestroyed) {
				if (adHandler.isBeingDestroyed) adListener.needAd = false;
			}
        } catch (Exception e) {
        	// don't worry about an error
        }
		if (adHandler != null) 
			try {
			    if (EpicFeedConsts.DEBUG_MSG_LIST_ACT) 
	    			Log.i(TAG, "destroyAds: AdHandler stopping...");
				adHandler.checkAd = false;
				adHandler.stopAdTimer();
				adHandler = null;
			} catch (Exception e) {
				// do nothing, ad will go away
			    if (EpicFeedConsts.DEBUG_MSG_LIST_ACT) {
    				Log.i(TAG, "destroyAds: AdHandler stopping: error");
    				e.printStackTrace();
				}
			}
		if (adView != null) adView.destroy();
	}
	
	public void resetConfig() {
		stopAds();
		if (EpicFeedConsts.DEBUG_MSG_LIST_ACT) Log.i(TAG, "resetConfig: stopped all... restarting");
		initVariables(savedState);
		getContact();
		mFeedMessageHandler.resetActConfig();
		setupTheme();
		setupLayout();
		setupAdHandler();
		setupListAdapter();
        updateUI();
        resumeAds();
        listenForMessages(true);
        if (waitDialog != null && waitDialog.isShowing()) waitDialog.cancel();
	}
	
	@Override
    public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		if (EpicFeedConsts.DEBUG_MSG_LIST_ACT) Log.i(TAG, "onConfigurationChanged: screen orientation changed");
		// don't change if export is in progress
		if (exportDialog == null) resetConfig();
		else if (!exportDialog.isShowing()) resetConfig();
    }

	public int getScreenOrientation () {
		Display getOrient = getWindowManager().getDefaultDisplay();
		int orientation = getOrient.getOrientation();
		// Sometimes you may get undefined orientation Value is 0
		// simple logic solves the problem compare the screen
		// X,Y Co-ordinates and determine the Orientation in such cases
		
		if(orientation==Configuration.ORIENTATION_UNDEFINED){
			Configuration config = getResources().getConfiguration();
			orientation = config.orientation;
			if (EpicFeedConsts.DEBUG_MSG_LIST_ACT) Log.i(TAG, "getScreenOrientation:screen orientation undefined, config.orientation: " + orientation);
			if(orientation==Configuration.ORIENTATION_UNDEFINED){
				//if height and width of screen are equal then
				// it is square orientation
				if(getOrient.getWidth()==getOrient.getHeight()){
					orientation = Configuration.ORIENTATION_SQUARE;
					if (EpicFeedConsts.DEBUG_MSG_LIST_ACT) Log.i(TAG, "getScreenOrientation:screen orientation square: " + orientation);
					}else{
						//if width is less than height than it is portrait
						if(getOrient.getWidth() < getOrient.getHeight()){
							if (EpicFeedConsts.DEBUG_MSG_LIST_ACT) Log.i(TAG, "getScreenOrientation:screen orientation portrait: " + orientation);
							orientation = Configuration.ORIENTATION_PORTRAIT;
							}else{
								// if it is not any of the above it will definitely be landscape
								if (EpicFeedConsts.DEBUG_MSG_LIST_ACT) Log.i(TAG, "getScreenOrientation:screen orientation landscape (by default): " + orientation);
								orientation = Configuration.ORIENTATION_LANDSCAPE;
								}
						}
				}
		}
		if (EpicFeedConsts.DEBUG_MSG_LIST_ACT) Log.i(TAG, "getScreenOrientation:screen orientation: " + orientation);
		return orientation;
		// return value 1 is portrait and 2 is Landscape Mode } 
	}
	
	
	
	@Override
	public void onPause() {
		scrollLocation = messageList.getFirstVisiblePosition();
		showInterstitial = false;
        listenForMessages(false);
		stopAds();
        if ((waitDialog != null) && waitDialog.isShowing()) waitDialog.cancel();
        mSpaUserAccount.removeSpaUserAccountListener(accountListener);
		super.onPause();
	}

	@Override
	public void onStop() {
		mFeedMessageHandler.oneStopOn();
		runAuthMonitor = false;
		if (authMonitorThread != null && authMonitorThread.isAlive()) authMonitorThread.interrupt();
		authMonitorThread = null;
		setResult(RESULT_OK);
		pageTotalTime += Calendar.getInstance().getTimeInMillis() - pageStartTime;
		pageStartTime = 0;
		pageViews++;
		super.onStop();
		EasyTracker.getInstance(this).activityStop(this);  
		FlurryAgent.onEndSession(this);
		mFeedMessageHandler.twoStopOn();
		mFeedMessageHandler.setFeedMessageHandler(null);
	}

	@Override
	public void onDestroy() {
		try {
			easyTracker.send(MapBuilder
	                .createEvent(EpicFeedConsts.gat_Info,     	// Event category (required)
	                		EpicFeedConsts.gat_PageInfo,			  	// Event action (required)
	                		EpicFeedConsts.gatPageView,	   			// Event label
	                		(long)pageViews)            						// Event value
	                .build()
	            );
			easyTracker.send(MapBuilder
	                .createEvent(EpicFeedConsts.gat_Info,     	// Event category (required)
	                		EpicFeedConsts.gat_PageInfo,			  	// Event action (required)
	                		EpicFeedConsts.gatPageTime,	   			// Event label
	                		pageTotalTime)            						// Event value
	                .build()
	            );
			easyTracker.send(MapBuilder
	                .createEvent(EpicFeedConsts.gat_Info,     	// Event category (required)
	                		EpicFeedConsts.gat_Button,			  	// Event action (required)
	                		EpicFeedConsts.gatSendMsg,	   			// Event label
	                		(long)msgsSent)            						// Event value
	                .build()
	            );
			if (pageViews > 0) {
				easyTracker.send(MapBuilder
		                .createEvent(EpicFeedConsts.gat_Info,     	// Event category (required)
		                		EpicFeedConsts.gat_PageInfo,			  	// Event action (required)
		                		EpicFeedConsts.gatPageAveTime,	   			// Event label
		                		(pageTotalTime)/(pageViews))            						// Event value
		                .build()
		            );
			}
			easyTracker.send(MapBuilder
	                .createEvent(EpicFeedConsts.gat_Info,     	// Event category (required)
	                		EpicFeedConsts.gat_Ads,			  	// Event action (required)
	                		EpicFeedConsts.gatAdRequests,	   			// Event label
	                		adListener.requestCount)            						// Event value
	                .build()
	            );
			easyTracker.send(MapBuilder
	                .createEvent(EpicFeedConsts.gat_Info,     	// Event category (required)
	                		EpicFeedConsts.gat_Ads,			  	// Event action (required)
	                		EpicFeedConsts.gatAdImpressions,	   			// Event label
	                		adListener.adCount)            						// Event value
	                .build()
	            );
			if ((adListener.requestCount - adListener.pauseCount) > 0) {
				easyTracker.send(MapBuilder
		                .createEvent(EpicFeedConsts.gat_Info,     	// Event category (required)
		                		EpicFeedConsts.gat_Ads,			  	// Event action (required)
		                		EpicFeedConsts.gatAdAveRequestTime,	   			// Event label
		                		(long)((int) adListener.adTotalTime)/((int)(adListener.requestCount - adListener.pauseCount)))            						// Event value
		                .build()
		            );
			}
		} catch (Exception e) {
			// do nothing, google will go away
		}
		destroyAds();
		super.onDestroy();
	}

	@Override
	public void onInterstitialLoaded(MoPubInterstitial interstitial) {
		interstitialAdHandler.reloadAd(false);
	}

	@Override
	public void onInterstitialFailed(MoPubInterstitial interstitial,
			MoPubErrorCode errorCode) {
		interstitialAdHandler.reloadAd(true);
	}

	@Override
	public void onInterstitialShown(MoPubInterstitial interstitial) {
		interstitialAdHandler.isShowing(true);
	}

	@Override
	public void onInterstitialDismissed(MoPubInterstitial interstitial) {
		interstitialAdHandler.isShowing(false);
		interstitialAdHandler.reloadAd(true);
	}

	@Override
	public void onInterstitialClicked(MoPubInterstitial interstitial) {
		// TODO Auto-generated method stub
		
	}
}