package com.sdmmllc.epicfeed.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.sdmm.epicfeed.R;
import com.sdmmllc.epicfeed.EpicFeedController;
import com.sdmmllc.epicfeed.SpaUserAccount;
import com.sdmmllc.epicfeed.utils.EpicFeedConsts;


public class SpaTextSettings extends Activity {

	private String TAG = "SpaTextSettings";

	private CheckBox settingsTimeoutChk;
	private EditText settingsTimeoutDuration;
	private RadioGroup settingsLogoutScreen;
	private RadioButton settingsHomeScreen, settingsMainScreen, settingsLoginScreen;
	private Context spaContext;
	private Bundle savedState;
    private LayoutInflater mInflater;
    private SharedPreferences settings;
    private SpaUserAccount mSpaUserAccount;
	private TextWatcher settingsTimeoutDurationListener;
	private boolean timeout, oldTimeout;
	private int oldDuration, oldLogoutScreen;
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater menuInfl = getMenuInflater();
		menuInfl.inflate(R.menu.configsettings_menu, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int itemId = item.getItemId();
		if (itemId == R.id.configSettingsHelp) {
			helpDialog();
			return true;
		}
		
		return false;
	}

	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.config_settings);
        savedState = savedInstanceState;
        spaContext = this;
        mSpaUserAccount = EpicFeedController.getSpaUserAccount(this.getApplicationContext());
        mInflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        settingsTimeoutChk = (CheckBox)findViewById(R.id.settingsTimeoutSetupCheck);
        settingsTimeoutDuration = (EditText)findViewById(R.id.settingsTimeoutDurationEditTxt);
        settingsLogoutScreen= (RadioGroup)findViewById(R.id.settingsLogoutPageRadioGroup);
        settingsHomeScreen= (RadioButton)findViewById(R.id.settingsLogoutHomeRadio);
        settingsMainScreen= (RadioButton)findViewById(R.id.settingsLogoutMainScreenRadio);
        settingsLoginScreen= (RadioButton)findViewById(R.id.settingsLogoutLoginScreenRadio);
        settings = getSharedPreferences(EpicFeedConsts.PREFS_NAME, 0);
        timeout = settings.getBoolean(EpicFeedConsts.timeoutEnable, false);
        oldTimeout = timeout;
       	settingsTimeoutChk.setChecked(timeout);
       	settingsTimeoutChk.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton arg0, boolean checked) {
			}
        	
        });
       	settingsTimeoutDuration.setEnabled(timeout);
       	oldDuration = settings.getInt(EpicFeedConsts.timeoutDuration, 5);
       	oldLogoutScreen = settings.getInt(EpicFeedConsts.timeoutScreen, EpicFeedConsts.HOMESCREEN);
       	if (timeout) {
       		if (oldDuration != 5)
       			settingsTimeoutDuration.setText(new String(""+ oldDuration));
       	}
       	
       	settingsTimeoutDurationListener = new TextWatcher() {
    		@Override
    		public synchronized void afterTextChanged(Editable s) {
				try {
					Integer.valueOf(s.toString());
				} catch (Exception e) {
			    	settingsTimeoutDuration.removeTextChangedListener(settingsTimeoutDurationListener);
					s.clear();
					settingsTimeoutDuration.setText(s);
			    	settingsTimeoutDuration.addTextChangedListener(settingsTimeoutDurationListener);
				}
				if (s.length() > 2) {
			    	settingsTimeoutDuration.removeTextChangedListener(settingsTimeoutDurationListener);
					s.delete(2, s.length());
					settingsTimeoutDuration.setTextKeepState(s);
			    	settingsTimeoutDuration.addTextChangedListener(settingsTimeoutDurationListener);
				}
	        	SharedPreferences.Editor editor = settings.edit();
    			if (((s.length() < 1)||(Integer.valueOf(s.toString()) < 1))&&(settingsTimeoutChk.isChecked())) {
		        	editor.putInt(EpicFeedConsts.timeoutDuration, 5);
		        	editor.commit();
	           		mSpaUserAccount.setTimeoutDuration(5);
            	} else {
		        	editor.putInt(EpicFeedConsts.timeoutDuration, Integer.valueOf(s.toString()));
		        	editor.commit();
	           		mSpaUserAccount.setTimeoutDuration(Integer.valueOf(s.toString()));
            	}
    		}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
			}
    	};
    	settingsTimeoutDuration.addTextChangedListener(settingsTimeoutDurationListener);
    	settingsLogoutScreen.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
	        	SharedPreferences.Editor editor = settings.edit();
                if (checkedId == R.id.settingsLogoutHomeRadio) {
					editor.putInt(EpicFeedConsts.timeoutScreen, EpicFeedConsts.HOMESCREEN);
					editor.commit();
					mSpaUserAccount.setTimeoutScreen(EpicFeedConsts.HOMESCREEN);
				} else if (checkedId == R.id.settingsLogoutMainScreenRadio) {
					editor.putInt(EpicFeedConsts.timeoutScreen, EpicFeedConsts.MAINSCREEN);
					editor.commit();
					mSpaUserAccount.setTimeoutScreen(EpicFeedConsts.MAINSCREEN);
				} else if (checkedId == R.id.settingsLogoutLoginScreenRadio) {
					editor.putInt(EpicFeedConsts.timeoutScreen, EpicFeedConsts.LOGINSCREEN);
					editor.commit();
					mSpaUserAccount.setTimeoutScreen(EpicFeedConsts.LOGINSCREEN);
				};
            }
        });
    	if (!settings.contains(EpicFeedConsts.timeoutScreen)) {
        	SharedPreferences.Editor editor = settings.edit();
        	editor.putInt(EpicFeedConsts.timeoutScreen, EpicFeedConsts.HOMESCREEN);
        	editor.commit();
       		mSpaUserAccount.setTimeoutScreen(EpicFeedConsts.HOMESCREEN);
    	}
    	if (settings.getInt(EpicFeedConsts.timeoutScreen, EpicFeedConsts.HOMESCREEN) == EpicFeedConsts.MAINSCREEN) {
    		settingsHomeScreen.setChecked(false);
    		settingsMainScreen.setChecked(true);
    		settingsLoginScreen.setChecked(false);
    	} else if (settings.getInt(EpicFeedConsts.timeoutScreen, EpicFeedConsts.HOMESCREEN) == EpicFeedConsts.LOGINSCREEN) {
    		settingsHomeScreen.setChecked(false);
    		settingsMainScreen.setChecked(false);
    		settingsLoginScreen.setChecked(true);
    	} else {
    		settingsHomeScreen.setChecked(true);
    		settingsMainScreen.setChecked(false);
    		settingsLoginScreen.setChecked(false);
    	}
	}
	
	public void helpDialog () {
		closeOptionsMenu();
		final View helpTextView = mInflater.inflate(R.layout.helpview, null);
		final TextView helpText = (TextView)helpTextView.findViewById(R.id.helptext);
		helpText.setText(Html.fromHtml(
				getText(R.string.settingsHelp).toString()));
		AlertDialog ad = new AlertDialog.Builder(spaContext)
        .setTitle(R.string.settingsHelpHeader)
        .setView(helpTextView)
        .setNeutralButton(getString(R.string.sysDone), new DialogInterface.OnClickListener() {
            @Override
			public void onClick(DialogInterface dialog, int which) {
            	dialog.cancel();
            }
        })
        .create();
		ad.show();
	}
	
	public void settingsSave(View v) {
		// just finish, everything is saved
		finish();
	}
	
	public void settingsCancel(View v) {
    	SharedPreferences.Editor editor = settings.edit();
    	editor.putBoolean(EpicFeedConsts.timeoutEnable, oldTimeout);
    	editor.putInt(EpicFeedConsts.timeoutDuration, oldDuration);
    	editor.putInt(EpicFeedConsts.timeoutScreen, oldLogoutScreen);
    	editor.commit();
   		mSpaUserAccount.setTimeoutDuration(oldDuration);
   		mSpaUserAccount.setTimeoutScreen(oldLogoutScreen);
   		mSpaUserAccount.setTimeout(oldTimeout);
    	finish();
	}
	
	public void setupTimeout(View v) {
		boolean checked = settingsTimeoutChk.isChecked();
    	timeout = checked;
    	SharedPreferences.Editor editor = settings.edit();
    	editor.putBoolean(EpicFeedConsts.timeoutEnable, checked);
    	editor.commit();
    	mSpaUserAccount.setTimeout(checked);
       	settingsTimeoutDuration.setEnabled(checked);
	}
	
	@Override
    public void onConfigurationChanged(Configuration newConfig) {
    	super.onPause();
		super.onStop();
    	onCreate(this.savedState);
    }

	@Override
	public void onStop() {
		super.onStop();
		if (settings.getBoolean(EpicFeedConsts.timeoutEnable, false))
			Toast.makeText(this, getString(R.string.settingsTimeoutDurationMsg) + " " + 
					settings.getInt(EpicFeedConsts.timeoutDuration, 5), Toast.LENGTH_LONG).show();
		else
			Toast.makeText(this, getString(R.string.settingsTimeoutOffMsg), Toast.LENGTH_LONG).show();

		setResult(RESULT_CANCELED);
		finish();
	}
}