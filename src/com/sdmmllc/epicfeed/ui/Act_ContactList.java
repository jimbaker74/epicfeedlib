package com.sdmmllc.epicfeed.ui;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.json.JSONObject;

import twitter4j.User;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Email;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.Contacts;
import android.provider.ContactsContract.Contacts.Photo;
import android.provider.ContactsContract.Data;
import android.provider.ContactsContract.RawContacts;
import android.telephony.PhoneNumberUtils;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockListActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.facebook.Session;
import com.flurry.android.FlurryAgent;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;
import com.mopub.mobileads.MoPubView;
import com.sdmm.epicfeed.R;
import com.sdmmllc.epicfeed.EpicFeedController;
import com.sdmmllc.epicfeed.FlurryConsts;
import com.sdmmllc.epicfeed.SpaContactLogManager;
import com.sdmmllc.epicfeed.SpaContactManager;
import com.sdmmllc.epicfeed.SpaTextCallLog;
import com.sdmmllc.epicfeed.SpaTextConfig;
import com.sdmmllc.epicfeed.SpaTextNotification;
import com.sdmmllc.epicfeed.SpaTextSMSSetup;
import com.sdmmllc.epicfeed.SpaUserAccount;
import com.sdmmllc.epicfeed.SpaUserAccountAdapter;
import com.sdmmllc.epicfeed.SpaUserAccountListener;
import com.sdmmllc.epicfeed.ads.MoPubAdHandler;
import com.sdmmllc.epicfeed.ads.MoPubAdListener;
import com.sdmmllc.epicfeed.data.Contact;
import com.sdmmllc.epicfeed.data.ContactFeed;
import com.sdmmllc.epicfeed.data.EpicContact;
import com.sdmmllc.epicfeed.data.EpicDB;
import com.sdmmllc.epicfeed.data.EpicDBConn;
import com.sdmmllc.epicfeed.data.FacebookData;
import com.sdmmllc.epicfeed.data.FeedContactList;
import com.sdmmllc.epicfeed.data.FeedContactList.FeedContactListComparator;
import com.sdmmllc.epicfeed.data.TwitterData;
import com.sdmmllc.epicfeed.model.FacebookConnectionModel;
import com.sdmmllc.epicfeed.model.Twitter11;
import com.sdmmllc.epicfeed.utils.EpicFeedConsts;
import com.sdmmllc.epicfeed.utils.EpicGAConsts;
import com.sdmmllc.epicfeed.view.FeedTextMsg;

public class Act_ContactList extends SherlockListActivity {
	
	public static final String TAG = "Act_ContactList";
	
	private Button setupContactBtn;
	private RelativeLayout mmAdLayout;
	private MoPubView mmAdView;
	private MoPubAdListener adListener;
    private MoPubAdHandler adHandler;
	private MoPubAdHandler.ReturnAdListener returnAdListener;
    private Thread adHandlerThread;
	private EpicDB epicDB;
	private Bundle savedState;
	private Context mContext;
	private WeakReference<Activity> contactListAct;
    private LayoutInflater mInflater;
    private SpaUserAccount mSpaUserAccount;
    private ArrayList<EpicContact> contactsList;
    private ArrayList<EpicContact> phoneFbContacts, 
           						   phoneTwtContacts,
           						   phoneAllSocialNetworksContacts;
    private ArrayAdapter<EpicContact> contactsAA;
    //SpaTextContact[] contactsList;
    private EpicDB.ContactListListener contactListListener;
	private SharedPreferences settings;
	private SharedPreferences.Editor editor;
	private ProgressDialog waitDialog;
    //GoogleAnalyticsTracker googTrack;
    private FacebookConnectionModel mFacebookConn;
    private FacebookData mFacebookData;
    private Twitter11 mTwitter11;
    private TwitterData mTwitterData;
    private EasyTracker easyTracker;
    private int SOCIAL_TYPE;
    public static String 
				FEED_TYPE_FACEBOOK 	= "facebook",
				FEED_TYPE_LINKEDIN 	= "linkedIn",
				FEED_TYPE_TWITTER 	= "twitter",
				FEED_TYPE_PINTEREST	= "pinterest";
    private boolean disconnectFbFlag = false,
    				disconnectTwtFlag = false;
    private int disconnectFbIndex,
    			disconnectTwtIndex;

    private SpaUserAccountListener accountListener;
    private Contact.UpdateListener mContactUpdateListener = new Contact.UpdateListener() {
		
		@Override
		public void onUpdate(Contact updated) {
    		if (EpicFeedConsts.DEBUG_FB) Log.d(TAG, "******** onUpdate ******** Contact:" + updated.getNameAndNumber());
			EpicDBConn tmpConn = epicDB.open(true);
			EpicContact contact = getContact(updated);
			boolean found = false;
				if (Session.getActiveSession().isOpened() && phoneFbContacts != null) {
					synchronized (phoneFbContacts) {
						if (phoneFbContacts != null && phoneFbContacts.size() > 0) {
				    		for (EpicContact checkContact : phoneFbContacts) {
								if (checkContact.phoneID.equals(contact.phoneID)) {
									found = true;
									phoneFbContacts.set(phoneFbContacts.indexOf(checkContact), contact);
						    		break;
								}
							} 
						}
						if (!found) phoneFbContacts.add(contact);
		    		}
				}
				
				if (mTwitter11.isloggedin() && phoneTwtContacts != null) {
					synchronized (phoneTwtContacts) {
						if (phoneTwtContacts != null && phoneTwtContacts.size() > 0) {
							for (EpicContact check : phoneTwtContacts) {
								if (check.phoneID.equals(contact.phoneID)) {
									found = true;
									phoneTwtContacts.set(phoneTwtContacts.indexOf(check), contact);
									break;
								}
							}
						}
						if (!found) phoneTwtContacts.add(contact);
					}
				}
			
			if (epicDB.containsPhoneID(updated.getNumber())) {
				epicDB.updateContact(contact);
				epicDB.close(tmpConn);
				return;
			} else {
    			epicDB.insertContact(contact);
    		}
    		for (ContactFeed feed : contact.getFeeds()) {
    			if (!epicDB.containsContactFeed(feed))
    				epicDB.insertNewContactFeed(feed);
    		}
			epicDB.close(tmpConn);
	        runOnUiThread(updateUi);
		}
	};
	
	private Runnable updateUi = new Runnable() {

		@Override
		public void run() {
			updateUI();
		}
		
	};
    
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater menuInfl = this.getSupportMenuInflater();
		menuInfl.inflate(R.menu.contacts_menu, menu);
		//return true;
	    return super.onCreateOptionsMenu(menu);
	}
	
    public void launchMenu(View view){
        this.getWindow().openPanel(Window.FEATURE_OPTIONS_PANEL, new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_MENU));
        this.getWindow().openPanel(Window.FEATURE_OPTIONS_PANEL, new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_MENU));
    }
    
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int itemId = item.getItemId();
		if (itemId == R.id.contactsHelp) {
			helpDialog();
			return true;
		} else if (itemId == R.id.configurationEdit) {
			configurationOptions();
			return true;
		} else if (itemId == R.id.contactsCancel) {
			return true;
		} else if (itemId == android.R.id.home) {
			finish();
			return true;
		}
		return false;
	}

    @Override
    public boolean onTouchEvent(MotionEvent e) {
    	//Log.i(TAG, "Touch event outside activity, do nothing...");
        mSpaUserAccount.updateAuthActivity();
    	return super.onTouchEvent(e);
    }
    
    @Override
    public void onUserInteraction() {
    	//Log.i(TAG, "Touch event...");
        mSpaUserAccount.updateAuthActivity();
    }
    
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_contact_list);
        EpicFeedActionBar.actionbar_contacts(new WeakReference<SherlockListActivity>(this));
        savedState = savedInstanceState;
        mContext = this;
		// FlurryAgent workaround...
		try {   
			Class.forName("android.os.AsyncTask");
		}  catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		easyTracker = EasyTracker.getInstance(this);
		FlurryAgent.onStartSession(this, getString(R.string.flurryAppKey));
        FlurryAgent.onPageView();

        contactListAct = new WeakReference<Activity>(this);
        mSpaUserAccount = EpicFeedController.getSpaUserAccount(this.getApplicationContext());
		mSpaUserAccount.setLoggedIn(true);
        accountListener = new SpaUserAccountAdapter(new WeakReference<Activity>(this)) {
        	
    		@Override
    		public void timeout() {
    			mSpaUserAccount.authenticated(contactListAct);
    			stopAds();
    			destroyAds();
    			finish();
    		}
        	
        };
    	settings = getSharedPreferences(EpicFeedConsts.PREFS_NAME, 0);
    	editor = settings.edit();
    	EpicFeedController.getGaTracker(this.getApplicationContext()).set(Fields.SCREEN_NAME, TAG);
   	
        mInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    	epicDB = EpicFeedController.getEpicDB(this.getApplicationContext());

    	setupContactBtn = (Button)findViewById(R.id.contactListAddContactBtn);
        contactsList = new ArrayList<EpicContact>();
        
        mFacebookConn = new FacebookConnectionModel(new WeakReference<Activity>(this));
        mFacebookData = new FacebookData(mFacebookConn.setupFB(savedInstanceState), new EpicContact("me"));
        
        mTwitter11 = new Twitter11(this, R.string.app_name, settings);
		mTwitter11.setupTwitter();
		mTwitterData = new TwitterData(mTwitter11, new EpicContact("me"));
		
		// listener is only setup once - handles both cases
		setupContactBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
		        mSpaUserAccount.updateAuthActivity();
				if (EpicFeedConsts.debugLevel > 5) Log.i(TAG, "setupContactBtn.setOnClickListener");
				selectContact();
			}
		});
        if (EpicFeedConsts.debugLevel > 3) Log.i(TAG, "onCreate: opening and closing database");

        if (EpicFeedConsts.debugLevel > 5) 
    		Log.i(TAG, "onCreate: setting up OnItemClickListener");
        ListView lv = getListView();
		lv.setTextFilterEnabled(false);
		lv.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
				int position, long id) {
				if (EpicFeedConsts.debugLevel > 5) 
					Log.i(TAG, "ItemClickListener: text list for:" + contactsList.get(position).phoneID);
	    		if (contactsList.size() > position) {
		   	 		Intent txtConversationIntent = new Intent(mContext, Act_FeedMessageList.class);
		    		txtConversationIntent.putExtra(EpicFeedConsts.AUTH_STATUS, true);
		    		txtConversationIntent.putExtra("pID", contactsList.get(position).phoneID);
		    		txtConversationIntent.putExtra("username", contactsList.get(position).name);
		    		startActivityForResult(txtConversationIntent, EpicFeedConsts.TEXTTHREAD);
	    		} else {
	    			Toast toast = Toast.makeText(mContext, 
	    					getString(R.string.smsContactListNoContactTapMsg) , Toast.LENGTH_LONG);
	    			toast.show();
	    		}
			}
		});
    	if (EpicFeedConsts.debugLevel > 5) 
			Log.i(TAG, "OnItemLongClickListener: setting up OnItemLongClickListener");
		lv.setOnItemLongClickListener(new OnItemLongClickListener() {
			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view,
				int position, long id) {
					mSpaUserAccount.updateAuthActivity();
        			if (EpicFeedConsts.debugLevel > 5) 
	        			Log.i(TAG, "OnItemLongClickListener: itemLongHold");
		 	       		itemLongHold(contactsList.get(position));
	 	       		return true;
	 	       	}
			}
		);
    }
	
	@Override
	public void onRestart() {
		super.onRestart();
	}
	
	public ArrayList<EpicContact> getContacts() {

        ArrayList<EpicContact> contacts = new ArrayList<EpicContact>();
        final String[] projection = new String[] { RawContacts.CONTACT_ID, RawContacts.DELETED };

        @SuppressWarnings("deprecation")
        final Cursor rawContacts = managedQuery(RawContacts.CONTENT_URI, projection, null, null, null);

        final int contactIdColumnIndex = rawContacts.getColumnIndex(RawContacts.CONTACT_ID);
        final int deletedColumnIndex = rawContacts.getColumnIndex(RawContacts.DELETED);

        if (rawContacts.moveToFirst()) {
            while (!rawContacts.isAfterLast()) {
                final int contactId = rawContacts.getInt(contactIdColumnIndex);
                final boolean deleted = (rawContacts.getInt(deletedColumnIndex) == 1);

                if (!deleted) {
                    EpicContact contactInfo 
                    	= new EpicContact(PhoneNumberUtils.stripSeparators(getPhoneNumber(contactId)));
                    getName(contactId, contactInfo);
                    contactInfo.email = getEmail(contactId);
                    //contactInfo.put("photo", getPhoto(contactId) != null ? getPhoto(contactId) : "");
                    //contactInfo.put("address", getAddress(contactId));
                    contacts.add(contactInfo);
                }
                rawContacts.moveToNext();
            }
        }

        rawContacts.close();

        return contacts;
    }

    private String getName(long contactId, EpicContact contactInfo) {
        Cursor cursor = this.getContentResolver().query(ContactsContract.Data.CONTENT_URI, null, 
        		ContactsContract.Data.RAW_CONTACT_ID + "='" + contactId + "'", null, null);

        int indexGivenName = cursor.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME);
        int indexFamilyName = cursor.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.StructuredName.FAMILY_NAME);
        int indexDisplayName = cursor.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME);

        while (cursor.moveToNext()) {
        	contactInfo.name = cursor.getString(indexDisplayName);
            contactInfo.firstName = cursor.getString(indexGivenName);
            contactInfo.lastName = cursor.getString(indexFamilyName);
        }
        
        cursor.close();
		if (EpicFeedConsts.DEBUG_FB) Log.d(TAG, "******** getName ******** PhoneID:" + contactInfo.phoneID);
		if (EpicFeedConsts.DEBUG_FB) Log.d(TAG, "******** getName ******** Name:" + contactInfo.name);
        
        //final String[] projection = new String[] { Contacts.DISPLAY_NAME };
        //final Cursor contact = managedQuery(Contacts.CONTENT_URI, projection, Contacts._ID + "=?", new String[] { String.valueOf(contactId) }, null);

        //if (contact.moveToFirst()) {
        //    name = contact.getString(contact.getColumnIndex(Contacts.DISPLAY_NAME));
        //    Contacts.
        //    contact.close();
        //}
        //contact.close();
        return contactInfo.name;

    }

    private String getEmail(int contactId) {
        String emailStr = "";
        final String[] projection = new String[] { Email.DATA, // use
                // Email.ADDRESS
                // for API-Level
                // 11+
                Email.TYPE };

        final Cursor email = managedQuery(Email.CONTENT_URI, projection, Data.CONTACT_ID + "=?", new String[] { String.valueOf(contactId) }, null);

        if (email.moveToFirst()) {
            final int contactEmailColumnIndex = email.getColumnIndex(Email.DATA);

            while (!email.isAfterLast()) {
                emailStr = emailStr + email.getString(contactEmailColumnIndex) + ";";
                email.moveToNext();
            }
        }
        email.close();
        return emailStr;

    }

    private Bitmap getPhoto(int contactId) {
        Bitmap photo = null;
        final String[] projection = new String[] { Contacts.PHOTO_ID };

        final Cursor contact = managedQuery(Contacts.CONTENT_URI, projection, Contacts._ID + "=?", new String[] { String.valueOf(contactId) }, null);

        if (contact.moveToFirst()) {
            final String photoId = contact.getString(contact.getColumnIndex(Contacts.PHOTO_ID));
            if (photoId != null) {
                photo = getBitmap(photoId);
            } else {
                photo = null;
            }
        }
        contact.close();

        return photo;
    }

    private Bitmap getBitmap(String photoId) {
        final Cursor photo = managedQuery(Data.CONTENT_URI, new String[] { Photo.PHOTO }, Data._ID + "=?", new String[] { photoId }, null);

        final Bitmap photoBitmap;
        if (photo.moveToFirst()) {
            byte[] photoBlob = photo.getBlob(photo.getColumnIndex(Photo.PHOTO));
            photoBitmap = BitmapFactory.decodeByteArray(photoBlob, 0, photoBlob.length);
        } else {
            photoBitmap = null;
        }
        photo.close();
        return photoBitmap;
    }

    private String getAddress(int contactId) {
        String postalData = "";
        String addrWhere = ContactsContract.Data.CONTACT_ID + " = ? AND " + ContactsContract.Data.MIMETYPE + " = ?";
        String[] addrWhereParams = new String[] { String.valueOf(contactId), ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_ITEM_TYPE };

        Cursor addrCur = managedQuery(ContactsContract.Data.CONTENT_URI, null, addrWhere, addrWhereParams, null);

        if (addrCur.moveToFirst()) {
            postalData = addrCur.getString(addrCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.FORMATTED_ADDRESS));
        }
        addrCur.close();
        return postalData;
    }

    private String getPhoneNumber(int contactId) {

        String phoneNumber = "";
        final String[] projection = new String[] { Phone.NUMBER, Phone.TYPE, };
        final Cursor phone = managedQuery(Phone.CONTENT_URI, projection, Phone.CONTACT_ID + "=?", new String[] { String.valueOf(contactId) }, null);

        if (phone.moveToFirst()) {
            final int contactNumberColumnIndex = phone.getColumnIndex(Phone.DATA);

            while (!phone.isAfterLast()) {
                phoneNumber = phoneNumber + phone.getString(contactNumberColumnIndex) + ";";
                phone.moveToNext();
            }

        }
        phone.close();
        return phoneNumber;
    }
    
    public EpicContact getContact(Contact newContact) {
	    String whereName = ContactsContract.Data.MIMETYPE + " = ? AND " + ContactsContract.Data.CONTACT_ID + " = ?";
	    String[] whereNameParams = new String[] { 
	    		ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE,
	    		newContact.getContactId() + ""};
	    Cursor nameCur = this.getContentResolver().query(ContactsContract.Data.CONTENT_URI, null, whereName, whereNameParams, ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME);
	    
    	EpicContact contact = new EpicContact(PhoneNumberUtils.stripSeparators(newContact.getNumber()));
	    while (nameCur.moveToNext()) {
	        contact.firstName = nameCur.getString(nameCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME));
	        contact.lastName = nameCur.getString(nameCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.FAMILY_NAME));
	        contact.name = nameCur.getString(nameCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME));
	        if (contact.name == null) contact.name = contact.firstName + " " + contact.lastName;
	        contact.email = nameCur.getString(nameCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.ADDRESS));
    		if (EpicFeedConsts.DEBUG_FB) Log.w(TAG, "checking phone contact: " + contact.toString());
		 
    			if(Session.getActiveSession().isOpened()) {
        			JSONObject jsonFriend = null;	
        			if (mFacebookData.userFbFriends != null) {
        				for (int i = 0; i < mFacebookData.userFbFriends.length(); i++) {
            	    		try {
            	    	    	jsonFriend = mFacebookData.userFbFriends.getJSONObject(i);
            	    	    	
            		    		if ((jsonFriend.getString("first_name").contains(contact.firstName)
            		    				&& jsonFriend.getString("last_name").equals(contact.lastName)) 
            		    				|| (jsonFriend.getString("email") != null &&
            		    				jsonFriend.getString("email").equals(contact.email))) {
            		    			ContactFeed newFeed = new ContactFeed(contact.phoneID);
            		    			newFeed.setFeedType(ContactFeed.FEED_TYPE_FACEBOOK);
            	    				contact.fbID = jsonFriend.getString("uid");
            		    			newFeed.setFeedSystemId(contact.fbID);
            		    			contact.addFeed(newFeed);
            		    			phoneFbContacts.add(contact);
            	        	    	if (EpicFeedConsts.DEBUG_FB) Log.w(TAG, "phone contact: +++++++ FOUND MATCH!! ++++++++++");
            		    			i = mFacebookData.userFbFriends.length();
            		    			break;
            		    		}
            	        	} catch (Throwable t) {
            	        		t.printStackTrace();
            	        	    Log.e(TAG, "JSON: \"" + jsonFriend + "\"");
            	        	}
                	    }
        			}
        	    	
        		}
    			
    			if(mTwitter11.isloggedin()) {
        			User twitterContact = null;
        			if (mTwitterData.userTwtFriends != null) {
        				for (int i = 0; i < mTwitterData.userTwtFriends.length; i++) {
            				try {
            					twitterContact = mTwitterData.userTwtFriends[i];
            					
            					if((twitterContact.getName().contains(contact.nameId) &&
            							twitterContact.getScreenName().equals(contact.name))) {
            						ContactFeed newFeed = new ContactFeed(contact.phoneID);
            						newFeed.setFeedType(ContactFeed.FEED_TYPE_TWITTER);
            						contact.fbID = String.valueOf(twitterContact.getId());
            						newFeed.setFeedSystemId(contact.fbID);
            						contact.addFeed(newFeed);
            						phoneTwtContacts.add(contact);
            						if (EpicFeedConsts.DEBUG_TWT) Log.w(TAG, "phone contact: +++++++ FOUND MATCH!! ++++++++++");
            		    			i = mTwitterData.userTwtFriends.length;
            		    			break;
            					}
            				} catch (Exception e) {
            					e.printStackTrace();
            				}
            			}
        			}
        		}
    		
	    }
	    
	    nameCur.close();
	    
	    return contact;
    }
    
    public void testContacts(ArrayList<String> phoneIds) {
	
	    JSONObject jsonFriend = null;
	    EpicContact twitterContact = null;

		ArrayList<EpicContact> contactList = getContacts();

	    String whereName = ContactsContract.Data.MIMETYPE + " = ?";
	    String[] whereNameParams = new String[] { ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE };
	    Cursor nameCur = this.getContentResolver().query(ContactsContract.Data.CONTENT_URI, null, whereName, whereNameParams, ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME);
	    
	    while (nameCur.moveToNext()) {
	    	EpicContact contact = new EpicContact(nameCur.getString(nameCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)));
	        contact.firstName = nameCur.getString(nameCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME));
	        contact.lastName = nameCur.getString(nameCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.FAMILY_NAME));
	        contact.name = nameCur.getString(nameCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME));
	        if (contact.name == null) contact.name = contact.firstName + " " + contact.lastName;
	        contact.email = nameCur.getString(nameCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.ADDRESS));
    		if (EpicFeedConsts.DEBUG_FB) Log.w(TAG, "checking phone contact: " + contact.toString());
    		
    			if(Session.getActiveSession().isOpened()) {
        			for (int i = 0; i < mFacebookData.userFbFriends.length(); i++) {
        	    		try {
        	    	    	jsonFriend = mFacebookData.userFbFriends.getJSONObject(i);
        	    	    	
        		    		if ((jsonFriend.getString("first_name").equals(contact.firstName)
        		    				&& jsonFriend.getString("last_name").equals(contact.lastName)) 
        		    				|| (jsonFriend.getString("email") != null &&
        		    				jsonFriend.getString("email").equals(contact.email))) {
        	    				contact.fbID = jsonFriend.getString("uid");
        		    			phoneFbContacts.add(contact);
        	        	    	if (EpicFeedConsts.DEBUG_FB) Log.w(TAG, "phone contact: +++++++ FOUND MATCH!! ++++++++++");
        	    	    		if (EpicFeedConsts.DEBUG_FB) Log.w(TAG, "phone contact: " + contact.toString());
        		    			i = mFacebookData.userFbFriends.length();
        		    		}
        	        	} catch (Throwable t) {
        	        		t.printStackTrace();
        	        	    Log.e(TAG, "JSON: \"" + jsonFriend + "\"");
        	        	}
            	    }        			
        		}
    			
    			if(mTwitter11.isloggedin()) {
        			for (int i = 0; i < mTwitterData.userTwtContacts.size(); i++) {
        				try {
        					twitterContact = mTwitterData.userTwtContacts.get(i);
        					
        					if((twitterContact.nameId.equals(contact.nameId)) &&
        							twitterContact.name.equals(contact.name)) {
        						contact.fbID = twitterContact.fbID;
        						phoneTwtContacts.add(contact);
        						if (EpicFeedConsts.DEBUG_TWT) Log.w(TAG, "phone contact: +++++++ FOUND MATCH!! ++++++++++");
        	    	    		if (EpicFeedConsts.DEBUG_TWT) Log.w(TAG, "phone contact: " + contact.toString());
        		    			i = mTwitterData.userTwtContacts.size();
        					}
        				} catch (Exception e) {
        					e.printStackTrace();
        				}
        			}
        		}
    		
	    }
	    
	    nameCur.close();
	
	    boolean found = false;
		for (EpicContact contact : contactList) {
    		if (EpicFeedConsts.DEBUG_FB) Log.d(TAG, "************** Checking cursor phone number:" + contact.phoneID);
    		if (EpicFeedConsts.DEBUG_FB) Log.d(TAG, "************** First Name:" + contact.firstName + " Last Name: " + contact.lastName);
    		
    		found = false;
    		for (int i = 0; i < phoneIds.size(); i++) {
    			if (PhoneNumberUtils.compare(phoneIds.get(i), contact.phoneID)) {
    				found = true;
            		if (EpicFeedConsts.DEBUG_FB) Log.d(TAG, "************** Found cursor phone number:" + contact.phoneID);
    				i = phoneIds.size();
    			}
    		}
    		
            if(found) { 
        			if(Session.getActiveSession().isOpened()) {
                		for (int i = 0; i < mFacebookData.userFbFriends.length(); i++) {
                	    	try {
        	        	    	jsonFriend = mFacebookData.userFbFriends.getJSONObject(i);
        	        	    	
        	    	    		if ((jsonFriend.getString("first_name").equals(contact.firstName)
        	    	    				&& jsonFriend.getString("last_name").equals(contact.lastName)) 
        	    	    				|| (jsonFriend.getString("email") != null &&
        	    	    				jsonFriend.getString("email").equals(contact.email))) {
        		    				contact.fbID = jsonFriend.getString("uid");
        	    	    			phoneFbContacts.add(contact);
        		        	    	if (EpicFeedConsts.DEBUG_FB) Log.w(TAG, "phone contact: +++++++ FOUND MATCH!! ++++++++++");
        		    	    		if (EpicFeedConsts.DEBUG_FB) Log.w(TAG, "phone contact: " + contact.toString());
        	    	    			i = mFacebookData.userFbFriends.length();
        	    	    		}
            	        	} catch (Throwable t) {
            	        		t.printStackTrace();
            	        	    Log.e(TAG, "JSON: \"" + jsonFriend + "\"");
            	        	}
                	    }
                	}
        			
        			if(mTwitter11.isloggedin()) {
                		for (int i = 0; i < mTwitterData.userTwtContacts.size(); i++) {
                			try {
                				twitterContact = mTwitterData.userTwtContacts.get(i);
                				
                				if((twitterContact.nameId.equals(contact.nameId)) &&
            							twitterContact.name.equals(contact.name)) {
                					contact.fbID = twitterContact.fbID;
                					phoneTwtContacts.add(contact);
                					if (EpicFeedConsts.DEBUG_TWT) Log.w(TAG, "phone contact: +++++++ FOUND MATCH!! ++++++++++");
        		    	    		if (EpicFeedConsts.DEBUG_TWT) Log.w(TAG, "phone contact: " + contact.toString());
        	    	    			i = mTwitterData.userTwtContacts.size();
                				}
                			} catch (Exception e) {
                				e.printStackTrace();
                			}
                		}
                	}
        		//}
            }
		}
	    
	    updateUI();

    }
    
 	public void listenForMessages(boolean start) {
		if (start) {
			if (contactListListener == null) contactListListener = getContactListListener();
			epicDB.setContactListListener(contactListListener);
			EpicDBConn tmpConn = epicDB.open(false);
			if (epicDB.execAllUnreadMsgCount() > 0) {
				clearNotification();
				updateUI();
			}
			epicDB.close(tmpConn);
		} else {
			epicDB.removeContactListListener();
			contactListListener = null;
		}
	}
	
	public void showMe(View v) {
	 	Intent txtConversationIntent = new Intent(mContext, Act_FeedMessageList.class);
		txtConversationIntent.putExtra(EpicFeedConsts.AUTH_STATUS, true);
		txtConversationIntent.putExtra("pID", "me");
		startActivityForResult(txtConversationIntent, EpicFeedConsts.TEXTTHREAD); // 10
	}
	
	public EpicDB.ContactListListener getContactListListener() {
		return new EpicDB.ContactListListener() {

			@Override
			public void onNewMsgs() {
				if (mSpaUserAccount.authenticated(contactListAct)) {
					if (EpicFeedConsts.debugLevel > 7) 
						Log.i(TAG, "getCheckMessageListener: onNewMsg");
					updateUI();
					clearNotification();
				}
			}
		};
	}
   
	public void clearNotification() {
		// do not clear notifications if the user timed out or is not logged in
		if (mSpaUserAccount.authenticated(contactListAct)) {
			Timer clrNotify = new Timer(TAG + " Clear Notification", true);
			clrNotify.schedule(new TimerTask() { 
				@Override
				public void run() { 
					if (mSpaUserAccount.authenticated(contactListAct)) {
						((NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE))
							.cancel(SpaTextNotification.getIconId(0));
						((NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE))
							.cancel(SpaTextNotification.getIconId(1));
						((NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE))
							.cancel(SpaTextNotification.getIconId(2));
						((NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE))
							.cancel(SpaTextNotification.getIconId(3));
						((NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE))
							.cancel(SpaTextNotification.getIconId(4));
					}
				}
			}, 500);
		}
	}
	
	public boolean checkAuth(Intent appIntent) {
		if (appIntent.hasExtra(EpicFeedConsts.AUTH_STATUS)) {
			Boolean authPass = appIntent.getBooleanExtra(EpicFeedConsts.AUTH_STATUS, false);
			if (!authPass) {
				if (EpicFeedConsts.debugLevel > 2) Log.i(TAG, "checkAuth: set auth status false");
				editor.putBoolean(EpicFeedConsts.AUTH_STATUS, false);
				editor.commit();
			} else if (EpicFeedConsts.debugLevel > 2) Log.i(TAG, "checkAuth: set auth status true");

			return authPass;
		}
		return true;
	}
	
	public void itemLongHold(EpicContact tmpContact) {
        mSpaUserAccount.updateAuthActivity();
		if (editor == null) editor = settings.edit();
    	final String contactId = tmpContact.phoneID;
    	final String contactName = tmpContact.name;
    	final EpicContact contactInfo = tmpContact;
		String tmpName = PhoneNumberUtils.formatNumber(contactId);
    		if (contactName != null && contactName.length() > 0) tmpName = contactName + " / " 
    			+ tmpName;
    	final String cntctName = tmpName;
		final String[] longHoldOptions = {
				getString(R.string.smsContactLongHoldEdit),
				getString(R.string.smsContactLongHoldCall),
				getString(R.string.smsContactLongHoldDelete),
				getString(R.string.smsContactLongHoldCancel),
				getString(R.string.smsContactLongHoldHelp)
			};

		AlertDialog ad = new AlertDialog.Builder(mContext)
        .setTitle(R.string.smsContactLongHoldOptionsTitle)
        .setItems(longHoldOptions, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int modeId) {
    	        mSpaUserAccount.updateAuthActivity();
            	AlertDialog ad;
            	final View helpTextView = mInflater.inflate(R.layout.helpview_chk, null);
    			final TextView infoText = (TextView)helpTextView.findViewById(R.id.helpText);
    			final CheckBox infoTextChk = (CheckBox)helpTextView.findViewById(R.id.helpCheck);
    			final float scale = mContext.getResources().getDisplayMetrics().density;
    			infoTextChk.setPadding(infoTextChk.getPaddingLeft() + (int)(10.0f * scale + 0.5f), 
    					infoTextChk.getPaddingTop(), 
    					infoTextChk.getPaddingRight(), 
    					infoTextChk.getPaddingBottom());
            	switch (modeId) {
            	case 0 :
			    	Intent editContactIntent = new Intent(mContext, SpaTextSMSSetup.class);
			    	editContactIntent.putExtra(EpicFeedConsts.AUTH_STATUS, true);
			    	editContactIntent.putExtra("pID", contactId);
			    	startActivityForResult(editContactIntent, EpicFeedConsts.CONTACTSETUP);
			    	dialog.cancel();
            		break;
            	case 1 :
			    	final TelephonyManager telMgr = (TelephonyManager)mContext.getSystemService(TELEPHONY_SERVICE);
			    	FeedTextMsg tmpMsg = new FeedTextMsg();
			    	SpaTextCallLog tmpCallLog = new SpaTextCallLog(tmpMsg);
			    	tmpCallLog.resetCallLog(contactInfo, mContext);
			    	tmpCallLog.setCallDialed(contactInfo, mContext);
			    	Intent callContactIntent = new Intent(Intent.ACTION_CALL);
			    	callContactIntent.setData(Uri.parse("tel:"+contactId));
			    	callContactIntent.putExtra("pID", contactId);
			    	startActivityForResult(callContactIntent, EpicFeedConsts.CONTACTCALL);
			    	dialog.cancel();
            		break;
            	case 2 :
					if (EpicFeedConsts.debugLevel > 7) Log.i(TAG, "itemLongHold: Delete Contact: " + contactId);
        			infoTextChk.setPadding(infoTextChk.getPaddingLeft() + (int)(10.0f * scale + 0.5f), 
        					infoTextChk.getPaddingTop(), infoTextChk.getPaddingRight(), infoTextChk.getPaddingBottom());
        			infoTextChk.setText(mContext.getString(R.string.deleteContactLockedTxt));
        			infoTextChk.setVisibility(View.GONE);
        			infoText.setText(Html.fromHtml(mContext.getString(R.string.deleteContactHelp)));
        			ad = new AlertDialog.Builder(mContext)
        	        .setTitle(mContext.getString(R.string.deleteContactHelpHeader))
        	        .setView(helpTextView)
					.setPositiveButton(mContext.getString(R.string.deleteContactYesBtn), new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int id) {
		        	        mSpaUserAccount.updateAuthActivity();
			        		EpicDBConn tmpConn = epicDB.open(true);
			            	epicDB.deleteTxtThread(contactId, true);
			            	epicDB.deleteNotification(contactId);
			            	epicDB.deletePhoneID(contactId);
			            	epicDB.close(tmpConn);
		 	        		Toast toast = Toast.makeText(mContext, getString(R.string.smsSetupDelContactMsgStart)
		 	        				+ " " + cntctName + " " 
		 	        				+ getString(R.string.smsSetupDelContactMsgEnd), Toast.LENGTH_LONG);
			        		toast.show();
		 	            	setResult(Activity.RESULT_OK);
		 	            	dialog.cancel();
							updateUI();
							if (EpicFeedConsts.debugLevel > 8) Log.i(TAG, "itemLongHold: resetting monitors");
						}
					})
					.setNegativeButton(mContext.getString(R.string.deleteContactNoBtn), new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int id) {
		        	        mSpaUserAccount.updateAuthActivity();
							dialog.cancel();
						}
					})
			        .create();
					ad.show();
            		break;
            	case 3 :
            		break;
            	case 4 :
            		infoTextChk.setVisibility(View.GONE);
            		infoText.setText(Html.fromHtml(
            				getText(R.string.smsContactLongHoldHelpBody).toString()));
            		ad = new AlertDialog.Builder(mContext)
                    .setTitle(R.string.smsContactLongHoldHelpHeader)
                    .setView(helpTextView)
                    .setNeutralButton(getString(R.string.smsContactLongHoldHelpDone), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            mSpaUserAccount.updateAuthActivity();
                        	dialog.cancel();
                        }
                    })
                    .create();
            		ad.show();
            		break;
            	}
            }
        })
        .create();
		ad.show();

	}
	
	public void itemLongHoldNew(EpicContact tmpContact) {
		if (editor == null) editor = settings.edit();
    	final String contactId = tmpContact.phoneID;
    	final String contactName = tmpContact.name;
		String tmpName = PhoneNumberUtils.formatNumber(contactId);
    		if (contactName != null && contactName.length() > 0) tmpName = contactName + " / " 
    			+ tmpName;
    	final String cntctName = tmpName;
    	final boolean fakeEnabled = tmpContact.isFake();
		final String[] longHoldOptions = {
				getString(R.string.smsContactLongHoldEdit),
				getString(R.string.smsContactLongHoldDelete),
				getString(R.string.smsContactLongHoldFakeMsgs),
				getString(R.string.smsContactLongHoldDeleteAll),
				getString(R.string.smsContactLongHoldCancel),
				getString(R.string.smsContactLongHoldHelp)
			};

		AlertDialog ad = new AlertDialog.Builder(mContext)
        .setTitle(R.string.smsContactLongHoldOptionsTitle)
        .setItems(longHoldOptions, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int modeId) {
    	        mSpaUserAccount.updateAuthActivity();
            	AlertDialog ad;
            	final View helpTextView = mInflater.inflate(R.layout.helpview_chk, null);
    			final TextView infoText = (TextView)helpTextView.findViewById(R.id.helpText);
    			final CheckBox infoTextChk = (CheckBox)helpTextView.findViewById(R.id.helpCheck);
    			final float scale = mContext.getResources().getDisplayMetrics().density;
    			infoTextChk.setPadding(infoTextChk.getPaddingLeft() + (int)(10.0f * scale + 0.5f), 
    					infoTextChk.getPaddingTop(), infoTextChk.getPaddingRight(), infoTextChk.getPaddingBottom());
            	switch (modeId) {
            	case 0 :
			    	Intent editContactIntent = new Intent(mContext, SpaTextSMSSetup.class);
			    	editContactIntent.putExtra(EpicFeedConsts.AUTH_STATUS, true);
			    	editContactIntent.putExtra("pID", contactId);
			    	startActivityForResult(editContactIntent, EpicFeedConsts.CONTACTSETUP);
			    	dialog.cancel();
            		break;
            	case 1 :
					if (EpicFeedConsts.debugLevel > 7) Log.i(TAG, "itemLongHold: Delete Contact: " + contactId);
        			infoTextChk.setPadding(infoTextChk.getPaddingLeft() + (int)(10.0f * scale + 0.5f), 
        					infoTextChk.getPaddingTop(), infoTextChk.getPaddingRight(), infoTextChk.getPaddingBottom());
        			infoTextChk.setText(mContext.getString(R.string.deleteThreadLockedTxt));
        			infoText.setText(Html.fromHtml(mContext.getString(R.string.deleteThreadHelp)));
        			ad = new AlertDialog.Builder(mContext)
        	        .setTitle(mContext.getString(R.string.deleteThreadHelpHeader))
        	        .setView(helpTextView)
					.setPositiveButton(mContext.getString(R.string.deleteThreadYesBtn), new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int id) {
		        	        mSpaUserAccount.updateAuthActivity();
			        		EpicDBConn tmpConn = epicDB.open(true);
			            	epicDB.deleteTxtThread(contactId, true);
			            	epicDB.deleteNotification(contactId);
			            	epicDB.deletePhoneID(contactId);
			            	epicDB.close(tmpConn);
		 	        		Toast toast = Toast.makeText(mContext, getString(R.string.smsSetupDelContactMsgStart)
		 	        				+ " " + cntctName + " "
		 	        				+ getString(R.string.smsSetupDelContactMsgEnd), Toast.LENGTH_LONG);
			        		toast.show();
		 	            	setResult(Activity.RESULT_OK);
		 	            	finish();
							updateUI();
						}
					})
					.setNegativeButton(mContext.getString(R.string.deleteThreadNoBtn), new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int id) {
		        	        mSpaUserAccount.updateAuthActivity();
							dialog.cancel();
						}
					})
			        .create();
					ad.show();
            		break;
            	case 2 :
					AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
					builder
					.setTitle(getString(R.string.smsContactLongHoldFakeSetupTitle))
					.setMessage(getString(R.string.smsContactLongHoldFakeSetupBody))
					.setCancelable(false)
					.setPositiveButton(getString(R.string.smsContactLongHoldFakeSetupYes)
							, new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int id) {
					        mSpaUserAccount.updateAuthActivity();
					    	Intent editContactIntent = new Intent(mContext, SpaTextSMSSetup.class);
					    	editContactIntent.putExtra(EpicFeedConsts.AUTH_STATUS, true);
					    	editContactIntent.putExtra("pID", contactId);
					    	startActivityForResult(editContactIntent, EpicFeedConsts.CONTACTSETUP);
						}
					})
					.setNegativeButton(getString(R.string.smsContactLongHoldFakeSetupCancel)
							, new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int id) {
					        mSpaUserAccount.updateAuthActivity();
							dialog.cancel();
						}
					});
					AlertDialog alert = builder.create();
					alert.show();
        	        mSpaUserAccount.updateAuthActivity();
            		break;
            	case 3 :
        	        mSpaUserAccount.updateAuthActivity();
            		break;
            	case 4 :
        	        mSpaUserAccount.updateAuthActivity();
            		break;
            	case 5 :
            		infoTextChk.setVisibility(View.GONE);
            		infoText.setText(Html.fromHtml(
            				getText(R.string.smsContactLongHoldHelpBody).toString()));
            		ad = new AlertDialog.Builder(mContext)
                    .setTitle(R.string.smsContactLongHoldHelpHeader)
                    .setView(helpTextView)
                    .setNeutralButton(getString(R.string.smsContactLongHoldHelpDone), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            mSpaUserAccount.updateAuthActivity();
                        	dialog.cancel();
                        }
                    })
                    .create();
            		ad.show();
        	        mSpaUserAccount.updateAuthActivity();
            		break;
            	}
            }
        })
        .create();
		ad.show();

	}
	
	public void contactSelect(View view) {
		int position = Integer.valueOf((String) view.getTag());
	}

	
	private ArrayAdapter<EpicContact> getArrayAdapter(Context context, ArrayList<EpicContact> contactAA) {
		return new ArrayAdapter<EpicContact>(context, R.layout.contact_list_item, contactAA){
			@Override
			public View getView(final int position, View convertView, ViewGroup parent) {
				View row;
				if (null == convertView) {
					row = mInflater.inflate(R.layout.contact_list_item, parent, false);
				} else {
					row = convertView;
				}
				row.setOnLongClickListener(new OnLongClickListener() {
					@Override
					public boolean onLongClick(View view) {
			        		if (EpicFeedConsts.debugLevel > 5) 
			        			Log.i(TAG, "OnItemLongClickListener: itemLongHold");
			 	       		itemLongHold(contactsList.get(position));
			 	       		return true;
			 	       	}
					}
				);
				row.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View view) {
				        mSpaUserAccount.updateAuthActivity();
						if (EpicFeedConsts.debugLevel > 5) 
							Log.i(TAG, "ItemClickListener: text list for:" + contactsList.get(position).phoneID);
			    		if (contactsList.size() > position) {
				   	 		Intent txtConversationIntent = new Intent(mContext, Act_FeedMessageList.class);
				    		txtConversationIntent.putExtra(EpicFeedConsts.AUTH_STATUS, true);
				    		txtConversationIntent.putExtra("pID", contactsList.get(position).phoneID);
				    		txtConversationIntent.putExtra("uid", contactsList.get(position).fbID);
				    		txtConversationIntent.putExtra("username", contactsList.get(position).name);
				    		startActivityForResult(txtConversationIntent, EpicFeedConsts.TEXTTHREAD);
			    		} else {
			    			Toast toast = Toast.makeText(mContext, 
			    					getString(R.string.smsContactListNoContactTapMsg) , Toast.LENGTH_LONG);
			    			toast.show();
			    		}
					}
				});
				// TODO
				ImageButton addSocialBtn = (ImageButton) row.findViewById(R.id.contactEpicImg);
				addSocialBtn.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View view) {
				        mSpaUserAccount.updateAuthActivity();
						if (EpicFeedConsts.debugLevel > 5) 
							Log.i(TAG, "ItemClickListener: text list for:" + contactsList.get(position).phoneID);
						
						if (disconnectFbFlag) {
				        	for (EpicContact contact : contactsList) {
				        		if (contact.phoneID == getIntent().getStringExtra("pID")) {
				        			if (contact.getFeed(FEED_TYPE_FACEBOOK) != null) {
				        				contact.removeFeed(contact.getFeed(FEED_TYPE_FACEBOOK));
				        				break;
					        		}
				        		}
				        	}
						}
						
						if (disconnectTwtFlag) {
							for (EpicContact contact : contactsList) {
								if (contact.phoneID == getIntent().getStringExtra("pID")) {
									if (contact.getFeed(FEED_TYPE_TWITTER) != null) {
										contact.removeFeed(contact.getFeed(FEED_TYPE_TWITTER));
										break;
									}
								}
							}
						}
						
						if (contactsList.get(position).getFeed(FEED_TYPE_FACEBOOK) != null) {
							SOCIAL_TYPE = EpicFeedConsts.FACEBOOK_DATA;
						}
						
						if (contactsList.get(position).getFeed(FEED_TYPE_TWITTER) != null) {
							if (SOCIAL_TYPE == EpicFeedConsts.FACEBOOK_DATA)
								SOCIAL_TYPE = EpicFeedConsts.ALL_SOCIAL_NETWORKS;
							else
								SOCIAL_TYPE = EpicFeedConsts.TWITTER_DATA;
						}
						
						if (SOCIAL_TYPE != EpicFeedConsts.FACEBOOK_DATA && 
							SOCIAL_TYPE != EpicFeedConsts.TWITTER_DATA &&
							SOCIAL_TYPE != EpicFeedConsts.ALL_SOCIAL_NETWORKS)
							SOCIAL_TYPE = EpicFeedConsts.NO_DATA;
						
					 	Intent txtConversationIntent = new Intent(mContext, Act_Authenticate.class);
						txtConversationIntent.putExtra(EpicFeedConsts.AUTH_STATUS, true);
						txtConversationIntent.putExtra("social_type", SOCIAL_TYPE);
						txtConversationIntent.putExtra("pID", contactsList.get(position).phoneID);
						txtConversationIntent.putExtra("uid", contactsList.get(position).fbID);
			    		txtConversationIntent.putExtra("username", contactsList.get(position).name);
			    		txtConversationIntent.putExtra("position", position);
						startActivityForResult(txtConversationIntent, EpicFeedConsts.AUTHENTICATE);
					}
				});
				if (contactsList.get(position).contactFeeds != null && contactsList.get(position).contactFeeds.size() > 0) {
					if (/*contactsList.get(position).contactFeeds.get(position).getFeedType().equals("facebook") &&
						contactsList.get(position).contactFeeds.get(position).getFeedType().equals("twitter")*/
						contactsList.get(position).contactFeeds.size() == 2) {
						addSocialBtn.setBackground(this.getContext().getResources().getDrawable(R.drawable.btn_search));
					} else if (contactsList.get(position).contactFeeds.get(0).getFeedType().equals("facebook")) {
						addSocialBtn.setBackground(this.getContext().getResources().getDrawable(R.drawable.btn_feed_fb));
					} else if (contactsList.get(position).contactFeeds.get(0).getFeedType().equals("twitter")) {
						addSocialBtn.setBackground(this.getContext().getResources().getDrawable(R.drawable.connect_twitter_icon));
					}
				/**} else if (disconnectFbFlag || disconnectTwtFlag) {
					if (disconnectFbFlag && disconnectTwtFlag)
						addSocialBtn.setBackground(this.getContext().getResources().getDrawable(R.drawable.btn_feed));
					else if (disconnectFbFlag) {
 						addSocialBtn.setBackground(this.getContext().getResources().getDrawable(R.drawable.btn_feed));
					} else if (disconnectTwtFlag) 
 						addSocialBtn.setBackground(this.getContext().getResources().getDrawable(R.drawable.btn_feed));**/
				} else addSocialBtn.setBackground(this.getContext().getResources().getDrawable(R.drawable.btn_feed));

				TextView contactName = (TextView) row.findViewById(R.id.contactItem);
				TextView contactDescr = (TextView) row.findViewById(R.id.contactItemDescr);
				contactName.setTag(position);
				EpicContact contact = getItem(position);
				String cName = "", cDescr = "";
				if (contact.name.length() > 0) cName =  contact.name + "\n" +
						"(" + PhoneNumberUtils.formatNumber(contact.phoneID) + ")";
				else cName =  PhoneNumberUtils.formatNumber(contact.phoneID);
				if (contact.msgCount <= 0) {
					contactName.setText(cName);
					if (cDescr.length() > 0) contactDescr.setText(Html.fromHtml(cDescr));
					else contactDescr.setText(mContext.getText(R.string.smsNewContactLblTxt));
				} else if (contact.newMsgs()) {
					String newMsgs = "";
					if (contact.newMsgCount > 0) {
						if (contact.newMsgCount > 1) newMsgs = contact.newMsgCount + " " + mContext.getText(R.string.sysMsgNewMsgs);
						else newMsgs = contact.newMsgCount + " " + mContext.getText(R.string.sysMsgNewMsg);
						if (contact.missedCallsCount > 0|
								contact.sendErrCount > 0|
								contact.deliveryErrCount > 0) newMsgs = newMsgs + " & ";
					}
					if ((contact.missedCallsCount > 0)&&(!contact.isCallBlocked())) {
						newMsgs = newMsgs + contact.missedCallsCount;
						if (contact.missedCallsCount > 1)  newMsgs = newMsgs + " " + mContext.getText(R.string.sysMsgNewMissedCalls);
						else newMsgs = newMsgs + " " + mContext.getText(R.string.sysMsgNewMissedCall);
						if (contact.sendErrCount > 0|
								contact.deliveryErrCount > 0) newMsgs = newMsgs + " & ";
					}
					if (contact.sendErrCount > 0) {
						newMsgs = newMsgs + contact.sendErrCount;
						if (contact.sendErrCount > 1)  newMsgs = newMsgs + " " + mContext.getText(R.string.sysMsgSendErrorsMsg);
						else newMsgs = newMsgs + " " + mContext.getText(R.string.sysMsgSendErrorMsg);
						if (contact.deliveryErrCount > 0) newMsgs = newMsgs + " & ";
					}
					if (contact.deliveryErrCount > 0) {
						newMsgs = newMsgs + contact.deliveryErrCount;
						if (contact.deliveryErrCount > 1)  newMsgs = newMsgs + " " + mContext.getText(R.string.sysMsgDeliveryErrorsMsg);
						else newMsgs = newMsgs + " " + mContext.getText(R.string.sysMsgDeliveryErrorMsg);
					}
					contactName.setText(cName); 
					contactDescr.setText(Html.fromHtml("<b><i>"+newMsgs+"</i></b>" + cDescr));
				} else {
					contactName.setText(cName);
					if (cDescr.length() > 0) contactDescr.setText(Html.fromHtml(cDescr));
					else contactDescr.setText(getText(R.string.smsTextMsgNoNewMsgs));
				}
				return row;
			}
		};
	}
	
	public void helpDialog () {
		closeOptionsMenu();
        easyTracker.send(MapBuilder
                .createEvent(EpicFeedConsts.gat_Button,     	// Event category (required)
                		EpicFeedConsts.gat_Click,			  	// Event action (required)
                		EpicFeedConsts.gatContactListHelp,	   			// Event label
                        1L)            						// Event value
                .build()
            );
		final View helpTextView = mInflater.inflate(R.layout.helpview, null);
		final TextView helpText = (TextView)helpTextView.findViewById(R.id.helptext);
		helpText.setText(Html.fromHtml(
				getText(R.string.contactsHelp).toString()));
		AlertDialog ad = new AlertDialog.Builder(mContext)
        .setTitle(R.string.contactsHelpHeader)
        .setView(helpTextView)
        .setNeutralButton(getString(R.string.helpDoneBtnTxt), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                mSpaUserAccount.updateAuthActivity();
            	dialog.cancel();
            }
        })
        .create();
		ad.show();
	}
	
	public void spaSMSSetup (View v) {
		setupContact();
	}
	
	public void selectContact() {
		final String[] notifyModes = {
				getString(R.string.smsContactAddContactList),
				getString(R.string.smsContactAddCallLogs),
				getString(R.string.smsContactAddNewContact),
				getString(R.string.smsContactAddContactCancel)	
			};
		AlertDialog ad = new AlertDialog.Builder(mContext)
        .setTitle(R.string.smsContactAddContactMenuTitle)
        .setItems(notifyModes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int modeId) {
                mSpaUserAccount.updateAuthActivity();
            	switch (modeId) {
            	case 0 :
                	Intent setupContactIntent = new Intent(mContext, SpaContactManager.class);
                	setupContactIntent.putExtra(EpicFeedConsts.AUTH_STATUS, true);
                	startActivityForResult(setupContactIntent, EpicFeedConsts.CONTACTSELECT);
            		break;
            	case 1 :
                	Intent setupContactLogIntent = new Intent(mContext, SpaContactLogManager.class);
                	setupContactLogIntent.putExtra(EpicFeedConsts.AUTH_STATUS, true);
                	startActivityForResult(setupContactLogIntent, EpicFeedConsts.CONTACTLOGSELECT);
            		break;
            	case 2 :
            	   	Intent spaSMSSetupIntent = new Intent(mContext, SpaTextSMSSetup.class);
            		startActivityForResult(spaSMSSetupIntent, EpicFeedConsts.CONTACTSETUP);
            		break;
            	case 3 :
            		dialog.cancel();
            	}
            }
        })
        .create();
		ad.show();
	}
	
	public void setupContact() {
        mSpaUserAccount.updateAuthActivity();
	   	Intent spaSMSSetupIntent = new Intent(this, SpaTextSMSSetup.class);
		startActivityForResult(spaSMSSetupIntent, EpicFeedConsts.CONTACTSETUP);
	}
	

	public void configurationOptions() {
        mSpaUserAccount.updateAuthActivity();
	   	Intent configIntent = new Intent(this, SpaTextConfig.class);
		startActivityForResult(configIntent, EpicFeedConsts.CONFIG);
	}
	
	public void launchConfiguration(View v) {
		configurationOptions();
	}

 	public void cancelNotification(Context context) {
		((NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE))
			.cancel(R.drawable.notification_icon);
		((NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE))
			.cancel(R.drawable.notification_icon2);
		((NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE))
			.cancel(R.drawable.notification_icon3);
		((NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE))
			.cancel(R.drawable.notification_icon4);
		((NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE))
			.cancel(R.drawable.notification_icon5);
 	}
 	
	private List<EpicContact> orderContacts(List<EpicContact> allContacts, String[] orderedContacts) {
		List<EpicContact> tmpAllContacts = new ArrayList<EpicContact>();
		if (FeedContactList.threadOrder.size() > 0) {
			for (EpicContact tmpContact:allContacts) {
				tmpContact.threadDt = FeedContactList.threadOrder.get(tmpContact.phoneID);
				if (tmpContact.threadDt == null) tmpContact.threadDt = 0L;
				tmpAllContacts.add(tmpContact);
			}
            Collections.sort(tmpAllContacts, FeedContactListComparator.descending(FeedContactListComparator.THREAD_DATE));
            return tmpAllContacts;
		}
		// put the ordered contacts in
		for (int i = 0; i < orderedContacts.length; i++) {
			if (EpicFeedConsts.debugLevel > 9) Log.i(TAG, "checking ordered contact phoneID: " + orderedContacts[i]);
			for (EpicContact tmpContact:allContacts) {
				if (EpicFeedConsts.debugLevel > 9) Log.i(TAG, "checking all contacts phoneID: " + orderedContacts[i]);
				if (tmpContact.phoneID.equals(orderedContacts[i])) {
					tmpAllContacts.add(tmpContact);
				}
			}
		}
		if (EpicFeedConsts.debugLevel > 9) Log.i(TAG, "ordered contacts done");
		// any non-ordered
		boolean found = false;
		for (EpicContact tmpContact:allContacts){
			for (EpicContact tmpContact2:tmpAllContacts){
				if (tmpContact.phoneID.equals(tmpContact2.phoneID)) {
					found = true;
				}
			}
			if (!found) tmpAllContacts.add(tmpContact);
			found = false;
		}
		return tmpAllContacts;
	}
	
	private void updateUI(){
		Log.i(TAG, "+++++ in updateUI()");
		if (EpicFeedConsts.debugLevel > 7) Log.i(TAG, "updateUI: updating UI");
        EpicDBConn tmpConn = epicDB.open(true);
        List<EpicContact> newList = epicDB.allContacts();
        String[] contactOrder = epicDB.getContactOrder();
        newList = orderContacts(newList, contactOrder);
        // TODO
        
        contactsList.clear();
        for (EpicContact tmpContact:newList) {
    		int[] newMsgs = epicDB.execNewMsgCount(tmpContact.phoneID);
    		tmpContact.newMsgCount = newMsgs[0];
    		tmpContact.missedCallsCount = newMsgs[1];
    		tmpContact.sendErrCount= newMsgs[2];
    		tmpContact.deliveryErrCount = newMsgs[3];
    		tmpContact.msgCount = epicDB.execMsgCount(tmpContact.phoneID);
    		tmpContact.setContactFeeds(epicDB.getContactFeeds(tmpContact.phoneID));
    		if (EpicFeedConsts.DEBUG_FB) Log.d(TAG, "************** contact feed size: " + tmpContact.getFeeds().size());
    		contactsList.add(tmpContact);
    		if (EpicFeedConsts.debugLevel > 7) 
    			Log.i(TAG, "updateUI: new txts: "
    			+ newMsgs[0] + " missed calls:" + newMsgs[1]);
        }
        
        //if (EpicFeedConsts.DEBUG_FB) {
        //	if (EpicFeedConsts.DEBUG_FB && phoneFbContacts != null) Log.i(TAG, "updating phoneFbContacts for " + phoneFbContacts.size() + " contacts");
        //	if (phoneFbContacts != null && phoneFbContacts.size() > 0) contactsList = phoneFbContacts;
        //}
        contactsAA = getArrayAdapter(mContext, contactsList);
        setListAdapter(contactsAA);
		cancelNotification(this);
		epicDB.close(tmpConn);
		//setAddContactBtn();
	}
	
    @Override
    protected void onActivityResult(int requestCode, int resultCode,
    		Intent data) {
    	
    	if (requestCode == EpicFeedConsts.TEXTTHREAD || requestCode == EpicFeedConsts.CONTACTSELECT
    			|| requestCode == EpicFeedConsts.CONTACTLOGSELECT || requestCode == EpicFeedConsts.CONTACTSETUP) {
    	}
    	if (requestCode == EpicFeedConsts.CONTACTCALL) {
    		if (resultCode == RESULT_OK) {
    			finish();
    		}
    	}
    	if (requestCode == EpicFeedConsts.ABOUT_PRIVACY) {
    		if (!settings.getBoolean(EpicFeedConsts.privacyNoticeStatus, false)) {
    			finish();    			
    		}
    	}
    	// if the activity didn't finish from a contact call, update the UI
		updateUI();
    	super.onActivityResult(requestCode, resultCode, data);
    }
    
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
		if (EpicFeedConsts.debugLevel > 2) Log.i(TAG, "onConfigurationChanged: screen orientation changed");
    	onPause();
		if (EpicFeedConsts.debugLevel > 2) Log.i("SpaTextList.onConfigurationChanged", "onPause done... new onStop...");
		onStop();
		destroyAds();
    	onCreate(this.savedState);
    	onStart();
    	onResume();
    }
    
    @Override
    public void onStart() {
    	super.onStart();
		EasyTracker.getInstance(this).activityStart(this);  
		FlurryAgent.onStartSession(this, getString(R.string.flurryAppKey));
		FlurryAgent.onPageView();
    	FlurryAgent.logEvent(FlurryConsts.PAGE_VIEW_EVENT, FlurryConsts.getMap(FlurryConsts.PAGE_VIEW_EVENT, TAG));
    	
    	Session.getActiveSession().addCallback(mFacebookConn.getStatusCallback());
           
    	if (!settings.getBoolean(EpicFeedConsts.privacyNoticeStatus, false)) {
   	 		Intent privacyIntent = new Intent(mContext, Act_AcceptTerms.class);
    		startActivityForResult(privacyIntent, EpicFeedConsts.ABOUT_PRIVACY);
    	}
   }
    
    @Override
    protected void onNewIntent(Intent intent) {
    	super.onNewIntent(intent);
    	// TODO
    	String phoneNumber = null;
    	
    	if (intent.hasExtra("pID")) phoneNumber = intent.getStringExtra("pID");
    	
    	if (intent.hasExtra("disconnect_fb")) {
    		if (intent.getBooleanExtra("disconnect_fb", false)) {
            	for (EpicContact contact : contactsList) {
            		if (phoneNumber != null && contact.phoneID.equals(phoneNumber)) {
            			if (contact.getFeed(FEED_TYPE_FACEBOOK) != null) {
            				contact.removeFeed(contact.getFeed(FEED_TYPE_FACEBOOK));
            				break;
    	        		}
            		}
            	}
    		}
    	}
    	
    	if (intent.hasExtra("disconnect_twt")) {
    		if (intent.getBooleanExtra("disconnect_twt", false)) {
    			for (EpicContact contact : contactsList) {
    				if (phoneNumber != null && contact.phoneID.equals(phoneNumber)) {
    					if (contact.getFeed(FEED_TYPE_TWITTER) != null) {
    						contact.removeFeed(contact.getFeed(FEED_TYPE_TWITTER));
    						break;
    					}
    				}
    			}
    		}
    	}
    	
    	updateUI();
    }

	@Override
    public void onResume() {
		super.onResume();
		
		if (!Session.getActiveSession().isOpened() && !mTwitter11.isloggedin()) {
    		startActivityForResult(new Intent(mContext, Act_Authenticate.class),
    				EpicFeedConsts.AUTHENTICATE);
			//contactsList = getContacts();
        } else {
    			if (Session.getActiveSession().isOpened()) {
            		phoneFbContacts = new ArrayList<EpicContact>();
                	mFacebookData.getFacebookFriends(mContactUpdateListener);
            	}
            	
            	if (mTwitter11.isloggedin()) {
            		phoneTwtContacts = new ArrayList<EpicContact>();
            		mTwitterData.getTwitterFollowers(mContactUpdateListener);
            	}
        	
        }	
        
        mSpaUserAccount.updateAuthActivity();
		startAds();
		resumeAds();
        mSpaUserAccount.setSpaUserAccountListener(accountListener);
        listenForMessages(true);
        updateUI();
		
    	easyTracker.send(MapBuilder
                .createEvent(EpicGAConsts.USER_ACTION_CATEGORY,     			// Event category (required)
                			EpicGAConsts.USER_MESSAGING,			  		// Event action (required)
                			EpicGAConsts.USER_VIEW_CONTACT_LIST,	   			// Event label
                            1L)            						// Event value
                .build()
            );
	}
	
	public void setupAdHandler() {
		if (getResources().getBoolean(R.bool.solitaireLicense)) {
			return;
		}
		if (EpicFeedConsts.debugLevel > 7) 
			Log.i(TAG, "setupAdHandler: setting up..");
		if (adListener == null) adListener = new MoPubAdListener(TAG, new WeakReference<Activity>(this), mmAdLayout);
		mmAdView.setBannerAdListener(adListener);
		//if (returnAdListener == null) returnAdListener = new AdHandler.ReturnAdListener() {
		returnAdListener = new MoPubAdHandler.ReturnAdListener() {
			@Override
			public void onGetNewAd() {
				runOnUiThread(returnAd);
			}
		};
		if (adHandler == null) {
			adHandler = new MoPubAdHandler(this, TAG + " null", mmAdView, mmAdLayout, adListener, returnAdListener);
		} else {
			if ((adHandler.getAdView() == null) || !adHandler.getAdView().equals(mmAdView)) {
				adHandler.setAdView(mmAdView);
			}
			if ((adHandler.getAdListener() == null) || !adHandler.getAdListener().equals(adListener)) {
				adHandler.setAdListener(adListener);
			}
			if ((adHandler.getReturnAdListener() == null) || !adHandler.getReturnAdListener().equals(returnAdListener)) {
				adHandler.setReturnAdListener(returnAdListener);
			}
		}
	}
	
	public void startAds() {
		if (getResources().getBoolean(R.bool.solitaireLicense)) {
			if (mmAdView == null) {
				mmAdView = (MoPubView)findViewById(R.id.mmadContactsList);
			}
			mmAdView.setVisibility(View.GONE);
			return;
		}
		
		if (mmAdLayout == null) mmAdLayout = (RelativeLayout)findViewById(R.id.contactsMMAdLayout);
		if (mmAdView == null) {
			mmAdView = (MoPubView)findViewById(R.id.mmadContactsList);
			mmAdView.setAdUnitId(getString(R.string.mopubAdID));
			mmAdView.setVisibility(View.GONE);
		}
        setupAdHandler();
		
		if (EpicFeedConsts.debugLevel > 7) 
			Log.i(TAG, "startAds: starting..");
		adHandler.resumeAds();
		adHandler.checkAd = true;
        if ((adHandlerThread == null)||(adHandlerThread.getState() == Thread.State.TERMINATED)
        		||!adHandlerThread.isAlive())
            	adHandlerThread = new Thread(null, adHandler, "Ad Handler Thread : " + TAG);
        if (adHandlerThread.getState() == Thread.State.NEW) {
        	adHandlerThread.start();
        }
        //adSizeCheck = new Thread(null, adSizeChecker, "Ad Size Checker");
        //adSizeCheck.start();
        //if (SpaTextConsts.debugLevel > 0) SpaTextApp.memoryInfo(spaContext, TAG);
	}

	private Runnable returnAd = new Runnable() {
		@Override
		public void run () {
			if (adHandler != null) {
				synchronized (adHandler.isBeingDestroyed) {
					if (adHandler.isBeingDestroyed) adListener.needAd = true;
				}
		    	if (EpicFeedConsts.debugLevel > 0) 
		    		Log.i(TAG, "returnAd thread: Got new ad...");
	    		Log.i("Ads", "SPA: requesting new ad " + TAG);
			} else {
	    		Log.i(TAG, "returnAd thread: adHandler dead");
			}
		}
	};
	
	public void resumeAds() {
		if (EpicFeedConsts.debugLevel > 2) Log.i(TAG, "resumeAds: setting state flags");
        adHandler.resumeAds();
		adHandler.checkAd = true;
	}
	
	public void stopAds() {
		if (EpicFeedConsts.debugLevel > 7) 
			Log.i(TAG, "stopAds: stopping..");
		if (adHandler != null) {
	        adHandler.stopAds();
		}
        try {
			synchronized (adHandler.isBeingDestroyed) {
				if (!adHandler.isBeingDestroyed) adListener.needAd = false;
			}
        } catch (Exception e) {
        	// don't worry about an error
        }
	}
	
	public void destroyAds() {
		if (EpicFeedConsts.debugLevel > 7) 
			Log.i(TAG, "destroyAds: destroying..");
        try {
			synchronized (adHandler.isBeingDestroyed) {
				adHandler.isBeingDestroyed = true;
			}
        } catch (Exception e) {
        	// don't worry about an error
        }
        try {
			synchronized (adHandler.isBeingDestroyed) {
				if (!adHandler.isBeingDestroyed) adListener.needAd = false;
			}
        } catch (Exception e) {
        	// don't worry about an error
        }
        
        try {
	        if (adHandler.getAdView() != null)
	    		try {
	    			((ViewGroup)adHandler.getAdView().getParent()).removeView(adHandler.getAdView());
	    		} catch (Exception e) {
	    		}
	
			//adView = null;
	
	    	if (adHandler != null) 
				try {
					synchronized (adHandler.isBeingDestroyed) {
						adHandler.isBeingDestroyed = true;
						adHandler.checkAd = false;
						adHandler.stopAdTimer();
				        adHandler.destroyAds();
				        mmAdView = null;
						adHandler = null;
					}
				} catch (Exception e) {
					// do nothing, ad will go away
				}
        } catch (Exception e) {
        	// do nothing, destroying anyway
        }
    }
	
	@Override
	public void onPause() {
        stopAds();
        listenForMessages(false);
        mSpaUserAccount.removeSpaUserAccountListener(accountListener);
		super.onPause();
	}

	@Override
	public void onStop() {
        if (adHandler != null) adHandler.stopAds();
		setResult(RESULT_OK);
		super.onStop();
		
		Session.getActiveSession().removeCallback(mFacebookConn.getStatusCallback());
            
		
		EasyTracker.getInstance(this).activityStop(this);  
        FlurryAgent.onEndSession(this);
	}

	@Override
	public void onDestroy() {
		contactListListener = null;
		destroyAds();
		super.onDestroy();
	}
}