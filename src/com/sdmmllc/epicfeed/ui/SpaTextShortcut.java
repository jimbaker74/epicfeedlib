package com.sdmmllc.epicfeed.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.sdmm.epicfeed.R;
import com.sdmmllc.epicfeed.SmsIntentReceiver;
import com.sdmmllc.epicfeed.SpaTextNotificationDefaultSetup;
import com.sdmmllc.epicfeed.utils.EpicFeedConsts;


public class SpaTextShortcut extends Activity {
    /** Called when the activity is first created. */
	EditText shortcutName;
	TextView shortcutLbl;
	Context spaContext;
	Bundle savedState;
	String TAG = "SpaTextShortcut";
    private LayoutInflater mInflater;
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater menuInfl = getMenuInflater();
		menuInfl.inflate(R.menu.shortcut_menu, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int itemId = item.getItemId();
		if (itemId == R.id.shortcutHelp) {
			helpDialog();
			return true;
		}
		return false;
	}

	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.shortcutset);
        savedState = savedInstanceState;
        shortcutLbl = (TextView)findViewById(R.id.shortcutNameLbl);
        shortcutName = (EditText)findViewById(R.id.shortcutEditTxt);
        spaContext = this;
		TextView infoText = (TextView)findViewById(R.id.shortcutHelptext);
		infoText.setText(Html.fromHtml(
				getText(R.string.shortcutPageHelp).toString()));
        mInflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	public void defaultNotificationSettings(View v) {
    	Intent defaultStatusbarNotificationIntent = new Intent(spaContext, SpaTextNotificationDefaultSetup.class);
    	startActivityForResult(defaultStatusbarNotificationIntent, EpicFeedConsts.DEFAULT_NOTIFICATION);
	}

	public void helpDialog () {
		closeOptionsMenu();
		final View helpTextView = mInflater.inflate(R.layout.helpview, null);
		final TextView helpText = (TextView)helpTextView.findViewById(R.id.helptext);
		helpText.setText(Html.fromHtml(
				getText(R.string.shortcutHelp).toString()));
		AlertDialog ad = new AlertDialog.Builder(spaContext)
        .setTitle(R.string.shortcutHelpHeader)
        .setView(helpTextView)
        .setNeutralButton("Done", new DialogInterface.OnClickListener() {
            @Override
			public void onClick(DialogInterface dialog, int which) {
            	dialog.cancel();
            }
        })
        .create();
		ad.show();
	}
	
	public void spaShortcutChange1 (View v) {
		AlertDialog alert = getAlert(spaContext, 1);
		alert.show();
	}

	public void spaShortcutChange2 (View v) {
		AlertDialog alert = getAlert(spaContext, 2);
		alert.show();
	}

	public void spaShortcutChange3 (View v) {
		AlertDialog alert = getAlert(spaContext, 3);
		alert.show();
	}

    public AlertDialog getAlert(final Context ctx, final int iconId)  {
		final View shortcutTextView = mInflater.inflate(R.layout.shortcutnameview, null);
		final EditText dialogText = (EditText)shortcutTextView.findViewById(R.id.shortcutNameEdit);
		EditText shortcutNameText = (EditText)findViewById(R.id.shortcutEditTxt);
		ImageView shortcutIconPic = (ImageView)shortcutTextView.findViewById(R.id.shortcutSelPic);
        if (iconId == 1 ) {
    		shortcutIconPic.setImageDrawable(getResources().getDrawable(R.drawable.icon1));
        } else if (iconId == 2) {
    		shortcutIconPic.setImageDrawable(getResources().getDrawable(R.drawable.icon2));
        } else {
    		shortcutIconPic.setImageDrawable(getResources().getDrawable(R.drawable.icon3));
        }
		dialogText.setText(shortcutNameText.getText());
		AlertDialog ad = new AlertDialog.Builder(ctx)
    	.setTitle("Create shortcut?")
    	.setView(shortcutTextView)
		.setCancelable(false)
		.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int id) {
				if (SmsIntentReceiver.debugLevel > 5) Log.i(TAG, "getAlert: Creating shortcut");
				// put intent stuff here
		        Intent shortcutIntent = new Intent(Intent.ACTION_MAIN);
		        shortcutIntent.setClassName(getString(R.string.homescreenPackage), getString(R.string.homescreenClass));
		        shortcutIntent.putExtra(getString(R.string.homescreenClass),
		        		dialogText.getText().toString());
		        shortcutIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		        if (SmsIntentReceiver.debugLevel > 5) Log.i(TAG, "getAlert: Created shortcut intent");
		        // Then, set up the container intent (the response to the caller)
		        Intent intent = new Intent();
		        intent.setAction("com.android.launcher.action.INSTALL_SHORTCUT");
		        intent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent);
		        intent.putExtra(Intent.EXTRA_SHORTCUT_NAME, dialogText.getText().toString());
		        Parcelable iconResource;
		        
		        if (iconId == 1 ) {
		        	iconResource = Intent.ShortcutIconResource.fromContext(
		        		ctx,  R.drawable.icon);
		        } else if (iconId == 2) {
		        	iconResource = Intent.ShortcutIconResource.fromContext(
			        		ctx,  R.drawable.icon2);
		        } else {
		        	iconResource = Intent.ShortcutIconResource.fromContext(
			        		ctx,  R.drawable.icon3);
		        }
		        intent.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE, iconResource);
		        // Now, return the result to the launcher
				if (SmsIntentReceiver.debugLevel > 5) Log.i(TAG, "getAlert: setting results...");
				sendBroadcast(intent);
		        setResult(RESULT_OK, intent);
		        shortcutLbl.setText("New shortcut created: '" + shortcutName.getText().toString() + "'");
		        shortcutName.setText("");
		        // done with this screen when an icon is created
		        finish();
			}
		})
			.setNegativeButton("No", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int id) {
					dialog.cancel();
				}
		})
		.create();
    	return ad;
    }
    
	@Override
    public void onConfigurationChanged(Configuration newConfig) {
    	super.onPause();
		super.onStop();
    	onCreate(this.savedState);
    }

	@Override
	public void onStop() {
		super.onStop();
		setResult(RESULT_CANCELED);
	}
}