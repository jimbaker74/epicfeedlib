package com.sdmmllc.epicfeed.ui;

import java.lang.ref.WeakReference;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.facebook.LoggingBehavior;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.Settings;
import com.flurry.android.FlurryAgent;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;
import com.mopub.mobileads.MoPubView;
import com.placed.client.android.persistent.PlacedAgent;
import com.sdmm.epicfeed.R;
import com.sdmmllc.epicfeed.CacheManager;
import com.sdmmllc.epicfeed.EpicFeedController;
import com.sdmmllc.epicfeed.FlurryConsts;
import com.sdmmllc.epicfeed.ads.MoPubAdHandler;
import com.sdmmllc.epicfeed.ads.MoPubAdListener;
import com.sdmmllc.epicfeed.data.EpicContact;
import com.sdmmllc.epicfeed.data.EpicDB;
import com.sdmmllc.epicfeed.data.EpicDBConn;
import com.sdmmllc.epicfeed.model.FacebookConnectionModel;
import com.sdmmllc.epicfeed.model.Twitter11;
import com.sdmmllc.epicfeed.utils.EpicFeedConsts;
import com.sdmmllc.epicfeed.utils.EpicGAConsts;

public class Act_Authenticate extends SherlockActivity {
	public static final String TAG = "SpaTextAuthenticate";
	
	private Button findFriendBtn,
				   findFollowerBtn;
    private Bundle savedState;
    private EasyTracker easyTracker;

	private SharedPreferences settings;
	private SharedPreferences.Editor editor;
    
    private Thread adHandlerThread;
	private MoPubAdHandler adHandler;
	private MoPubAdListener adListener;
	private MoPubAdHandler.ReturnAdListener returnAdListener;
	private MoPubView mmAdView;
	//private AdLayout mmAdView;
	private RelativeLayout mmAdLayout;
	private Button fbLoginButton;
	private Button twtLoginButton;
    private FacebookConnectionModel mFacebookConn;
    private TextView fbAccountNameTxt, 
    				 fbAccountNameDescr,
    				 twitterNameTxt,
    				 twitterNameDescr;
    private int SOCIAL_TYPE;
    private EpicContact chosenOne;
    private boolean connectionFbFlag = true,
    				connectionTwtFlag = true;
    private Context authContext;
    
    // Twitter 
    private Twitter11 mTwitter11;
    public static final int TWITTER_CALLBACK = 31;
    // End Twitter

    @Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;
		}
		return false;
	}

	@Override
    public void onCreate(Bundle savedInstanceState) {
    	CacheManager.trimCache(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_authenticate);
        savedState = savedInstanceState;
        easyTracker = EasyTracker.getInstance(this);
        EpicFeedActionBar.actionbar_authenticate(new WeakReference<SherlockActivity>(this));
        settings = getSharedPreferences(EpicFeedConsts.PREFS_NAME, 0);
        editor = settings.edit();
        PlacedAgent.registerApp(this.getApplicationContext(), getString(R.string.placedId));
		// FlurryAgent workaround...
		try {   
			Class.forName("android.os.AsyncTask");
		}  catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		authContext = this;

	    mmAdLayout = (RelativeLayout)findViewById(R.id.authAdLayout);
		mmAdView = (MoPubView)findViewById(R.id.mmAuthAdview);
		mmAdView.setAdUnitId(getString(R.string.mopubAdID));
		mmAdView.setBannerAdListener(new MoPubAdListener(TAG, new WeakReference<Activity>(this), mmAdLayout));

		if (EpicFeedConsts.debugLevel > 3) Log.i(TAG, "onCreate: Initializing values...");
        findFriendBtn = (Button)findViewById(R.id.authenticateLoggedInConnectBtn);
        findFriendBtn.setVisibility(View.GONE);
        findFollowerBtn = (Button) findViewById(R.id.authTwitterLoggedInConnectBtn);
        findFollowerBtn.setVisibility(View.GONE);
        TextView nameLabel = (TextView)findViewById(R.id.authenticateAccountLoggedInLbl);
        if (getIntent().hasExtra("pID")) {
        	String phoneId = getIntent().getStringExtra("pID");
        	EpicDB db = EpicFeedController.getEpicDB(EpicFeedController.getContext());
        	EpicDBConn conn = db.open(false);
        	chosenOne = db.getContact(phoneId);
        	db.close(conn);
        	if (phoneId != null && phoneId.length() > 0) nameLabel.setText(getString(R.string.authenticateAccountLoggedInLblTxt)+  " " +
		        		getIntent().getStringExtra("pID"));
		} //else {
		//	
		//}
        if (getIntent().hasExtra("username")) {
            String username = getIntent().getStringExtra("username"); 
        	if (username != null && username.length() > 0) nameLabel.setText(getString(R.string.authenticateAccountLoggedInLblTxt)+  " " +
		        		username);
		} //else {
		//	
		//}
        
        if (getIntent().hasExtra("social_type")) {
        	SOCIAL_TYPE = getIntent().getIntExtra("social_type", EpicFeedConsts.NO_DATA);
        }
        
        
        if (EpicFeedConsts.debugLevel > 3) 
        	Log.i(TAG, "onCreate: license checked...");
		FlurryAgent.onStartSession(this, getString(R.string.flurryAppKey));
		FlurryAgent.onPageView();
    	EpicFeedController.getGaTracker(this.getApplicationContext()).set(Fields.SCREEN_NAME, TAG);
		if (settings.getInt(EpicFeedConsts.receivedMsgCnt, 0) > 0) {
			easyTracker.send(MapBuilder
	                .createEvent(EpicFeedConsts.gat_Info,     	// Event category (required)
	                		EpicFeedConsts.gat_MsgInfo,			  	// Event action (required)
	                		EpicFeedConsts.gatReceiveMsg,	   			// Event label
	                        (long)settings.getInt(EpicFeedConsts.receivedMsgCnt, 0))            						// Event value
	                .build()
	            );
			editor.putInt(EpicFeedConsts.receivedMsgCnt, 0);
			editor.commit();
		}
		if (settings.getInt(EpicFeedConsts.rnReceiveMsgCnt, 0) > 0) {
			easyTracker.send(MapBuilder
	                .createEvent(EpicFeedConsts.gat_Rednote,     	// Event category (required)
	                		EpicFeedConsts.gat_RednoteMsg,			  	// Event action (required)
	                		EpicFeedConsts.gatReceivedRednoteMsgCnt,	   			// Event label
	                        (long)settings.getInt(EpicFeedConsts.gatReceivedRednoteMsgCnt, 0))            						// Event value
	                .build()
	            );
			editor.putInt(EpicFeedConsts.rnReceiveMsgCnt, 0);
			editor.commit();
		}
		if (settings.getInt(EpicFeedConsts.blockedMsgCnt, 0) > 0) {
			easyTracker.send(MapBuilder
	                .createEvent(EpicFeedConsts.gat_Info,     	// Event category (required)
	                		EpicFeedConsts.gat_MsgInfo,			  	// Event action (required)
	                		EpicFeedConsts.gatBlockedMsg,	   			// Event label
	                        (long)settings.getInt(EpicFeedConsts.blockedMsgCnt, 0))            						// Event value
	                .build()
	            );
			editor.putInt(EpicFeedConsts.blockedMsgCnt, 0);
			editor.commit();
		}
        
		mFacebookConn = new FacebookConnectionModel(new WeakReference<Activity>(this));
		mFacebookConn.setStatusCallback(statusCallback);
		mFacebookConn.setupFB(savedInstanceState);
        fbLoginButton = (Button)findViewById(R.id.login_button);
        fbAccountNameTxt = (TextView)findViewById(R.id.authenticateAccountDescTitle);
        fbAccountNameDescr = (TextView)findViewById(R.id.authenticateAccountDescName);
        fbAccountNameTxt.setText(getString(R.string.authenticateAddAccountFacebook));
        fbAccountNameDescr.setText(getString(R.string.authenticateAddAccountPleaseLogin));
        
        Settings.addLoggingBehavior(LoggingBehavior.INCLUDE_ACCESS_TOKENS);

        updateViewForFbLogin();
        
        mTwitter11 = new Twitter11(this, R.string.app_name, settings);
        twtLoginButton = (Button) findViewById(R.id.twitterFakeBtn);
        twtLoginButton.setText("  " + getString(R.string.authTwitterLogInBtnTxt));
        twitterNameTxt = (TextView)findViewById(R.id.authTwitterDescTitle);
        twitterNameDescr = (TextView)findViewById(R.id.authTwitterDescName);
        twitterNameTxt.setText(getString(R.string.authenticateAddAccountTwitter));
        twitterNameDescr.setText(getString(R.string.authenticateAddAccountPleaseLogin));
        
        updateTwitterView();
        
    }
    
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == EpicFeedConsts.SOCIAL_CONNECT && resultCode == RESULT_OK) {
        	finish();
        }
        Session.getActiveSession().onActivityResult(this, requestCode, resultCode, data);
        
        if (resultCode == Activity.RESULT_OK) {
        	if (requestCode == TWITTER_CALLBACK) {
        		if (data.getData() != null) {
        			mTwitter11.logincallback(data, new Runnable(){
        				public void run(){
        					updateTwitterView();
        					Toast.makeText(Act_Authenticate.this, mTwitter11.isloggedin() ? "Logged In" : "Not Logged In", 
        							Toast.LENGTH_SHORT).show();
        				}
        			});
        		}
        	}
        } else if (requestCode == Activity.RESULT_CANCELED) {
        	if(requestCode==TWITTER_CALLBACK)
				Toast.makeText(Act_Authenticate.this, mTwitter11.isloggedin() ? "Logged In" : "Logged Cancelled", 
						Toast.LENGTH_SHORT).show();
        }
    }
    
    @Override
    protected void onStart() {
    	super.onStart();
		EasyTracker.getInstance(this).activityStart(this);  
		FlurryAgent.onStartSession(this, getString(R.string.flurryAppKey));
		FlurryAgent.onPageView();
       	FlurryAgent.logEvent(FlurryConsts.PAGE_VIEW_EVENT, FlurryConsts.getMap(FlurryConsts.PAGE_VIEW_EVENT, TAG));
        Session.getActiveSession().addCallback(statusCallback);
    }

    @Override
    protected void onResume() {
        super.onResume();
		startAds();
		resumeAds();
    	easyTracker.send(MapBuilder
                .createEvent(EpicGAConsts.USER_ACTION_CATEGORY,     			// Event category (required)
                			EpicGAConsts.USER_MESSAGING,			  		// Event action (required)
                			EpicGAConsts.USER_VIEW_AUTH_SCREEN,	   			// Event label
                            1L)            						// Event value
                .build()
            );
    }

	@Override
	public void onRestart() {
		super.onRestart();
	}
	
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Session session = Session.getActiveSession();
        Session.saveSession(session, outState);
    }

	// Facebook login stuff
    // ****** FB Start *************

    
    private void updateViewForFbLogin() {
        final Session session = Session.getActiveSession();
        if (session.isOpened()) {
        	updateUIForFb();
        	fbLoginButton.setVisibility(View.GONE);
        	fbLoginButton.setText("Logout");
        	fbLoginButton.setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                	mFacebookConn.onClickFbLogout();
                }
            });
        } else {
        	fbLoginButton.setVisibility(View.VISIBLE);
        	fbLoginButton.setText("Login");
        	fbLoginButton.setOnClickListener(new OnClickListener() {
                public void onClick(View view) { 
                	mFacebookConn.onClickFbLogin(statusCallback);
                }
            });
        }
    }
    
    public void showLoginBtn(View v) {
    	updateViewForFbLogin();
        if (Session.getActiveSession().isOpened() && !fbLoginButton.isShown()) {
        	fbLoginButton.setVisibility(View.VISIBLE);
        } else if (!Session.getActiveSession().isOpened()) {
        	fbLoginButton.setVisibility(View.VISIBLE);
        } else {
        	fbLoginButton.setVisibility(View.GONE);
        }
    	
    }

    private Session.StatusCallback statusCallback = new FbSessionStatusCallback();
    private class FbSessionStatusCallback implements Session.StatusCallback {
        @Override
        public void call(Session session, SessionState state, Exception exception) {
        	if (EpicFeedConsts.DEBUG_FB) Log.i(TAG, "Session.StatusCallback.call() method executing");
            if (session != null && state.equals(SessionState.OPENED)) mFacebookConn.makeMeRequest(session);
            updateViewForFbLogin();
        }
    }
	
	private void updateUIForFb() {
        Session session = Session.getActiveSession();
        boolean enableButtons = (session != null && session.isOpened());

        findFriendBtn.setEnabled(enableButtons);
        findFriendBtn.setVisibility(View.VISIBLE);
        //if (chosenOne != null) {
        	//if (chosenOne.getFeed(ContactFeed.FEED_TYPE_FACEBOOK) != null) {
            if (SOCIAL_TYPE == EpicFeedConsts.FACEBOOK_DATA || SOCIAL_TYPE == EpicFeedConsts.ALL_SOCIAL_NETWORKS) {
            	findFriendBtn.setText(getString(R.string.authenticateDisconnectBtnTxt));
            	connectionFbFlag = false;
            }
        //}
        	
        if (session.isOpened() && EpicFeedController.getGraphUser() == null) {
        	mFacebookConn.makeMeRequest(session);
        } else if (session.isOpened() && EpicFeedController.getGraphUser() != null) {
        	fbAccountNameTxt.setText(EpicFeedController.getGraphUser().getFirstName() + " " + EpicFeedController.getGraphUser().getLastName());
        	fbAccountNameDescr.setText(EpicFeedController.getGraphUser().getUsername());
        }
	}
	
    // ****** FB End *************
	
	// *  *  *  *  *  *  *  *  Twitter  *  *  *  *  *  *  *  *  
	
	private void updateUIForTwitter() {
		boolean enable = (mTwitter11 != null && mTwitter11.isloggedin());
		findFollowerBtn.setEnabled(enable);
		findFollowerBtn.setVisibility(View.VISIBLE);
		//if (chosenOne != null) {
			//if (chosenOne.getFeed(ContactFeed.FEED_TYPE_TWITTER) != null) {
		    if (SOCIAL_TYPE == EpicFeedConsts.TWITTER_DATA || SOCIAL_TYPE == EpicFeedConsts.ALL_SOCIAL_NETWORKS) {
				findFollowerBtn.setText(getString(R.string.authenticateDisconnectBtnTxt));
				connectionTwtFlag = false;
			}
		//}
			
		if (mTwitter11.isloggedin() && (EpicFeedController.getTwitterUserName() == null || 
				EpicFeedController.getTwitterUserScreenName() == null)) {
			mTwitter11.getUserNames();
			twitterNameTxt.setText("");
			twitterNameDescr.setText("");
		} else if(mTwitter11.isloggedin() && (EpicFeedController.getTwitterUserName() != null && 
				EpicFeedController.getTwitterUserScreenName() != null)) {
			twitterNameTxt.setText(EpicFeedController.getTwitterUserName());
			twitterNameDescr.setText(EpicFeedController.getTwitterUserScreenName());
		}
	}
	
	public void showTwitterLoginBtn(View v) {
		updateTwitterView();
        if (mTwitter11.isloggedin() && !twtLoginButton.isShown()) {
        	twtLoginButton.setVisibility(View.VISIBLE);
        } else if (!mTwitter11.isloggedin()) {
        	twtLoginButton.setVisibility(View.VISIBLE);
        } else {
        	twtLoginButton.setVisibility(View.GONE);
        }
    	
    }
	
	private void updateTwitterView() {
		if (mTwitter11.isloggedin()){
			updateUIForTwitter();
			twtLoginButton.setVisibility(View.GONE);
			twtLoginButton.setText("    Logout");
			twtLoginButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					mTwitter11.logout();
				}
			});
			Log.i(TAG, "*  *  *  *  *  Twitter11 is logged in *  *  *  *  *");
			
		} else {
			twtLoginButton.setVisibility(View.VISIBLE);
			twtLoginButton.setText("  " + getString(R.string.authTwitterLogInBtnTxt));
			twtLoginButton.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					mTwitter11.login();
				}
			});
			Log.i(TAG, "*  *  *  *  * Twitter11 is not logged in *  *  *  *  *");
		}
	}
	
	public void twitterContactList (View v) {
			if (!connectionTwtFlag) {
				startActivity(new Intent(authContext, Act_ContactList.class)
								  .putExtra("disconnect_twt", true)
								  .putExtra("pID", getIntent().getStringExtra("pID")));
			} else {
				if (SOCIAL_TYPE == EpicFeedConsts.FACEBOOK_DATA)
					SOCIAL_TYPE = EpicFeedConsts.ALL_SOCIAL_NETWORKS;
				else SOCIAL_TYPE = EpicFeedConsts.TWITTER_DATA;
				
				Intent launchSearch = getIntent();
				launchSearch.putExtra("social_network", SOCIAL_TYPE);
				launchSearch.putExtra("search_network", EpicFeedConsts.TWITTER_DATA);
				launchSearch.setClass(this, Act_SocialContactsList.class);
				startActivityForResult(launchSearch, EpicFeedConsts.SOCIAL_CONNECT);
			}
	}
	// *  *  *  *  *  *  *  *  *  End Twitter  *  *  *  *  *  *  *  *

    public void facebookContactList (View v) {
    		if (!connectionFbFlag) {
    			startActivity(new Intent(authContext, Act_ContactList.class)
    							  .putExtra("disconnect_fb", true)
    							  .putExtra("pID", getIntent().getStringExtra("pID")));
    		} else {
    			if (SOCIAL_TYPE == EpicFeedConsts.TWITTER_DATA)
					SOCIAL_TYPE = EpicFeedConsts.ALL_SOCIAL_NETWORKS;
				else SOCIAL_TYPE = EpicFeedConsts.FACEBOOK_DATA;
    			
    			Intent launchSearch = getIntent();
        		launchSearch.putExtra("social_network", SOCIAL_TYPE);
        		launchSearch.putExtra("search_network", EpicFeedConsts.FACEBOOK_DATA);
            	launchSearch.setClass(this, Act_SocialContactsList.class);
            	startActivityForResult(launchSearch, EpicFeedConsts.SOCIAL_CONNECT);
    		}
    }
    
    @Override
    protected void onPause() {
        super.onPause();
        stopAds();
    }

    @Override
	public void onStop() {
		setResult(RESULT_CANCELED);
		super.onStop();
        Session.getActiveSession().removeCallback(statusCallback);
        FlurryAgent.onEndSession(this);
        EasyTracker.getInstance(this).activityStop(this);
	}
	
	@Override
    public void onConfigurationChanged(Configuration newConfig) {
    	onPause();
		onStop();
		stopAds();
		destroyAds();
    	onCreate(this.savedState);
    	onStart();
    	onResume();
    }
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		destroyAds();
	}
	
	public void setupAdHandler() {
		if (getResources().getBoolean(R.bool.solitaireLicense)) {
			mmAdView.setVisibility(View.GONE);
			return;
		}
		if (EpicFeedConsts.debugLevel > 7) 
			Log.i(TAG, "setupAdHandler: setting up..");
		if (adListener == null) adListener = new MoPubAdListener(TAG, new WeakReference<Activity>(this), mmAdLayout);
		mmAdView.setBannerAdListener(adListener);
		//if (returnAdListener == null) returnAdListener = new AdHandler.ReturnAdListener() {
		returnAdListener = new MoPubAdHandler.ReturnAdListener() {
			@Override
			public void onGetNewAd() {
				runOnUiThread(returnAd);
			}
		};
		if (adHandler == null) {
			adHandler = new MoPubAdHandler(this, TAG + " null", mmAdView, mmAdLayout, adListener, returnAdListener);
		} else {
			if ((adHandler.getAdView() == null) || !adHandler.getAdView().equals(mmAdView)) {
				adHandler.setAdView(mmAdView);
			}
			if ((adHandler.getAdListener() == null) || !adHandler.getAdListener().equals(adListener)) {
				adHandler.setAdListener(adListener);
			}
			if ((adHandler.getReturnAdListener() == null) || !adHandler.getReturnAdListener().equals(returnAdListener)) {
				adHandler.setReturnAdListener(returnAdListener);
			}
		}
	}
	
	public void startAds() {
		if (getResources().getBoolean(R.bool.solitaireLicense)) {
			if (mmAdView != null) mmAdView.setVisibility(View.GONE);
			return;
		}
		
		//mmAdView.setTimeout(45000);
		
		if (mmAdLayout == null) mmAdLayout = (RelativeLayout)findViewById(R.id.authAdLayout);
		if (mmAdView == null) {
			mmAdView = (MoPubView)findViewById(R.id.mmAuthAdview);
			mmAdView.setAdUnitId(getString(R.string.mopubAdID));
			mmAdView.setVisibility(View.GONE);
			//mmAdView = (AdLayout)findViewById(R.id.mmAuthAdview);
			//mmAdView.setVisibility(View.GONE);
		}
		//AdTargetingOptions adOptions = new AdTargetingOptions();
		//adOptions.enableGeoLocation(true);
		//adOptions.setAge(age);
		//adOptions.setGender();
		
        setupAdHandler();
		//if(!mmAdView.isLoading()) 
			//mmAdView.loadAd(adOptions);
		
		if (EpicFeedConsts.debugLevel > 7) 
			Log.i(TAG, "startAds: starting..");
		adHandler.resumeAds();
		adHandler.checkAd = true;
        if ((adHandlerThread == null)||(adHandlerThread.getState() == Thread.State.TERMINATED)
        		||!adHandlerThread.isAlive())
            	adHandlerThread = new Thread(null, adHandler, "Ad Handler Thread : " + TAG);
        if (adHandlerThread.getState() == Thread.State.NEW) {
        	adHandlerThread.start();
        }
        //adSizeCheck = new Thread(null, adSizeChecker, "Ad Size Checker");
        //adSizeCheck.start();
        //if (SpaTextConsts.debugLevel > 0) SpaTextApp.memoryInfo(spaContext, TAG);
	}

	private Runnable returnAd = new Runnable() {
		@Override
		public void run () {
			if (adHandler != null) {
				synchronized (adHandler.isBeingDestroyed) {
					if (adHandler.isBeingDestroyed) adListener.needAd = true;
				}
		    	if (EpicFeedConsts.debugLevel > 0) 
		    		Log.i(TAG, "returnAd thread: Got new ad...");
	    		Log.i("Ads", "SPA: requesting new ad " + TAG);
			} else {
	    		Log.i(TAG, "returnAd thread: adHandler dead");
			}
		}
	};
	
	public void resumeAds() {
		if (EpicFeedConsts.debugLevel > 2) Log.i(TAG, "resumeAds: setting state flags");
		if (adHandler != null) {
			adHandler.resumeAds();
			adHandler.checkAd = true;
		}
		
		/*mmAdView.setTimeout(45000);
		
		AdTargetingOptions adOptions = new AdTargetingOptions();
		adOptions.enableGeoLocation(true);
		//adOptions.setAge(age);
	    //adOptions.setGender();
		
		if(!mmAdView.isLoading())
			mmAdView.loadAd(adOptions);*/
	}
	
	public void stopAds() {
		if (EpicFeedConsts.debugLevel > 7) 
			Log.i(TAG, "stopAds: stopping..");
		if (adHandler != null) {
	        adHandler.stopAds();
		}
        try {
			synchronized (adHandler.isBeingDestroyed) {
				if (!adHandler.isBeingDestroyed) adListener.needAd = false;
			}
        } catch (Exception e) {
        	// don't worry about an error
        }
		
		//mmAdView.collapseAd();
	}
	
	public void destroyAds() {
		if (EpicFeedConsts.debugLevel > 7) 
			Log.i(TAG, "destroyAds: destroying..");
		
		//mmAdView.destroy();
		
        try {
			synchronized (adHandler.isBeingDestroyed) {
				adHandler.isBeingDestroyed = true;
			}
        } catch (Exception e) {
        	// don't worry about an error
        }
        try {
			synchronized (adHandler.isBeingDestroyed) {
				if (!adHandler.isBeingDestroyed) adListener.needAd = false;
			}
        } catch (Exception e) {
        	// don't worry about an error
        }
        
        try {
	        if (adHandler.getAdView() != null)
	    		try {
	    			((ViewGroup)adHandler.getAdView().getParent()).removeView(adHandler.getAdView());
	    		} catch (Exception e) {
	    		}
	
			//adView = null;
	
	    	if (adHandler != null) 
				try {
					synchronized (adHandler.isBeingDestroyed) {
						adHandler.isBeingDestroyed = true;
						adHandler.checkAd = false;
						adHandler.stopAdTimer();
				        adHandler.destroyAds();
				        mmAdView = null;
						adHandler = null;
					}
				} catch (Exception e) {
					// do nothing, ad will go away
				}
        } catch (Exception e) {
        	// do nothing, destroying anyway
        }
    }
}