package com.sdmmllc.epicfeed.ui;

import java.lang.ref.WeakReference;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.actionbarsherlock.app.SherlockActivity;
import com.flurry.android.FlurryAgent;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.MapBuilder;
import com.mopub.mobileads.MoPubView;
import com.sdmm.epicfeed.R;
import com.sdmmllc.epicfeed.CacheManager;
import com.sdmmllc.epicfeed.EpicFeedController;
import com.sdmmllc.epicfeed.FlurryConsts;
import com.sdmmllc.epicfeed.SpaTextSplash;
import com.sdmmllc.epicfeed.ads.MoPubAdHandler;
import com.sdmmllc.epicfeed.ads.MoPubAdListener;
import com.sdmmllc.epicfeed.data.EpicDB;
import com.sdmmllc.epicfeed.utils.EpicFeedConsts;
import com.sdmmllc.epicfeed.utils.EpicGAConsts;

public class Act_EpicHome extends SherlockActivity {
	private String TAG = "SpaTextDecoyHome";
    public static EpicDB spaTextDB;
    private Boolean loaded = false;
    private SharedPreferences settings;
    private SharedPreferences.Editor editor;
    private Activity spaContext;
    private Thread adHandlerThread;
	private MoPubAdHandler adHandler;
	private MoPubAdListener adListener;
	private MoPubAdHandler.ReturnAdListener returnAdListener;

	private MoPubView mmAdView;
	private RelativeLayout mmAdLayout;
    private EasyTracker easyTracker;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	CacheManager.trimCache(this);
    	super.onCreate(savedInstanceState);
    	setContentView(R.layout.act_epic_home);
    	EpicFeedActionBar.actionbar_main(new WeakReference<SherlockActivity>(this));
        
    	easyTracker = EasyTracker.getInstance(this);

        spaContext = this;
    	spaTextDB = EpicFeedController.getEpicDB(spaContext.getApplicationContext());
        settings = getSharedPreferences(EpicFeedConsts.PREFS_NAME, 0);
		editor = settings.edit();
		// FlurryAgent workaround...
		try {   
			Class.forName("android.os.AsyncTask");
		}  catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		mmAdLayout = (RelativeLayout)findViewById(R.id.mainAdLayout);
		mmAdView = (MoPubView)findViewById(R.id.mmHomeAdview);
		mmAdView.setAdUnitId(getString(R.string.mopubAdID));
		mmAdView.setBannerAdListener(new MoPubAdListener(TAG, new WeakReference<Activity>(this), mmAdLayout));
		if (EpicFeedConsts.DEBUG_ADS) Log.i(TAG, "starting ad handler");

		FlurryAgent.onStartSession(this, getString(R.string.flurryAppKey));
        FlurryAgent.onPageView();
    	
        final Handler splashHandler = new Handler();
    	final Runnable showSplash = new Runnable() { 
    		@Override 
    		public void run() { 
        			Intent splashIntent = new Intent(spaContext, SpaTextSplash.class);
        			startActivityForResult(splashIntent, EpicFeedConsts.SPLASHSCREEN);
    		} 
    	};
    	splashHandler.post(showSplash);
		loaded = true;
	}
    
    @Override
    protected void onStart() {
    	super.onStart();
		EasyTracker.getInstance(this).activityStart(this);  
		FlurryAgent.onStartSession(this, getString(R.string.flurryAppKey));
		FlurryAgent.onPageView();
       	FlurryAgent.logEvent(FlurryConsts.PAGE_VIEW_EVENT, FlurryConsts.getMap(FlurryConsts.PAGE_VIEW_EVENT, TAG));
    }

   public void contact_list(View v) {
	   Intent authIntent = new Intent(this, Act_ContactList.class);
	   startActivityForResult(authIntent, EpicFeedConsts.CONTACTLIST);
   }
   
   @Override
   public void onResume() {
	   super.onResume();
       startAds();
       resumeAds();
       easyTracker.send(MapBuilder
	            .createEvent(EpicGAConsts.USER_ACTION_CATEGORY,     			// Event category (required)
	            			EpicGAConsts.USER_MESSAGING,			  		// Event action (required)
	            			EpicGAConsts.USER_VIEW_CONTACT_LIST,	   			// Event label
	                        1L)            						// Event value
	            .build()
        );
   }
   
   @Override
   public void onPause() {
	   super.onPause();
	   stopAds();
   }
   
	@Override
	public void onStop() {
		super.onStop();
		EasyTracker.getInstance(this).activityStop(this);  
		FlurryAgent.onEndSession(this);
	}

   @Override
   public void onDestroy() {
	   destroyAds();
	   super.onPause();
   }

	public void setupAdHandler() {
		if (EpicFeedConsts.debugLevel > 7) 
			Log.i(TAG, "setupAdHandler: setting up..");
		if (adListener == null) adListener = new MoPubAdListener(TAG, new WeakReference<Activity>(this), mmAdLayout);
		mmAdView.setBannerAdListener(adListener);
		//if (returnAdListener == null) returnAdListener = new AdHandler.ReturnAdListener() {
		returnAdListener = new MoPubAdHandler.ReturnAdListener() {
			@Override
			public void onGetNewAd() {
				runOnUiThread(returnAd);
			}
		};
		if (adHandler == null) {
			adHandler = new MoPubAdHandler(this, TAG + " null", mmAdView, mmAdLayout, adListener, returnAdListener);
		} else {
			if ((adHandler.getAdView() == null) || !adHandler.getAdView().equals(mmAdView)) {
				adHandler.setAdView(mmAdView);
			}
			if ((adHandler.getAdListener() == null) || !adHandler.getAdListener().equals(adListener)) {
				adHandler.setAdListener(adListener);
			}
			if ((adHandler.getReturnAdListener() == null) || !adHandler.getReturnAdListener().equals(returnAdListener)) {
				adHandler.setReturnAdListener(returnAdListener);
			}
		}
	}
	
	public void startAds() {
		if (getResources().getBoolean(R.bool.solitaireLicense)) {
			if (mmAdView != null) mmAdView.setVisibility(View.GONE);
			return;
		}
		if (mmAdLayout == null) mmAdLayout = (RelativeLayout)findViewById(R.id.mainAdLayout);
		if (mmAdView == null) {
			mmAdView = (MoPubView)findViewById(R.id.mmHomeAdview);
			mmAdView.setAdUnitId(getString(R.string.mopubAdID));
			mmAdView.setVisibility(View.GONE);
		}
        setupAdHandler();
		if (EpicFeedConsts.debugLevel > 7) 
			Log.i(TAG, "startAds: starting..");
		adHandler.resumeAds();
		adHandler.checkAd = true;
        if ((adHandlerThread == null)||(adHandlerThread.getState() == Thread.State.TERMINATED)
        		||!adHandlerThread.isAlive())
            	adHandlerThread = new Thread(null, adHandler, "Ad Handler Thread : " + TAG);
        if (adHandlerThread.getState() == Thread.State.NEW) {
        	adHandlerThread.start();
        }
        //adSizeCheck = new Thread(null, adSizeChecker, "Ad Size Checker");
        //adSizeCheck.start();
        //if (SpaTextConsts.debugLevel > 0) SpaTextApp.memoryInfo(spaContext, TAG);
	}

	private Runnable returnAd = new Runnable() {
		@Override
		public void run () {
			if (adHandler != null) {
				synchronized (adHandler.isBeingDestroyed) {
					if (adHandler.isBeingDestroyed) adListener.needAd = true;
				}
		    	if (EpicFeedConsts.debugLevel > 0) 
		    		Log.i(TAG, "returnAd thread: Got new ad...");
	    		Log.i("Ads", "SPA: requesting new ad " + TAG);
			} else {
	    		Log.i(TAG, "returnAd thread: adHandler dead");
			}
		}
	};
	
	public void resumeAds() {
		if (EpicFeedConsts.debugLevel > 2) Log.i(TAG, "resumeAds: setting state flags");
        adHandler.resumeAds();
		adHandler.checkAd = true;
	}
	
	public void stopAds() {
		if (EpicFeedConsts.debugLevel > 7) 
			Log.i(TAG, "stopAds: stopping..");
		if (adHandler != null) {
	        adHandler.stopAds();
		}
        try {
			synchronized (adHandler.isBeingDestroyed) {
				if (!adHandler.isBeingDestroyed) adListener.needAd = false;
			}
        } catch (Exception e) {
        	// don't worry about an error
        }
	}
	
	public void destroyAds() {
		if (EpicFeedConsts.debugLevel > 7) 
			Log.i(TAG, "destroyAds: destroying..");
        try {
			synchronized (adHandler.isBeingDestroyed) {
				adHandler.isBeingDestroyed = true;
			}
        } catch (Exception e) {
        	// don't worry about an error
        }
        try {
			synchronized (adHandler.isBeingDestroyed) {
				if (!adHandler.isBeingDestroyed) adListener.needAd = false;
			}
        } catch (Exception e) {
        	// don't worry about an error
        }
        
        try {
	        if (adHandler.getAdView() != null)
	    		try {
	    			((ViewGroup)adHandler.getAdView().getParent()).removeView(adHandler.getAdView());
	    		} catch (Exception e) {
	    		}
	
			//adView = null;
	
	    	if (adHandler != null) 
				try {
					synchronized (adHandler.isBeingDestroyed) {
						adHandler.isBeingDestroyed = true;
						adHandler.checkAd = false;
						adHandler.stopAdTimer();
				        adHandler.destroyAds();
				        mmAdView = null;
						adHandler = null;
					}
				} catch (Exception e) {
					// do nothing, ad will go away
				}
        } catch (Exception e) {
        	// do nothing, destroying anyway
        }
    }
}