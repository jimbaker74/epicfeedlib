package com.sdmmllc.epicfeed.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Html;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import com.flurry.android.FlurryAgent;
import com.google.analytics.tracking.android.EasyTracker;
import com.sdmm.epicfeed.R;
import com.sdmmllc.epicfeed.utils.EpicFeedConsts;


public class Act_AcceptTerms extends Activity {

	private CheckBox termsChk;
	private Button acceptBtn;
	private SharedPreferences settings;
	private SharedPreferences.Editor editor;
    private LayoutInflater mInflater;
    private EasyTracker easyTracker;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_accept_terms_screen);
        termsChk = (CheckBox)findViewById(R.id.welcomePrivacyAcceptChk);
        acceptBtn = (Button)findViewById(R.id.welcomePrivacyAcceptBtn);
    	settings = getSharedPreferences(EpicFeedConsts.PREFS_NAME, 0);
		acceptBtn.setEnabled(true);
    	editor = settings.edit();
    	termsChk.setChecked(true);
		easyTracker = EasyTracker.getInstance(this);
    }
	
    @Override
    public void onStart() {
		super.onStart();
		EasyTracker.getInstance(this).activityStart(this);  
		FlurryAgent.onStartSession(this, getString(R.string.flurryAppKey));
		FlurryAgent.onPageView();
    }
    
    @Override
    public void onStop() {
        EasyTracker.getInstance(this).activityStop(this);
        FlurryAgent.onEndSession(this);
    	super.onStop();
    }
    
	public void termsCheck(View v) {
		editor.putBoolean(EpicFeedConsts.privacyNoticeStatus, termsChk.isChecked());
		editor.commit();
		if (termsChk.isChecked()) {
    		acceptBtn.setEnabled(true);
    		termsChk.setChecked(true);
    	} else {
    		acceptBtn.setEnabled(false);
    		termsChk.setChecked(false);
    	}
	}
	
	public void viewTerms(View v) {
	 	Intent privacyIntent = new Intent(this, SpaTextAboutPrivacy.class);
		startActivityForResult(privacyIntent, EpicFeedConsts.ABOUT_PRIVACY);
	}
	
	public void termsAccept(View v) {
		if (termsChk.isChecked()) {
			editor.putBoolean(EpicFeedConsts.privacyNoticeStatus, true);
			editor.commit();
			finish();
		} else {
	        mInflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			final View helpTextView = mInflater.inflate(R.layout.helpview, null);
			final TextView updatesText = (TextView)helpTextView.findViewById(R.id.helptext);
			updatesText.setGravity(Gravity.CENTER_HORIZONTAL);
			updatesText.setText(Html.fromHtml(getString(R.string.privacyAcceptConfirmTxt)));
			AlertDialog ad = new AlertDialog.Builder(this)
	        .setTitle(getString(R.string.privacyAcceptConfirmHeaderTxt))
	        .setView(helpTextView)
	        .setPositiveButton(getString(R.string.privacyAcceptTermsTxt), new DialogInterface.OnClickListener() {
	            public void onClick(DialogInterface dialog, int which) {
	        		editor.putBoolean(EpicFeedConsts.privacyNoticeStatus, true);
	        		editor.commit();
	            	dialog.cancel();
	            	finish();
	            }
	        })
	        .setNegativeButton(getString(R.string.sysNo), new DialogInterface.OnClickListener() {
	            public void onClick(DialogInterface dialog, int which) {
	            	dialog.cancel();
	            }
	        })
	        .create();
			ad.show();
		}
	}
}