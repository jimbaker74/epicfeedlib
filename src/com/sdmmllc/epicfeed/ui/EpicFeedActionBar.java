package com.sdmmllc.epicfeed.ui;

import java.lang.ref.WeakReference;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewConfiguration;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.app.SherlockListActivity;
import com.sdmm.epicfeed.R;

public class EpicFeedActionBar {
	
	private static boolean needMenuButton(Context act) {
	    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
	        return (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB);
	    } else {
	        return ViewConfiguration.get(act).hasPermanentMenuKey();
	    }
	}


	public static void mainActActionBar(WeakReference<SherlockActivity> act) {
		ActionBar actionBar = act.get().getSupportActionBar();
		actionBar.setDisplayHomeAsUpEnabled(false);
		actionBar.setDisplayShowCustomEnabled(true);
		actionBar.setDisplayShowTitleEnabled(false);
		actionBar.setLogo(R.drawable.icon);
		actionBar.setBackgroundDrawable(
				act.get().getResources().getDrawable(R.drawable.bg_blue));

		if (needMenuButton(act.get().getApplicationContext())) {
			LayoutInflater inflator = (LayoutInflater) act.get().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View v = inflator.inflate(R.layout.epic_feed_actionbar, null);
			actionBar.setCustomView(v);
		} else {
			LayoutInflater inflator = (LayoutInflater) act.get().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View v = inflator.inflate(R.layout.epic_feed_actionbar_nomenu, null);
			actionBar.setCustomView(v);
		}
	}
	
	public static void actionbar_main(WeakReference<SherlockActivity> act) {
		ActionBar actionBar = act.get().getSupportActionBar();
		actionBar.setDisplayHomeAsUpEnabled(false);
		actionBar.setDisplayShowCustomEnabled(true);
		actionBar.setDisplayShowTitleEnabled(false);
		actionBar.setLogo(R.drawable.icon);
		actionBar.setBackgroundDrawable(
				act.get().getResources().getDrawable(R.drawable.bg_blue));

		if (needMenuButton(act.get().getApplicationContext())) {
			LayoutInflater inflator = (LayoutInflater) act.get().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View v = inflator.inflate(R.layout.epic_feed_actionbar, null);
			actionBar.setCustomView(v);
		} else {
			LayoutInflater inflator = (LayoutInflater) act.get().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View v = inflator.inflate(R.layout.epic_feed_actionbar_nomenu, null);
			actionBar.setCustomView(v);
		}
	}
	
	public static void actionbar_authenticate(WeakReference<SherlockActivity> act) {
		ActionBar actionBar = act.get().getSupportActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setDisplayShowCustomEnabled(true);
		actionBar.setDisplayShowTitleEnabled(false);
		actionBar.setLogo(R.drawable.icon);
		actionBar.setBackgroundDrawable(
				act.get().getResources().getDrawable(R.drawable.bg_blue));

		if (needMenuButton(act.get().getApplicationContext())) {
			LayoutInflater inflator = (LayoutInflater) act.get().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View v = inflator.inflate(R.layout.epic_feed_actionbar, null);
			actionBar.setCustomView(v);
		} else {
			LayoutInflater inflator = (LayoutInflater) act.get().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View v = inflator.inflate(R.layout.epic_feed_actionbar_nomenu, null);
			actionBar.setCustomView(v);
		}
	}
	
	public static void actionbar_contacts(WeakReference<SherlockListActivity> act) {
		ActionBar actionBar = act.get().getSupportActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setDisplayShowCustomEnabled(true);
		actionBar.setDisplayShowTitleEnabled(false);
		actionBar.setLogo(R.drawable.icon);
		actionBar.setBackgroundDrawable(
				act.get().getResources().getDrawable(R.drawable.bg_blue));

		if (needMenuButton(act.get().getApplicationContext())) {
			LayoutInflater inflator = (LayoutInflater) act.get().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View v = inflator.inflate(R.layout.epic_feed_actionbar, null);
			actionBar.setCustomView(v);
		} else {
			LayoutInflater inflator = (LayoutInflater) act.get().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View v = inflator.inflate(R.layout.epic_feed_actionbar_nomenu, null);
			actionBar.setCustomView(v);
		}
	}
	
	public static void actionbar_compose(WeakReference<SherlockActivity> act) {
		ActionBar actionBar = act.get().getSupportActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setDisplayShowCustomEnabled(true);
		actionBar.setDisplayShowTitleEnabled(false);
		actionBar.setLogo(R.drawable.icon);
		actionBar.setBackgroundDrawable(
				act.get().getResources().getDrawable(R.drawable.bg_blue));

		if (needMenuButton(act.get().getApplicationContext())) {
			LayoutInflater inflator = (LayoutInflater) act.get().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View v = inflator.inflate(R.layout.epic_feed_actionbar, null);
			actionBar.setCustomView(v);
		} else {
			LayoutInflater inflator = (LayoutInflater) act.get().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View v = inflator.inflate(R.layout.epic_feed_actionbar_nomenu, null);
			actionBar.setCustomView(v);
		}
	}
	
	public static void actionbar_smsthread(WeakReference<SherlockListActivity> act) {
		ActionBar actionBar = act.get().getSupportActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setDisplayShowCustomEnabled(true);
		actionBar.setDisplayShowTitleEnabled(false);
		actionBar.setLogo(R.drawable.icon);
		actionBar.setBackgroundDrawable(
				act.get().getResources().getDrawable(R.drawable.bg_blue));

		if (needMenuButton(act.get().getApplicationContext())) {
			LayoutInflater inflator = (LayoutInflater) act.get().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View v = inflator.inflate(R.layout.epic_feed_actionbar, null);
			actionBar.setCustomView(v);
		} else {
			LayoutInflater inflator = (LayoutInflater) act.get().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View v = inflator.inflate(R.layout.epic_feed_actionbar_nomenu, null);
			actionBar.setCustomView(v);
		}
	}
	
	public static void actionbar_smsthread_asc_ls(WeakReference<SherlockListActivity> act) {
		ActionBar actionBar = act.get().getSupportActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setDisplayShowCustomEnabled(true);
		actionBar.setDisplayShowTitleEnabled(false);
		actionBar.setLogo(R.drawable.icon);
		actionBar.setBackgroundDrawable(
				act.get().getResources().getDrawable(R.drawable.bg_blue));

		if (needMenuButton(act.get().getApplicationContext())) {
			LayoutInflater inflator = (LayoutInflater) act.get().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View v = inflator.inflate(R.layout.epic_feed_actionbar, null);
			actionBar.setCustomView(v);
		} else {
			LayoutInflater inflator = (LayoutInflater) act.get().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View v = inflator.inflate(R.layout.epic_feed_actionbar_nomenu, null);
			actionBar.setCustomView(v);
		}
	}
	
	public static void actionbar_smsthread_asc(WeakReference<SherlockListActivity> act) {
		ActionBar actionBar = act.get().getSupportActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setDisplayShowCustomEnabled(true);
		actionBar.setDisplayShowTitleEnabled(false);
		actionBar.setLogo(R.drawable.icon);
		actionBar.setBackgroundDrawable(
				act.get().getResources().getDrawable(R.drawable.bg_blue));

		if (needMenuButton(act.get().getApplicationContext())) {
			LayoutInflater inflator = (LayoutInflater) act.get().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View v = inflator.inflate(R.layout.spa_actionbar_thread, null);
			actionBar.setCustomView(v);
		} else {
			LayoutInflater inflator = (LayoutInflater) act.get().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View v = inflator.inflate(R.layout.epic_feed_actionbar_nomenu, null);
			actionBar.setCustomView(v);
		}
	}
	
	public static void actionbar_smsthread_ls(WeakReference<SherlockListActivity> act) {
		ActionBar actionBar = act.get().getSupportActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setDisplayShowCustomEnabled(true);
		actionBar.setDisplayShowTitleEnabled(false);
		actionBar.setLogo(R.drawable.icon);
		actionBar.setBackgroundDrawable(
				act.get().getResources().getDrawable(R.drawable.bg_blue));

		if (needMenuButton(act.get().getApplicationContext())) {
			LayoutInflater inflator = (LayoutInflater) act.get().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View v = inflator.inflate(R.layout.spa_actionbar_thread, null);
			actionBar.setCustomView(v);
		} else {
			LayoutInflater inflator = (LayoutInflater) act.get().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View v = inflator.inflate(R.layout.epic_feed_actionbar_nomenu, null);
			actionBar.setCustomView(v);
		}
	}
	
	
}
						
