package com.sdmmllc.epicfeed.ui;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.telephony.PhoneNumberUtils;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockListActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.facebook.Session;
import com.flurry.android.FlurryAgent;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;
import com.mopub.mobileads.MoPubView;
import com.sdmm.epicfeed.R;
import com.sdmmllc.epicfeed.EpicFeedController;
import com.sdmmllc.epicfeed.FlurryConsts;
import com.sdmmllc.epicfeed.SpaTextConfig;
import com.sdmmllc.epicfeed.SpaTextNotification;
import com.sdmmllc.epicfeed.SpaUserAccount;
import com.sdmmllc.epicfeed.SpaUserAccountAdapter;
import com.sdmmllc.epicfeed.SpaUserAccountListener;
import com.sdmmllc.epicfeed.ads.MoPubAdHandler;
import com.sdmmllc.epicfeed.ads.MoPubAdListener;
import com.sdmmllc.epicfeed.data.ContactFeed;
import com.sdmmllc.epicfeed.data.EpicContact;
import com.sdmmllc.epicfeed.data.EpicDB;
import com.sdmmllc.epicfeed.data.EpicDBConn;
import com.sdmmllc.epicfeed.data.FacebookData;
import com.sdmmllc.epicfeed.data.FacebookDataListener;
import com.sdmmllc.epicfeed.data.FeedContactList;
import com.sdmmllc.epicfeed.data.TwitterData;
import com.sdmmllc.epicfeed.data.TwitterDataListener;
import com.sdmmllc.epicfeed.model.FacebookConnectionModel;
import com.sdmmllc.epicfeed.model.Twitter11;
import com.sdmmllc.epicfeed.utils.EpicFeedConsts;
import com.sdmmllc.epicfeed.utils.EpicGAConsts;
import com.sdmmllc.epicfeed.utils.EpicImageAdapter;
import com.sdmmllc.epicfeed.utils.EpicImageUtils.EpicBitmaps;

public class Act_SocialContactsList extends SherlockListActivity {

	String TAG = "Act_SocialContactsList";
	
	private EditText searchEditText;
	private RelativeLayout mmAdLayout;
	private MoPubView mmAdView;
	private MoPubAdListener adListener;
    private MoPubAdHandler adHandler;
	private MoPubAdHandler.ReturnAdListener returnAdListener;
    private Thread adHandlerThread;
	private EpicDB epicDB;
	private Bundle savedState;
	private Context spaContext;
	private WeakReference<Activity> contactListAct;
    private LayoutInflater mInflater;
    private SpaUserAccount mSpaUserAccount;
    private EpicContact contactMatch, me;
    private FeedContactList contactsList;
    private FeedContactList phoneFbContacts, 
    						phoneTwtContacts;
    private ArrayAdapter<EpicContact> contactsAA;
    private EpicDB.ContactListListener contactListListener;
	// check in onCreate if this is licensed or has more
	private SharedPreferences settings;
	private SharedPreferences.Editor editor;
	private ProgressDialog waitDialog;
    private FacebookConnectionModel mFacebookConn;
    private FacebookData mFacebookData;
    private int SOCIAL_TYPE;
     
    private Twitter11 mTwitter11;
    private TwitterData mTwitterData; 
    
    private FacebookDataListener mFacebookListener = new FacebookDataListener() {
		@Override
		public void onUpdate() {
			phoneFbContacts = mFacebookData.userFbContacts;
			phoneFbContacts.setSearch(searchEditText.getText().toString());
			runOnUiThread(updateUi);
		}

		@Override
		public void onComplete() {
			// updates are already complete...
		}
    };
    
    private TwitterDataListener mTwitterDataListener = new TwitterDataListener() {
    	@Override
    	public void onUpdate() {
    		phoneTwtContacts = mTwitterData.userTwtContacts;
    		phoneTwtContacts.setSearch(searchEditText.getText().toString());
    		runOnUiThread(updateUi);
    	}
    	
    	@Override
    	public void onComplete() {
    		
    	}
    }; 

    private SpaUserAccountListener accountListener;
	private Runnable updateUi = new Runnable() {

		@Override
		public void run() {
			updateUI();
		}
		
	};
    private EasyTracker easyTracker;
    
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater menuInfl = this.getSupportMenuInflater();
		menuInfl.inflate(R.menu.contacts_menu, menu);
		//return true;
	    return super.onCreateOptionsMenu(menu);
	}
	
    public void launchMenu(View view){
        this.getWindow().openPanel(Window.FEATURE_OPTIONS_PANEL, new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_MENU));
        this.getWindow().openPanel(Window.FEATURE_OPTIONS_PANEL, new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_MENU));
    }
    
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int itemId = item.getItemId();
		if (itemId == R.id.contactsHelp) {
			helpDialog();
			return true;
		} else if (itemId == R.id.configurationEdit) {
			configurationOptions();
			return true;
		} else if (itemId == R.id.contactsCancel) {
			return true;
		} else if (itemId == android.R.id.home) {
			finish();
			return true;
		}
		return false;
	}

    @Override
    public boolean onTouchEvent(MotionEvent e) {
    	//Log.i(TAG, "Touch event outside activity, do nothing...");
        mSpaUserAccount.updateAuthActivity();
    	return super.onTouchEvent(e);
    }
    
    @Override
    public void onUserInteraction() {
    	//Log.i(TAG, "Touch event...");
        mSpaUserAccount.updateAuthActivity();
    }
    
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_social_contacts_list);
        EpicFeedActionBar.actionbar_contacts(new WeakReference<SherlockListActivity>(this));
        savedState = savedInstanceState;
        spaContext = this;
        easyTracker = EasyTracker.getInstance(this);
        FlurryAgent.onStartSession(this, getString(R.string.flurryAppKey));
        FlurryAgent.onPageView();
        
        SOCIAL_TYPE = getIntent().getIntExtra("search_network", EpicFeedConsts.NO_DATA);
        Log.i(TAG, "+++++ SOCIAL_TYPE : " + SOCIAL_TYPE);
        contactListAct = new WeakReference<Activity>(this);
        mSpaUserAccount = EpicFeedController.getSpaUserAccount(this.getApplicationContext());
		mSpaUserAccount.setLoggedIn(true);
        accountListener = new SpaUserAccountAdapter(new WeakReference<Activity>(this)) {
        	
    		@Override
    		public void timeout() {
    			mSpaUserAccount.authenticated(contactListAct);
    			stopAds();
    			destroyAds();
    			finish();
    		}
        	
        };
	    
    	settings = getSharedPreferences(EpicFeedConsts.PREFS_NAME, 0);
    	editor = settings.edit();
    	EpicFeedController.getGaTracker(this.getApplicationContext()).set(Fields.SCREEN_NAME, TAG);
   	
		if (mSpaUserAccount.authenticated(new WeakReference<Activity>(this))) { 
    		// user is authenticated
	        mInflater = (LayoutInflater)spaContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	    	epicDB = EpicFeedController.getEpicDB(spaContext.getApplicationContext()); 

	        contactsList = new FeedContactList();
	        
	        if(SOCIAL_TYPE == EpicFeedConsts.FACEBOOK_DATA) {
	        	mFacebookConn = new FacebookConnectionModel(new WeakReference<Activity>(this));
		        mFacebookData = new FacebookData(mFacebookConn.setupFB(savedInstanceState), new EpicContact("me"));
		        mFacebookData.getFriendsData(mFacebookListener);
	        }
	        
	        if(SOCIAL_TYPE == EpicFeedConsts.TWITTER_DATA) {
		        mTwitter11 = new Twitter11(this, R.string.app_name, settings);
		        mTwitter11.setupTwitter();
		        mTwitterData = new TwitterData(mTwitter11, new EpicContact("me"));
		        mTwitterData.getTwitterFriend(mTwitterDataListener);
	        }

	        searchEditText = (EditText)findViewById(R.id.socialContactListSearchEdit);
	         
	        if(getIntent().getStringExtra("pID") != null) {
	        	getContact();
	        	Log.i(TAG, "+++++ pID : " + getIntent().getStringExtra("pID"));
	        } else {
	        	startActivity(new Intent(this, Act_ContactList.class));
	        }
	        
	        TextView contactLbl = (TextView)findViewById(R.id.socialContactListNotificationLbl);
	        if (getIntent().hasExtra("pID")) {
	        	String phoneId = getIntent().getStringExtra("pID");
		        if (phoneId != null && phoneId.length() > 0) contactLbl.setText(getString(R.string.authenticateAccountLoggedInLblTxt)+  " " +
			        		phoneId);
			}
	        if (getIntent().hasExtra("username")) {
	        	String username = getIntent().getStringExtra("username");
		        if (username != null && username.length() > 0) contactLbl.setText(getString(R.string.authenticateAccountLoggedInLblTxt)+  " " +
			        		username);
			}
	        
	        final View activityRootView = findViewById(R.id.socialContactsLayout);
	        activityRootView.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
	            @Override
	            public void onGlobalLayout() {
                    LinearLayout accountsTab = (LinearLayout)findViewById(R.id.socialContactsListTab); 
	                int heightDiff = activityRootView.getRootView().getHeight() - activityRootView.getHeight();
	                if (heightDiff > 200) { // if more than 100 pixels, its probably a keyboard...
	                	accountsTab.setVisibility(View.GONE);
	                } else {
	                	accountsTab.setVisibility(View.VISIBLE);
	                }
	             }
	        });
	        
	        setupSearchBar();
	        updateUI();
	
	        if (EpicFeedConsts.debugLevel > 5) 
        		Log.i(TAG, "onCreate: setting up OnItemClickListener");
	        ListView lv = getListView();
			lv.setTextFilterEnabled(false);
			lv.setOnItemClickListener(new OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
					if (EpicFeedConsts.debugLevel > 5) 
						Log.i(TAG, "ItemClickListener: text list for:" + contactsList.get(position).phoneID);
		    		if (contactsList.size() > position) {
			   	 		Intent txtConversationIntent = new Intent(spaContext, Act_FeedMessageList.class);
			    		txtConversationIntent.putExtra(EpicFeedConsts.AUTH_STATUS, true);
			    		txtConversationIntent.putExtra("pID", contactsList.get(position).phoneID);
			    		//txtConversationIntent.putExtra("me", me);
			    		startActivityForResult(txtConversationIntent, EpicFeedConsts.TEXTTHREAD);
		    		} else {
		    			Toast toast = Toast.makeText(spaContext, 
		    					getString(R.string.smsContactListNoContactTapMsg) , Toast.LENGTH_LONG);
		    			toast.show();
		    		}
				}
			});
        	if (EpicFeedConsts.debugLevel > 5) 
				Log.i(TAG, "OnItemLongClickListener: setting up OnItemLongClickListener");
			lv.setOnItemLongClickListener(new OnItemLongClickListener() {
				@Override
				public boolean onItemLongClick(AdapterView<?> parent, View view,
					int position, long id) {
						mSpaUserAccount.updateAuthActivity();
	        			if (EpicFeedConsts.debugLevel > 5) 
		        			Log.i(TAG, "OnItemLongClickListener: itemLongHold");
		 	       		return true;
		 	       	}
				}
			);
	
    	} else {
    		// not authenticated or timed out
    		finish();
    	}
    }
	
	@Override
	public void onRestart() {
		super.onRestart();
	}
	
	private TextWatcher tw;

	public void setupSearchBar() {
        tw = new TextWatcher() {
        	@Override
        	public void afterTextChanged(Editable s) {
        		// remove leading whitespace
        		
        		if(SOCIAL_TYPE == EpicFeedConsts.FACEBOOK_DATA) {
        			if (s.toString().trim().length() < 1) {
            			s.clear();
            			if (phoneFbContacts != null) phoneFbContacts.setSearch("");
            		}
            		else {
            			if (phoneFbContacts != null) 
            				phoneFbContacts.setSearch(s.toString());
            		}
                    updateUI();
        		}
        		
        		if(SOCIAL_TYPE == EpicFeedConsts.TWITTER_DATA) {
        			if (s.toString().trim().length() < 1) {
            			s.clear();
            			if (phoneTwtContacts != null) phoneTwtContacts.setSearch("");
            		}
            		else {
            			if (phoneTwtContacts != null)
            				phoneTwtContacts.setSearch(s.toString());
            		}
                    updateUI();
        		}
        	}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {
			}

			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
			}
        };
        searchEditText.addTextChangedListener(tw);
	}
	
	public String getMyPhoneNumber()
	{
	    return ((TelephonyManager) getSystemService(TELEPHONY_SERVICE))
	            .getLine1Number();
	}

	public void getContact() {
		EpicDBConn tmpConn = epicDB.open(true);
		contactMatch = epicDB.getContact(getIntent().getStringExtra("pID"));
		contactMatch.notifyOptions = epicDB.getNotification(contactMatch.phoneID);
		contactMatch.setContactFeeds(epicDB.getContactFeeds(contactMatch.phoneID));
		epicDB.close(tmpConn);
		if (EpicFeedConsts.DEBUG_FB) Log.d(TAG, "************** Loaded contact: " + contactMatch.phoneID);
		if (EpicFeedConsts.DEBUG_FB) Log.d(TAG, "************** contact feed size: " + contactMatch.getFeeds().size());
		if (EpicFeedConsts.debugLevel > 5) Log.i(TAG, "onCreate: pID - " + contactMatch.phoneID);
	}
	
	public void listenForMessages(boolean start) {
		if (start) {
			if (contactListListener == null) contactListListener = getContactListListener();
			epicDB.setContactListListener(contactListListener);
			EpicDBConn tmpConn = epicDB.open(false);
			if (epicDB.execAllUnreadMsgCount() > 0) {
				clearNotification();
				updateUI();
			}
			epicDB.close(tmpConn);
		} else {
			epicDB.removeContactListListener();
			contactListListener = null;
		}
	}
	
	public void showMe(View v) {
	}
	
	public EpicDB.ContactListListener getContactListListener() {
		return new EpicDB.ContactListListener() {

			@Override
			public void onNewMsgs() {
				if (mSpaUserAccount.authenticated(contactListAct)) {
					if (EpicFeedConsts.debugLevel > 7) 
						Log.i(TAG, "getCheckMessageListener: onNewMsg");
					updateUI();
					clearNotification();
				}
			}
		};
	}
   
	public void clearNotification() {
		// do not clear notifications if the user timed out or is not logged in
		if (mSpaUserAccount.authenticated(contactListAct)) {
			Timer clrNotify = new Timer(TAG + " Clear Notification", true);
			clrNotify.schedule(new TimerTask() { 
				@Override
				public void run() { 
					if (mSpaUserAccount.authenticated(contactListAct)) {
						((NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE))
							.cancel(SpaTextNotification.getIconId(0));
						((NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE))
							.cancel(SpaTextNotification.getIconId(1));
						((NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE))
							.cancel(SpaTextNotification.getIconId(2));
						((NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE))
							.cancel(SpaTextNotification.getIconId(3));
						((NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE))
							.cancel(SpaTextNotification.getIconId(4));
					}
				}
			}, 500);
		}
	}
	
	public boolean checkAuth(Intent appIntent) {
		if (appIntent.hasExtra(EpicFeedConsts.AUTH_STATUS)) {
			Boolean authPass = appIntent.getBooleanExtra(EpicFeedConsts.AUTH_STATUS, false);
			if (!authPass) {
				if (EpicFeedConsts.debugLevel > 2) Log.i(TAG, "checkAuth: set auth status false");
				editor.putBoolean(EpicFeedConsts.AUTH_STATUS, false);
				editor.commit();
			} else if (EpicFeedConsts.debugLevel > 2) Log.i(TAG, "checkAuth: set auth status true");

			return authPass;
		}
		return true;
	}
	
	private ArrayAdapter<EpicContact> getArrayAdapter(Context context, ArrayList<EpicContact> contactAA) {
		return new ArrayAdapter<EpicContact>(context, R.layout.contact_search_item, contactAA){
			@Override
			public View getView(final int position, View convertView, ViewGroup parent) {
				View row;
				if (null == convertView) {
					row = mInflater.inflate(R.layout.contact_search_item, parent, false);
				} else {
					row = convertView;
				}
				
				Log.i(TAG, "+++++ contactsList.size() : " + contactsList.size());
				if (contactsList.size() == 0) {
					//startActivity(new Intent(spaContext, Act_ContactList.class)
					//				  .putExtra("socialContactListFlag", true));
				}
				
				if(SOCIAL_TYPE == EpicFeedConsts.FACEBOOK_DATA) {
					ImageView avatar = (ImageView)row.findViewById(R.id.contactAvatar);
					avatar.setTag(this.getItem(position).getNameOrPhoneId());
					avatar.setImageDrawable(getResources().getDrawable(R.drawable.sample_avatar));
					downloadImg(contactsList.get(position).getFeed(ContactFeed.FEED_TYPE_FACEBOOK).profileURL, 
								avatar, this.getItem(position).getNameOrPhoneId());
				} 
				
				if(SOCIAL_TYPE == EpicFeedConsts.TWITTER_DATA) {
					ImageView avatar = (ImageView)row.findViewById(R.id.contactAvatar);
					avatar.setTag(this.getItem(position).getNameOrPhoneId());
					avatar.setImageDrawable(getResources().getDrawable(R.drawable.sample_avatar));
					downloadImg(contactsList.get(position).getFeed(ContactFeed.FEED_TYPE_TWITTER).profileURL,                                         
							avatar, this.getItem(position).getNameOrPhoneId());
				}
				
				row.setOnLongClickListener(new OnLongClickListener() {
					@Override
					public boolean onLongClick(View view) {
			        		if (EpicFeedConsts.debugLevel > 5) 
			        			Log.i(TAG, "OnItemLongClickListener: itemLongHold");
			 	       		return true;
			 	       	}
					}
				);
				row.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View view) {
				        mSpaUserAccount.updateAuthActivity();
						if (EpicFeedConsts.DEBUG_FB) 
							Log.i(TAG, "ItemClickListener: social contact info for:" + contactsList.get(position).phoneID);
					}
				});
				
				ImageButton addSocialBtn = (ImageButton) row.findViewById(R.id.contactEpicImg); 
				addSocialBtn.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View view) {
				        mSpaUserAccount.updateAuthActivity();
						if (EpicFeedConsts.DEBUG_FB)
							Log.i(TAG, "*  *  *  *  * ItemClickListener: text list for FB ID:" + contactsList.get(position).fbID);
				    	AlertDialog ad;
				    	final View helpTextView = mInflater.inflate(R.layout.linkview, null);
						final TextView nameTxt1 = (TextView)helpTextView.findViewById(R.id.linksviewLink1Name);
						final TextView nameTxt2 = (TextView)helpTextView.findViewById(R.id.linksviewLink2Name);
						if (EpicFeedConsts.DEBUG_FB) 
							Log.i(TAG, "itemClick: Connect Contact: " + contactMatch.phoneID +
								" to FB user: " + contactsList.get(position).getNameOrPhoneId() + " fb uid: " +
								contactsList.get(position).phoneID);
						nameTxt1.setText(contactMatch.getNameOrPhoneId());
						nameTxt2.setText(contactsList.get(position).name);
						ad = new AlertDialog.Builder(spaContext)
				        .setTitle(getString(R.string.actSocialContactsListConnectTitleTxt) + " "
				        		+ contactsList.get(position).name)
				        .setView(helpTextView)
						.setPositiveButton(spaContext.getString(R.string.sysYes), new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int id) {
				    	        mSpaUserAccount.updateAuthActivity();
				    	        contactMatch.fbID = contactsList.get(position).phoneID;
				    	        ContactFeed newFeed = new ContactFeed(contactMatch.phoneID);
				    	        EpicDBConn tmpConn = epicDB.open(true);
				    	        
				    	        if(SOCIAL_TYPE == EpicFeedConsts.FACEBOOK_DATA) {
				    	        	newFeed.setFeedType(ContactFeed.FEED_TYPE_FACEBOOK);
					    	        newFeed.setFeedSystemId(contactMatch.fbID);
					    	        contactMatch.addFeed(newFeed);
					        		epicDB.insertNewContactFeed(newFeed);
					            	epicDB.close(tmpConn);
						        	Toast toast = Toast.makeText(spaContext, "Facebook feed added for:"
						        				+ " " + contactMatch.getNameOrPhoneId(), Toast.LENGTH_LONG);
					        		toast.show();
				    	        } 
				    	        
				    	        if(SOCIAL_TYPE == EpicFeedConsts.TWITTER_DATA) {
					        		newFeed.setFeedType(ContactFeed.FEED_TYPE_TWITTER);
					        		newFeed.setFeedSystemId(contactMatch.fbID);
					        		contactMatch.addFeed(newFeed);
					        		tmpConn = epicDB.open(true);
					        		epicDB.insertNewContactFeed(newFeed);
					        		epicDB.close(tmpConn);
					        		Toast twitterToast = Toast.makeText(spaContext, "Twitter feed added for:" +
					        		    " " + contactMatch.getNameOrPhoneId(), Toast.LENGTH_LONG);
					        		twitterToast.show();
				    	        }
					            	setResult(Activity.RESULT_OK);
					            	dialog.cancel();

							 	Intent txtConversationIntent = new Intent(spaContext, Act_FeedMessageList.class);
								txtConversationIntent.putExtra(EpicFeedConsts.AUTH_STATUS, true);
								txtConversationIntent.putExtra("pID", contactMatch.phoneID);
								txtConversationIntent.putExtra("uid", contactMatch.fbID);
								txtConversationIntent.putExtra("social_type", SOCIAL_TYPE);
								startActivityForResult(txtConversationIntent, EpicFeedConsts.TEXTTHREAD);
							}
						})
						.setNegativeButton(spaContext.getString(R.string.sysNo), new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int id) {
				    	        mSpaUserAccount.updateAuthActivity();
								dialog.cancel();
							}
						})
				        .create();
						ad.show();
					}
				});

				TextView contactName = (TextView) row.findViewById(R.id.contactItem);
				TextView contactDescr = (TextView) row.findViewById(R.id.contactItemDescr);
				contactName.setTag(position);
				EpicContact contact = getItem(position);
				String cName = "", cDescr = "";
				if ((contact.name == null)||(contact.name.length() < 1))
					cName = PhoneNumberUtils.formatNumber(contact.phoneID);
				else cName = contact.name;
				if (contact.msgCount <= 0) {
					contactName.setText(cName);
					if (cDescr.length() > 0) contactDescr.setText(Html.fromHtml(cDescr));
					else contactDescr.setText(spaContext.getText(R.string.smsNewContactLblTxt));
				} else if (contact.newMsgs()) {
					String newMsgs = "";
					if (contact.newMsgCount > 0) {
						if (contact.newMsgCount > 1) newMsgs = contact.newMsgCount + " " + spaContext.getText(R.string.sysMsgNewMsgs);
						else newMsgs = contact.newMsgCount + " " + spaContext.getText(R.string.sysMsgNewMsg);
						if (contact.missedCallsCount > 0|
								contact.sendErrCount > 0|
								contact.deliveryErrCount > 0) newMsgs = newMsgs + " & ";
					}
					if ((contact.missedCallsCount > 0)&&(!contact.isCallBlocked())) {
						newMsgs = newMsgs + contact.missedCallsCount;
						if (contact.missedCallsCount > 1)  newMsgs = newMsgs + " " + spaContext.getText(R.string.sysMsgNewMissedCalls);
						else newMsgs = newMsgs + " " + spaContext.getText(R.string.sysMsgNewMissedCall);
						if (contact.sendErrCount > 0|
								contact.deliveryErrCount > 0) newMsgs = newMsgs + " & ";
					}
					if (contact.sendErrCount > 0) {
						newMsgs = newMsgs + contact.sendErrCount;
						if (contact.sendErrCount > 1)  newMsgs = newMsgs + " " + spaContext.getText(R.string.sysMsgSendErrorsMsg);
						else newMsgs = newMsgs + " " + spaContext.getText(R.string.sysMsgSendErrorMsg);
						if (contact.deliveryErrCount > 0) newMsgs = newMsgs + " & ";
					}
					if (contact.deliveryErrCount > 0) {
						newMsgs = newMsgs + contact.deliveryErrCount;
						if (contact.deliveryErrCount > 1)  newMsgs = newMsgs + " " + spaContext.getText(R.string.sysMsgDeliveryErrorsMsg);
						else newMsgs = newMsgs + " " + spaContext.getText(R.string.sysMsgDeliveryErrorMsg);
					}
					contactName.setText(cName); 
					contactDescr.setText(Html.fromHtml("<b><i>"+newMsgs+"</i></b>" + cDescr));
				} else {
					contactName.setText(cName);
					if (cDescr.length() > 0) contactDescr.setText(Html.fromHtml(cDescr));
					else contactDescr.setText(getText(R.string.smsTextMsgNoNewMsgs));
				}
				return row;
			}
			
			private EpicBitmaps bitmaps = EpicBitmaps.getInstance();
			
			private synchronized void downloadImg(final String imgURL, final ImageView avatar, final String id) {
		    	if (imgURL == null || 
		    			imgURL.length() < 5 ||
		    			avatar == null) return;
				Log.i(TAG, " downloadImg avatar: " + avatar.getTag() + " url: " + imgURL);
		    	if (bitmaps.hasUrl(imgURL)) {
					Log.i(TAG, " downloadImg avatar getting cached: " + avatar.getTag() + " url: " + imgURL);
					avatar.setImageDrawable(new BitmapDrawable(getResources(), bitmaps.get(imgURL)));
					avatar.setScaleType(ScaleType.CENTER_CROP);
					avatar.invalidate();
					return;
		    	}
				Log.i(TAG, " downloadImg avatar: " + avatar.getTag() + " id: " + id);
				EpicImageAdapter listener = new EpicImageAdapter() {
					@Override
					public void onLoaded(Bitmap b, String imgUrl) {
						Log.i(TAG, " downloadImg listener avatar: " + avatar.getTag() + " id: " + id);
						if (imgUrl.equals(imgURL)) {
							avatar.setImageBitmap(b);
							done = true;
						}
					}
				};
				bitmaps.get(imgURL, id, listener);
		    }
		};
	}
	
	public void helpDialog () {
		closeOptionsMenu();		
		easyTracker.send(MapBuilder
                .createEvent(EpicFeedConsts.gat_Button,     	// Event category (required)
                		EpicFeedConsts.gat_Click,			  	// Event action (required)
                		EpicFeedConsts.gatContactListHelp,	   			// Event label
                        1L)            						// Event value
                .build()
            );

		final View helpTextView = mInflater.inflate(R.layout.helpview, null);
		final TextView helpText = (TextView)helpTextView.findViewById(R.id.helptext);
		helpText.setText(Html.fromHtml(
				getText(R.string.contactsHelp).toString()));
		AlertDialog ad = new AlertDialog.Builder(spaContext)
        .setTitle(R.string.contactsHelpHeader)
        .setView(helpTextView)
        .setNeutralButton(getString(R.string.helpDoneBtnTxt), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                mSpaUserAccount.updateAuthActivity();
            	dialog.cancel();
            }
        })
        .create();
		ad.show();
	}
	
	public void configurationOptions() {
        mSpaUserAccount.updateAuthActivity();
	   	Intent configIntent = new Intent(this, SpaTextConfig.class);
		startActivityForResult(configIntent, EpicFeedConsts.CONFIG);
	}
	
	public void launchConfiguration(View v) {
		configurationOptions();
	}

 	public void cancelNotification(Context context) {
		((NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE))
			.cancel(R.drawable.notification_icon);
		((NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE))
			.cancel(R.drawable.notification_icon2);
		((NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE))
			.cancel(R.drawable.notification_icon3);
		((NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE))
			.cancel(R.drawable.notification_icon4);
		((NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE))
			.cancel(R.drawable.notification_icon5);
 	}
 	
	private void updateUI(){
		if (EpicFeedConsts.debugLevel > 7) Log.i(TAG, "updateUI: updating UI");
        if(SOCIAL_TYPE == EpicFeedConsts.FACEBOOK_DATA) {
        	contactsList.clear();
        	if (phoneFbContacts != null) {
    	        for (EpicContact tmpContact : phoneFbContacts.getSearchList()) {
    	    		if (tmpContact != null) contactsList.add(tmpContact);
    	        }
            }
        } 
        
        if(SOCIAL_TYPE == EpicFeedConsts.TWITTER_DATA) {
        	contactsList.clear();
        	if (phoneTwtContacts != null) {
    	        for (EpicContact tmpContact : phoneTwtContacts.getSearchList()) {
    	    		if (tmpContact != null) contactsList.add(tmpContact);
    	        }
            }
        }
        
        contactsAA = getArrayAdapter(spaContext, contactsList);
        setListAdapter(contactsAA);
		cancelNotification(this);
	}
	
    @Override
    protected void onActivityResult(int requestCode, int resultCode,
    		Intent data) {
    	if (requestCode == EpicFeedConsts.TEXTTHREAD) {
    		finish();
    	}
    	if (requestCode == EpicFeedConsts.CONTACTCALL) {
    		if (resultCode == RESULT_OK) {
    			finish();
    		}
    	}
    	if (requestCode == EpicFeedConsts.ABOUT_PRIVACY) {
    		if (!settings.getBoolean(EpicFeedConsts.privacyNoticeStatus, false)) {
    			finish();    			
    		}
    	}
    	// if the activity didn't finish from a contact call, update the UI
		updateUI();
    	super.onActivityResult(requestCode, resultCode, data);
    }
    
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
		if (EpicFeedConsts.debugLevel > 2) Log.i(TAG, "onConfigurationChanged: screen orientation changed");
    	onPause();
		if (EpicFeedConsts.debugLevel > 2) Log.i("SpaTextList.onConfigurationChanged", "onPause done... new onStop...");
		onStop();
		destroyAds();
    	onCreate(this.savedState);
    	onStart();
    	onResume();
    }
    
    @Override
    public void onStart() {
    	super.onStart();
		EasyTracker.getInstance(this).activityStart(this);  
		FlurryAgent.onStartSession(this, getString(R.string.flurryAppKey));
		FlurryAgent.onPageView();
    	FlurryAgent.logEvent(FlurryConsts.PAGE_VIEW_EVENT, FlurryConsts.getMap(FlurryConsts.PAGE_VIEW_EVENT, TAG));
        if(SOCIAL_TYPE == EpicFeedConsts.FACEBOOK_DATA)
        	Session.getActiveSession().addCallback(mFacebookConn.getStatusCallback());
   }

	@Override
    public void onResume() {
		super.onResume();
        mSpaUserAccount.updateAuthActivity();
		startAds();
		resumeAds();
        mSpaUserAccount.setSpaUserAccountListener(accountListener);
        listenForMessages(true);
    	if (!settings.getBoolean(EpicFeedConsts.privacyNoticeStatus, false)) {
   	 		Intent privacyIntent = new Intent(spaContext, Act_AcceptTerms.class);
    		startActivityForResult(privacyIntent, EpicFeedConsts.ABOUT_PRIVACY);
    	}
    	easyTracker.send(MapBuilder
                .createEvent(EpicGAConsts.USER_ACTION_CATEGORY,     			// Event category (required)
                			EpicGAConsts.USER_MESSAGING,			  		// Event action (required)
                			EpicGAConsts.USER_VIEW_SOCIAL_LIST,	   			// Event label
                            1L)            						// Event value
                .build()
            );
	}
	
	public void setupAdHandler() {
		if (getResources().getBoolean(R.bool.solitaireLicense)) {
			return;
		}
		if (EpicFeedConsts.debugLevel > 7) 
			Log.i(TAG, "setupAdHandler: setting up..");
		if (adListener == null) adListener = new MoPubAdListener(TAG, new WeakReference<Activity>(this), mmAdLayout);
		mmAdView.setBannerAdListener(adListener);
		//if (returnAdListener == null) returnAdListener = new AdHandler.ReturnAdListener() {
		returnAdListener = new MoPubAdHandler.ReturnAdListener() {
			@Override
			public void onGetNewAd() {
				runOnUiThread(returnAd);
			}
		};
		if (adHandler == null) {
			adHandler = new MoPubAdHandler(this, TAG + " null", mmAdView, mmAdLayout, adListener, returnAdListener);
		} else {
			if ((adHandler.getAdView() == null) || !adHandler.getAdView().equals(mmAdView)) {
				adHandler.setAdView(mmAdView);
			}
			if ((adHandler.getAdListener() == null) || !adHandler.getAdListener().equals(adListener)) {
				adHandler.setAdListener(adListener);
			}
			if ((adHandler.getReturnAdListener() == null) || !adHandler.getReturnAdListener().equals(returnAdListener)) {
				adHandler.setReturnAdListener(returnAdListener);
			}
		}
	}
	
	public void startAds() {
		if (getResources().getBoolean(R.bool.solitaireLicense)) {
			if (mmAdView == null)
				mmAdView = (MoPubView)findViewById(R.id.mmadContactsList);
			mmAdView.setVisibility(View.GONE);
			return;
		}
		
		if (mmAdLayout == null) mmAdLayout = (RelativeLayout)findViewById(R.id.contactsMMAdLayout);
		if (mmAdView == null) {
			mmAdView = (MoPubView)findViewById(R.id.mmadContactsList);
			mmAdView.setAdUnitId(getString(R.string.mopubAdID));
			mmAdView.setVisibility(View.GONE);
		}
        setupAdHandler();
		
		if (EpicFeedConsts.debugLevel > 7) 
			Log.i(TAG, "startAds: starting..");
		adHandler.resumeAds();
		adHandler.checkAd = true;
        if ((adHandlerThread == null)||(adHandlerThread.getState() == Thread.State.TERMINATED)
        		||!adHandlerThread.isAlive())
            	adHandlerThread = new Thread(null, adHandler, "Ad Handler Thread : " + TAG);
        if (adHandlerThread.getState() == Thread.State.NEW) {
        	adHandlerThread.start();
        }
	}

	private Runnable returnAd = new Runnable() {
		@Override
		public void run () {
			if (adHandler != null) {
				synchronized (adHandler.isBeingDestroyed) {
					if (adHandler.isBeingDestroyed) adListener.needAd = true;
				}
		    	if (EpicFeedConsts.debugLevel > 0) 
		    		Log.i(TAG, "returnAd thread: Got new ad...");
	    		Log.i("Ads", "SPA: requesting new ad " + TAG);
			} else {
	    		Log.i(TAG, "returnAd thread: adHandler dead");
			}
		}
	};
	
	public void resumeAds() {
		if (EpicFeedConsts.debugLevel > 2) Log.i(TAG, "resumeAds: setting state flags");
        adHandler.resumeAds();
		adHandler.checkAd = true;
	}
	
	public void stopAds() {
		if (EpicFeedConsts.debugLevel > 7) 
			Log.i(TAG, "stopAds: stopping..");
		if (adHandler != null) {
	        adHandler.stopAds();
		}
        try {
			synchronized (adHandler.isBeingDestroyed) {
				if (!adHandler.isBeingDestroyed) adListener.needAd = false;
			}
        } catch (Exception e) {
        	// don't worry about an error
        }
	}
	
	public void destroyAds() {
		if (EpicFeedConsts.debugLevel > 7) 
			Log.i(TAG, "destroyAds: destroying..");
        try {
			synchronized (adHandler.isBeingDestroyed) {
				adHandler.isBeingDestroyed = true;
			}
        } catch (Exception e) {
        	// don't worry about an error
        }
        try {
			synchronized (adHandler.isBeingDestroyed) {
				if (!adHandler.isBeingDestroyed) adListener.needAd = false;
			}
        } catch (Exception e) {
        	// don't worry about an error
        }
        
        try {
	        if (adHandler.getAdView() != null)
	    		try {
	    			((ViewGroup)adHandler.getAdView().getParent()).removeView(adHandler.getAdView());
	    		} catch (Exception e) {
	    		}
	
			//adView = null;
	
	    	if (adHandler != null) 
				try {
					synchronized (adHandler.isBeingDestroyed) {
						adHandler.isBeingDestroyed = true;
						adHandler.checkAd = false;
						adHandler.stopAdTimer();
				        adHandler.destroyAds();
				        mmAdView = null;
						adHandler = null;
					}
				} catch (Exception e) {
					// do nothing, ad will go away
				}
        } catch (Exception e) {
        	// do nothing, destroying anyway
        }
    }
	
	@Override
	public void onPause() {
        stopAds();
        listenForMessages(false);
        mSpaUserAccount.removeSpaUserAccountListener(accountListener);
		super.onPause();
	}

	@Override
	public void onStop() {
        if (adHandler != null) adHandler.stopAds();
		setResult(RESULT_OK);
		super.onStop();
		FlurryAgent.onEndSession(this);
		EasyTracker.getInstance(this).activityStop(this);  
        if(SOCIAL_TYPE == EpicFeedConsts.FACEBOOK_DATA)
        	Session.getActiveSession().removeCallback(mFacebookConn.getStatusCallback());
	}

	@Override
	public void onDestroy() {
		contactListListener = null;
		destroyAds();
		super.onDestroy();
	}
}