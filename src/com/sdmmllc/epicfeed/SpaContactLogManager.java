package com.sdmmllc.epicfeed;

import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.CallLog;
import android.telephony.PhoneNumberUtils;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sdmm.epicfeed.R;
import com.sdmmllc.epicfeed.data.EpicContact;
import com.sdmmllc.epicfeed.data.EpicDB;
import com.sdmmllc.epicfeed.data.EpicDBConn;
import com.sdmmllc.epicfeed.utils.EpicFeedConsts;
import com.sdmmllc.epicfeed.view.FeedMessageTheme;
import com.sdmmllc.epicfeed.view.FeedSeparatedMessageListAdapter;

public final class SpaContactLogManager extends ListActivity implements OnScrollListener {
	
	private String TAG = "SpaContactManager";
    private LayoutInflater mInflater;
    ArrayAdapter<EpicContact> contactsListAA, existingContactsListAA;
    FeedSeparatedMessageListAdapter contactsAA;
    List<EpicContact> contactsList, existingContactsList;
	EpicDB spaDB;
	Context spaContext;
	Bundle savedState;
	Button setupContactBtn;
	RelativeLayout unknownBlockLayout;
	View unknownBlockOptions;
	ContentResolver cr;
    private static final Uri CONTENT_URI = Uri.parse("content://call_log/calls"); 
	// check in onCreate if this is licensed or has more
	int maxContacts = EpicFeedConsts.freeVersionContacts,
		callLogsListLength = 50;
	boolean allowAddContact = true, blockOptionsDisplayed = false, blockOptionsFirst = true;
	String contactListSectionHeader, hiddenContactListSectionHeader,
		addOrCopyContact, otherCopyInfo;
	private Runnable viewContacts;
	ProgressDialog waitDialog;
	boolean initialLoadDone = false;
    SharedPreferences settings;
    SharedPreferences.Editor editor;
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater menuInfl = getMenuInflater();
		menuInfl.inflate(R.menu.contactlogmgr_menu, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int itemId = item.getItemId();
		if (itemId == R.id.contactManagerHelp) {
			helpDialog();
			return true;
		} else if (itemId == R.id.contactManagerAdd) {
			Intent setupContactIntent = new Intent(spaContext, SpaContactManager.class);
			setupContactIntent.putExtra(EpicFeedConsts.AUTH_STATUS, true);
			startActivityForResult(setupContactIntent, EpicFeedConsts.CONTACTSELECT);
			return true;
		} else if (itemId == R.id.contactAdd) {
			setupContact();
			return true;
		}
		return false;
	}

   @Override
    public void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);
    	setContentView(R.layout.contactcalllogselect);
        savedState = savedInstanceState;
        settings = getSharedPreferences(EpicFeedConsts.PREFS_NAME, 0);
        editor = settings.edit();
    	contactListSectionHeader = getString(R.string.smsContactManagerListSectionHeader);
    	hiddenContactListSectionHeader = getString(R.string.smsContactManagerHiddenListSectionHeader);
    	addOrCopyContact = getString(R.string.smsContactManagerAddOrCopyContact);
    	otherCopyInfo = getString(R.string.smsContactManagerOtherCopyInfo);
		checkAuth(getIntent());
    	if ((settings.contains(EpicFeedConsts.AUTH_STATUS))&&
        		settings.getBoolean(EpicFeedConsts.AUTH_STATUS, true)) { 
	        spaContext = this;
	    	if ((!EpicFeedController.getBoolean(R.bool.freeVersion))&&(EpicFeedController.isLicenseVerified())) 
	    		maxContacts = EpicFeedConsts.licensedVerContacts;
	        mInflater = (LayoutInflater)spaContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	        // Make the query. 
	        cr = getContentResolver();
	        
	    	Button blockBtn = (Button)findViewById(R.id.callLogBlockUnknownBtn);
	    	blockBtn.setOnClickListener(new OnClickListener () {
    			@Override
    			public void onClick(View v) {
    				//blockUnknown(v);
    			}
            });
	    	// remove for now
            blockBtn.setEnabled(false);
            blockBtn.setVisibility(View.GONE);
	    	TextView blockHeader = (TextView)findViewById(R.id.callLogBlockUnknownHeader);
	    	blockHeader.setEnabled(false);
	    	blockHeader.setVisibility(View.GONE);
	    	
	    	if (EpicFeedConsts.debugLevel > 3) Log.i(TAG, "onCreate: opening and closing database");
	    	spaDB = EpicFeedController.getEpicDB(spaContext);
	
			setResult(RESULT_OK);
	        waitDialog = ProgressDialog.show(spaContext, getString(R.string.smsContactManagerLoadingCallLogsTitle),
	        		"Please wait...", true);
	        viewContacts = new Runnable() {
	        	@Override
	        	public void run() {
	        		queryContacts();
	        	}
	        };
	        Thread thread = new Thread(null, viewContacts, "RetrievingContacts");
	        thread.start();
	        getListView().setFastScrollEnabled(true);
    	} else {
    		// user is not authenticated
    		finish();
    	}
    }
   
	@Override
	public void onRestart() {
		super.onRestart();
		checkAuth(getIntent());
		if (!settings.getBoolean(EpicFeedConsts.AUTH_STATUS, false)) finish();;
	}
	
	public void checkAuth(Intent appIntent) {
		if (appIntent.hasExtra(EpicFeedConsts.AUTH_STATUS)) {
			Boolean authPass = appIntent.getBooleanExtra(EpicFeedConsts.AUTH_STATUS, false);
			if (!authPass) {
				editor.putBoolean(EpicFeedConsts.AUTH_STATUS, false);
		    	editor.commit();
			}
		}
	}
   
	public void onScroll(AbsListView view,
			int firstVisible, int visibleCount, int totalCount) {
		boolean loadMore =
			firstVisible + visibleCount + 10 >= totalCount;
		if (loadMore) {
			contactsAA.limitCount += 50;
			contactsAA.notifyDataSetChanged();
		}
	}
	
	public void onScrollStateChanged (AbsListView view, int s) {
		
	}
	
	private Runnable returnContacts = new Runnable() {
		@Override
		public void run () {
	        loadUI();
	        waitDialog.cancel();
	        initialLoadDone = true;
		}
	};
    
	private ArrayAdapter<EpicContact> getContactsArrayAdapter(Context context, final List<EpicContact> conList) {
		return new ArrayAdapter<EpicContact>(context, R.layout.contact_select_item, conList){
			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
				View row;
				if (null == convertView) {
					row = mInflater.inflate(R.layout.contact_select_item, parent, false);
				} else {
					row = convertView;
				}
				bindView(position, row);
				return row;
			}
			private void bindView(int position, View row) {
				final EpicContact contact = conList.get(position);
				TextView contactInfo = (TextView) row.findViewById(R.id.contactSelInfo);
				TextView contactNum = (TextView) row.findViewById(R.id.contactSelNumTxt);
				Button contactAdd = (Button) row.findViewById(R.id.contactSelBtn);
				ImageView contactPic = (ImageView) row.findViewById(R.id.contactSelPic);
				// set values accordingly
				contactAdd.setText(getString(R.string.smsContactManagerAddContactMsg));
				contactAdd.setVisibility(View.VISIBLE);
				if (allowAddContact) {
					contactAdd.setEnabled(true);
				} else {
					contactAdd.setEnabled(false);
				}
				final String actionMsg = getString(R.string.smsContactManagerAddContactConfirm) + " ";
				contactAdd.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						AlertDialog.Builder builder = new AlertDialog.Builder(spaContext);
						builder.setMessage(actionMsg + contact.name.trim()
								+ " - " + PhoneNumberUtils.formatNumber(contact.phoneID.trim()) + " ?")
						.setCancelable(false)
						.setPositiveButton(getString(R.string.sysYes), new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int id) {
						    	Intent editContactIntent = new Intent(spaContext, SpaTextSMSSetup.class);
						    	editContactIntent.putExtra("pContact", contact.name.trim());
						    	editContactIntent.putExtra("pID", contact.phoneID.trim());
						    	startActivityForResult(editContactIntent, EpicFeedConsts.CONTACTSETUP);
							}
						})
						.setNegativeButton(getString(R.string.sysNo), new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
							}
						});
						AlertDialog alert = builder.create();
						alert.show();
					}
				});
				if (EpicFeedConsts.debugLevel > 5) Log.i(TAG, "getContactsArrayAdapter: adding contact:" + contact.name + " Type: " + contact.type);
				String tmpType = "";
				if (contact.type.length() > 0) tmpType = contact.type + ": ";
				contactInfo.setText(contact.name);
				if (contact.isBlocked())
					contactNum.setText(Html.fromHtml(
							tmpType + PhoneNumberUtils.formatNumber(contact.phoneID) + 
						"<br><b><i>" + getString(R.string.smsContactManagerBlockedContactMsg)+ "</b></i>"));
				else 
					contactNum.setText(tmpType + PhoneNumberUtils.formatNumber(contact.phoneID));
				if (contact.exists) {
					if (contact.isBlocked())
						contactAdd.setText(getString(R.string.smsContactManagerBlockedContactMsg));
					else
						contactAdd.setText(getString(R.string.smsContactManagerHiddenContactMsg));
					contactAdd.setEnabled(false);
					row.setBackgroundResource(R.color.instDarkGray);
					if (contact.isBlocked())
						contactPic.setImageDrawable(getResources().getDrawable(R.drawable.blockicon));
					else
						contactPic.setImageDrawable(getResources().getDrawable(R.drawable.icon1));
					contactPic.setPadding(0, 0, 10, 0);
					contactPic.setVisibility(View.VISIBLE);
				} else {
					contactPic.setImageDrawable(null);
					contactPic.setPadding(0, 0, 0, 0);
					contactPic.setVisibility(View.INVISIBLE);
				}
			}
		};
	}
	
	
	private ArrayAdapter<EpicContact> getHiddenContactsArrayAdapter(Context context, final List<EpicContact> conList) {
		return new ArrayAdapter<EpicContact>(context, R.layout.contact_select_item, conList){
			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
				View row;
				if (null == convertView) {
					row = mInflater.inflate(R.layout.contact_select_item, parent, false);
				} else {
					row = convertView;
				}
	            if (EpicFeedConsts.debugLevel > 5) 
        			Log.i(TAG, "getHiddenContactsArrayAdapter: no contacts?");
				bindView(position, row);
				return row;
			}
			private void bindView(int position, View row) {
				final EpicContact contact = conList.get(position);
				row.setBackgroundResource(R.color.instDarkGray);
				// get list item views
				TextView contactInfo = (TextView) row.findViewById(R.id.contactSelInfo);
				TextView contactNum = (TextView) row.findViewById(R.id.contactSelNumTxt);
				Button contactAdd = (Button) row.findViewById(R.id.contactSelBtn);
				ImageView contactPic = (ImageView) row.findViewById(R.id.contactSelPic);

				if (contact.phoneID.length() == 0) { //contact is dummy contact
					contactPic.setImageDrawable(null);
					contactPic.setPadding(0, 0, 0, 0);
					contactPic.setVisibility(View.INVISIBLE);
					contactAdd.setText("");
					contactAdd.setEnabled(false);
					contactAdd.setVisibility(View.INVISIBLE);
				} else {
					if (contact.isBlocked()|contact.isCallBlocked())
						contactPic.setImageDrawable(getResources().getDrawable(R.drawable.blockicon));
					else
						contactPic.setImageDrawable(getResources().getDrawable(R.drawable.icon1));
					contactPic.setPadding(0, 0, 10, 0);
					contactPic.setVisibility(View.VISIBLE);
					contactAdd.setText(getString(R.string.smsContactManagerEditRemoveContactMsg));
					contactAdd.setEnabled(true);
					contactAdd.setVisibility(View.VISIBLE);
					contactAdd.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							AlertDialog.Builder builder = new AlertDialog.Builder(spaContext);
							builder.setMessage(getString(R.string.smsContactManagerEditRemoveContactConfirm) +
									" " + contact.getNameOrPhoneId() + " ?")
							.setCancelable(false)
							.setPositiveButton(getString(R.string.sysYes), new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog, int id) {
							    	Intent editContactIntent = new Intent(spaContext, SpaTextSMSSetup.class);
							    	editContactIntent.putExtra("pID", contact.phoneID);
							    	startActivityForResult(editContactIntent, EpicFeedConsts.CONTACTSETUP);
								}
							})
							.setNegativeButton(getString(R.string.sysNo), new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog, int id) {
									dialog.cancel();
								}
							});
							AlertDialog alert = builder.create();
							alert.show();
						}
					});
				}
				if (EpicFeedConsts.debugLevel > 5) Log.i(TAG, "ArrayAdapter: adding contact:" + contact.name + " Type: " + contact.type);
				String tmpType = "";
				if (contact.type.length() > 0) tmpType = contact.type + ": ";
				contactInfo.setText(contact.name);
				if (contact.isBlocked()|contact.isCallBlocked()) {
					String cDescr = "<i>";
					if (contact.isCallBlocked()) 
						cDescr = cDescr + " " + spaContext.getText(R.string.callLogBlockedContactsDescr);
					if (contact.isCallBlocked()&contact.isBlocked())
						cDescr = cDescr + " &amp;";
					cDescr = cDescr + " <b>" + spaContext.getText(R.string.sysContactListBlockedEndDescr)
							+ "</b></i>";
					contactNum.setText(Html.fromHtml(
							tmpType + PhoneNumberUtils.formatNumber(contact.phoneID) + 
						"<br>" + cDescr));
				} else 
					contactNum.setText(tmpType + PhoneNumberUtils.formatNumber(contact.phoneID));
			}
		};
	}

	public void setupContact() {
	   	Intent spaSMSSetupIntent = new Intent(this, SpaTextSMSSetup.class);
		startActivityForResult(spaSMSSetupIntent, EpicFeedConsts.CONTACTSETUP);
	}
	
	private void setupAddContactBtn () {
        setupContactBtn = (Button)findViewById(R.id.callLogAddContactBtn);
    	if (!allowAddContact) setupContactBtn.setText(getString(R.string.smsContactSelMaxAddBtn) 
    			+ " (" + maxContacts + ")");
    	else setupContactBtn.setText(getString(R.string.smsContactSelAddBtn));
		setupContactBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (EpicFeedConsts.debugLevel > 5) Log.i(TAG, "setupContactBtn.setOnClickListener");
				if (allowAddContact) {
			    	Intent setupContactIntent = new Intent(spaContext, SpaTextSMSSetup.class);
			    	startActivityForResult(setupContactIntent, EpicFeedConsts.CONTACTSETUP);
				} else {
		    		Toast toast = Toast.makeText(spaContext, 
		    				getString(R.string.smsContactManagerNoMoreContactsMsg), Toast.LENGTH_LONG);
		    		toast.show();
				} 
			}
		});
	}
	
	public void setupBackBtn (View v) {
		if (EpicFeedConsts.debugLevel > 5) Log.i(TAG, "setupBackBtn.setOnClickListener");
		finish();
	}
	
	private void queryContacts() {
        contactsList = new ArrayList<EpicContact>();
        try { 
        	EpicContact[] tmpLogList = new EpicContact[callLogsListLength];
            int count = 0, countAdded =0;
        	// get all call logs
        	String[] projection = {CallLog.Calls.NUMBER, CallLog.Calls.CACHED_NAME};
            Cursor c =  spaContext.getContentResolver().query(CONTENT_URI, 
        		projection, // return just the number 
        		null, // no filter
        		null, 
        		CallLog.Calls.DEFAULT_SORT_ORDER); 
     		if (EpicFeedConsts.debugLevel > 8) 
            	Log.i(TAG, "Cursor Count" + c.getCount());
            if (c.getCount() < 1) return;
            c.moveToFirst();
            while (!c.isAfterLast() & countAdded < callLogsListLength) {
            	String pID = PhoneNumberUtils.stripSeparators(c.getString(0));
            	if (pID != null) {
	            	EpicContact tmpContact = new EpicContact(pID);
	            	tmpContact.name = c.getString(1);
	            	if (tmpContact.name == null) tmpContact.name = "";
	            	tmpContact.exists = false;
	         		if (EpicFeedConsts.debugLevel > 8) 
	        			Log.i(TAG, "queryContacts: found call log, pID:" + pID);
	         		tmpLogList[countAdded] = tmpContact;
	         		countAdded++;
            	}
            	count++;
            	c.moveToNext();
            }
            for (int i=0; i<countAdded; i++) {
            	contactsList.add(tmpLogList[i]);
            }
        } catch(Exception e) { 
            e.getMessage(); 
        } 
        
		// find duplicates
        List<EpicContact> tmpContactsList = new ArrayList<EpicContact>();
        boolean found = false;
		for (EpicContact tmpContact:contactsList) {
			// if it has not been found, search for it
			for (EpicContact tmpContact2:tmpContactsList) {
				if ((tmpContact != null)&&
						(tmpContact.phoneID != null)) {
					// if it hasn't been found, it's unique
		            if (EpicFeedConsts.debugLevel > 9) 
	            		Log.i(TAG, "queryContacts: contactList unique:" + tmpContact.name);
					// don't check previous values
					if (PhoneNumberUtils.compare(tmpContact.phoneID, 
							tmpContact2.phoneID)) {
						found = true;
					}
				}
			} 
			if (!found) {
				tmpContactsList.add(tmpContact);
			}
			found = false;
		}
		contactsList = tmpContactsList;
        if (EpicFeedConsts.debugLevel > 5) {
        	Log.i(TAG, "queryContacts: thread sleep");
        	try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				Log.i(TAG, "queryContacts: threw error on sleep");
				e.printStackTrace();
			}
        }
		runOnUiThread(returnContacts);
	}
	
	private void loadUI() {
		EpicDBConn tmpConn = spaDB.open(false);
        existingContactsList = spaDB.allContacts();
		spaDB.close(tmpConn);
        if (existingContactsList.size() >= maxContacts) allowAddContact = false;
        else allowAddContact = true;
        if (EpicFeedConsts.debugLevel > 5) 
        	Log.i(TAG, "loadUI: existingContactsList.length:" + existingContactsList.size());
        if (existingContactsList.size() == 0) {
        	EpicContact tmpContact = new EpicContact("");
        	tmpContact.name = addOrCopyContact;
        	tmpContact.type = otherCopyInfo;
        	existingContactsList.add(tmpContact);
        } else {
			// mark existing
        	boolean found = false;
			for (int i = 0; i < contactsList.size(); i++) {
				for (EpicContact tmpExistingContact:existingContactsList) {
					if (PhoneNumberUtils.compare(tmpExistingContact.phoneID, 
							contactsList.get(i).phoneID)) {
						contactsList.get(i).exists = true;
						contactsList.get(i).setBlocked(tmpExistingContact.isBlocked());
						found = true;
					}
				}
				if (!found) {
					contactsList.get(i).exists = false;
					contactsList.get(i).setBlocked(false);
				} else found = false;
			}
        }
        if (EpicFeedConsts.debugLevel > 5) Log.i(TAG, "loadUI: contactList adding:" + contactsList.size() + " contacts");
        contactsAA = new FeedSeparatedMessageListAdapter(this, new FeedMessageTheme(spaContext));
    	existingContactsListAA = getHiddenContactsArrayAdapter(
	        	spaContext, existingContactsList);
        contactsAA.addSection(hiddenContactListSectionHeader, existingContactsListAA);
        contactsListAA = getContactsArrayAdapter(
            	spaContext, contactsList);
        contactsAA.addSection(contactListSectionHeader, contactsListAA);
        setListAdapter(contactsAA);
		setupAddContactBtn();
		getListView().setOnScrollListener(this);
	}
	
	private void updateUI() {
		loadUI();
	}

	public void helpDialog () {
		closeOptionsMenu();
		final View helpTextView = mInflater.inflate(R.layout.helpview, null);
		final TextView helpText = (TextView)helpTextView.findViewById(R.id.helptext);
		helpText.setText(Html.fromHtml(
				getText(R.string.contactManagerLogHelp).toString()));
		AlertDialog ad = new AlertDialog.Builder(spaContext)
        .setTitle(R.string.contactManagerLogHelpHeader)
        .setView(helpTextView)
        .setNeutralButton(getString(R.string.helpDoneBtnTxt), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            	dialog.cancel();
            }
        })
        .create();
		ad.show();
	}

    @Override
    protected void onActivityResult(int requestCode, int resultCode,
    		Intent data) {
    	if (requestCode == EpicFeedConsts.CONTACTSETUP) {
    		if (resultCode == RESULT_OK) {
    			updateUI();
    		}
    	}
    	super.onActivityResult(requestCode, resultCode, data);
    }
    
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
		if (EpicFeedConsts.debugLevel > 2) Log.i(TAG, "onConfigurationChanged: screen orientation changed");
    	super.onPause();
    }

	@Override
    public void onResume() {
		super.onResume();
		if (initialLoadDone) updateUI();
	}
}
