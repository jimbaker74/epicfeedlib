package com.sdmmllc.epicfeed;

import java.util.Date;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.sdmmllc.epicfeed.data.EpicContact;
import com.sdmmllc.epicfeed.data.EpicDB;
import com.sdmmllc.epicfeed.data.EpicDBConn;
import com.sdmmllc.epicfeed.utils.EpicFeedConsts;

public class CallIntentReceiver extends BroadcastReceiver {       
	String TAG = "CallIntentReceiver";
    EpicDB spaDB;
    SharedPreferences settings;
    Context spaContext = null;
    boolean onCall = false;
    boolean outgoingCall = false;
    Date startCall = null;
	String pID = "", callStateStr = "";
	TelephonyManager telephony;

	@Override
	public void onReceive(Context context, Intent intent) {
		if (EpicFeedConsts.debugLevel > 3) 
			Log.i(TAG, "Phone state changed...");
		spaContext = context;
		if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
			if (EpicFeedConsts.debugLevel > 8) Log.i(TAG, "onReceive: boot complete, resetting monitors");
	    	return;
		}
		telephony = (TelephonyManager) spaContext.getSystemService(Context.TELEPHONY_SERVICE);
        Bundle bundle = intent.getExtras();
		// check if outgoing call...
		if (intent.hasExtra(Intent.EXTRA_PHONE_NUMBER)) {
			pID = intent.getStringExtra(Intent.EXTRA_PHONE_NUMBER);
			if (EpicFeedConsts.debugLevel > 8) Log.i(TAG, "onReceive: outgoing call / outgoing number: " + pID);
			outgoingCall = true;
		} else if (bundle.containsKey(TelephonyManager.EXTRA_INCOMING_NUMBER)){
	        pID = bundle.getString(TelephonyManager.EXTRA_INCOMING_NUMBER);
			if (EpicFeedConsts.debugLevel > 8) Log.i(TAG, "onReceive: incoming call / incoming number: " + pID);
			outgoingCall = false;
		}
		if (EpicFeedConsts.debugLevel > 8) 
			callStateStr = logState();
		// should have number at this point, if not send a blank pID and call state
		if ((pID == null | pID.length() < 1) & 
				(telephony.getCallState() == TelephonyManager.CALL_STATE_IDLE)) {
			//monitorCall("", telephony.getCallState());
			return;
		}
		if (spaDB == null) spaDB = EpicFeedController.getEpicDB(context);
		EpicDBConn tmpConn = spaDB.open(false);
		if (EpicFeedConsts.debugLevel > 1) Log.i(TAG, "onReceive: filter phone calls");
		if (EpicFeedConsts.debugLevel > 8) Log.i(TAG, "onReceive: processing number: " + pID);
		String filterPID = spaDB.comparePhoneID(pID);
		if (filterPID.length() > 0) {
			// since contact is on list, get contact info
			EpicContact tmpContact = spaDB.getContact(filterPID);
			if (EpicFeedConsts.debugLevel > 8) Log.i(TAG, "onReceive: found ID, setting up call monitor: " + pID);
			if (tmpContact.isCallBlocked()|tmpContact.isCallLogDelete()) monitorCall(tmpContact.phoneID, telephony.getCallState());
 		}
		if (spaDB.isOpen()) spaDB.close(tmpConn);
	}
	
	public void monitorCall(String pID, int callType) {
		if (EpicFeedConsts.debugLevel > 8) Log.i(TAG, "monitorCall: setting monitor: " + pID + " call state:" + callStateStr);
	}
	
	public void cancelInScreenCall(String pID, Intent intent) {
		Bundle extras = intent.getExtras();
		if (EpicFeedConsts.debugLevel > 8) 
			Log.i(TAG, "cancelInScreenCall: setting monitor: " + pID + " call state:" + callStateStr);
    	Intent cancelScreen = new Intent();
    	cancelScreen.setClassName("com.android.phone", "com.android.phone.InCallScreen");
    	extras.putString(android.telephony.TelephonyManager.EXTRA_INCOMING_NUMBER, "5558501111");
    	cancelScreen.putExtras(extras);
    	cancelScreen.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);		
    	spaContext.startActivity(cancelScreen);
	}
	
	public String logState() {
		switch (telephony.getCallState()) {
		case android.telephony.TelephonyManager.CALL_STATE_IDLE:
			Log.i(TAG, "logState: telephony.getCallState:idle");
			return "IDLE";
		case android.telephony.TelephonyManager.CALL_STATE_OFFHOOK:
			Log.i(TAG, "logState: telephony.getCallState:offhook");
			return "OFFHOOK";
		case android.telephony.TelephonyManager.CALL_STATE_RINGING:
			Log.i(TAG, "logState: telephony.getCallState:ringing");
			return "RINGING";
		}
		Log.i(TAG, "Call state unknown");
		return "UNKNOWN";
	}
}

