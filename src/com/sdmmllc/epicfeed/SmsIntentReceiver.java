package com.sdmmllc.epicfeed;

import java.util.Calendar;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;

import com.sdmmllc.epicfeed.data.EpicContact;
import com.sdmmllc.epicfeed.data.EpicDB;
import com.sdmmllc.epicfeed.data.EpicDBConn;
import com.sdmmllc.epicfeed.rednote.RednoteParser;
import com.sdmmllc.epicfeed.utils.EpicFeedConsts;
import com.sdmmllc.epicfeed.view.FeedTextMsg;
import com.sdmmllc.superdupersmsmanager.sdk.SDSmsConsts;
import com.sdmmllc.superdupersmsmanager.sdk.SDSmsManager;

public class SmsIntentReceiver extends BroadcastReceiver {

	String TAG = "SmsIntentReceiver";
    public static int debugLevel = EpicFeedConsts.debugLevel;
    public static final int MSGNOTIFICATION = 110;
    private EpicDB spaDB;
	private SharedPreferences.Editor editor;
	private SharedPreferences settings;
	private RednoteParser rnParser;
	private SDSmsManager smsManager;
	private String abort = "abort";

	private SmsMessage[] getMessagesFromIntent(Intent intent) {
		SmsMessage retMsgs[] = null;
		Bundle bdl = intent.getExtras();
		try{
			Object pdus[] = (Object [])bdl.get("pdus");
			retMsgs = new SmsMessage[pdus.length];
			for(int n=0; n < pdus.length; n++) {
				byte[] byteData = (byte[])pdus[n];
				retMsgs[n] = SmsMessage.createFromPdu(byteData);
			}	
		} catch(Exception e) {
			Log.e("GetMessages", "fail", e);
		}
		return retMsgs;
	}
	
	private void removeMessageFromIntent(Intent intent) {
		// msg consumed, dispose of broadcast
		abortBroadcast();
	}
	
	public boolean onReceive(Intent intent) {
		onReceive(EpicFeedController.getContext(), intent);
		if (intent.hasExtra(abort)) return intent.getBooleanExtra(abort, false);
		return false;
	}
	
	@Override
	public void onReceive(Context context, Intent intent) {
		if (intent.getAction() != null) Log.i(TAG, "Intent received - action: " + intent.getAction());
		// start messaging service on Boot, if subscriber
		if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
			SharedPreferences settings = context.getSharedPreferences(EpicFeedConsts.PREFS_NAME, 0);
		}
		
		if (!EpicFeedConsts.hideMessages) return;

		if((!intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED"))) {
			return;
		}
		
		rnParser = new RednoteParser(context);
		SmsMessage msg[] = getMessagesFromIntent(intent);
        settings = context.getSharedPreferences(EpicFeedConsts.PREFS_NAME, 0);
        editor = context.getSharedPreferences(EpicFeedConsts.PREFS_NAME, 0).edit();
		
		for(int i = 0; i < msg.length; i++) {
			// ignore null entries
			if (msg[i] != null) {
				// get the sender address to determine if it should be hidden
				String pID = msg[i].getOriginatingAddress();
		    	spaDB = EpicFeedController.getEpicDB(context);
				if (EpicFeedConsts.debugLevel > 8) Log.i(TAG, "onReceive: open database");
				EpicDBConn tmpConn = spaDB.open(false);
				if (EpicFeedConsts.debugLevel > 8) Log.i(TAG, "onReceive: filter contacts");
				String filterPID = spaDB.comparePhoneID(pID);
				if (filterPID.length() > 0) {
					String sdsmsDup = "";
					if (intent.hasExtra(SDSmsConsts.SDSMS_DUPLICATE)) sdsmsDup = SDSmsConsts.SDSMS_DUPLICATE;
					if (!SDSmsConsts.SDSMS_DUPLICATE.equals(sdsmsDup)) {
						// if a phone number is found, the message should be consumed
						if (EpicFeedConsts.debugLevel > 8) Log.i(TAG, "onReceive: checking for null msgs");
						String message = msg[i].getDisplayMessageBody();
						EpicContact tmpContact = new EpicContact(pID);
						if (EpicFeedConsts.debugLevel > 8) Log.i(TAG, "onReceive: originating address:" + pID);
						// make sure a null/blank message was not sent
						if (message != null) message = message.trim();
						if (message != null && message.length() > 0) {
							FeedTextMsg tmpMsg = new FeedTextMsg(); 
							tmpMsg.setReceivedMsg(true);
							tmpMsg.setNewMsg(true);
							tmpMsg.msgDate = Calendar.getInstance().getTime();
							tmpMsg.msgTxt = message;
							tmpMsg.msgPhoneID = filterPID;
							try {
								// get contact info, including message block settings
								tmpContact = spaDB.getContact(filterPID);
								// get notification options
								tmpContact.notifyOptions = spaDB.getNotification(filterPID);
								// is contact is not blocked, process normally
								if (!tmpContact.isBlocked()) spaDB.insertTxtEntry(tmpMsg);
								// if messages are blocked, then only save if the flag is set
								else if (tmpContact.isBlocked()&&tmpContact.saveBlockedMsgs()) {
									tmpMsg.setBlockedMsg(true);
									spaDB.insertTxtEntry(tmpMsg);
								}
							} catch (Exception e) {
								// do nothing - just need to make sure all of these happen together
								// not a transaction-safe implementation, but a reminder to do so
							}
							// stop message not broadcast, regardless of what happens above
							if (!tmpContact.isBlocked()) {
								EpicFeedController.smsHandler.sendNotification(tmpContact);
								editor.putInt(EpicFeedConsts.receivedMsgCnt, settings.getInt(EpicFeedConsts.receivedMsgCnt, 0) + 1);
				                if (rnParser.hasRednote(message)) {
				                	editor.putInt(EpicFeedConsts.rnReceiveMsgCnt, settings.getInt(EpicFeedConsts.rnReceiveMsgCnt, 0) + 1);
				                }
								editor.commit();
							} else {
								editor.putInt(EpicFeedConsts.blockedMsgCnt, settings.getInt(EpicFeedConsts.blockedMsgCnt, 0) + 1);
								editor.commit();
							}
						}
						removeMessageFromIntent(intent);
					} else {
						Log.i(TAG, "aborting intent");
						setResultCode(0);
						setResultData(SDSmsConsts.SDSMS_ABORT);
					}
				}
				spaDB.close(tmpConn);
			} else {
				// message was null, do nothing - it has no originating address
			}
		}
	}
}
