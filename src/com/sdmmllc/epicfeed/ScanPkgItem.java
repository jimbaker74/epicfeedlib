package com.sdmmllc.epicfeed;

import android.content.pm.PackageInfo;
import android.graphics.drawable.Drawable;
import android.util.Log;

public class ScanPkgItem {
	
	public static final String TAG = "PkgScanInfo";
	
	public PackageInfo pkgPerms;
	public int 
		pkgFlgs = 0, 
		versionCode = 0, 
		pkgCats = 0, 
		recPriority = 0;
    
	public String
    	appName = "", 
    	pkgName = "", 
    	versionName = "", 
    	permFound = "",
    	reqPermFound = "", 
    	filtersFound = "";
    
    public Drawable icon;
    public long installDt = 0;

    public void print() {
        Log.i(TAG, appName + "\t" + pkgName + "\t" + versionName + "\t" + versionCode);
    }
	
	public static int SAFE = 1, UNKNOWN = 2, UNSAFE = 4, UNKNOWN_HIGH_RISK = 8, USR_SAFE = 16,
		USR_UNSAFE = 32, NEW_INSTALL = 64;
	public static int CATEGORY_SMS = 1, CATEGORY_PHONE = 2, CATEGORY_LOC = 4, CATEGORY_EMAIL = 8,
		CATEGORY_CAMERA = 16, CATEGORY_MIC = 32, CATEGORY_FILES = 64;
	
	public void setCategoryFiles(boolean file_risk) {
		if (file_risk) pkgCats |= CATEGORY_FILES;
		else pkgCats = pkgCats & ~CATEGORY_FILES;
	}
	
	public boolean isCategoryFiles() {
		return ((pkgCats & CATEGORY_FILES) == CATEGORY_FILES);
	}

	public void setCategoryMic(boolean mic_risk) {
		if (mic_risk) pkgCats |= CATEGORY_MIC;
		else pkgCats = pkgCats & ~CATEGORY_MIC;
	}
	
	public boolean isCategoryMic() {
		return ((pkgCats & CATEGORY_MIC) == CATEGORY_MIC);
	}

	public void setCategoryCamera(boolean camera_risk) {
		if (camera_risk) pkgCats |= CATEGORY_CAMERA;
		else pkgCats = pkgCats & ~CATEGORY_CAMERA;
	}
	
	public boolean isCategoryCamera() {
		return ((pkgCats & CATEGORY_CAMERA) == CATEGORY_CAMERA);
	}

	public void setCategoryEmail(boolean email_risk) {
		if (email_risk) pkgCats |= CATEGORY_EMAIL;
		else pkgCats = pkgCats & ~CATEGORY_EMAIL;
	}
	
	public boolean isCategoryEmail() {
		return ((pkgCats & CATEGORY_EMAIL) == CATEGORY_EMAIL);
	}

	public void setCategoryLocation(boolean loc_risk) {
		if (loc_risk) pkgCats |= CATEGORY_LOC;
		else pkgCats = pkgCats & ~CATEGORY_LOC;
	}
	
	public boolean isCategoryLocation() {
		return ((pkgCats & CATEGORY_LOC) == CATEGORY_LOC);
	}

	public void setCategoryPhone(boolean phone_risk) {
		if (phone_risk) pkgCats |= CATEGORY_PHONE;
		else pkgCats = pkgCats & ~CATEGORY_PHONE;
	}
	
	public boolean isCategoryPhone() {
		return ((pkgCats & CATEGORY_PHONE) == CATEGORY_PHONE);
	}

	public void setCategorySMS(boolean sms_risk) {
		if (sms_risk) pkgCats |= CATEGORY_SMS;
		else pkgCats = pkgCats & ~CATEGORY_SMS;
	}
	
	public boolean isCategorySMS() {
		return ((pkgCats & CATEGORY_SMS) == CATEGORY_SMS);
	}


	public void setNewInstall(boolean new_install) {
		if (new_install) pkgFlgs |= NEW_INSTALL;
		else pkgFlgs = pkgFlgs & ~NEW_INSTALL;
	}
	
	public boolean isNewInstall() {
		return ((pkgFlgs & NEW_INSTALL) == NEW_INSTALL);
	}

	public void setSafe(boolean safe) {
		if (safe) pkgFlgs |= SAFE;
		else pkgFlgs = pkgFlgs & ~SAFE;
	}
	
	public boolean isSafe() {
		return ((pkgFlgs & SAFE) == SAFE);
	}

	public void setUserSafe(boolean usr_safe) {
		if (usr_safe) pkgFlgs |= USR_SAFE;
		else pkgFlgs = pkgFlgs & ~USR_SAFE;
	}
	
	public boolean isUserSafe() {
		return ((pkgFlgs & USR_SAFE) == USR_SAFE);
	}

	public void setUserUnsafe(boolean usr_unsafe) {
		if (usr_unsafe) pkgFlgs |= USR_UNSAFE;
		else pkgFlgs = pkgFlgs & ~USR_UNSAFE;
	}
	
	public boolean isUserUnsafe() {
		return ((pkgFlgs & USR_UNSAFE) == USR_UNSAFE);
	}

	public void setUnknown(boolean unknown) {
		if (unknown) pkgFlgs |= UNKNOWN;
		else pkgFlgs = pkgFlgs & ~UNKNOWN;
	}
	
	public boolean isUnknown() {
		return ((pkgFlgs & UNKNOWN) == UNKNOWN);
	}

	public void setHighRisk(boolean high_risk) {
		if (high_risk) pkgFlgs |= UNKNOWN_HIGH_RISK;
		else pkgFlgs = pkgFlgs & ~UNKNOWN_HIGH_RISK;
	}
	
	public boolean isHighRisk() {
		return ((pkgFlgs & UNKNOWN_HIGH_RISK) == UNKNOWN_HIGH_RISK);
	}

	public void setUnsafe(boolean unsafe) {
		if (unsafe) pkgFlgs |= UNSAFE;
		else pkgFlgs = pkgFlgs & ~UNSAFE;
	}
	
	public boolean isUnsafe() {
		return ((pkgFlgs & UNSAFE) == UNSAFE);
	}

}
