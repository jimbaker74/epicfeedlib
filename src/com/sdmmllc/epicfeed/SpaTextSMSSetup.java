package com.sdmmllc.epicfeed;

import java.lang.ref.WeakReference;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.telephony.PhoneNumberUtils;
import android.text.Editable;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.sdmm.epicfeed.R;
import com.sdmmllc.epicfeed.data.EpicContact;
import com.sdmmllc.epicfeed.data.EpicDB;
import com.sdmmllc.epicfeed.data.EpicDBConn;
import com.sdmmllc.epicfeed.utils.EpicFeedConsts;

public class SpaTextSMSSetup extends Activity {
    /** Called when the activity is first created. */
	String TAG = "SpaTextSMSSetup", PHONEID = "PhoneId";
	EditText phoneNum, contactName;
	CheckBox blockedChk, forbiddenChk, callBlockChk, callLogChk,
		spaIDChk, statusbarChk, vibrateChk, lightChk, soundChk;
	private Button addContactBtn, statusbarCustomBtn;
	private TextView blockMsgDescr, blockCallDescr, callLogDescr, statusbarDescr;
	private String phoneID = "", blockMsgDescrTxt = "";
	private EpicDB spaDB;
	private Bundle savedState;
    private LayoutInflater mInflater;
    private boolean saveBlockedMsgsFlg = false;
	EpicContact newContact = new EpicContact("");
	private Context spaContext;
	private WeakReference<Activity> spaActivity;
	private SharedPreferences settings;
	private SharedPreferences.Editor editor;
	private EpicContact tmpContact;
    public static int minPhoneLength = 1;
    private SpaUserAccount mSpaUserAccount;
    private SpaUserAccountListener accountListener;
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
    	mSpaUserAccount.updateAuthActivity();
		MenuInflater menuInfl = getMenuInflater();
		menuInfl.inflate(R.menu.smssetup_menu, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int itemId = item.getItemId();
		if (itemId == R.id.smsSetupHelp) {
			helpDialog();
			return true;
		} else if (itemId == R.id.smsSetupBack) {
			this.finish();
			return true;
		}
		return false;
	}

	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contactsetup);
        savedState = savedInstanceState;
        spaContext = this;
        spaActivity = new WeakReference<Activity>(this);
        mSpaUserAccount = EpicFeedController.getSpaUserAccount(this);
        accountListener = new SpaUserAccountAdapter(new WeakReference<Activity>(this)) {
        	
    		@Override
    		public void timeout() {
    			mSpaUserAccount.authenticated(spaActivity);
    			finish();
    		}
        	
        };
        mInflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        settings = getSharedPreferences(EpicFeedConsts.FLAGS_FILE, 0);
    	spaDB = EpicFeedController.getEpicDB(spaContext.getApplicationContext());
        if (!settings.contains(EpicFeedConsts.contactInstFlg)|
        		!settings.getBoolean(EpicFeedConsts.contactInstFlg, false)) {
        	EpicDBConn tmpConn = spaDB.open(false);
        	int numContacts = spaDB.allPhoneID().length;
        	spaDB.close(tmpConn);
        	if (numContacts > 0) {
        		firstTimeHelpDialog(false);
        	} else {
        		firstTimeHelpDialog(true);
        	}
        	editor = settings.edit();
        	editor.putBoolean(EpicFeedConsts.contactInstFlg, true);
        	editor.commit();
        }
    	phoneNum = (EditText)findViewById(R.id.setupPhoneIDEditTxt);
    	phoneNum.addTextChangedListener(new PhoneNumberFormattingTextWatcher() {
    		@Override
    		public synchronized void afterTextChanged(Editable s) {
    			if ((s.length() < minPhoneLength)&&(statusbarChk.isChecked())) {
            		statusbarCustomBtn.setEnabled(false);
    	        	statusbarDescr.setVisibility(View.VISIBLE);
            		statusbarDescr.setText(getString(R.string.smsSetupNoContactStatusBarLblTxt));
            	} else {
	            	statusbarDescr.setText(getString(R.string.sysContactSetupStatusBarDescr));
            		statusbarDescr.setVisibility(View.GONE);
	            	statusbarCustomBtn.setEnabled(true);
            	}
    		}
    	});
    	// remove SPA ID for now
    	((TextView)findViewById(R.id.setupSPAIDHeader)).setVisibility(View.GONE);
    	((CheckBox)findViewById(R.id.setupSPAIDCheckBox)).setVisibility(View.GONE);
    	((Button)findViewById(R.id.smsWebBtn)).setVisibility(View.GONE);
    	contactName = (EditText)findViewById(R.id.setupNameEditTxt);
    	forbiddenChk = (CheckBox)findViewById(R.id.setupForbiddenCheckBox);
    	callBlockChk = (CheckBox)findViewById(R.id.setupContactCallBlockCheckBox);
    	blockCallDescr = (TextView)findViewById(R.id.setupContactCallBlockDescr);
    	callLogChk = (CheckBox)findViewById(R.id.setupCallLogHideCheckBox);
    	callLogDescr = (TextView)findViewById(R.id.setupContactCallLogDescr);
    	blockedChk = (CheckBox)findViewById(R.id.setupBlockedCheckBox);
    	blockMsgDescr = (TextView)findViewById(R.id.setupContactBlockedDescr);
    	blockMsgDescrTxt = blockMsgDescr.getText().toString();
    	statusbarChk = (CheckBox)findViewById(R.id.setupStatusbarCheckBox);
       	statusbarDescr = (TextView)findViewById(R.id.setupContactStatusBarDescr);
    	soundChk = (CheckBox)findViewById(R.id.setupSoundCheckBox);
    	lightChk = (CheckBox)findViewById(R.id.setupLightCheckBox);
    	vibrateChk = (CheckBox)findViewById(R.id.setupVibrateCheckBox);
    	spaIDChk = (CheckBox)findViewById(R.id.setupSPAIDCheckBox);
    	addContactBtn = (Button)findViewById(R.id.setupAddContactBtn);
    	statusbarCustomBtn = (Button)findViewById(R.id.setupContactStatusBarBtn);
    	if (getIntent().hasExtra("pContact")) {
        	tmpContact = new EpicContact(getIntent().getStringExtra("pID"));
        	tmpContact.name = getIntent().getStringExtra("pContact");
            if (EpicFeedConsts.debugLevel > 3) 
            	Log.i(TAG, "onCreate: populating contact: " + tmpContact.phoneID);
        	phoneNum.setText(tmpContact.phoneID);
			phoneNum.setEnabled(false);
			addContactBtn.setText(getString(R.string.smsSetupSave));
        	contactName.setText(tmpContact.name);
        	setNotificationChkBoxes();
        } else if (getIntent().hasExtra("pID")) {
        	String pID = getIntent().getStringExtra("pID");
            if (EpicFeedConsts.debugLevel > 3) Log.i(TAG, "savePhoneID: pID from Intent:" + pID);
        	EpicDBConn tmpConn = spaDB.open(false);
			String filterPID = spaDB.comparePhoneID(pID);
        	tmpContact = new EpicContact("");
    		if (filterPID.length() > 0) {
    			tmpContact = spaDB.getContact(filterPID);
    			tmpContact.notifyOptions = spaDB.getNotification(pID);
    			phoneNum.setEnabled(false);
    			saveBlockedMsgsFlg = tmpContact.saveBlockedMsgs();
            	forbiddenChk.setChecked(tmpContact.isForbidden());
            	callBlockChk.setChecked(tmpContact.isCallBlocked());
        		int hgt = phoneNum.getHeight();
            	if (tmpContact.isKeepBlockedCallLogs()&tmpContact.isCallBlockedNoVoicemail())
            		blockCallDescr.setText(getString(R.string.sysContactSetupCallBlockLogNoVMDescr));
            	else if (tmpContact.isKeepBlockedCallLogs())
            		blockCallDescr.setText(getString(R.string.sysContactSetupCallBlockLogDescr));
               	else if (tmpContact.isCallBlockedNoVoicemail())
                	blockCallDescr.setText(getString(R.string.sysContactSetupCallBlockNoVMDescr));
               	else blockCallDescr.setText(getString(R.string.sysContactSetupCallBlockDescr));
        		phoneNum.setHeight(hgt);
            	callLogChk.setChecked(tmpContact.isCallLogDelete());
            	if (tmpContact.isKeepCallLogs()) {
                	callLogDescr.setText(getString(R.string.sysContactSetupCallLogKeepDescr));
            	}
    			addContactBtn.setText(getString(R.string.smsSetupSaveChanges));
    		}
        	spaDB.close(tmpConn);
            if (EpicFeedConsts.debugLevel > 3) Log.i(TAG, "savePhoneID: populating contact: " + tmpContact.phoneID);
        	phoneNum.setText(tmpContact.phoneID);
        	contactName.setText(tmpContact.name);
        	setNotificationChkBoxes();
        } else {
        	tmpContact = new EpicContact("");
        	statusbarChk.setChecked(true);
        	soundChk.setChecked(true);
        	lightChk.setChecked(true);
        	vibrateChk.setChecked(true);
        	statusbarCustomBtn.setVisibility(View.VISIBLE);
        	statusbarCustomBtn.setEnabled(false);
        	statusbarDescr.setText(getString(R.string.smsSetupNoContactStatusBarLblTxt));
        }
	}
	
	public void setNotificationChkBoxes() {
    	blockedChk.setChecked(tmpContact.isBlocked());
		if (tmpContact.isBlocked()) {
        	spaIDChk.setChecked(false);
        	statusbarChk.setChecked(false);
        	soundChk.setChecked(false);
        	lightChk.setChecked(false);
        	vibrateChk.setChecked(false);
        	spaIDChk.setEnabled(false);
        	statusbarChk.setEnabled(false);
        	statusbarCustomBtn.setVisibility(View.GONE);
        	statusbarDescr.setVisibility(View.VISIBLE);
        	soundChk.setEnabled(false);
        	lightChk.setEnabled(false);
        	vibrateChk.setEnabled(false);
        	if (tmpContact.saveBlockedMsgs())
        		blockMsgDescr.setText(Html.fromHtml(blockMsgDescrTxt + "<br><b><i>Incoming messages will be saved</i></b>"));
		} else {
        	spaIDChk.setChecked(tmpContact.isSpaId());
        	statusbarChk.setChecked(tmpContact.notifyOptions.statusbar);
        	if (tmpContact.notifyOptions.statusbar) {
	        	statusbarCustomBtn.setVisibility(View.VISIBLE);
	        	if (tmpContact.phoneID.length() < minPhoneLength) {
	        		statusbarDescr.setText(getString(R.string.smsSetupNoContactStatusBarLblTxt));
		        	statusbarDescr.setVisibility(View.VISIBLE);
	        	}
	        	else {
	        		statusbarDescr.setText(getString(R.string.sysContactSetupStatusBarDescr));
		        	statusbarDescr.setVisibility(View.GONE);
	        	}
        	} else {
	        	statusbarCustomBtn.setVisibility(View.GONE);
        		statusbarDescr.setText(getString(R.string.sysContactSetupStatusBarDescr));
	        	statusbarDescr.setVisibility(View.VISIBLE);
        	}
        	soundChk.setChecked(tmpContact.notifyOptions.sound);
        	lightChk.setChecked(tmpContact.notifyOptions.light);
        	vibrateChk.setChecked(tmpContact.notifyOptions.vibrate);
        	spaIDChk.setEnabled(true);
        	statusbarChk.setEnabled(true);
        	soundChk.setEnabled(true);
        	lightChk.setEnabled(true);
        	vibrateChk.setEnabled(true);
    		blockMsgDescr.setText(blockMsgDescrTxt);
		}
	}
	
	public void setCallBlock (View v) {
    	mSpaUserAccount.updateAuthActivity();
		if (tmpContact.isCallBlocked()) {
        	// if the box was clicked and the contact was already checked at blocked,
        	// then unset all block options
        	tmpContact.setCallBlock(false);
        	tmpContact.setKeepBlockedCallLogs(false);
    		tmpContact.setCallBlockedNoVoicemail(false);
    		callBlockChk.setChecked(false);
        	blockCallDescr.setText(getString(R.string.sysContactSetupCallBlockDescr));
        } else {
			final View helpTextView = mInflater.inflate(R.layout.helpview_2chk, null);
			final TextView callBlockText = (TextView)helpTextView.findViewById(R.id.helpText);
			final CheckBox callBlockTextChk = (CheckBox)helpTextView.findViewById(R.id.helpCheck);
			final float scale = this.getResources().getDisplayMetrics().density;
			callBlockTextChk.setPadding(callBlockTextChk.getPaddingLeft() + (int)(10.0f * scale + 0.5f), 
					callBlockTextChk.getPaddingTop(), callBlockTextChk.getPaddingRight(), 
					callBlockTextChk.getPaddingBottom());
			callBlockTextChk.setText(getString(R.string.contactCallBlockSaveTxt));
			// default the call logs enabled
			callBlockTextChk.setChecked(true);
			final CheckBox callBlockText2Chk = (CheckBox)helpTextView.findViewById(R.id.helpCheck2);
			final float scale2 = this.getResources().getDisplayMetrics().density;
			callBlockText2Chk.setPadding(callBlockText2Chk.getPaddingLeft() + (int)(10.0f * scale2 + 0.5f), 
					callBlockText2Chk.getPaddingTop(), callBlockText2Chk.getPaddingRight(), 
					callBlockText2Chk.getPaddingBottom());
			callBlockText2Chk.setText(getString(R.string.contactCallBlockNoVMTxt));
			//temporarily disable the no VM option
			callBlockText2Chk.setChecked(false);
			callBlockText2Chk.setVisibility(View.GONE);
			callBlockText.setText(Html.fromHtml(getString(R.string.contactCallBlockHelp)));
			AlertDialog ad = new AlertDialog.Builder(spaContext)
	        .setTitle(getString(R.string.contactCallBlockHelpHeader))
	        .setView(helpTextView)
	        .setPositiveButton(getString(R.string.contactCallBlockYesBtn), new DialogInterface.OnClickListener() {
	            public void onClick(DialogInterface dialog, int which) {
	            	mSpaUserAccount.updateAuthActivity();
	            	tmpContact.setCallBlock(true);
            		int hgt = phoneNum.getHeight();
                	blockCallDescr.setText(getString(R.string.sysContactSetupCallBlockDescr));
	            	if (callBlockTextChk.isChecked()) {
	            		tmpContact.setKeepBlockedCallLogs(true);
	            		blockCallDescr.setText(getString(R.string.sysContactSetupCallBlockLogDescr));
	            	} else {
	            		tmpContact.setKeepBlockedCallLogs(false);
	            	}
	            	if (callBlockText2Chk.isChecked()) {
	            		tmpContact.setCallBlockedNoVoicemail(true);
	            		blockCallDescr.setText(getString(R.string.sysContactSetupCallBlockNoVMDescr));
	            	} else {
	            		tmpContact.setCallBlockedNoVoicemail(false);
	            	}
	            	if (tmpContact.isKeepBlockedCallLogs()&tmpContact.isCallBlockedNoVoicemail())
	            		blockCallDescr.setText(getString(R.string.sysContactSetupCallBlockLogNoVMDescr));
            		phoneNum.setHeight(hgt);
	            }
	        })
	        .setNegativeButton(getString(R.string.contactCallBlockNoBtn), new DialogInterface.OnClickListener() {
	            public void onClick(DialogInterface dialog, int which) {
	            	mSpaUserAccount.updateAuthActivity();
	            	tmpContact.setCallBlock(false);
	            	tmpContact.setKeepBlockedCallLogs(false);
            		tmpContact.setCallBlockedNoVoicemail(false);
	            	callBlockChk.setChecked(false);
                	blockCallDescr.setText(getString(R.string.sysContactSetupCallBlockDescr));
	            	dialog.cancel();
	            }
	        })
	        .create();
			ad.show();
        }
	}

	public void setHideCallLogs (View v) {
    	mSpaUserAccount.updateAuthActivity();
		// temporarily make this call block
        if (tmpContact.isCallLogDelete()) {
        	tmpContact.setCallLogDelete(false);
        	tmpContact.setKeepCallLogs(false);
        	tmpContact.setCallHide(false);
        	callLogChk.setChecked(false);
        	callLogDescr.setText(getString(R.string.sysContactSetupCallLogDescr));
        } else {
			final View helpTextView = mInflater.inflate(R.layout.helpview_2chk, null);
			final TextView callLogText = (TextView)helpTextView.findViewById(R.id.helpText);
			final CheckBox callLogTextChk = (CheckBox)helpTextView.findViewById(R.id.helpCheck);
			final float scale = this.getResources().getDisplayMetrics().density;
			callLogTextChk.setPadding(callLogTextChk.getPaddingLeft() + (int)(10.0f * scale + 0.5f), 
					callLogTextChk.getPaddingTop(), callLogTextChk.getPaddingRight(), 
					callLogTextChk.getPaddingBottom());
			callLogTextChk.setText(getString(R.string.contactCallLogSaveTxt));
			callLogTextChk.setChecked(true);
			final CheckBox callLogTextChk2 = (CheckBox)helpTextView.findViewById(R.id.helpCheck2);
			final float scale2 = this.getResources().getDisplayMetrics().density;
			callLogTextChk2.setPadding(callLogTextChk2.getPaddingLeft() + (int)(10.0f * scale2 + 0.5f), 
					callLogTextChk2.getPaddingTop(), callLogTextChk2.getPaddingRight(), 
					callLogTextChk2.getPaddingBottom());
			callLogTextChk2.setText(getString(R.string.smsSetupHideCalls));
			callLogTextChk2.setVisibility(View.GONE);
			callLogText.setText(Html.fromHtml(getString(R.string.contactCallLogHelp)));
			AlertDialog ad = new AlertDialog.Builder(spaContext)
	        .setTitle(getString(R.string.contactCallLogHelpHeader))
	        .setView(helpTextView)
	        .setPositiveButton(getString(R.string.contactCallLogYesBtn), new DialogInterface.OnClickListener() {
	            public void onClick(DialogInterface dialog, int which) {
	            	mSpaUserAccount.updateAuthActivity();
	            	tmpContact.setCallLogDelete(true);
	            	if (callLogTextChk.isChecked()) {
	            		tmpContact.setKeepCallLogs(true);
	            		int hgt = phoneNum.getHeight();
	            		callLogDescr.setText(getString(R.string.sysContactSetupCallLogKeepDescr));
	            		phoneNum.setHeight(hgt);
	            	} else {
	            		tmpContact.setKeepCallLogs(false);
	            		callLogDescr.setText(getString(R.string.sysContactSetupCallLogDescr));
	            	}
	            	if (callLogTextChk2.isChecked()) {
	            		tmpContact.setCallHide(true);
	            		int hgt = phoneNum.getHeight();
	            		callLogDescr.setText(getString(R.string.sysContactSetupCallLogKeepDescr));
	            		phoneNum.setHeight(hgt);
	            	} else {
	            		tmpContact.setCallHide(false);
	            		callLogDescr.setText(getString(R.string.sysContactSetupCallLogDescr));
	            	}
	            }
	        })
	        .setNegativeButton(getString(R.string.contactCallLogNoBtn), new DialogInterface.OnClickListener() {
	            public void onClick(DialogInterface dialog, int which) {
	            	mSpaUserAccount.updateAuthActivity();
	            	tmpContact.setCallLogDelete(false);
	            	tmpContact.setKeepCallLogs(false);
	            	tmpContact.setCallHide(false);
	            	callLogChk.setChecked(false);
	            	callLogDescr.setText(getString(R.string.sysContactSetupCallLogDescr));
	            	dialog.cancel();
	            }
	        })
	        .create();
			ad.show();
        }
	}

	public void setFakeNEW (View v) {
    	mSpaUserAccount.updateAuthActivity();
        if (tmpContact.isFake()) {
        	tmpContact.setFake(false);
        	//fakeChk.setChecked(false);
        } else {
			final View helpTextView = mInflater.inflate(R.layout.helpview_chk, null);
			final TextView fakeText = (TextView)helpTextView.findViewById(R.id.helpText);
			final CheckBox fakeTextChk = (CheckBox)helpTextView.findViewById(R.id.helpCheck);
			final float scale = this.getResources().getDisplayMetrics().density;
			fakeTextChk.setPadding(fakeTextChk.getPaddingLeft() + (int)(10.0f * scale + 0.5f), 
					fakeTextChk.getPaddingTop(), fakeTextChk.getPaddingRight(), fakeTextChk.getPaddingBottom());
			fakeTextChk.setVisibility(View.GONE);
			fakeText.setText(Html.fromHtml(getString(R.string.contactFakeHelp)));
			AlertDialog ad = new AlertDialog.Builder(spaContext)
	        .setTitle(getString(R.string.contactFakeHelpHeader))
	        .setView(helpTextView)
	        .setPositiveButton(getString(R.string.contactFakeYesBtn), new DialogInterface.OnClickListener() {
	            public void onClick(DialogInterface dialog, int which) {
	            	mSpaUserAccount.updateAuthActivity();
	            	tmpContact.setFake(true);
	            }
	        })
	        .setNegativeButton(getString(R.string.contactFakeNoBtn), new DialogInterface.OnClickListener() {
	            public void onClick(DialogInterface dialog, int which) {
	            	mSpaUserAccount.updateAuthActivity();
	            	tmpContact.setFake(false);
	            	//fakeChk.setChecked(false);
	            	dialog.cancel();
	            }
	        })
	        .create();
			ad.show();
        }
	}

	public void setForbidden (View v) {
    	mSpaUserAccount.updateAuthActivity();
        if (tmpContact.isForbidden()) {
        	tmpContact.setForbidden(false);
        	forbiddenChk.setChecked(false);
        } else {
			final View helpTextView = mInflater.inflate(R.layout.helpview_chk, null);
			final TextView forbiddenText = (TextView)helpTextView.findViewById(R.id.helpText);
			final CheckBox forbiddenTextChk = (CheckBox)helpTextView.findViewById(R.id.helpCheck);
			final float scale = this.getResources().getDisplayMetrics().density;
			forbiddenTextChk.setPadding(forbiddenTextChk.getPaddingLeft() + (int)(10.0f * scale + 0.5f), 
					forbiddenTextChk.getPaddingTop(), forbiddenTextChk.getPaddingRight(), forbiddenTextChk.getPaddingBottom());
			forbiddenTextChk.setVisibility(View.GONE);
			forbiddenText.setText(Html.fromHtml(getString(R.string.contactForbiddenHelp)));
			AlertDialog ad = new AlertDialog.Builder(spaContext)
	        .setTitle(getString(R.string.contactForbiddenHelpHeader))
	        .setView(helpTextView)
	        .setPositiveButton(getString(R.string.contactForbiddenYesBtn), new DialogInterface.OnClickListener() {
	            public void onClick(DialogInterface dialog, int which) {
	            	mSpaUserAccount.updateAuthActivity();
	            	tmpContact.setForbidden(true);
	            }
	        })
	        .setNegativeButton(getString(R.string.contactForbiddenNoBtn), new DialogInterface.OnClickListener() {
	            public void onClick(DialogInterface dialog, int which) {
	            	mSpaUserAccount.updateAuthActivity();
	            	tmpContact.setForbidden(false);
	            	forbiddenChk.setChecked(false);
	            	dialog.cancel();
	            }
	        })
	        .create();
			ad.show();
        }
	}

	public void setBlocked (View v) {
    	mSpaUserAccount.updateAuthActivity();
        if (tmpContact.isBlocked()) {
        	tmpContact.setBlocked(false);
        	setNotificationChkBoxes();
        } else {
			final View helpTextView = mInflater.inflate(R.layout.helpview_chk, null);
			final TextView blockedText = (TextView)helpTextView.findViewById(R.id.helpText);
			final CheckBox blockedTextChk = (CheckBox)helpTextView.findViewById(R.id.helpCheck);
			final float scale = this.getResources().getDisplayMetrics().density;
			blockedTextChk.setPadding(blockedTextChk.getPaddingLeft() + (int)(10.0f * scale + 0.5f), 
					blockedTextChk.getPaddingTop(), blockedTextChk.getPaddingRight(), blockedTextChk.getPaddingBottom());
			blockedTextChk.setText(getString(R.string.contactBlockSaveTxt));
			blockedText.setText(Html.fromHtml(getString(R.string.contactBlockHelp)));
			AlertDialog ad = new AlertDialog.Builder(spaContext)
	        .setTitle(getString(R.string.contactBlockHelpHeader))
	        .setView(helpTextView)
	        .setPositiveButton(getString(R.string.contactBlockYesBtn), new DialogInterface.OnClickListener() {
	            public void onClick(DialogInterface dialog, int which) {
	            	mSpaUserAccount.updateAuthActivity();
	            	tmpContact.setBlocked(true);
	            	if (blockedTextChk.isChecked()) {
		            	tmpContact.setSaveBlockedMsgs(true);
		            	saveBlockedMsgsFlg = true;
		        		blockMsgDescr.setText(Html.fromHtml(blockMsgDescrTxt + "<br><b><i>Incoming messages will be saved</i></b>"));
	            	}
	            	setNotificationChkBoxes();
	            }
	        })
	        .setNegativeButton(getString(R.string.contactBlockNoBtn), new DialogInterface.OnClickListener() {
	            public void onClick(DialogInterface dialog, int which) {
	            	mSpaUserAccount.updateAuthActivity();
	            	tmpContact.setBlocked(false);
	            	tmpContact.setSaveBlockedMsgs(false);
	            	saveBlockedMsgsFlg = false;
	            	setNotificationChkBoxes();
	            	blockMsgDescr.setText(blockMsgDescrTxt);
	            	dialog.cancel();
	            }
	        })
	        .create();
			ad.show();
        }
	}
	
	public void setStatusBar(View v) {
		tmpContact.notifyOptions.statusbar = statusbarChk.isChecked();
    	if (tmpContact.notifyOptions.statusbar) {
        	statusbarCustomBtn.setVisibility(View.VISIBLE);
        	if (tmpContact.phoneID.length() < minPhoneLength) {
        		statusbarDescr.setText(getString(R.string.smsSetupNoContactStatusBarLblTxt));
	        	statusbarDescr.setVisibility(View.VISIBLE);
        	}
        	else {
        		statusbarDescr.setText(getString(R.string.sysContactSetupStatusBarDescr));
	        	statusbarDescr.setVisibility(View.GONE);
        	}
    	} else {
        	statusbarCustomBtn.setVisibility(View.GONE);
        	statusbarDescr.setVisibility(View.VISIBLE);
       		statusbarDescr.setText(getString(R.string.sysContactSetupStatusBarDescr));
    	}
	}

	public void helpDialog () {
    	mSpaUserAccount.updateAuthActivity();
		closeOptionsMenu();
		final View helpTextView = mInflater.inflate(R.layout.helpview, null);
		final TextView helpText = (TextView)helpTextView.findViewById(R.id.helptext);
		helpText.setText(Html.fromHtml(
				getText(R.string.smsSetupHelp).toString()));
		AlertDialog ad = new AlertDialog.Builder(spaContext)
        .setTitle(R.string.smsSetupHelpHeader)
        .setView(helpTextView)
        .setNeutralButton(getString(R.string.instDoneBtnTxt), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            	mSpaUserAccount.updateAuthActivity();
            	dialog.cancel();
            }
        })
        .create();
		ad.show();
	}

	public void firstTimeHelpDialog (boolean noContacts) {
		closeOptionsMenu();
		final View helpTextView = mInflater.inflate(R.layout.helpview, null);
		final TextView helpText = (TextView)helpTextView.findViewById(R.id.helptext);
		String title = "";
		if (noContacts) {
			title = getString(R.string.contactSetupInstructionsTitle);
		} else {
			title = getString(R.string.contactSetupInstructionsReminderTitle);
		}
		helpText.setText(getString(R.string.contactSetupInstructions));
		AlertDialog ad = new AlertDialog.Builder(spaContext)
        .setTitle(title)
        .setView(helpTextView)
        .setNeutralButton(getString(R.string.instDoneBtnTxt), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            	mSpaUserAccount.updateAuthActivity();
            	dialog.cancel();
            }
        })
        .create();
		ad.show();
	}

	public void quitWithoutSaveDialog (View v) {
    	mSpaUserAccount.updateAuthActivity();
		AlertDialog ad = new AlertDialog.Builder(v.getContext())
        .setTitle(getString(R.string.sysDone))
        .setMessage(getString(R.string.sysContactSetupQuit))
        .setPositiveButton(getString(R.string.sysYes), new DialogInterface.OnClickListener() {
            @Override
			public void onClick(DialogInterface dialog, int which) {
            	mSpaUserAccount.updateAuthActivity();
            	finish();
            }
        })
        .setNegativeButton(getString(R.string.sysNo), new DialogInterface.OnClickListener() {
            @Override
			public void onClick(DialogInterface dialog, int which) {
	            mSpaUserAccount.updateAuthActivity();
            	dialog.cancel();
            }
        })
        .create();
		ad.show();
	}
	
	public void customStatusBarNotification(View v) {
    	EpicDBConn tmpConn = spaDB.open(true);
 		if (!spaDB.containsNotification(tmpContact.phoneID)) spaDB.insertNotificationEntry(tmpContact.notifyOptions);
    	spaDB.close(tmpConn);
    	Intent statusbarNotificationIntent = new Intent(spaContext, SpaTextNotificationContactSetup.class);
    	statusbarNotificationIntent.putExtra("pID", tmpContact.phoneID);
    	startActivityForResult(statusbarNotificationIntent, EpicFeedConsts.CUSTOM_NOTIFICATION);
	}

    public void savePhoneID (View v) {
     	mSpaUserAccount.updateAuthActivity();
    	String pID = phoneNum.getText().toString().trim();
    	if (pID.length() < minPhoneLength){
     		Toast toast = Toast.makeText(spaContext, 
     				getString(R.string.sysContactSetupPhoneNumRequiredMsg), Toast.LENGTH_LONG);
     		toast.show();
     		return;
     	}        
    	pID = PhoneNumberUtils.stripSeparators(pID);
    	final String newPID = pID;
    	tmpContact.phoneID = newPID;
    	tmpContact.name = contactName.getText().toString();
    	tmpContact.setBlocked(blockedChk.isChecked());
    	tmpContact.setSaveBlockedMsgs(saveBlockedMsgsFlg&&blockedChk.isChecked());
    	if (!tmpContact.isBlocked()) {
	    	tmpContact.setSpaId(spaIDChk.isChecked());
	    	tmpContact.notifyOptions.statusbar = statusbarChk.isChecked();
	    	tmpContact.notifyOptions.light = lightChk.isChecked();
	    	tmpContact.notifyOptions.sound = soundChk.isChecked();
	    	tmpContact.notifyOptions.vibrate = vibrateChk.isChecked();
    	} else {
    		// settings should already be set
    	}
		EpicDBConn tmpConn = spaDB.open(false);
    	int countContacts = spaDB.allPhoneID().length;
		boolean isFound = spaDB.containsPhoneID(newPID);
    	spaDB.close(tmpConn);
    	int maxContacts = EpicFeedConsts.freeVersionContacts;
    	if ((!getResources().getBoolean(R.bool.freeVersion))&&(EpicFeedController.isLicenseVerified())) 
    		maxContacts = EpicFeedConsts.licensedVerContacts;
    	if (isFound) {
    		String cntctName = PhoneNumberUtils.formatNumber(tmpContact.phoneID);
    		if (tmpContact.name.length() > 0) cntctName = tmpContact.name + " / " 
    			+ cntctName;
 			AlertDialog ad = new AlertDialog.Builder(v.getContext())
 	        .setTitle(getString(R.string.sysContactSetupContactExistsTitle))
 	        .setMessage(getString(R.string.sysContactSetupContactExistsUpdate) + " " + cntctName + "?")
 	        .setPositiveButton(getString(R.string.sysContactSetupContactExistsUpdateYes), new DialogInterface.OnClickListener() {
 	            @Override
 				public void onClick(DialogInterface dialog, int which) {
 	            	mSpaUserAccount.updateAuthActivity();
 	        		EpicDBConn tmpConn = spaDB.open(true);
 	        		spaDB.updateContact(tmpContact);
 	             	if (EpicFeedConsts.debugLevel > 3) 
 	        			Log.i(TAG, "savePhoneID: tmpContact.notifyOptions.statusbarTicker: " + tmpContact.notifyOptions.statusbarTicker);
 	        		if (spaDB.containsNotification(tmpContact.phoneID)) spaDB.updateNotificationEntry(tmpContact.notifyOptions);
 	        		else spaDB.insertNotificationEntry(tmpContact.notifyOptions);
 	        		if (!tmpContact.isBlocked()) spaDB.unblockMsgs(tmpContact.phoneID);
 	            	spaDB.close(tmpConn);
 	        		Toast toast = Toast.makeText(spaContext, 
 	        				getString(R.string.sysContactSetupContactExistsUpdateConfirmMsg) 
 	        				+ " " + tmpContact.getNameOrPhoneId(), Toast.LENGTH_LONG);
 	        		toast.show();
 	        		setResult(Activity.RESULT_OK);
 	            	finish();
 	            }
 	        })
 	        .setNegativeButton(getString(R.string.sysCancel), new DialogInterface.OnClickListener() {
 	            @Override
 				public void onClick(DialogInterface dialog, int which) {
 	            	mSpaUserAccount.updateAuthActivity();
 	            	// do nothing
 	            	dialog.cancel();
 	            }
 	        })
 	        .create();
 			ad.show();    	
     	} else if (countContacts >= maxContacts){
    		Toast toast = Toast.makeText(spaContext, getString(R.string.sysContactSetupMaxContactsReachedMsg) 
    			+ " " + maxContacts + ".", Toast.LENGTH_LONG);
    		toast.show();
    	} else  {
	    	// at this point, the phone number is acceptable, so delete previous messages
	    	// and save the new number
	    	if (EpicFeedConsts.debugLevel > 3) 
	    		Log.i(TAG, "savePhoneID: Phone ID to add: " + newPID);
	    	if (EpicFeedConsts.debugLevel > 3) 
	    		Log.i(TAG, "savePhoneID: Phone ID tmpContact.notifyOptions.statusbarTicker: " 
	    				+ tmpContact.notifyOptions.statusbarTicker);
	    	tmpContact.notifyOptions.phoneID = tmpContact.phoneID;
	    	tmpConn = spaDB.open(true);
        	spaDB.insertContact(tmpContact);
    		spaDB.insertNotificationEntry(tmpContact.notifyOptions);
        	spaDB.close(tmpConn);
        	Toast toast = Toast.makeText(spaContext, 
        			getString(R.string.sysContactSetupContactExistsSaveConfirmMsg) 
        			+ " " + tmpContact.getNameOrPhoneId(), Toast.LENGTH_LONG);
    		toast.show();
    		setResult(Activity.RESULT_OK);
         	finish();
    	}
    }
    
	public void deletePhoneID (View  v) {
        mSpaUserAccount.updateAuthActivity();
    	String pID = phoneNum.getText().toString();
    	if (pID.length() < 1){
     		Toast toast = Toast.makeText(spaContext, 
     				getString(R.string.sysContactSetupNoContactDelMsg), Toast.LENGTH_LONG);
     		toast.show();
     		return;
     	}        
    	pID = PhoneNumberUtils.stripSeparators(pID);
    	final String newPID = pID;
    	final EpicContact tmpContact = new EpicContact(newPID);
    	tmpContact.name = contactName.getText().toString();
    	tmpContact.setSpaId(spaIDChk.isChecked());
		EpicDBConn tmpConn = spaDB.open(false);
		boolean isFound = spaDB.containsPhoneID(newPID);
    	spaDB.close(tmpConn);
    	if (isFound) {
    		String cntctName = PhoneNumberUtils.formatNumber(tmpContact.phoneID);
    		if (tmpContact.name.length() > 0) cntctName = tmpContact.name + " / " 
    			+ cntctName;
    		if (cntctName.length() < 1) cntctName = PhoneNumberUtils.formatNumber(tmpContact.phoneID);
			AlertDialog ad = new AlertDialog.Builder(v.getContext())
	        .setTitle(getString(R.string.sysContactSetupRemoveContactTitle))
	        .setMessage(getString(R.string.sysContactSetupRemoveContactDescr) + " " + cntctName + "?")
	        .setPositiveButton(getString(R.string.sysContactSetupRemoveContactYes), 
	        		new DialogInterface.OnClickListener() {
	            @Override
				public void onClick(DialogInterface dialog, int which) {
	                mSpaUserAccount.updateAuthActivity();
	        		EpicDBConn tmpConn = spaDB.open(true);
	            	spaDB.deleteTxtThread(newPID, true);
	            	spaDB.deleteNotification(newPID);
	            	spaDB.deletePhoneID(newPID);
	            	spaDB.close(tmpConn);
	        		String cntctName = PhoneNumberUtils.formatNumber(tmpContact.phoneID);
	        		if (tmpContact.name.length() > 0) cntctName = tmpContact.name + " / " 
	        			+ cntctName;
	        		Toast toast = Toast.makeText(spaContext, cntctName + " "
	        				+ getString(R.string.sysContactSetupRemoveContactConfirmMsg), 
	        				Toast.LENGTH_LONG);
	        		toast.show();
	        		setResult(Activity.RESULT_OK);
 	            	finish();
	            }
	        })
	        .setNegativeButton(getString(R.string.sysCancel), new DialogInterface.OnClickListener() {
	            @Override
				public void onClick(DialogInterface dialog, int which) {
	                mSpaUserAccount.updateAuthActivity();
	            	// do nothing
	            	dialog.cancel();
	            }
	        })
	        .create();
			ad.show();    	
    	} else {
    		Toast toast = Toast.makeText(spaContext, getString(R.string.sysContactSetupContactNotFoundMsg), 
    				Toast.LENGTH_LONG);
    		toast.show();
    	}
    }

	public void getNotificationSettings(String tmpId) {
        if (EpicFeedConsts.debugLevel > 3) 
        	Log.i(TAG, "getNotificationSettings: getting notification settings tmpId:" + tmpId);
    	EpicDBConn tmpConn = spaDB.open(false);
		tmpContact.notifyOptions = spaDB.getNotification(tmpId);
		if (tmpId.equals(SpaTextNotification.tmpId)) {
			spaDB.deleteNotification(tmpId);
   			tmpContact.notifyOptions.phoneID = tmpContact.phoneID;
		}
		spaDB.close(tmpConn);
		if (EpicFeedConsts.debugLevel > 3) 
			Log.i(TAG, "getNotificationSettings: got notification settings");
	}
	
    @Override
    protected void onActivityResult(int requestCode, int resultCode,
    		Intent data) {
        if (EpicFeedConsts.debugLevel > 3) 
        	Log.i(TAG, "onActivityResult: requestCode:" + requestCode
        		+ ", resultCode:" + resultCode + ", data.hasExtra(pID):" + data.hasExtra("pID"));
        if (EpicFeedConsts.debugLevel > 3) 
        	Log.i(TAG, "onActivityResult: requestCode SpaTextConsts.CUSTOM_NOTIFICATION:" 
        		+ (EpicFeedConsts.CUSTOM_NOTIFICATION == requestCode));
        if (requestCode == EpicFeedConsts.CUSTOM_NOTIFICATION) {
        	if (data.getExtras().getBoolean(SpaTextNotification.notificationUpdated)) {
	            String tmpId = data.getExtras().getString("pID");
	            if (EpicFeedConsts.debugLevel > 3) 
	            	Log.i(TAG, "onActivityResult: getting notification settings tmpId:" + tmpId);
	   			if (!tmpId.equals(tmpContact.phoneID)) {
	   	   			getNotificationSettings(tmpId);
	   			} else {
	   				getNotificationSettings(tmpContact.phoneID);
	   			}
        	}
        }
    	super.onActivityResult(requestCode, resultCode, data);
    }
    
    @Override
    public boolean onTouchEvent(MotionEvent e) {
    	//Log.i(TAG, "Touch event outside activity, do nothing...");
        mSpaUserAccount.updateAuthActivity();
    	return super.onTouchEvent(e);
    }
    
    @Override
    public void onUserInteraction() {
    	//Log.i(TAG, "Touch event...");
        mSpaUserAccount.updateAuthActivity();
    }
    
    @Override
    public void onResume() {
		super.onResume();
        if (!mSpaUserAccount.authenticated(new WeakReference<Activity>(this))) finish();
        mSpaUserAccount.setSpaUserAccountListener(accountListener);
    }
    
    @Override
    public void onStart() {
		super.onStart();
    }
    
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
    	onPause();
		onStop();
    	onCreate(this.savedState);
    	onResume();
    }
	@Override
	public void onPause() {
        mSpaUserAccount.removeSpaUserAccountListener(accountListener);
		super.onPause();
	}

	@Override
	public void onStop() {
		super.onStop();
		setResult(RESULT_OK);
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
	}
}