package com.sdmmllc.epicfeed;

import android.graphics.drawable.Drawable;
import android.view.View;

import com.sdmmllc.epicfeed.SpaImageUtils.SpaDrawables;

public class SpaImageUtilsV1 {

	public static void setBackgroundDrawableNull(View v) {
		v.setBackgroundDrawable(null);
	}

	public static void setBackgroundDrawable(View v, int resId) {
		v.setBackgroundDrawable(SpaDrawables.getInstance().get(resId));
	}

	public static void setBackgroundDrawable(View v, Drawable sd) {
		v.setBackgroundDrawable(sd);
	}
	
}
