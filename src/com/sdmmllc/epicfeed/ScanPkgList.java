package com.sdmmllc.epicfeed;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.PermissionInfo;
import android.content.pm.ResolveInfo;
import android.util.Log;

import com.sdmm.epicfeed.R;
import com.sdmmllc.epicfeed.utils.EpicFeedConsts;
import com.sdmmllc.superdupersmsmanager.sdk.SDSmsManager;


public class ScanPkgList {

	String TAG = "ScanPkgList";
	
	List<ScanPkgItem> scanList;
	
	public ScanPkgList () {
		scanList = new ArrayList<ScanPkgItem>();
	}
	
	public ScanPkgList (List<ScanPkgItem> tmpList) {
		scanList = tmpList;
	}
	
	public List<ScanPkgItem> getNonSpaApps() {
		List<ScanPkgItem> tmpList = new ArrayList<ScanPkgItem>(); 
		for (ScanPkgItem tmpItem:scanList) {
			if (!tmpItem.pkgName.startsWith("com.secondphoneapps")) {
				tmpList.add(tmpItem);
			}
		}
		return tmpList;
	}
	
	public List<ScanPkgItem> getSpaApps() {
		List<ScanPkgItem> tmpList = new ArrayList<ScanPkgItem>(); 
		for (ScanPkgItem tmpItem:scanList) {
			if (tmpItem.pkgName.startsWith("com.secondphoneapps")) {
				tmpList.add(tmpItem);
			}
		}
		return tmpList;
	}
	
	public boolean hasThisSpaApp(Context ctx) {
		for (ScanPkgItem tmpItem:scanList) {
			if (tmpItem.pkgName.equals(ctx.getPackageName())) {
				return true;
			}
		}
		return false;
	}
	
	public boolean hasFreeVersionSpaApp(Context ctx) {
		for (ScanPkgItem tmpItem:scanList) {
			if (tmpItem.appName.equals(ctx.getString(R.string.app_name)+"Lic")) {
				return true;
			}
		}
		return false;
	}
	
	public boolean hasPaidVersionSpaApp(Context ctx) {
		for (ScanPkgItem tmpItem:scanList) {
			if ((tmpItem.appName+"Lic").equals(ctx.getString(R.string.app_name))) {
				return true;
			}
		}
		return false;
	}
	
	public boolean isThisSpaApp(Context ctx) {
		for (ScanPkgItem tmpItem:scanList) {
			if (tmpItem.appName.equals(ctx.getString(R.string.app_name))) {
				return true;
			}
		}
		return false;
	}
	
	public int packageIndex(String pkgName) {
		for (ScanPkgItem tmpItem:scanList) {
			if (tmpItem.pkgName.equals(pkgName)) {
				return scanList.indexOf(tmpItem);
			}
		}
		return -1;
	}
	
	public boolean has999App() {
		for (ScanPkgItem tmpItem:scanList) {
			if (tmpItem.recPriority == 999) {
				return true;
			}
		}
		return false;
	}
	
	public boolean hasOver999App() {
		for (ScanPkgItem tmpItem:scanList) {
			if (tmpItem.recPriority > 999) {
				return true;
			}
		}
		return false;
	}
	
	public List<ScanPkgItem> getInstalledApps(PackageManager pm, boolean getSysPackages) {
		scanList = new ArrayList<ScanPkgItem>();
	    List<PackageInfo> packs = pm.getInstalledPackages(PackageManager.GET_PERMISSIONS|
	    		 PackageManager.GET_PROVIDERS|
	    		 PackageManager.GET_RECEIVERS|
	    		 PackageManager.GET_SERVICES);
	    for (PackageInfo p:packs) {
	         if (getSysPackages | (!(
	        		((p.applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM) == ApplicationInfo.FLAG_SYSTEM)|
	        		((p.applicationInfo.flags & ApplicationInfo.FLAG_UPDATED_SYSTEM_APP) 
	        			 == ApplicationInfo.FLAG_UPDATED_SYSTEM_APP))
	        		)) {
		         if (packageIndex(p.packageName) < 0) {
		        	 ScanPkgItem newInfo = new ScanPkgItem();
			         newInfo.appName = p.applicationInfo.loadLabel(pm).toString();
			         newInfo.pkgName = p.packageName;
			         newInfo.versionName = p.versionName;
			         newInfo.versionCode = p.versionCode;
			         newInfo.icon = p.applicationInfo.loadIcon(pm);
			         try {
			        	 newInfo.pkgPerms = pm.getPackageInfo(
			        			 p.packageName, 
			        			 PackageManager.GET_PERMISSIONS
			        	 );
			         } catch (NameNotFoundException e) {
			         	if (EpicFeedConsts.debugLevel > 7) 
			         		Log.i(TAG, "Error getting package permissions for:" + p.packageName);
				    	e.printStackTrace();
			         }
			         newInfo.setUnknown(true);
			         scanList.add(newInfo);
		         }
	         }
	    }
	    return scanList;
	}
	
	public List<ScanPkgItem> scanListForPermissions(List<ScanPkgItem> tmpList) {
		scanList = tmpList;
		return scanListForPermissions();
	}
	
	public List<ScanPkgItem> scanListForPermissions() {
		for (ScanPkgItem tmpInfo:scanList) {
	    	if (EpicFeedConsts.debugLevel > 7) 
	    		Log.i(TAG, "Getting permissions for " + tmpInfo.pkgName);
    	    PermissionInfo[] permissions = tmpInfo.pkgPerms.permissions;
    	    if (permissions == null) {
    	    	tmpInfo.permFound = "No declared permissions";
    	    }
    	    else {
    	    	if (EpicFeedConsts.debugLevel > 7) 
    	    		Log.i(TAG, "Permissions found:" + permissions.length);
	    		String permissionsText = "";
	        	if (EpicFeedConsts.debugLevel > 7) 
	        		Log.i(TAG, "Getting permissions for " + tmpInfo.pkgName);
	    		for (int j = 0; j < permissions.length; j++) {
	    		    permissionsText += permissions[j].name + "\n";
	    		}
	    		tmpInfo.permFound = permissionsText;
    	    }

    	    String[] requestedPermissions = tmpInfo.pkgPerms.requestedPermissions;
    	    if (requestedPermissions == null) {
    	    	tmpInfo.reqPermFound = "No requested permissions";
    	    }
    	    else {
	    		String reqPermText = "";
	    		for (int j = 0; j < requestedPermissions.length; j++) {
	    		    reqPermText += requestedPermissions[j] + "\n";
	    		}
	    		tmpInfo.reqPermFound = reqPermText;
    	    }
		}
		return scanList;
	}
	
	public List<ScanPkgItem> scanListForSMS(PackageManager pm, List<ScanPkgItem> tmpList) {
		scanList = tmpList;
		return scanListForSMS(pm);
	}
	
	public List<ScanPkgItem> scanListForSMS(PackageManager pm) {
    	if (EpicFeedConsts.debugLevel > 7) 
    		Log.i(TAG, "Getting intent receivers");
	    Intent filterIntent = new Intent("android.provider.Telephony.SMS_RECEIVED");
	    //filterIntent.setClassName(tmpInfo.pkgFilters.packageName, activity.name);
	    List<ResolveInfo> resolves = pm.queryBroadcastReceivers(filterIntent,0);
	    if (resolves != null) {
	    	if (EpicFeedConsts.debugLevel > 7) 
	    		Log.i(TAG, "Found intent receivers:" + resolves.size());
		    for (ResolveInfo resolve : resolves) {
    		    if (resolve != null) {
    		    	if (EpicFeedConsts.debugLevel > 7) 
    		    		Log.i(TAG, "Found intent receiver for:" + resolve.activityInfo.applicationInfo.packageName);
    				for (ScanPkgItem tmpInfo:scanList) {
    					if (tmpInfo.pkgName.equals(resolve.activityInfo.applicationInfo.packageName)) {
    						tmpInfo.filtersFound += "Receives SMS: priority: " + resolve.priority;
    						tmpInfo.setCategorySMS(true);
    						tmpInfo.setHighRisk(true);
    					}
    				}
    		    }
		    }
	    }
		for (ScanPkgItem tmpInfo:scanList) {
			if (tmpInfo.filtersFound.length() < 1) {
				tmpInfo.filtersFound = "No SMS receivers";
				}
		}
		
		return scanList;
	}
	
	public List<ScanPkgItem> scanForSMS(PackageManager pm) {
		return scanForSMS(pm, false, false);
	}
	
	public List<ScanPkgItem> scanForSMS(PackageManager pm, boolean newlist, boolean getSysPackages) {
		if (newlist) scanList =  new ArrayList<ScanPkgItem>();
    	if (EpicFeedConsts.debugLevel > 7) 
    		Log.i(TAG, "Getting intent receivers");
	    Intent filterIntent = new Intent("android.provider.Telephony.SMS_RECEIVED");
	    //filterIntent.setClassName(tmpInfo.pkgFilters.packageName, activity.name);
	    List<ResolveInfo> resolves = pm.queryBroadcastReceivers(filterIntent,0);
	    if (resolves != null) {
	    	if (EpicFeedConsts.debugLevel > 7) 
	    		Log.i(TAG, "Found intent receivers:" + resolves.size());
		    for (ResolveInfo resolve : resolves) {
    		    if (resolve != null) {
    		    	if (newlist) {
						try {
	    			        PackageInfo p = pm.getPackageInfo(resolve.activityInfo.applicationInfo.packageName, 
									PackageManager.GET_PERMISSIONS|
									PackageManager.GET_PROVIDERS|
									PackageManager.GET_RECEIVERS|
									PackageManager.GET_SERVICES);
					         if (getSysPackages | (!(
					        		 ((p.applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM) == ApplicationInfo.FLAG_SYSTEM)|
					        		 ((p.applicationInfo.flags & ApplicationInfo.FLAG_UPDATED_SYSTEM_APP) 
					        				 == ApplicationInfo.FLAG_UPDATED_SYSTEM_APP))
					        				 )) {
						        if (packageIndex(p.packageName) < 0) {
			    			        ScanPkgItem newInfo = new ScanPkgItem();
			    			        newInfo.appName = p.applicationInfo.loadLabel(pm).toString();
			    			        newInfo.pkgName = p.packageName;
			    			        newInfo.versionName = p.versionName;
			    			        newInfo.versionCode = p.versionCode;
			    			        newInfo.icon = p.applicationInfo.loadIcon(pm);
			    			        newInfo.recPriority = resolve.priority;
			    			        newInfo.pkgPerms = p;
			    			        newInfo.filtersFound += "Receives SMS: priority: " + resolve.priority;
			    			    	if (EpicFeedConsts.debugLevel > 7) 
			    			    		Log.i(TAG, "Found intent receiver for:" + resolve.activityInfo.applicationInfo.packageName 
				    		    			+ " with priority: " + newInfo.recPriority);
			    			        newInfo.setCategorySMS(true);
			    			        newInfo.setHighRisk(true);
									scanList.add(newInfo);
						        } else {
			    			        if (scanList.get(packageIndex(p.packageName)).recPriority < resolve.priority) {
			    			        	scanList.get(packageIndex(p.packageName)).recPriority = resolve.priority;
			    			        }
						        }
					         }
						} catch (NameNotFoundException e) {
							e.printStackTrace();
					    	if (EpicFeedConsts.debugLevel > 7) 
					    		Log.e(TAG, "scanForSMS: Error getting packageInfo");
						}
    		    	} else {
    		        	if (EpicFeedConsts.debugLevel > 7) 
    		        		Log.i(TAG, "Found intent receiver for:" + resolve.activityInfo.applicationInfo.packageName 
	    		    			+ " with priority: " + resolve.priority);
	    				for (ScanPkgItem tmpInfo:scanList) {
	    					if (tmpInfo.pkgName.equals(resolve.activityInfo.applicationInfo.packageName)) {
	    				    	if (EpicFeedConsts.debugLevel > 7) 
	    				    		Log.i(TAG, "Package updated:" + resolve.activityInfo.applicationInfo.packageName);
	    						tmpInfo.filtersFound += "Receives SMS: priority: " + resolve.priority;
		    			        tmpInfo.recPriority = resolve.priority;
	    						tmpInfo.setCategorySMS(true);
	    						tmpInfo.setHighRisk(true);
	    					}
	    				}
    		    	}
    		    }
		    }
	    }
		for (ScanPkgItem tmpInfo:scanList) {
			if (tmpInfo.filtersFound.length() < 1) {
				tmpInfo.filtersFound = "No SMS receivers";
				}
		}
		
		return scanList;
	}
	
	public boolean addInstalledApp(PackageManager pm, String appName) {
		scanList = new ArrayList<ScanPkgItem>();
		PackageInfo p;
		try {
			p = pm.getPackageInfo(appName, 
					PackageManager.GET_PERMISSIONS|
					PackageManager.GET_PROVIDERS|
					PackageManager.GET_RECEIVERS|
					PackageManager.GET_SERVICES);
		    ScanPkgItem newInfo = new ScanPkgItem();
	        if (((p.applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM) == ApplicationInfo.FLAG_SYSTEM)|
	        	 ((p.applicationInfo.flags & ApplicationInfo.FLAG_UPDATED_SYSTEM_APP) 
	        			 == ApplicationInfo.FLAG_UPDATED_SYSTEM_APP)) {
	            return false;
	        }
		    newInfo.appName = p.applicationInfo.loadLabel(pm).toString();
		    newInfo.pkgName = p.packageName;
		    newInfo.versionName = p.versionName;
		    newInfo.versionCode = p.versionCode;
		    newInfo.icon = p.applicationInfo.loadIcon(pm);
		    newInfo.pkgPerms = pm.getPackageInfo(
	    			 p.packageName, 
	    			 PackageManager.GET_PERMISSIONS);
		    newInfo.setUnknown(true);
		    scanList.add(newInfo);
	    	if (EpicFeedConsts.debugLevel > 7) 
		    	Log.i(TAG, "addInstalledApp: app found: " + newInfo.appName);
		} catch (NameNotFoundException e1) {
			e1.printStackTrace();
	    	if (EpicFeedConsts.debugLevel > 7) 
				Log.i(TAG, "addInstalledApp: app not found: " + appName);
			return false;
		}
		return true;
	}
	
	public void addPkg(ScanPkgItem item) {
		scanList.add(item);
	}
	
	public int getSafeCount() {
		int count = 0;
		for (ScanPkgItem item:scanList) {
			if (item.isSafe()|item.isUserSafe()) count++;
		}
		return count;
	}

	public List<ScanPkgItem> getSafeList() {
		List<ScanPkgItem> tmpList = new ArrayList<ScanPkgItem>();
		for (ScanPkgItem item:scanList) {
			if (item.isSafe()|item.isUserSafe()) tmpList.add(item);
		}
		return tmpList;
	}
	
	public int getUnsafeCount() {
		int count = 0;
		for (ScanPkgItem item:scanList) {
			if (item.isUnsafe()|item.isUserUnsafe()) count++;
		}
		return count;
	}

	public List<ScanPkgItem> getUnsafeList() {
		List<ScanPkgItem> tmpList = new ArrayList<ScanPkgItem>();
		for (ScanPkgItem item:scanList) {
			if (item.isUnsafe()|item.isUserUnsafe()) tmpList.add(item);
		}
		return tmpList;
	}
	
	public int getUnknownCount() {
		int count = 0;
		for (ScanPkgItem item:scanList) {
			if (item.isUnknown()) count++;
		}
		return count;
	}

	public List<ScanPkgItem> getUnknownList() {
		List<ScanPkgItem> tmpList = new ArrayList<ScanPkgItem>();
		for (ScanPkgItem item:scanList) {
			if (item.isUnknown()) tmpList.add(item);
		}
		return tmpList;
	}
	
	public int getHighRiskCount() {
		int count = 0;
		for (ScanPkgItem item:scanList) {
			if (item.isHighRisk()) count++;
		}
		return count;
	}

	public List<ScanPkgItem> getHighRiskList() {
		List<ScanPkgItem> tmpList = new ArrayList<ScanPkgItem>();
		for (ScanPkgItem item:scanList) {
			if (item.isHighRisk()) tmpList.add(item);
		}
		return tmpList;
	}
	
	public List<ScanPkgItem> getList(int list_type) {
		
		if (list_type == ScanPkgItem.SAFE) return getSafeList();
		if (list_type == ScanPkgItem.UNKNOWN) return getUnknownList();
		if (list_type == ScanPkgItem.UNKNOWN_HIGH_RISK) return getHighRiskList();
		if (list_type == ScanPkgItem.UNSAFE) return getUnsafeList();
		
		return null;
	}

	public boolean warnKitKat() {
		return SDSmsManager.hasKitKat() && 
				!SDSmsManager.isSdsmsDefaultSmsApp();
	}
	
	public boolean warnHangoutsInstalled(final Context context) {
		if (SDSmsManager.hasKitKat()) return false;
		SharedPreferences settings = context.getSharedPreferences(EpicFeedConsts.PREFS_NAME, 0);
		SharedPreferences.Editor editor = settings.edit();
	    PackageManager pm = context.getPackageManager();
	    assert pm != null;
	    // was Hangouts previously installed
	    boolean prevWarning = settings.getBoolean(EpicFeedConsts.warnHangoutsInstalled, false);
	    /// and was the user previously warned
	    prevWarning &= settings.getBoolean(EpicFeedConsts.warnHangouts, false);

	    // check for babel aka hangouts app 
	    try {
	        if (pm.getApplicationInfo("com.google.android.apps.babel", 0) != null) {
	    		if (EpicFeedConsts.DEBUG_HANGOUTS) Log.i(TAG, "warnHangoutsInstalled found: pm.getApplicationInfo(\"com.google.android.apps.babel\", 0): found");
	        	editor.putBoolean(EpicFeedConsts.warnHangoutsInstalled, true);
	        	editor.commit();
	        	// return true unless there was a previous warning
	            return true && !prevWarning;
	        }
	    } catch (PackageManager.NameNotFoundException e) {
	    	if (EpicFeedConsts.DEBUG_HANGOUTS) Log.e(TAG, "warnHangoutsInstalled babel not found, check for talk app");
	    }

	    // check for talk app or updated talk app now known as hangouts
	    try {
	        if (pm.getApplicationInfo("com.google.android.talk", 0) == null) {
	            // no talk, no update
	        	editor.putBoolean(EpicFeedConsts.warnHangoutsInstalled, false);
	        	editor.putBoolean(EpicFeedConsts.warnHangouts, false);
	        	editor.commit();
		    	if (EpicFeedConsts.DEBUG_HANGOUTS) Log.e(TAG, "warnHangoutsInstalled did not find pm.getApplicationInfo(\"com.google.android.talk\", 0) == null");
	            return false;
	        }
	    } catch (PackageManager.NameNotFoundException e) {
	    	if (EpicFeedConsts.DEBUG_HANGOUTS) Log.e(TAG, "warnHangoutsInstalled talk app not found");
	        // no talk, no update
	    	editor.putBoolean(EpicFeedConsts.warnHangoutsInstalled, false);
        	editor.putBoolean(EpicFeedConsts.warnHangouts, false);
	    	editor.commit();
	        return false;
	    }
    	
    	if (EpicFeedConsts.DEBUG_HANGOUTS) Log.e(TAG, "warnHangoutsInstalled found: pm.getApplicationInfo(\"com.google.android.talk\", 0) == null");
	    // talk app found
	    // check for talk update
	    boolean permsGranted = PackageManager.PERMISSION_GRANTED == pm
	            .checkPermission("android.permission.SEND_SMS",
	                             "com.google.android.talk");
	    if (!permsGranted) {
        	editor.putBoolean(EpicFeedConsts.warnHangouts, false);
        	editor.putBoolean(EpicFeedConsts.warnHangoutsInstalled, false);
	    } else {
	    	editor.putBoolean(EpicFeedConsts.warnHangoutsInstalled, true);
	    }
    	editor.commit();

    	if (EpicFeedConsts.DEBUG_HANGOUTS) Log.e(TAG, "warnHangoutsInstalled permsGranted : " + permsGranted);
    	// return true if perms are granted AND there was not a previous warning
    	return permsGranted && !prevWarning;
	}	
	
	public boolean checkHangoutsInstalled(final Context context) {
		SharedPreferences settings = context.getSharedPreferences(EpicFeedConsts.PREFS_NAME, 0);
	    PackageManager pm = context.getPackageManager();
	    assert pm != null;

	    // check for babel aka hangouts app 
	    try {
	        if (pm.getApplicationInfo("com.google.android.apps.babel", 0) != null) {
	            return true;
	        }
	    } catch (PackageManager.NameNotFoundException e) {
	    	if (EpicFeedConsts.DEBUG_HANGOUTS) Log.e(TAG, "checkHangoutsPerms babel not found, check for talk app");
	    }

	    // check for talk app or updated talk app now known as hangouts
	    try {
	        if (pm.getApplicationInfo("com.google.android.talk", 0) == null) {
		    	if (EpicFeedConsts.DEBUG_HANGOUTS) Log.e(TAG, "checkHangoutsPerms did not find pm.getApplicationInfo(\"com.google.android.talk\", 0) == null");
	            return false;
	        }
	    } catch (PackageManager.NameNotFoundException e) {
	    	if (EpicFeedConsts.DEBUG_HANGOUTS) Log.e(TAG, "checkHangoutsPerms talk app not found");
	        return false;
	    }
    	
	    return true;
	}
	
	public boolean checkHangoutsPerms(final Context context) {
		SharedPreferences settings = context.getSharedPreferences(EpicFeedConsts.PREFS_NAME, 0);
	    PackageManager pm = context.getPackageManager();
	    assert pm != null;

	    // check for babel aka hangouts app 
	    try {
	        if (pm.getApplicationInfo("com.google.android.apps.babel", 0) != null) {
	            return true;
	        }
	    } catch (PackageManager.NameNotFoundException e) {
	    	if (EpicFeedConsts.DEBUG_HANGOUTS) Log.e(TAG, "checkHangoutsPerms babel not found, check for talk app");
	    }

	    // check for talk app or updated talk app now known as hangouts
	    try {
	        if (pm.getApplicationInfo("com.google.android.talk", 0) == null) {
		    	if (EpicFeedConsts.DEBUG_HANGOUTS) Log.e(TAG, "checkHangoutsPerms did not find pm.getApplicationInfo(\"com.google.android.talk\", 0) == null");
	            return false;
	        }
	    } catch (PackageManager.NameNotFoundException e) {
	    	if (EpicFeedConsts.DEBUG_HANGOUTS) Log.e(TAG, "checkHangoutsPerms talk app not found");
	        return false;
	    }
    	
    	if (EpicFeedConsts.DEBUG_HANGOUTS) Log.e(TAG, "checkHangoutsPerms found: pm.getApplicationInfo(\"com.google.android.talk\", 0) == null");
	    // talk app found
	    // check for talk update
	    boolean permsGranted = PackageManager.PERMISSION_GRANTED == pm
	            .checkPermission("android.permission.SEND_SMS",
	                             "com.google.android.talk");
    	if (EpicFeedConsts.DEBUG_HANGOUTS) Log.e(TAG, "warnHangoutsInstalled permsGranted : " + permsGranted);
    	return permsGranted;
	}
}
