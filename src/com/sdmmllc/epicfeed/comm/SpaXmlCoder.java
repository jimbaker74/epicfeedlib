package com.sdmmllc.epicfeed.comm;

import android.content.Context;
import android.util.Log;

import com.sdmmllc.epicfeed.rednote.RednoteEmo;

public class SpaXmlCoder {
	private static final String TAG = "SpaXmlCoder"; 
    private static final String SPA_UPDATE_URL = "http://www.sirispublishing.com/cgi-bin/rednote_emo.pl";
    boolean haveUpdates = false;
    SpaXmlCoderListener mSpaXmlCoderListener;
    
    private SpaXmlHttpRetriever httpRetriever;
    private CommManager commManager;
    private Context spaContext;
    
    public SpaXmlCoder(Context context) {
        httpRetriever = new SpaXmlHttpRetriever(context);
    	commManager = CommManager.getCommManager(context);
    	spaContext = context;
    }
    
    
    public void setSpaXmlCoderListener(SpaXmlCoderListener spaXmlCoderListener) {
    	mSpaXmlCoderListener = spaXmlCoderListener;
    }
    
    public RednoteEmo getSongEmo(RednoteEmo emo, String songId) {
        if (!commManager.networkAvailable()) return emo;

        songId = songId.replaceAll(" ", "+");
        String url = String.format(SPA_UPDATE_URL + "?song="+songId);        
        Log.i(TAG, "Getting Rednote emo: " + url);
        String response = httpRetriever.retrieve(url);
        Log.i(TAG, "Got Rednote emo response: " + response);
        emo = SpaXmlParser.parseXmlRednoteMood(spaContext, response);
        if (emo != null) {
        	emo.loaded = true;
        	if (mSpaXmlCoderListener != null)
        		mSpaXmlCoderListener.getUpdateInfoResponse();
        }
        return emo;
    }
}
