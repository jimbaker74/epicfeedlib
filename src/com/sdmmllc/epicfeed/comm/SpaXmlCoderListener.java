package com.sdmmllc.epicfeed.comm;

public interface SpaXmlCoderListener {
	public void getUpdateInfoResponse();
	public void getRiskListInfoResponse();
	public void getCheckIdResponse();
	public void getAuthResponse();
	public void getCreateAccountResponse();
}
