package com.sdmmllc.epicfeed.comm;

public abstract class SpaXmlCoderAdapter implements SpaXmlCoderListener {
	public void getUpdateInfoResponse() {
	}
	public void getRiskListInfoResponse() {
	}
	public void getCheckIdResponse() {
	}
	public void getAuthResponse() {
	}
	public void getCreateAccountResponse() {
	}
}
