package com.sdmmllc.epicfeed.comm;

import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import android.content.Context;
import android.util.Log;

import com.sdmmllc.epicfeed.rednote.RednoteEmo;
import com.sdmmllc.epicfeed.utils.EpicFeedConsts;

public class SpaXmlParser {
	
    private final static String TAG = "SpaXmlParser";
    
    private static CommManager commManager;
    
	public SpaXmlParser(Context context) {
		commManager = CommManager.getCommManager(context);
	}

    public static RednoteEmo parseXmlRednoteMood(Context context, String response) {
		if (commManager == null) commManager = CommManager.getCommManager(context);
        RednoteEmo result = new RednoteEmo();
		//if (SpaTextConsts.debugLevel > 9) 
    	Log.i(TAG, "parseXmlRednoteMood: parsing Rednote emo response: ");
        
        try {
            
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse(new InputSource(new StringReader(response)));
            
            NodeList resultNodes = doc.getElementsByTagName(RednoteEmo.rootNode);            
            Node resultNode = resultNodes.item(0);            
            NodeList attrsList = resultNode.getChildNodes();
            
            for (int i=0; i < attrsList.getLength(); i++) {
                
                Node pkgNode = attrsList.item(i);                
                Node firstPkgChild = pkgNode.getFirstChild();
        		//if (SpaTextConsts.debugLevel > 9) 
            	Log.i(TAG, "parseXmlRednoteMood: pkgNode.getNodeName(): " + pkgNode.getNodeName());
        		//if (SpaTextConsts.debugLevel > 9) 
            	Log.i(TAG, "parseXmlRednoteMood: pkgNode.getNodeValue(): " + pkgNode.getFirstChild().getNodeValue());
                if (RednoteEmo.moodIdNode.equalsIgnoreCase(pkgNode.getNodeName()) && firstPkgChild!=null) {
            		//if (SpaTextConsts.debugLevel > 9) 
            			Log.i(TAG, "parseXmlRednoteMood: got mood ID: " + pkgNode.getFirstChild().getNodeValue());
                	result.moodId = pkgNode.getFirstChild().getNodeValue();
                } else if (RednoteEmo.moodTxtNode.equalsIgnoreCase(pkgNode.getNodeName()) && firstPkgChild!=null) {
            		//if (SpaTextConsts.debugLevel > 9) 
            			Log.i(TAG, "parseXmlRednoteMood: got mood text: " + pkgNode.getFirstChild().getNodeValue());
                	result.moodTxt = pkgNode.getFirstChild().getNodeValue();
                } else if (RednoteEmo.songIdNode.equalsIgnoreCase(pkgNode.getNodeName()) && firstPkgChild!=null) {
            		//if (SpaTextConsts.debugLevel > 9) 
            			Log.i(TAG, "parseXmlRednoteMood: got song ID: " + pkgNode.getFirstChild().getNodeValue());
                	result.songId = pkgNode.getFirstChild().getNodeValue();
                } else if (RednoteEmo.moodListIdNode.equalsIgnoreCase(pkgNode.getNodeName()) && firstPkgChild!=null) {
            		//if (SpaTextConsts.debugLevel > 9) 
            			Log.i(TAG, "parseXmlRednoteMood: got mood List ID: " + pkgNode.getFirstChild().getNodeValue());
                	result.moodListId = pkgNode.getFirstChild().getNodeValue();
                }
            }
    		if (EpicFeedConsts.debugLevel > 9) 
    			Log.d(TAG, "parseXmlRednoteMood : got mood!");
            
            return result;
        }
        catch (Exception e) {
            commManager.parseError();
            e.printStackTrace();
        }
        return null;
    }
}
