package com.sdmmllc.epicfeed;

import com.actionbarsherlock.app.SherlockListActivity;

public class Version11Helper {
	static void refreshActionBarMenu(SherlockListActivity activity) {
		activity.invalidateOptionsMenu();
	}
}
