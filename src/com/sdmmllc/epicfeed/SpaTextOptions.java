package com.sdmmllc.epicfeed;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.RadioButton;
import android.widget.TextView;

import com.sdmm.epicfeed.R;
import com.sdmmllc.epicfeed.utils.EpicFeedConsts;


public class SpaTextOptions extends Activity {
	CheckBox optionsConfirmSendChk, optionsConfirmDeliveryChk;
	RadioButton optionsLoginScreen, optionsHomeScreen, optionsContactsScreen, optionsDoNothing;
	Context spaContext;
	Bundle savedState;
	String TAG = "SpaTextOptions";
    private LayoutInflater mInflater;
    SharedPreferences settings;
    ScanPkgList scanList;
	ProgressDialog waitDialog;
	final Handler scanHandler = new Handler();
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater menuInfl = getMenuInflater();
		menuInfl.inflate(R.menu.configoptions_menu, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int itemId = item.getItemId();
		if (itemId == R.id.configOptionsHelp) {
			helpDialog();
			return true;
		}
		
		return false;
	}

	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.config_options);
        savedState = savedInstanceState;
        spaContext = this;
        mInflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        optionsConfirmSendChk = (CheckBox)findViewById(R.id.optionsMsgSentSetupCheck);
        optionsConfirmDeliveryChk = (CheckBox)findViewById(R.id.optionsMsgDeliverySetupCheck);
        optionsLoginScreen = (RadioButton)findViewById(R.id.optionsNotificationLaunchPageLoginRadio);
        optionsHomeScreen = (RadioButton)findViewById(R.id.optionsNotificationLaunchPageHomeRadio);
        optionsContactsScreen = (RadioButton)findViewById(R.id.optionsNotificationLaunchPageContactsRadio);
        optionsDoNothing = (RadioButton)findViewById(R.id.optionsNotificationLaunchPageDoNothingRadio);
        settings = getSharedPreferences(EpicFeedConsts.PREFS_NAME, 0);
        String screen = settings.getString(EpicFeedConsts.NOTIFY_SCREEN, EpicFeedConsts.NOTIFY_LOGIN_SCREEN);
        if (screen.equals(EpicFeedConsts.NOTIFY_LOGIN_SCREEN)) {
        	optionsLoginScreen.setChecked(true);
        } else if (screen.equals(EpicFeedConsts.NOTIFY_CONTACTS_SCREEN)) {
            optionsContactsScreen.setChecked(true);
        } else if (screen.equals(EpicFeedConsts.NOTIFY_DO_NOTHING)) {
        	optionsDoNothing.setChecked(true);
        } else {
        	optionsHomeScreen.setChecked(true);
        }
        optionsHomeScreen.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton arg0, boolean checked) {
				if (checked) {
		        	SharedPreferences.Editor editor = settings.edit();
		        	editor.putString(EpicFeedConsts.NOTIFY_SCREEN, EpicFeedConsts.NOTIFY_HOME_SCREEN);
		        	editor.commit();
				}
			}
        	
        });
        optionsLoginScreen.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton arg0, boolean checked) {
				if (checked) {
		        	SharedPreferences.Editor editor = settings.edit();
		        	editor.putString(EpicFeedConsts.NOTIFY_SCREEN, EpicFeedConsts.NOTIFY_LOGIN_SCREEN);
		        	editor.commit();
				}
			}
        	
        });
        optionsDoNothing.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton arg0, boolean checked) {
				if (checked) {
		        	SharedPreferences.Editor editor = settings.edit();
		        	editor.putString(EpicFeedConsts.NOTIFY_SCREEN, EpicFeedConsts.NOTIFY_DO_NOTHING);
		        	editor.commit();
				}
			}
        	
        });
        optionsContactsScreen.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton arg0, boolean checked) {
				if (checked) {
		        	SharedPreferences.Editor editor = settings.edit();
		        	editor.putString(EpicFeedConsts.NOTIFY_SCREEN, EpicFeedConsts.NOTIFY_CONTACTS_SCREEN);
		        	editor.commit();
				}
			}
        	
        });
        // until further testing...
        optionsContactsScreen.setVisibility(View.GONE);
        if (settings.contains(EpicFeedConsts.confirmSendKey)) {
        	if (settings.getString(EpicFeedConsts.confirmSendKey, EpicFeedConsts.confirmSendYes)
        			.equals(EpicFeedConsts.confirmSendYes)){
        		optionsConfirmSendChk.setChecked(true);
        	} else optionsConfirmSendChk.setChecked(false);
        } else {
        	SharedPreferences.Editor editor = settings.edit();
        	editor.putString(EpicFeedConsts.confirmSendKey, EpicFeedConsts.confirmSendYes);
        	editor.commit();
        	optionsConfirmSendChk.setChecked(true);
        }
        if (settings.contains(EpicFeedConsts.confirmDeliveryKey)) {
        	if (settings.getString(EpicFeedConsts.confirmDeliveryKey, EpicFeedConsts.confirmDeliveryYes)
        			.equals(EpicFeedConsts.confirmDeliveryYes)){
        		optionsConfirmDeliveryChk.setChecked(true);
        	} else optionsConfirmDeliveryChk.setChecked(false);
        } else {
        	SharedPreferences.Editor editor = settings.edit();
        	editor.putString(EpicFeedConsts.confirmDeliveryKey, EpicFeedConsts.confirmDeliveryNo);
        	editor.commit();
        	optionsConfirmDeliveryChk.setChecked(false);
        }
	}
	
	public void helpDialog () {
		closeOptionsMenu();
		final View helpTextView = mInflater.inflate(R.layout.helpview, null);
		final TextView helpText = (TextView)helpTextView.findViewById(R.id.helptext);
		helpText.setText(Html.fromHtml(
				getText(R.string.optionsHelp).toString()));
		AlertDialog ad = new AlertDialog.Builder(spaContext)
        .setTitle(R.string.optionsHelpHeader)
        .setView(helpTextView)
        .setNeutralButton(getString(R.string.sysDone), new DialogInterface.OnClickListener() {
            @Override
			public void onClick(DialogInterface dialog, int which) {
            	dialog.cancel();
            }
        })
        .create();
		ad.show();
	}
	
	public void setupDeliveryConfirmation(View v) {
		if (optionsConfirmDeliveryChk.isChecked()) {
        	SharedPreferences.Editor editor = settings.edit();
        	editor.putString(EpicFeedConsts.confirmDeliveryKey, EpicFeedConsts.confirmDeliveryYes);
        	editor.commit();
        	EpicFeedController.smsHandler.confirmDelivery(null);
		} else {
	    	SharedPreferences.Editor editor = settings.edit();
	    	editor.putString(EpicFeedConsts.confirmDeliveryKey, EpicFeedConsts.confirmDeliveryNo);
        	editor.commit();
        	EpicFeedController.smsHandler.disableConfirmDelivery();
		}
	}
	
	public void setupSendConfirmation(View v) {
		if (optionsConfirmSendChk.isChecked()) {
        	SharedPreferences.Editor editor = settings.edit();
        	editor.putString(EpicFeedConsts.confirmSendKey, EpicFeedConsts.confirmSendYes);
        	editor.commit();
        	EpicFeedController.smsHandler.confirmSend(null);
		} else {
	    	SharedPreferences.Editor editor = settings.edit();
	    	editor.putString(EpicFeedConsts.confirmSendKey, EpicFeedConsts.confirmSendNo);
        	editor.commit();
        	EpicFeedController.smsHandler.disableConfirmSend();
		}
	}
	
	public void scanNow(View v) {
        waitDialog = ProgressDialog.show(spaContext, getString(R.string.instAppFoundScanWarnTitle),
        		getString(R.string.instAppFoundScanMsg), true);
		scanList = new ScanPkgList();
        Thread getScanThread = new Thread(null, scanApps, "Scan Apps Thread");
        getScanThread.start();
	}
	
	public void alertWarningInfo() {
		final View helpTextView = mInflater.inflate(R.layout.helpview, null);
		final TextView updatesText = (TextView)helpTextView.findViewById(R.id.helptext);
		updatesText.setGravity(Gravity.CENTER_HORIZONTAL);
		updatesText.setText(Html.fromHtml(getString(R.string.warningHelp)));
		AlertDialog ad = new AlertDialog.Builder(spaContext)
        .setTitle(getString(R.string.warningHelpHeader))
        .setView(helpTextView)
        .setNeutralButton(getString(R.string.sysDone), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            	dialog.cancel();
            }
        })
        .create();
		ad.show();
	}
	
	private Runnable scanApps = new Runnable() {
		@Override
		public void run () {
			scanList.scanForSMS(getPackageManager(), true, false);
			if (scanList.has999App()|scanList.hasOver999App()) {
				scanHandler.post(warnApps);
	    		Log.i(TAG, "found high priority SMS apps");
			}
    		Log.i(TAG, "scanned for SMS apps");
		}
	};
	
	private Runnable warnApps = new Runnable() {
		@Override
		public void run () {
			waitDialog.cancel();
		}
	};
	
	@Override
    public void onConfigurationChanged(Configuration newConfig) {
    	super.onPause();
		super.onStop();
    	onCreate(this.savedState);
    }

	@Override
	public void onStop() {
		super.onStop();
		setResult(RESULT_CANCELED);
		finish();
	}
}