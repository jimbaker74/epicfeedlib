package com.sdmmllc.epicfeed;

import java.util.Calendar;
import java.util.Date;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.sdmm.epicfeed.R;
import com.sdmmllc.epicfeed.data.EpicContact;
import com.sdmmllc.epicfeed.utils.EpicFeedConsts;
import com.sdmmllc.epicfeed.view.FeedTextMsg;


public class SpaTextCallLog implements Parcelable {
	String TAG = "SpaTextCallLog";
	private FeedTextMsg parcelMsg;
	
	public SpaTextCallLog(FeedTextMsg mMsg) {
		setParcelMsg(mMsg);
	}
	
	public SpaTextCallLog(Parcel source) {
		setParcelMsg(new FeedTextMsg());
		if (EpicFeedConsts.debugLevel > 7) Log.i(TAG, "writeToParcel");
		parcelMsg.msgID = source.readString();
		parcelMsg.msgPhoneID = source.readString();
		parcelMsg.msgTxt = source.readString();
		parcelMsg.msgRingDuration = source.readLong();
		parcelMsg.msgFlgs = source.readInt();
		parcelMsg.msgDate = new Date(source.readLong());
	}
	
	@Override
	public int describeContents() {
		return this.hashCode();
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		if (EpicFeedConsts.debugLevel > 7) Log.i(TAG, "writeToParcel");
		dest.writeString(parcelMsg.msgID);
		dest.writeString(parcelMsg.msgPhoneID);
		dest.writeString(parcelMsg.msgTxt);
		dest.writeLong(parcelMsg.msgRingDuration);
		dest.writeInt(parcelMsg.msgFlgs);
		dest.writeLong(parcelMsg.msgDate.getTime());
	}

	public static final Parcelable.Creator<SpaTextCallLog> CREATOR = 
	new Parcelable.Creator<SpaTextCallLog>() {

		@Override
		public SpaTextCallLog createFromParcel(Parcel source) {
			return new SpaTextCallLog(source);
		}

		@Override
		public SpaTextCallLog[] newArray(int size) {
			return new SpaTextCallLog[size];
		}
	};
	
	public void setCallRinging(EpicContact contactInfo, Context spaContext) {
		resetCallLog(contactInfo, spaContext);
		parcelMsg.setCallLogIncoming(true);
		parcelMsg.setCallLogMissed(true);
	}
	
	public void setCallDialed(EpicContact contactInfo, Context spaContext) {
		resetCallLog(contactInfo, spaContext);
		parcelMsg.setCallLogMissed(false);
		parcelMsg.setNewMsg(false);
		parcelMsg.setCallLogOutgoing(true);
		parcelMsg.msgTxt = spaContext.getString(R.string.callLogOutgoingCall);
	}
	
	public void setCallBlocked(EpicContact contactInfo, Context spaContext) {
		resetCallLog(contactInfo, spaContext);
		parcelMsg.setCallLogBlocked(true);
		parcelMsg.setCallLogMissed(false);
		parcelMsg.setCallLogIncoming(true);
		parcelMsg.msgTxt = spaContext.getString(R.string.callLogOutgoingCall);
	}
	
	public void setCallAnswered(EpicContact contactInfo, Context spaContext) {
		parcelMsg.msgTxt = spaContext.getString(R.string.callLogIncomingCall);
		parcelMsg.setCallLogMissed(false);
		parcelMsg.setNewMsg(false);
		Calendar tmpCal = Calendar.getInstance();
		Calendar tmpCal2 = Calendar.getInstance();
		tmpCal2.setTime(parcelMsg.msgDate);
		parcelMsg.msgRingDuration = tmpCal.getTimeInMillis() - tmpCal2.getTimeInMillis();
		parcelMsg.msgDate = tmpCal.getTime();
	}
	
	public void setCallCompleted(EpicContact contactInfo, Context spaContext) {
		Calendar tmpCal = Calendar.getInstance();
		tmpCal.setTime(this.getTime());
		long callDuration = Calendar.getInstance().getTimeInMillis() - 
			tmpCal.getTimeInMillis();
		if (this.isCallLogMissed()) {
			setLogText(spaContext.getString(R.string.callLogMissedDescr));
		} else if (this.isCallLogBlocked()) {
			setLogText(spaContext.getString(R.string.callLogBlockedDescr));
		} else if (this.isCallLogIncoming()) {
			setLogText(spaContext.getString(R.string.callLogIncomingCall)
				+ " " + callDurationString(callDuration));
		} else {
			setLogText(spaContext.getString(R.string.callLogOutgoingCall)
			+ " " + callDurationString(callDuration));
		}
		if (EpicFeedConsts.debugLevel > 7) Log.i(TAG, " setCallCompleted: Call log completed");
	}

	public void resetCallLog (EpicContact contactInfo, Context spaContext) {
		setParcelMsg(new FeedTextMsg());
		parcelMsg.msgPhoneID = contactInfo.phoneID;
		parcelMsg.msgDate = Calendar.getInstance().getTime();
		parcelMsg.setCallLog(true);
		parcelMsg.setCallLogBlocked(false);
		parcelMsg.setCallLogMissed(true);
		parcelMsg.setNewMsg(true);
		parcelMsg.msgTxt = spaContext.getString(R.string.callLogUnknown);
	}

	public String callDurationString(long duration) {
		String retStr = "";
		int callHr = 0, callMin = 0, callSec = 0;
	    try {
			// hours
		    callHr = new Integer(duration/(60*60*1000) + "");
		    duration = duration - (callHr*60*60*1000);
		    callMin = new Integer(duration/(60*1000) + "");
		    duration = duration - (callMin*60*1000);
		    callSec = new Integer(duration/1000 + "");
	    } catch (Exception e) {
	    	e.printStackTrace();
	    	if (EpicFeedConsts.debugLevel > 7) Log.i(TAG, "callDurationString error");
	    }
	    if (callHr > 0) {
		    if (callHr < 10) retStr = "0" + callHr + ":";
		    else retStr = callHr + ":";
	    }
	    if (callMin < 10) retStr = retStr + "0" + callMin + ":";
	    else retStr = retStr + callMin + ":";
	    if (callSec < 10) retStr = retStr + "0" + callSec;
	    else retStr = retStr + callSec;
	    return retStr;
	}

	public void setLogText (String text) {
		parcelMsg.msgTxt = text;
	}
	
	public String getText() {
		return parcelMsg.msgTxt;
	}
	
	public void setPhoneID (String pID) {
		parcelMsg.msgPhoneID = pID;
	}
	
	public String getPhoneID() {
		return parcelMsg.msgPhoneID;
	}
	
	public void setTime (Date timestamp) {
		parcelMsg.msgDate = timestamp;
	}
	
	public Date getTime() {
		return parcelMsg.msgDate;
	}
	

	
	public void callDuration(long end_call) {
		
	}
	// wrapper methods
	public void setNewMsg(boolean new_message) {
		parcelMsg.setNewMsg(new_message);
	}
	
	public boolean isNewMsg() {
		return parcelMsg.isNewMsg();
	}
	
	public void setReceivedMsg(boolean rec_message) {
		parcelMsg.setReceivedMsg(rec_message);
	}
	
	public boolean isReceivedMsg() {
		return parcelMsg.isReceivedMsg();
	}
	
	public void setSentMsg(boolean sent_message) {
		parcelMsg.setSentMsg(sent_message);
	}
	
	public boolean isSentMsg() {
		return parcelMsg.isSentMsg();
	}
	
	public void setBlockedMsg(boolean block_message) {
		parcelMsg.setBlockedMsg(block_message);
	}
	
	public boolean isBlockedMsg() {
		return parcelMsg.isBlockedMsg();
	}
	
	public void setInfoMsg(boolean info_message) {
		parcelMsg.setInfoMsg(info_message);
	}
	
	public boolean isInfoMsg() {
		return parcelMsg.isInfoMsg();
	}
	
	public void setCallLog(boolean call_log) {
		parcelMsg.setCallLog(call_log);
	}
	
	public boolean isCallLog() {
		return parcelMsg.isCallLog();
	}
	
	public void setCallLogIncoming(boolean call_log) {
		parcelMsg.setCallLogIncoming(call_log);
	}
	
	public boolean isCallLogIncoming() {
		return parcelMsg.isCallLogIncoming();
	}
	
	public void setCallLogOutgoing(boolean call_log) {
		parcelMsg.setCallLogOutgoing(call_log);
	}
	
	public boolean isCallLogOutgoing() {
		return parcelMsg.isCallLogOutgoing();
	}
	
	public void setCallLogMissed(boolean call_log) {
		parcelMsg.setCallLogMissed(call_log);
	}
	
	public boolean isCallLogMissed() {
		return parcelMsg.isCallLogMissed();
	}
	
	public void setCallLogBlocked(boolean call_log) {
		parcelMsg.setCallLogBlocked(call_log);
	}
	
	public boolean isCallLogBlocked() {
		return parcelMsg.isCallLogBlocked();
	}
	
	public void setForbiddenMsg(boolean forbidden_message) {
		parcelMsg.setForbiddenMsg(forbidden_message);
	}
	
	public boolean isForbiddenMsg() {
		return parcelMsg.isForbiddenMsg();
	}
	
	public void setDraftMsg(boolean block_message) {
		parcelMsg.setDraftMsg(block_message);
	}
	
	public boolean isDraftMsg() {
		return parcelMsg.isDraftMsg();
	}
	
	public void setLockMsg(boolean lock_message) {
		parcelMsg.setLockMsg(lock_message);
	}
	
	public boolean isLockedMsg() {
		return parcelMsg.isLockedMsg();
	}
	
	public void setSendErr(boolean info_message) {
		parcelMsg.setSendErr(info_message);
	}
	
	public boolean hasSendErr() {
		return parcelMsg.hasSendErr();
	}
	
	public String printLog() {
		return parcelMsg.printLog();
	}

	public void setParcelMsg(FeedTextMsg parcelMsg) {
		this.parcelMsg = parcelMsg;
	}

	public FeedTextMsg getParcelMsg() {
		return parcelMsg;
	}
}

