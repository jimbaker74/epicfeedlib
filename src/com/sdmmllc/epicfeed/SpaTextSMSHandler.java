package com.sdmmllc.epicfeed;

import java.util.Calendar;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.SmsManager;
import android.util.Log;
import android.widget.Toast;

import com.sdmm.epicfeed.R;
import com.sdmmllc.epicfeed.data.EpicContact;
import com.sdmmllc.epicfeed.data.EpicDB;
import com.sdmmllc.epicfeed.data.EpicDBConn;
import com.sdmmllc.epicfeed.ui.Act_Authenticate;
import com.sdmmllc.epicfeed.ui.Act_ContactList;
import com.sdmmllc.epicfeed.ui.Act_EpicHome;
import com.sdmmllc.epicfeed.ui.Act_FeedMessageList;
import com.sdmmllc.epicfeed.utils.EpicFeedConsts;
import com.sdmmllc.epicfeed.view.FeedTextMsg;
import com.sdmmllc.superdupersmsmanager.sdk.SDSmsManager;

public class SpaTextSMSHandler {
	String TAG = "SpaTextSMSHandler";
    private BroadcastReceiver smsReceiverSend;
    private BroadcastReceiver smsReceiverDelivery;
    Context spaContext;
    private Runnable smsHandler;
	private final Handler SmsSendHander = new Handler();
    public static final String 
    	NOTIFY_MODE = "NotifyMode",
    	NOTIFY_NORMAL = "Normal", 
    	NOTIFY_QUIET = "Quiet",
    	NOTIFY_STEALTH = "Stealth";
    boolean modeNormal = true, modeQuiet = false, modeStealth = false;
	boolean deliveryConfirm = false, sendConfirm = false, checkModes = true, noSentNotification = false;
    private String SENT = "SPA_SMS_SENT";
    private String DELIVERED = "SPA_SMS_DELIVERED";
    private EpicContact defaultNotification = new EpicContact("");
    private Class launchClass = Act_Authenticate.class;
    private EpicDB spaTextDB;
	private SharedPreferences settings;
	private FeedTextMsg draftMsg;
    
    public SpaTextSMSHandler(Context context) {
    	spaContext = context;
		spaTextDB = EpicFeedController.getEpicDB(spaContext.getApplicationContext());
        settings = spaContext.getSharedPreferences(EpicFeedConsts.PREFS_NAME, 0);
    }
    
    public void disableConfirmSend() {
    	sendConfirm = false;
		if (smsReceiverSend != null) try {
			spaContext.unregisterReceiver(smsReceiverSend);
		} catch (Exception e) {
			// not registered
		}
    }
    
    public void confirmSend(String msgId) {
    	sendConfirm = true;
    	if (sendConfirm&&msgId != null) {
            //---when the SMS has been sent--
            smsReceiverSend = getSmsReceiverSend();
        	spaContext.registerReceiver(smsReceiverSend, new IntentFilter(SENT + msgId));
    	}
    }
    
    public void disableConfirmDelivery() {
    	deliveryConfirm = false;
		if (smsReceiverDelivery != null) try {
			spaContext.unregisterReceiver(smsReceiverDelivery);
		} catch (Exception e) {
			// not registered
		}
    }
    
    public void confirmDelivery(String msgId) {
    	deliveryConfirm = true;
    	if (deliveryConfirm&&msgId != null) {
            //---when the SMS has been delivered---
        	smsReceiverDelivery = getSmsReceiverDelivery();
        	spaContext.registerReceiver(smsReceiverDelivery, new IntentFilter(DELIVERED + msgId));
    	}
    }
    
	public void sendTxtMsg(EpicContact contact, String msgToSend) {
		spaTextDB = EpicFeedController.getEpicDB(spaContext.getApplicationContext());
		noSentNotification = !settings.getString(EpicFeedConsts.confirmSendKey, EpicFeedConsts.confirmSendYes)
				.equals(EpicFeedConsts.confirmSendYes);
		String partMsg = "";
		int partMsgEnd = Act_FeedMessageList.singleMsgLength;
			while (msgToSend.length() > 0) {
				if (msgToSend.length() > partMsgEnd) {
					partMsg = msgToSend.substring(0, partMsgEnd);
					if (EpicFeedConsts.debugLevel > 10) 
						Log.i(TAG, "sendTxtMsgBtn.click: partMsgEnd: " 
							+ partMsgEnd + " ; partMsg.length(): " + partMsg.length() + " ; wholeMsg.length(): " + msgToSend.length());
					partMsgEnd = partMsg.lastIndexOf(" ");
					// check for messages without spaces
					if (partMsgEnd < 0) {
						partMsgEnd = partMsg.length();
					}
					partMsg = partMsg.substring(0, partMsgEnd);
					if (EpicFeedConsts.debugLevel > 10) Log.i(TAG, "sendTxtMsgBtn.click: after length check, partMsgEnd: " 
							+ partMsgEnd + " ; partMsg.length():" + partMsg.length() + " ; wholeMsg.length(): " + msgToSend.length());
					msgToSend = msgToSend.substring(partMsg.length(), msgToSend.length());
					partMsgEnd = Act_FeedMessageList.singleMsgLength;
				} else {
					partMsg = msgToSend;
					msgToSend = "";
				}
				FeedTextMsg tmpMsg = new FeedTextMsg();
				tmpMsg.setSentMsg(true);
				if (noSentNotification) tmpMsg.setSentFlg(true);
				tmpMsg.setForbiddenMsg(contact.isForbidden());
				tmpMsg.msgTxt = new String(partMsg);
				tmpMsg.msgDate = Calendar.getInstance().getTime();
				tmpMsg.msgPhoneID = contact.phoneID;
				if (EpicFeedConsts.debugLevel > 5) Log.i(TAG, "sendTxtMsgBtn.setOnClickListener: " + 
						"Inserting msg to phoneID:" + tmpMsg.msgPhoneID + " Msg: " + tmpMsg.msgTxt);
				try {
					EpicDBConn tmpConn = spaTextDB.open(true);
					tmpMsg.msgID = Integer.valueOf(spaTextDB.insertTxtEntry(tmpMsg)).toString();
					spaTextDB.close(tmpConn);
					if (EpicFeedConsts.debugLevel > 7) 
						Log.i(TAG, "sendTxtMsgBtn.onClick: msgID:" + tmpMsg.msgID);
					draftMsg = new FeedTextMsg();
					draftMsg.msgPhoneID = contact.phoneID;
					draftMsg.msgTxt = "";
				} catch (Exception e) {
					if (EpicFeedConsts.debugLevel > 0) Log.i(TAG, "sendTxtMsgBtn.setOnClickListener: error"); 
					if (EpicFeedConsts.debugLevel > 0) e.printStackTrace(); 
				}
				if ((tmpMsg.msgID != null)&&
						(tmpMsg.msgID.length() > 0)) {
					if (!tmpMsg.isForbiddenMsg()) {
						if (contact.isSpaId()) sendMsg(contact.phoneID, tmpMsg.msgTxt);
						else {
							//SpaTextApp.smsHandler.sendSMS(contact.phoneID, new String(partMsg), tmpMsg.msgID);
							SmsSendHander.post(
									EpicFeedController.smsHandler.getSMSRunnable(contact.phoneID, new String(partMsg), tmpMsg.msgID));
						}
					}
					//if (txtMsgListGroup.hasGroup(1)) txtMsgListGroup.insert(tmpMsg, 1);
					//else {
					//	txtMsgListGroup.insert(tmpMsg, 1);
					//	txtSepMsgAA.addSection(
					//			txtMsgListGroup.getGroup(1).listTitle, 
					//			txtMsgListGroup.getGroup(1).msgListAA);
					//}
				}
			}
			EpicDBConn tmpConn = spaTextDB.open(true);
			spaTextDB.clearDraftMsgs(contact.phoneID);
			spaTextDB.close(tmpConn);
			//txtSepMsgAA.notifyDataSetChanged();
	        //updateUI();
	        //msgsSent++;
	}
	
    //---sends Secret message to another device---
    private void sendMsg(String contactId, String message)    {        
		if (EpicFeedConsts.debugLevel > 7) Log.i(TAG, "send Msg");
    }
    
    public void sendSMS(String pID, String msg, String mId) {
    	smsHandler = getSMSRunnable(pID, msg, mId);
		Thread smsThread = new Thread(null, smsHandler, "SMS Send");
		smsThread.start();
    }
    
    private BroadcastReceiver getSmsReceiverSend() {
    	return new BroadcastReceiver(){
	        @Override
	        public void onReceive(Context sContext, Intent sentIntent) {
	        	Context baseC = sContext.getApplicationContext();
	        	EpicDB tmpDB;
	        	int sendStatus = 0;
	        	switch (getResultCode()) {
	                case Activity.RESULT_OK:
	                    //Toast.makeText(baseC, baseC.getString(R.string.sysMsgSentOK), 
	                    //        Toast.LENGTH_SHORT).show();
	                    sendStatus = FeedTextMsg.SENT_FLG;
	                    break;
	                case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
	                    Toast.makeText(baseC, baseC.getString(R.string.sysMsgGenPhoneError), 
	                            Toast.LENGTH_SHORT).show();
	                    sendStatus = FeedTextMsg.SEND_ERR_FLG|FeedTextMsg.NEW_MSG_FLG;
	                    break;
	                case SmsManager.RESULT_ERROR_NO_SERVICE:
	                    Toast.makeText(baseC, baseC.getString(R.string.sysMsgNoService), 
	                            Toast.LENGTH_SHORT).show();
	                    sendStatus = FeedTextMsg.SEND_ERR_FLG|FeedTextMsg.NEW_MSG_FLG;
	                    break;
	                case SmsManager.RESULT_ERROR_NULL_PDU:
	                    Toast.makeText(baseC, baseC.getString(R.string.sysMsgCorrupt), 
	                            Toast.LENGTH_SHORT).show();
	                    sendStatus = FeedTextMsg.SEND_ERR_FLG|FeedTextMsg.NEW_MSG_FLG;
	                    break;
	                case SmsManager.RESULT_ERROR_RADIO_OFF:
	                    Toast.makeText(baseC, baseC.getString(R.string.sysMsgPhoneSigOff), 
	                            Toast.LENGTH_SHORT).show();
	                    sendStatus = FeedTextMsg.SEND_ERR_FLG|FeedTextMsg.NEW_MSG_FLG;
	                    break;
	            }
				if (EpicFeedConsts.debugLevel > 7) 
					Log.i(TAG, "smsReceiverSend.onReceive: msg flags: " + sendStatus);
	       		Bundle sBundle = sentIntent.getExtras();
	       		String msgId = sBundle.getString("msgId");
				if (EpicFeedConsts.debugLevel > 7) 
    				Log.i(TAG, "smsReceiverSend.onReceive: getting database");
				tmpDB = EpicFeedController.getEpicDB(spaContext.getApplicationContext());
        		EpicDBConn tmpConn = tmpDB.open(true);
        		if ((msgId.length() > 0)&&
        				tmpDB.containsMsg(msgId)) {
        			tmpDB.setSendStatus(msgId, sendStatus);
	        		if (sendStatus != FeedTextMsg.SENT_FLG) {
	        			EpicFeedController.smsHandler.sendNotification(tmpDB.getContact(
	        					tmpDB.getMessage(msgId)
	        					.msgPhoneID));
	        		}
	        		tmpDB.notifySentStatus();
        		}
        		tmpDB.close(tmpConn);
    			if (EpicFeedConsts.debugLevel > 7) 
        			Log.i(TAG, "smsReceiverSend.onReceive: message updated");
	        }
	    };
    }

    private BroadcastReceiver getSmsReceiverDelivery() { 
    	return new BroadcastReceiver(){
	        @Override
	        public void onReceive(Context sContext, Intent deliverI) {
	        	Context baseC = sContext.getApplicationContext();
	        	EpicDB tmpDB;
	        	int deliveryStatus = 0;
	            switch (getResultCode())
	            {
	                case Activity.RESULT_OK:
	                    //Toast.makeText(baseC, baseC.getString(R.string.sysMsgDelivered), 
	                    //        Toast.LENGTH_SHORT).show();
	                    deliveryStatus = FeedTextMsg.DELIVERED_FLG;
	                    break;
	                case Activity.RESULT_CANCELED:
	                    Toast.makeText(baseC, baseC.getString(R.string.sysMsgDeliveryError), 
	                            Toast.LENGTH_SHORT).show();
	                    deliveryStatus = FeedTextMsg.DELIVERY_ERR_FLG|FeedTextMsg.NEW_MSG_FLG;
	                    break;                        
	            }
	       		Bundle sBundle = deliverI.getExtras();
	       		String msgId = sBundle.getString("msgId");
				tmpDB = EpicFeedController.getEpicDB(spaContext.getApplicationContext());
        		EpicDBConn tmpConn = tmpDB.open(true);
        		if ((msgId.length() > 0)&&
        				tmpDB.containsMsg(msgId)) {
        			tmpDB.setDeliveryStatus(msgId, deliveryStatus);
	        		if (deliveryStatus != FeedTextMsg.DELIVERED_FLG) {
	        			EpicFeedController.smsHandler.sendNotification(tmpDB.getContact(
	        					tmpDB.getMessage(msgId).msgPhoneID));
	        		}
	        		tmpDB.notifyDeliveryStatus();
	        	}
        		tmpDB.close(tmpConn);
	        }
	    };
    }

    //---sends an SMS message to another device---
    public Runnable getSMSRunnable(String pID, String msg, String mId) {
    	if (checkModes) {
    		setNotificationMode();
    		checkModes = false;
    	}
    	final String phoneNumber = pID;
    	final String message = msg;
    	final String msgId = mId;
    	return new Runnable() {
    		@Override
    		public void run() {
    	 
    			if (msgId.length() > 0) {
    				
	    	        Bundle mBundle = new Bundle();
	    	        mBundle.putString("msgId", msgId);

	    	        PendingIntent sentPI = null;
    				if (sendConfirm) {
		    	        Intent sendI = new Intent(SENT + msgId);
		    	        sendI.putExtras(mBundle);
		    	        sentPI = PendingIntent.getBroadcast(spaContext, 0,
				    	        sendI, 0);
		    	        confirmSend(msgId);
    				}
    				
    				PendingIntent deliveredPI = null;
    				if (deliveryConfirm) {
		    	        Intent deliveryI = new Intent(DELIVERED + msgId);
		    	        deliveryI.putExtras(mBundle);
		    	        deliveredPI = PendingIntent.getBroadcast(spaContext, 0,
				    	        deliveryI, 0);
		    	        confirmDelivery(msgId);
    				}
	    	 
	    	        //SmsManager sms = SmsManager.getDefault();
    				SDSmsManager sms = EpicFeedController.getSDSmsManager();
	    	        try {
	    	        	sms.sendTextMessage(phoneNumber, null, message, sentPI, deliveredPI);
	    	        } catch (Exception e) {
	    	        	// error sending message, notify user and update database
	    				sendError(msgId, phoneNumber);
	    	        }
    			} else {
    				sendError(msgId, phoneNumber);
    			}
	    	}
    	};
	}
    
    public void sendError(String msgId, String phoneNumber) {
    	// error sending message, notify user and update database
	    EpicDB tmpDB;
		tmpDB = EpicFeedController.getEpicDB(spaContext.getApplicationContext());
	    EpicDBConn tmpConn = tmpDB.open(true);
		if ((msgId.length() > 0)&&
				tmpDB.containsMsg(msgId))
			tmpDB.setSendStatus(msgId, FeedTextMsg.SEND_ERR_FLG|FeedTextMsg.NEW_MSG_FLG);
		sendNotification(tmpDB.getContact(phoneNumber));
		tmpDB.close(tmpConn);
		tmpDB.notifySentStatus();
    }
    
    public void destroy() {
    	if (smsReceiverSend != null) try {
    		spaContext.unregisterReceiver(smsReceiverSend);
    	} catch(Exception e) {
    		// not registered
    	}
    	if (smsReceiverDelivery != null) try {
	    	spaContext.unregisterReceiver(smsReceiverDelivery);
    	} catch(Exception e) {
    		// not registered
    	}
    	spaContext = null;
    }
    
    public void setNotificationMode() {
        SharedPreferences settings = spaContext.getSharedPreferences(EpicFeedConsts.PREFS_NAME, 0);
        // notifications for sent messages
        boolean userSendConfirm = settings.getString(EpicFeedConsts.confirmSendKey, EpicFeedConsts.confirmSendYes)
			.equals(EpicFeedConsts.confirmSendYes);
    	if (userSendConfirm&&!sendConfirm) {
    		confirmSend(null);
    	} else if (!userSendConfirm&&sendConfirm) disableConfirmSend();
    	boolean userDeliveryConfirm = settings.getString(EpicFeedConsts.confirmDeliveryKey, EpicFeedConsts.confirmDeliveryNo)
			.equals(EpicFeedConsts.confirmDeliveryYes);
    	if (userDeliveryConfirm&&!deliveryConfirm) {
    		confirmDelivery(null);
    	} else if (!userDeliveryConfirm&&deliveryConfirm) disableConfirmDelivery();
    	// incoming notifications
    	String screen = settings.getString(EpicFeedConsts.NOTIFY_SCREEN, EpicFeedConsts.NOTIFY_LOGIN_SCREEN);
        if (screen.equals(EpicFeedConsts.NOTIFY_HOME_SCREEN)) {
        	try {
				launchClass = Class.forName(spaContext.getString(R.string.homescreenClass));
			} catch (ClassNotFoundException e) {
				launchClass = Act_EpicHome.class;
			}
        } else if (screen.equals(EpicFeedConsts.NOTIFY_CONTACTS_SCREEN)) {
            launchClass = Act_ContactList.class;
        } else if (screen.equals(EpicFeedConsts.NOTIFY_DO_NOTHING)) {
            launchClass = SpaTextDecoyLaunch.class;
        } else {
        	launchClass = Act_Authenticate.class;
        }
		if (!settings.contains(NOTIFY_MODE)) {
			modeNormal = true; modeQuiet = false; modeStealth = false;
		} else {
			String notifyMode = settings.getString(NOTIFY_MODE, NOTIFY_NORMAL);
			if (notifyMode.equals(NOTIFY_NORMAL)) {
				modeNormal = true; modeQuiet = false; modeStealth = false;
			} else if (notifyMode.equals(NOTIFY_QUIET)) {
				modeNormal = false; modeQuiet = true; modeStealth = false;
			} else if (notifyMode.equals(NOTIFY_STEALTH)) {
				modeNormal = false; modeQuiet = false; modeStealth = true;
			}
		}
    }
    
    public void setStatusBarNotification(EpicContact tContact) {
        SharedPreferences settings = spaContext.getSharedPreferences(EpicFeedConsts.PREFS_NAME, 0);
		// set the default notification icon
		if (tContact.notifyOptions.statusbarDefaultIcon) {
			defaultNotification.notifyOptions.statusbarDefaultIcon = 
				settings.getBoolean(EpicFeedConsts.NOTIFY_USE_DEF_ICON, true);
			if (!defaultNotification.notifyOptions.statusbarDefaultIcon)
				defaultNotification.notifyOptions.statusbarIcon = 
					settings.getInt(EpicFeedConsts.NOTIFY_DEF_ICON, 0);
			else defaultNotification.notifyOptions.statusbarIcon = 0;
		}
		// set the default notification text
		if (tContact.notifyOptions.statusbarDefaultNotification) {
			defaultNotification.notifyOptions.statusbarDefaultNotification = 
				settings.getBoolean(EpicFeedConsts.NOTIFY_USE_DEF_TEXT, true);
			if (!defaultNotification.notifyOptions.statusbarDefaultNotification) {
	    		defaultNotification.notifyOptions.statusbarDefaultTicker = 
	    			settings.getBoolean(EpicFeedConsts.NOTIFY_USE_DEF_TICKER, true);
	    		if (!defaultNotification.notifyOptions.statusbarDefaultTicker)
	        		defaultNotification.notifyOptions.statusbarTicker = 
	        			settings.getString(EpicFeedConsts.NOTIFY_DEF_TICKER, 
	        					spaContext.getString(R.string.statusNotificationTicker));
	    		else defaultNotification.notifyOptions.statusbarTicker = 
	    			spaContext.getString(R.string.statusNotificationTicker);
	    		defaultNotification.notifyOptions.statusbarDefaultTitle = 
	    			settings.getBoolean(EpicFeedConsts.NOTIFY_USE_DEF_TITLE, true);
	    		if (!defaultNotification.notifyOptions.statusbarDefaultTitle)
	        		defaultNotification.notifyOptions.statusbarTitle = 
	        			settings.getString(EpicFeedConsts.NOTIFY_DEF_TITLE, 
	        					spaContext.getString(R.string.statusNotificationTitle));
	    		else defaultNotification.notifyOptions.statusbarTitle = 
	    			spaContext.getString(R.string.statusNotificationTitle);
	    		defaultNotification.notifyOptions.statusbarDefaultText = 
	    			settings.getBoolean(EpicFeedConsts.NOTIFY_USE_DEF_MSG, true);
	    		if (!defaultNotification.notifyOptions.statusbarDefaultText)
	        		defaultNotification.notifyOptions.statusbarText = 
	        			settings.getString(EpicFeedConsts.NOTIFY_DEF_MSG, 
	        					spaContext.getString(R.string.statusNotificationText));
	    		else defaultNotification.notifyOptions.statusbarText = 
	    			spaContext.getString(R.string.statusNotificationText);
			} else {
	    		defaultNotification.notifyOptions.statusbarTicker = 
	    			spaContext.getString(R.string.statusNotificationTicker);
	        	defaultNotification.notifyOptions.statusbarTitle = 
	        		spaContext.getString(R.string.statusNotificationTitle);
				defaultNotification.notifyOptions.statusbarText = 
					spaContext.getString(R.string.statusNotificationText);
			}
		}
    }
    
	public void sendNotification(EpicContact tmpContact) {
    	setNotificationMode();
    	setStatusBarNotification(tmpContact);
		if (modeStealth) return;
		NotificationManager mNotificationManager = (NotificationManager) spaContext.getSystemService(Context.NOTIFICATION_SERVICE);
		Intent notificationIntent = new Intent(spaContext, launchClass);
		notificationIntent.putExtra(EpicFeedConsts.AUTH_STATUS, false);
		notificationIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                | Intent.FLAG_ACTIVITY_CLEAR_TOP);		
		PendingIntent contentIntent = PendingIntent.getActivity(spaContext, 0, notificationIntent, 0);
		int icon = SpaTextNotification.getIcon(defaultNotification.notifyOptions.statusbarIcon);
		if (!tmpContact.notifyOptions.statusbarDefaultIcon) 
			icon = SpaTextNotification.getIcon(tmpContact.notifyOptions.statusbarIcon);
		Notification notification;
		boolean notifyMe = false;
		CharSequence tickerText = defaultNotification.notifyOptions.statusbarTicker;
		CharSequence contentTitle = defaultNotification.notifyOptions.statusbarTitle;
		CharSequence contentText = defaultNotification.notifyOptions.statusbarText;
		if (!tmpContact.notifyOptions.statusbarDefaultNotification) {
			if (!tmpContact.notifyOptions.statusbarDefaultTicker) tickerText = tmpContact.notifyOptions.statusbarTicker;
			if (!tmpContact.notifyOptions.statusbarDefaultTitle) contentTitle = tmpContact.notifyOptions.statusbarTitle;
			if (!tmpContact.notifyOptions.statusbarDefaultText) contentText = tmpContact.notifyOptions.statusbarText;
		}
		if (tmpContact.notifyOptions.statusbar) {
			notifyMe = true;
			long when = System.currentTimeMillis();
			notification = new Notification(icon, tickerText, when);
			notification.flags |= Notification.FLAG_AUTO_CANCEL;
		} else {
			notification = new Notification();
		}
		if ((tmpContact.notifyOptions.sound)&&(modeNormal)) {
			notification.defaults |= Notification.DEFAULT_SOUND;
			notifyMe = true;
		}
		if ((tmpContact.notifyOptions.vibrate)&&(modeNormal)) {
			notification.defaults |= Notification.DEFAULT_VIBRATE;
			notifyMe = true;
		}
		if (tmpContact.notifyOptions.light) {
			notification.defaults |= Notification.DEFAULT_LIGHTS;
		    notification.flags |= Notification.FLAG_SHOW_LIGHTS; 
		    notifyMe = true;
		}
		if (notifyMe) {
			notification.setLatestEventInfo(spaContext, contentTitle, contentText, contentIntent);
			mNotificationManager.notify(icon, notification);
		} else {
			mNotificationManager.notify(icon, new Notification());
		}
	}
}
